#ifndef __AGENT_H__
#define __AGENT_H__

#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include "BWAPI.h"
#endif


/** Agent is the base class of all abstract agents
* 
*/
class Agent{

protected:

public:
	Agent();
	~Agent();
	virtual void printStatus() const = 0;
	virtual void processActions() = 0;
};

#endif	//AGENT_H