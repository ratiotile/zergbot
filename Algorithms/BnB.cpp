#include "BnB.h"

bool operator < (const node& lhs, const node& rhs){ return lhs.bound > rhs.bound; }
void BnB::travel( const unsigned int n, int const* const* W )
{
	optTour_.clear();
	n_ = n;
	priority_queue<node> PQ;
	node u, v;
	v.level = 0;
	v.path.push_back(0);	// start at v1 = 0
	v.bound = getBound( v.path, W, n_ );
	minLength_ = numeric_limits<int>::max(); // in case we compare with int
	PQ.push(v);
	vector<bool> visited( n, false );
	while ( !PQ.empty() ){
		v = PQ.top();
		PQ.pop();
		//			cout << "checking node: " << v.bound << " - "; printVector(v.path);
		if ( v.bound < minLength_ ) {
			//				cout << "Expanding" << endl;
			u.level = v.level + 1;
			for ( unsigned int i = 1; i < n_; i++ )
			{
				fillVisited( v.path, visited );
				if ( !visited[i] ) // i is not in v.path
				{
					u.path = v.path;
					u.path.push_back(i);
					if ( u.level == n - 2 )
					{
						visited[i] = true;	// set i to visited, so that only 1 unvisited remains
						for ( unsigned int j = 0; j < visited.size(); j ++){
							if ( !visited[j] ) // put last vertex into u.path
								u.path.push_back(j);
						}
						u.path.push_back(0); // make first vertex last one to complete tour
						unsigned int pathLength = getPathLength(u.path,W);
						//							cout << "finished tour found: "; printVector(u.path);
						//							cout << "pathLength " << pathLength; 
						if ( pathLength < minLength_ ){
							minLength_ = pathLength;
							optTour_ = u.path;
							//								cout << ", optimal!" << endl;
						} 
						//							else cout << ", not optimal" << endl;
					}else {
						u.bound = getBound( u.path, W, n_ );
						//							cout << "path u: " << u.bound << " - "; printVector(u.path);
						if (u.bound < minLength_)
							PQ.push(u);
					}
				}
			}
		}
	}
// 	cout << "Branch and Bound path: "; printVector(optTour_);
// 	cout << "Length: " << minLength_ << endl;
}

void BnB::fillVisited( const vector<unsigned int>& path, vector<bool>& visited )
{
	// assume that visited is right size
	for ( unsigned int i = 0; i < visited.size(); i++ ) // zero vector
		visited[i] = false;
	for ( unsigned int i = 0; i < path.size(); i++)
		visited[path[i]] = true;
}

void BnB::greedy( const unsigned int n, int const* const* W )
{
	n_ = n;
	priority_queue<node> PQ;
	priority_queue<node> empty;
	node u, v;
	v.level = 0;
	v.path.clear();
	v.path.push_back(0);	// start at v1 = 0
	v.bound = getBound( v.path, W, n_ );
	//minLength = numeric_limits<unsigned int>::max();
	PQ.push(v);
	vector<bool> visited( n, false );
	//		cout << "bound: " << v.bound << endl;
	v.level = -1;
	while ( !PQ.empty() ){
		v = PQ.top();
		visited[v.path.back()] = true;
		PQ = empty;
		u.level = v.level + 1;
		for ( unsigned int i = 1; i < n; i++ )
		{
			if ( !visited[i] )
			{
				u.path = v.path;
				u.path.push_back(i);
				if ( u.level == n-1 ) // tour complete
				{
					u.path.push_back(0); // complete
					greedyTour_ = u.path;
				}
				else
				{
					u.bound = getBound( u.path, W, n_ );
					PQ.push(u);
				}
			}
		}
	}
	lengthGreedy =  getPathLength( greedyTour_, W );
// 	cout << "Greedy Bound path: "; printVector(greedyTour_);
// 	cout << "Length: " << lengthGreedy << endl;
}

unsigned int BnB::getBound( const vector<unsigned int>& partialPath, int const* const* W, const unsigned int n )
{
	assert( W != NULL );
	unsigned int bound = 0;
	vector<bool> visited( n, false ); // track which vertices are included in partialPath
	vector<unsigned int>::const_iterator citer = partialPath.begin();
	unsigned int current = *citer;
	citer++; // now calculate weights for v1 ... vk-1
	unsigned int next;
	for ( citer; citer != partialPath.end(); citer++ )
	{ // add weights of edges in path
		assert( current < n );
		next = *citer;
		assert( next < n ); // assume that vertices of path are within bounds
		visited[current] = true; // we don't visit vk yet
		//			cout << "current: " << current << " next: " << next << endl;
		bound += W[current][next];
		current = next;
	} // now calculate weight for vk, which cannot return to visited v1...vk-1
	//		cout << "bound after visited nodes: " << bound << endl;
	unsigned int vk = partialPath.back();
	visited[vk] = true;
	int minWeight = numeric_limits<int>::max();
	for ( unsigned int j = 0; j < n; j++) { // choose minimum unvisited edge to leave vk
		if ( !visited[j] ) {// path from vk cannot return to visited vertices
			if ( W[vk][j] < minWeight )
				minWeight = W[vk][j];
		}
	}
	bound += minWeight;
	//		cout << "bound after vk: " << bound << endl;

	visited[0] = false; // remaining unvisited vertices can return to v1
	for ( unsigned int i = 0; i < n; i++ ) 
	{// add minimum weights for vertices not in path
		if ( !visited[i] && i != partialPath.front() ) // can't leave from visited vertices
		{ // get minimum weight among edges
			minWeight = numeric_limits<int>::max();
			for ( unsigned int j = 0; j < n; j++)
			{
				if ( !visited[j] && j != i ) // cannot return to v2 ... vk
				{
					if ( W[i][j] < minWeight )
						minWeight = W[i][j];
				}
			}
			bound += minWeight;
		}
	}
	// 		cout << "Bound: "<< bound << " partialPath: ";
	// 		printVector(partialPath);
	return bound;
}