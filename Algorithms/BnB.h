// Branch and Bound approach to solving the Travelling Salesman Problem
#pragma once
#ifndef BNB_H
#define BNB_H

#include <assert.h>
#include <vector>
#include <queue>
#include <limits>
#include "helpers.h"


struct node
{
	int level;
	vector<unsigned int> path;
	unsigned int bound;
};
// quick way to make priority queue return minimum
bool operator < (const node& lhs, const node& rhs);

class BnB {
private:
	unsigned int n_;	// number of vertices in graph
	vector<unsigned int> optTour_;
	vector<unsigned int> greedyTour_;
	unsigned int minLength_;
	unsigned int lengthGreedy;
public:
	inline const vector<unsigned int>& getTour() const{return optTour_;}
	inline const vector<unsigned int>& getGreedyTour() const{return greedyTour_;}
	inline const unsigned int getLength() const{return minLength_;}
	inline const unsigned int getLengthGreedy() const{ return lengthGreedy; }
	// use branch and bound approach to find optimal path
	void travel( const unsigned int n, int const* const* W );

	void fillVisited( const vector<unsigned int>& path, vector<bool>& visited );

	// calculate optimal tour using greedy approach
	void greedy( const unsigned int n, int const* const* W );

	// calculate bound on a partial path containing v1 ... vk
	unsigned int getBound( const vector<unsigned int>& partialPath, int const* const* W, const unsigned int n );
};
#endif