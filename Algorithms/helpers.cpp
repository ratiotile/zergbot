#include "helpers.h"

using namespace std;

void printMultiGraph( vector<vector<vector<int>>> multi ){
	//	cout << multi.size() << "," << multi[0].size() << "," << multi[0][0].size() << endl;
	for ( unsigned int i = 0; i < multi.size(); i++ )
	{
		for ( unsigned int j = 0; j < multi[i].size(); j++ )
		{
			for ( unsigned int k = 0; k < multi[i][j].size(); k++ )
			{
				if (multi[i][j][k] == -1) cout << "_";
				else cout << multi[i][j][k];
				if ( (k + 1 < multi[i][j].size()) ) cout << "/";
				else cout << "\t";
			}
			//cout <<  " ";
		}
		cout << endl;
	}
}
void print2DSquareArray( const unsigned int n, int const* const* A ){
	for (unsigned int i = 0; i < n ; i++)
	{
		for (unsigned int j = 0; j < n ; j++)
		{
			if (A[i][j] == -1) cout << "_ ";
			else cout << A[i][j];
			if (A[i][j] <= 9 && A[i][j] >= 0)
				cout << "  ";
			else cout << " ";
		}
		cout << endl;
	}
}
unsigned int countElements(vector<bool> const &counter){
	unsigned int sum = 0;
	for (unsigned int i = 0; i < counter.size(); i++)
		sum += counter[i];
	return sum;
}
unsigned int getPathLength( const vector<unsigned int>& path, int const* const* W ){
	if ( path.size() == 0 ) // guard
		return 0;
	unsigned int pathLength = 0; // iterate through path and add up edge costs
	unsigned int currentPos = path.front();
	unsigned int nextPos;
	vector<unsigned int>::const_iterator citer;
	citer = path.begin();
	citer++; // move to next vertex
	for ( citer; citer != path.end(); citer++)
	{// find edge, add to length
		nextPos = *citer;
		pathLength += W[currentPos][nextPos];
		currentPos = nextPos;
	}
//	cout << "Length of path: " << pathLength << endl;
	return pathLength;
}

