#ifndef HELPERS_H
#define HELPERS_H
#include <iostream>
#include <vector>
#include <algorithm>
//#include <assert.h>
using namespace std;

// print vector, template class
template <typename T> void printVector( const vector<T> &vec){
	for (unsigned int i = 0; i < vec.size(); i++)
	{
		cout << vec[i];
		if (i + 1 < vec.size()) cout << ",";
	}
	cout << endl;
}
// print 2d vector, template class
template <typename T> void print2DVector( vector<vector<T>> &vec2d){
	for (unsigned int i = 0; i < vec2d.size(); i++)
	{
		printVector(vec2d[i]);
	}
	cout << endl;
}
// print multigraph
void printMultiGraph( vector<vector<vector<int>>> multi );

void print2DSquareArray( const unsigned int n, int const* const* A );

unsigned int countElements(vector<bool> const &counter);

unsigned int getPathLength( const vector<unsigned int>& path, int const* const* W );

// deletes contents of vector, then sets pointer to NULL
template <typename T>
void delete2DArray( const unsigned int n, T** V )
{
	//assert( V != NULL ); // dont want to delete twice
	for ( unsigned int i = 0; i < n; i++ )
	{
		delete[] V[i];
	}
	delete[] V;
	V = NULL;
}

// returns average value of vector
template <typename T>
double getMean( const vector<T>& v ){
	double result = 0;
	for ( unsigned int i = 0; i < v.size(); i++ )
	{
		result += (double)v[i];
	}
	return result/v.size();
}

// returns maximum of vector
template <typename T>
T getMax( const vector<T>& v ){
	T result = 0;
	for ( unsigned int i = 0; i < v.size(); i++ )
	{
		if ( result < v[i] )
		{
			result = v[i];
		}
	}
	return result;
}

#endif