#pragma once
#ifndef INCLUDED_BASESTATE_H
#define INCLUDED_BASESTATE_H
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#ifndef INCLUDED_BWTA_H
	#define INCLUDED_BWTA_H
	#include <BWTA.h>
#endif

using namespace std;
using BWAPI::Position;

/* This class will be like a struct, the idea is to share information
	among the other agents. Contains data about our bases*/
class BaseState 
{
public:
	enum state_t{
		sUNCLAIMED,
		sSECURE,
		sSENT_PROBE,
		sBUILDING,	// now will have corresponding BaseAgent/position
		sOPERATIONAL
	};
	state_t state;
	Position location;
	bool isIsland;
	set<const BWAPI::Unit*> attackers;
	BWTA::BaseLocation* baseLocation;

	BaseState(BWTA::BaseLocation* nBL);
};
#endif