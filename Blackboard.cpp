#include "Blackboard.h"
#include "common/Config.h"

namespace BBS{
	Blackboard BB;	// need to use extern in header
}

Blackboard::Blackboard():
isMapAnalyzed(false),
config_(NULL),
resourcesRequired(),
lastFrameRegroup(0),
DRAW_UAB_DEBUG(true),
rallyPoint(BWAPI::Positions::None)
{
	PS.devState = PlayerState::EARLY_GAME;
	PS.isMapAnalyzed = false;
	PS.supplyAvailable = 0;	// BWAPI supply is double in-game supply
	ES.wkrsOnGas = 0;
	ES.wkrsOnMin = 0;
	ES.gasRate = 0;
	ES.minRate = 0;
	ES.getGas = false;
	gatewayWarpInTime = 0;
	numSquads = 0;
}

Blackboard::~Blackboard()
{
	if (config_)
	{
		delete config_;
		config_ = NULL;
	}
}
/// called in OnStart()
void Blackboard::init()
{
	config_ = new Config("ai_settings.info");
}
BWAPI::UnitType Blackboard::getPriorityProduction( UnitType factoryType )
{
	UnitType prodTarget = BWAPI::UnitTypes::None;
	double currentDemand = 0;
	map<UnitType,int>::const_iterator iPT;
	//logfile << "getPriority Production: prodTargets size " << PS.unitProductionTargets_.size() << endl;
	for (iPT = PS.unitProductionTargets_.begin(); iPT != PS.unitProductionTargets_.end(); ++iPT)
	{
		if (iPT->first.whatBuilds().first == factoryType)
		{ // if the production is built by this factoryType
			if (ES.getUnitTotal(iPT->first) < PS.getProductionTarget(iPT->first))
			{ // if production target is not met
				double newDemand = PS.getProductionTarget(iPT->first) 
					/ (double)(0.1 + ES.getUnitTotal(iPT->first));
				if (newDemand > currentDemand)
				{
					prodTarget = iPT->first;
					currentDemand = newDemand;
				}
			}
		}
	}
	//logfile << "Selected prodTarget " << prodTarget.getName() << endl;
	return prodTarget;
}
void Blackboard::update()
{
	updatePS();
	BQ.update();
	//displayUnitCount();
	// display rally
	if (rallyPoint.isValid())
	{
		Broodwar->drawTextMap(rallyPoint.x(),rallyPoint.y(),"_RALLY_");
	}
}

void Blackboard::updatePS()
{
	PS.supplyAvailable = Broodwar->self()->supplyTotal() - Broodwar->self()->supplyUsed();
}

void Blackboard::displayUnitCount()
{
	map<UnitType,BuildingCountInfo>::const_iterator ci = ES.buildingCount_.begin();
	int line = 0;
	for (ci; ci != ES.buildingCount_.end(); ++ci)
	{
		Broodwar->drawTextScreen(500,55+11*line,"%s: %d/%d/%d", ci->first.getName().c_str(), 
			ci->second.numComplete, ci->second.numConstructing, ci->second.numOrdered);
		line++;
	} // also display units
	map<UnitType,UnitCountInfo>::const_iterator ui = ES.unitCount_.begin();
	for (ui; ui != ES.unitCount_.end(); ++ui)
	{
		Broodwar->drawTextScreen(500,55+11*line,"%s: %d/%d/%d", ui->first.getName().c_str(), 
			ui->second.numActive, ui->second.numTraining, ui->second.numOrdered);
		line++;
	}
}

vector<const UIptr> Blackboard::getEnemyInRadius( Position center, int pixelRadius ) const
{
	//return reconMan->getEnemyInRadius( center,  pixelRadius);
	vector<const UIptr> enemies;
	return enemies;
}

vector<const UIptr> Blackboard::getEnemyCombatInRadius( Position center, int pixelRadius ) const
{
 	vector<const UIptr> enemies; //= reconMan->getEnemyInRadius( center,  pixelRadius);
// 	vector<const UIptr>::iterator iUnits = enemies.begin();
// 	while (iUnits != enemies.end())
// 	{
// 		if (!(*iUnits)->getUnit()->getType().canAttack() || (*iUnits)->getUnit()->getType().isWorker())
// 		{
// 			iUnits = enemies.erase(iUnits);
// 		}else{
// 			iUnits++;
// 		}
// 	}
 	return enemies;
}
/*
void Blackboard::updateBase( BWTA::BaseLocation* bLoc, Position bPos, BaseState::state_t nState )
{
	bool found = false;
	for (vector<BaseState>::iterator iBase = bases.begin();
	iBase != bases.end(); iBase++)
	{
		if (iBase->baseLocation == bLoc)
		{
			found = true;
			if (bPos != BWAPI::Positions::None) iBase->location = bPos;
			iBase->state = nState;
		}
	}
	if (!found)
	{
		BaseState newBase(bLoc);
		if (bPos != BWAPI::Positions::None) newBase.location = bPos;
		newBase.state = nState;
		bases.push_back(newBase);
	}
}*/

bool Blackboard::haveNewExpo() const
{
	bool haveExpo = false;
	for (vector<BaseState>::const_iterator iBS = bases.begin();
		iBS != bases.end(); iBS++)
	{
		if (iBS->state != BaseState::sOPERATIONAL) haveExpo = true;
	}
	return haveExpo;
}

bool Blackboard::reachedProductionTarget( UnitType prodType ) const
{
	bool reached = ES.getUnitTotal(prodType) >= PS.getProductionTarget(prodType);
	return reached;
}

int Blackboard::groundThreatRange( UnitType ut )
{
	if (ut.groundWeapon() == BWAPI::WeaponTypes::None) return 0;
	if (ut.groundWeapon().maxRange() < 32)
	{
		return ut.groundWeapon().maxRange() + 32*5;
	}
	else return ut.groundWeapon().maxRange() + 32*3;
}