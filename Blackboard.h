#pragma once
#ifndef INCLUDED_BLACKBOARD_H
#define INCLUDED_BLACKBOARD_H
#include <BWAPI.h>
#include <vector>
#include <map>
#ifndef INCLUDED_LOGGING
	#define INCLUDED_LOGGING
	#include <fstream>
#endif
#ifndef INCLUDED_PRINT_H
	#include "common/print.h"
#endif
#include "EconState.h"
#include "PlayerState.h"
#include "BaseState.h"
#include "common/UnitInfo.h"
#include "common/typedefs.h"
#include "BuildQueue.h"
#include "common/ResourceSet.h"

using namespace std;
using BWAPI::UnitType;
using BWAPI::Position;
extern ofstream logfile;

class UnitInfo;
class Config;


class Blackboard
{
public:
	PlayerState PS;
	EconState ES;	// should only be updated by Economy
	unsigned short gatewayWarpInTime; // time for warp-in animation to play
	int numSquads; // updated by OpsManager
	vector<BaseState> bases;
	//void updateBase(BWTA::BaseLocation* bLoc, Position bPos, BaseState::state_t nState);
	bool haveNewExpo() const;
	bool isMapAnalyzed;
	BuildQueue BQ;
	ResourceSet resourcesRequired; // updated by Commander, no longer used
	// required by UAB combat system, no longer used
	int lastFrameRegroup;
	bool DRAW_UAB_DEBUG;
	BWAPI::Position rallyPoint;


	Blackboard();
	~Blackboard();
	void init(); // call in exampleModule initialization, as BWAPI cannot be called at startup
	UnitType getPriorityProduction(UnitType factoryType); // finds highest demand unit factory produces
	void update();
	
	vector<const UIptr> getEnemyInRadius(Position center, int pixelRadius) const;
	vector<const UIptr> getEnemyCombatInRadius(Position center, int pixelRadius) const;
	bool reachedProductionTarget(UnitType prodType) const; 
	const Config& Cfg() const { return *config_; }
	void displayUnitCount();
	int groundThreatRange(UnitType ut);

private:
	Config* config_;

	void updatePS();
	
};
namespace BBS{
	extern Blackboard BB;	// use extern here, define in cpp file
}
#endif