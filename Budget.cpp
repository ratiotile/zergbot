#include "Budget.h"
#include "common/cpp_includes.h"
#include "tasks/C_BuildUnit.h"
#include "tasks/typedef_Task.h"
#include "common/ResourceSet.h"
#include <algorithm>
#include <sstream>
#include "Blackboard.h"

Budget::Budget()
:mMineralsReserved(0)
,mGasReserved(0)
,mSupplyReserved(0)
,mMineralShortfall(0)
,mGasShortfall(0)
{

}

void Budget::update()
{
	//processJobs();
	displayReserve();
}

void Budget::displayReserve() const
{
	Broodwar->drawTextScreen(437,15,"Reserve:%i/%i/%i",mMineralsReserved,mGasReserved,mSupplyReserved);
	Broodwar->drawTextScreen(437,24,"Available:%i/%i/%i",mineralsAvailable(),gasAvailable(),supplyAvailable());
}

bool Budget::addContract( pContract job )
{
	if ( !job )
	{
		logfile << "Error in Budget addContract: bad ptr\n";
		return false;
	}
	if ( job->name == "BuildUnit" )
	{
		mContracts.push_back(job);
		// take action to immediate update
		pCBU buildJob = boost::static_pointer_cast<C_BuildUnit>(job);
		if (buildJob)
		{
			mMineralsReserved += buildJob->buildType.mineralPrice();
			mGasReserved += buildJob->buildType.gasPrice();
			mSupplyReserved += buildJob->buildType.supplyRequired();
		}
		//logfile << "Budget accepts BuildUnit contract to reserve.\n";
		return true;
	}
	logfile << "Warning in Budget addContract: unable to handle\n";
	return false;
}

void Budget::cancel( pContract job )
{
	//logfile << "Budget canceling reserve contract... ";
	list<pContract>::iterator ipc = mContracts.begin();
	for (ipc; ipc != mContracts.end(); )
	{
		if ((*ipc) == job)
		{
			ipc = mContracts.erase(ipc);
			//logfile << "successful\n";
			return;
		}
		else ++ipc;
	}
	//logfile << "Warning - not found!\n";
}

void Budget::processJobs()
{
// 	mMineralsReserved = 0;
// 	mGasReserved = 0;
// 	mSupplyReserved = 0;
// 
// 	// go through each reserve unit and subtract, if remain > 0, complete
// 	list<pContract>::iterator lci = mContracts.begin();
// 	for ( lci; lci != mContracts.end(); ++lci )
// 	{
// 		pContract job = *lci;
// 		if(!job)
// 		{
// 			logfile << "Error in Budget calculateReserve: bad pContract\n";
// 			continue;
// 		}
// 		if (job->name == "BuildUnit")
// 		{
// 			pCBU buildJob = boost::static_pointer_cast<C_BuildUnit>(job);
// 			if (buildJob)
// 			{
// 				UnitType ut = buildJob->buildType;
// 				mMineralsReserved += ut.mineralPrice();
// 				mGasReserved += ut.gasPrice();
// 				mSupplyReserved += ut.supplyRequired();
// 
// 				mMineralsAvailable -= ut.mineralPrice();
// 				mGasAvailable -= ut.gasPrice();
// 				mSupplyAvailable -= ut.supplyRequired();
// 				// if positive left over, mark as reserve fulfilled
// 				if (mMineralsAvailable >= 0 && mGasAvailable >= 0
// 					&& (mSupplyAvailable >= 0 || ut.supplyRequired() == 0))
// 				{	// allow building of supply-less units when maxed
// 					buildJob->status = TaskStatus::complete;
// 				}
// 			}
// 			else
// 			{
// 				logfile << "Error in Budget calculateReserve: bad cast\n";
// 			}
// 		}
// 	}
}

int Budget::supplyAvailable() const
{
	return Broodwar->self()->supplyTotal() - Broodwar->self()->supplyUsed()
		- mSupplyReserved;
}

int Budget::mineralsAvailable() const
{
	return Broodwar->self()->minerals() - mMineralsReserved;
}

int Budget::gasAvailable() const
{
	return Broodwar->self()->gas() - mGasReserved;
}

ResourceSet Budget::reserveResources( const ResourceSet& resources )
{ // do some checking,reserve only as much as we have available
	ResourceSet toReserve = canReserve(resources);
	mMineralsReserved += toReserve.minerals;
	mGasReserved += toReserve.gas;
	mSupplyReserved += toReserve.supply;
// 	logfile << "Budget: reserving resources: " << resources.toString() << " Budgeted["
// 		<< mMineralsReserved << "," << mGasReserved << "], Avl:[" << mineralsAvailable()
// 		<< "," << gasAvailable() << "], Actual:[" << Broodwar->self()->minerals() <<
// 		"," << Broodwar->self()->gas() <<"]" << endl;
	return toReserve;
}

void Budget::clearReserve( const ResourceSet& resources )
{
// 	logfile << "Budget: clearing reserve: " << resources.toString() <<
// 		" Now have reserved:[" << mMineralsReserved << "," << mGasReserved <<
// 		"], Actual:[" << Broodwar->self()->minerals() <<
// 		"," << Broodwar->self()->gas() <<"]\n";
	mMineralsReserved -= resources.minerals;
	mGasReserved -= resources.gas;
	mSupplyReserved -= resources.supply;
}

bool Budget::enoughResourcesFor( const BWAPI::UnitType unitType ) const
{
	if ( mineralsAvailable() < unitType.mineralPrice() || 
		gasAvailable() < unitType.gasPrice() ||
		supplyAvailable() < unitType.supplyRequired() )
		return false;
	return true;
}

ResourceSet Budget::availableResources() const
{
	return ResourceSet(mineralsAvailable(),gasAvailable(),supplyAvailable());
}

ResourceSet Budget::canReserve( const ResourceSet& resources )
{
	ResourceSet reservable( min(mineralsAvailable(),resources.minerals),
		min(gasAvailable(),resources.gas),
		min(supplyAvailable(),resources.supply));
	mMineralShortfall = mineralsAvailable() - resources.minerals;
	mGasShortfall = gasAvailable() - resources.gas;
	reservable.minerals = max(0,reservable.minerals);
	reservable.gas = max(0,reservable.gas);
	reservable.supply = max(0, reservable.supply);
	return reservable;
}

std::string Budget::resourceString() const
{
	ostringstream oss;
	oss << "M:" << mineralsAvailable();
	return oss.str();
}