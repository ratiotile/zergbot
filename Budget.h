/**
Budget manages reserving resources for planned units. Construction and training
controllers request the current available minerals/gas from Budget, which is
the amount remaining after taking away the reserve. Budget looks for tasks
from EconManager.
Budget now also reserves supply as well. Uses game-code supply, which is double displayed.
*/
#pragma once
#include "common/Singleton.h"
#include "Contractor.h"
#include "typedef_Contract.h"
#include <BWAPI.h>

class ResourceSet;

class Budget: public Singleton<Budget>, public Contractor
{
public:
	Budget();
	/// call every frame after EconManager
	void update();
	// stop using contracts TODO: eliminate!
	bool addContract(pContract job);
	void cancel(pContract job);
	/// draw reserve min/gas under the resource display
	void displayReserve() const;
	/// return available non-negative supply
	int supplyAvailable() const;
	/// return minerals after subtracting reserve
	int mineralsAvailable() const;
	/// return gas after subracting reserved
	int gasAvailable() const;
	/// use when allocating resources to tasks
	ResourceSet reserveResources( const ResourceSet& resources );
	/// call whenever reserved resources are spent
	void clearReserve( const ResourceSet& resources );
	/// return ResourceSet of all free resources
	ResourceSet availableResources() const;
	/// true if resource is in short supply
	bool isGasShortfall() const {return mGasShortfall < 0;}
	bool isMineralShortfall() const {return mMineralShortfall < 0;}

	/// given a unit type, do we have enough free resources to build?
	bool enoughResourcesFor( const BWAPI::UnitType unitType ) const;
	/// given resources needed, what relevant resources do we have? sets shortfall
	ResourceSet canReserve( const ResourceSet& resources );
	std::string resourceString() const;
private:
	// inherited from Contractor:
	// std::list<pTask> myTasks;

	int mMineralsReserved;
	int mGasReserved;
	int mSupplyReserved;

	/// if negative, not enough of resource
	int mMineralShortfall;
	int mGasShortfall;

	void processJobs();
};