#include "BuildHelper.h"

// define static member variables
bool BuildHelper::isFirstPylon = true;
vector<vector<bool>> BuildHelper::gatewayPylonMask;
vector<vector<bool>> BuildHelper::TechPylonMask;
vector<vector<bool>> BuildHelper::pylonMask4x3;
vector<vector<bool>> BuildHelper::pylonMask3x2;
vector<vector<bool>> BuildHelper::pylonMaskCannon;


vector<vector<bool>> BuildHelper::initGatewayPylonMask()
{
	// 16x11, [x][y]
	vector<vector<bool>> tempVec;
	bool col01[11] = {0,0,1,1,1,1,1,1,1,0,0};
	bool col02[11] = {0,0,1,1,1,1,1,1,1,1,0};
	bool col03[11] = {0,1,1,1,1,1,1,1,1,1,0};
	bool col04[11] = {0,1,1,1,1,1,1,1,1,1,0};
	bool col05[11] = {0,1,1,1,1,1,1,1,1,1,1};
	bool col06[11] = {1,1,1,1,1,1,1,1,1,1,1};
	bool col07[11] = {1,1,1,1,0,0,0,1,1,1,1};
	bool col08[11] = {1,1,1,1,0,0,0,1,1,1,1};
	bool col09[11] = {1,1,1,1,0,0,0,1,1,1,1};
	bool col10[11] = {1,1,1,1,0,0,0,1,1,1,1};
	bool col11[11] = {1,1,1,1,1,1,1,1,1,1,1};
	bool col12[11] = {0,1,1,1,1,1,1,1,1,1,1};
	bool col13[11] = {0,1,1,1,1,1,1,1,1,1,0};
	bool col14[11] = {0,1,1,1,1,1,1,1,1,1,0};
	bool col15[11] = {0,0,1,1,1,1,1,1,1,1,0};
	bool col16[11] = {0,0,1,1,1,1,1,1,1,0,0};
	tempVec.push_back(vector<bool>(col01, col01 + 11));
	tempVec.push_back(vector<bool>(col02, col02 + 11));
	tempVec.push_back(vector<bool>(col03, col03 + 11));
	tempVec.push_back(vector<bool>(col04, col04 + 11));
	tempVec.push_back(vector<bool>(col05, col05 + 11));
	tempVec.push_back(vector<bool>(col06, col06 + 11));
	tempVec.push_back(vector<bool>(col07, col07 + 11));
	tempVec.push_back(vector<bool>(col08, col08 + 11));
	tempVec.push_back(vector<bool>(col09, col09 + 11));
	tempVec.push_back(vector<bool>(col10, col10 + 11));
	tempVec.push_back(vector<bool>(col11, col11 + 11));
	tempVec.push_back(vector<bool>(col12, col12 + 11));
	tempVec.push_back(vector<bool>(col13, col13 + 11));
	tempVec.push_back(vector<bool>(col14, col14 + 11));
	tempVec.push_back(vector<bool>(col15, col15 + 11));
	tempVec.push_back(vector<bool>(col16, col16 + 11));
	//printLog2DArray(tempVec,tempVec.size(),tempVec[0].size());
	return tempVec;
}
vector<vector<bool>> BuildHelper::initTechPylonMask()
{
	// 17x10, [x][y]
	vector<vector<bool>> tempVec;
	bool col01[10] = {0,0,0,1,1,1,1,1,0,0};
	bool col02[10] = {0,0,1,1,1,1,1,1,1,0};
	bool col03[10] = {0,1,1,1,1,1,1,1,1,1};
	bool col04[10] = {0,1,1,1,1,1,1,1,1,1};
	bool col05[10] = {0,1,1,1,1,1,1,1,1,1};
	bool col06[10] = {1,1,1,1,1,1,1,1,1,1};
	bool col07[10] = {1,1,1,1,1,1,1,1,1,1};
	bool col08[10] = {1,1,1,1,0,0,1,1,1,1};
	bool col09[10] = {1,1,1,1,0,0,1,1,1,1};
	bool col10[10] = {1,1,1,1,0,0,1,1,1,1};
	bool col11[10] = {1,1,1,1,1,1,1,1,1,1};
	bool col12[10] = {1,1,1,1,1,1,1,1,1,1};
	bool col13[10] = {0,1,1,1,1,1,1,1,1,1};
	bool col14[10] = {0,1,1,1,1,1,1,1,1,1};
	bool col15[10] = {0,1,1,1,1,1,1,1,1,1};
	bool col16[10] = {0,0,1,1,1,1,1,1,1,0};
	bool col17[10] = {0,0,0,1,1,1,1,1,0,0};
	tempVec.push_back(vector<bool>(col01, col01 + 10));
	tempVec.push_back(vector<bool>(col02, col02 + 10));
	tempVec.push_back(vector<bool>(col03, col03 + 10));
	tempVec.push_back(vector<bool>(col04, col04 + 10));
	tempVec.push_back(vector<bool>(col05, col05 + 10));
	tempVec.push_back(vector<bool>(col06, col06 + 10));
	tempVec.push_back(vector<bool>(col07, col07 + 10));
	tempVec.push_back(vector<bool>(col08, col08 + 10));
	tempVec.push_back(vector<bool>(col09, col09 + 10));
	tempVec.push_back(vector<bool>(col10, col10 + 10));
	tempVec.push_back(vector<bool>(col11, col11 + 10));
	tempVec.push_back(vector<bool>(col12, col12 + 10));
	tempVec.push_back(vector<bool>(col13, col13 + 10));
	tempVec.push_back(vector<bool>(col14, col14 + 10));
	tempVec.push_back(vector<bool>(col15, col15 + 10));
	tempVec.push_back(vector<bool>(col16, col16 + 10));
	tempVec.push_back(vector<bool>(col17, col17 + 10));
	//printLog2DArray(tempVec,tempVec.size(),tempVec[0].size());
	return tempVec;
}
vector<vector<bool>> BuildHelper::initPylonMask4x3()
{
	// 18x12, [x][y]
	vector<vector<bool>> tempVec;
	bool col01[12] = {0,0,1,1,1,1,1,1,1,1,1,0};
	bool col02[12] = {0,1,1,1,1,1,1,1,1,1,1,0};
	bool col03[12] = {0,1,1,1,1,1,1,1,1,1,1,1};
	bool col04[12] = {0,1,1,1,1,1,1,1,1,1,1,1};
	bool col05[12] = {1,1,1,1,1,1,1,1,1,1,1,1};
	bool col06[12] = {1,1,1,1,1,1,1,1,1,1,1,1};
	bool col07[12] = {1,1,1,1,1,1,1,1,1,1,1,1};
	bool col08[12] = {1,1,1,1,1,1,1,1,1,1,1,1};
	bool col09[12] = {1,1,1,1,1,0,0,1,1,1,1,1};
	bool col10[12] = {1,1,1,1,1,0,0,1,1,1,1,1};
	bool col11[12] = {1,1,1,1,1,1,1,1,1,1,1,1};
	bool col12[12] = {1,1,1,1,1,1,1,1,1,1,1,1};
	bool col13[12] = {1,1,1,1,1,1,1,1,1,1,1,1};
	bool col14[12] = {1,1,1,1,1,1,1,1,1,1,1,1};
	bool col15[12] = {0,1,1,1,1,1,1,1,1,1,1,1};
	bool col16[12] = {0,1,1,1,1,1,1,1,1,1,1,1};
	bool col17[12] = {0,1,1,1,1,1,1,1,1,1,1,0};
	bool col18[12] = {0,0,1,1,1,1,1,1,1,1,1,0};
	tempVec.push_back(vector<bool>(col01, col01 + 12));
	tempVec.push_back(vector<bool>(col02, col02 + 12));
	tempVec.push_back(vector<bool>(col03, col03 + 12));
	tempVec.push_back(vector<bool>(col04, col04 + 12));
	tempVec.push_back(vector<bool>(col05, col05 + 12));
	tempVec.push_back(vector<bool>(col06, col06 + 12));
	tempVec.push_back(vector<bool>(col07, col07 + 12));
	tempVec.push_back(vector<bool>(col08, col08 + 12));
	tempVec.push_back(vector<bool>(col09, col09 + 12));
	tempVec.push_back(vector<bool>(col10, col10 + 12));
	tempVec.push_back(vector<bool>(col11, col11 + 12));
	tempVec.push_back(vector<bool>(col12, col12 + 12));
	tempVec.push_back(vector<bool>(col13, col13 + 12));
	tempVec.push_back(vector<bool>(col14, col14 + 12));
	tempVec.push_back(vector<bool>(col15, col15 + 12));
	tempVec.push_back(vector<bool>(col16, col16 + 12));
	tempVec.push_back(vector<bool>(col17, col17 + 12));
	tempVec.push_back(vector<bool>(col18, col18 + 12));
	//printLog2DArray(tempVec,tempVec.size(),tempVec[0].size());
	return tempVec;
}
vector<vector<bool>> BuildHelper::initPylonMask3x2()
{
	// 18x10, [x][y]
	vector<vector<bool>> tempVec;
	bool col01[10] = {0,0,1,1,1,1,1,0,0,0};
	bool col02[10] = {0,1,1,1,1,1,1,1,0,0};
	bool col03[10] = {1,1,1,1,1,1,1,1,1,0};
	bool col04[10] = {1,1,1,1,1,1,1,1,1,0};
	bool col05[10] = {1,1,1,1,1,1,1,1,1,0};
	bool col06[10] = {1,1,1,1,1,1,1,1,1,1};
	bool col07[10] = {1,1,1,1,1,1,1,1,1,1};
	bool col08[10] = {1,1,1,1,1,1,1,1,1,1};
	bool col09[10] = {1,1,1,1,0,0,1,1,1,1};
	bool col10[10] = {1,1,1,1,0,0,1,1,1,1};
	bool col11[10] = {1,1,1,1,1,1,1,1,1,1};
	bool col12[10] = {1,1,1,1,1,1,1,1,1,1};
	bool col13[10] = {1,1,1,1,1,1,1,1,1,1};
	bool col14[10] = {1,1,1,1,1,1,1,1,1,0};
	bool col15[10] = {1,1,1,1,1,1,1,1,1,0};
	bool col16[10] = {1,1,1,1,1,1,1,1,1,0};
	bool col17[10] = {0,1,1,1,1,1,1,1,0,0};
	bool col18[10] = {0,0,1,1,1,1,1,0,0,0};
	tempVec.push_back(vector<bool>(col01, col01 + 10));
	tempVec.push_back(vector<bool>(col02, col02 + 10));
	tempVec.push_back(vector<bool>(col03, col03 + 10));
	tempVec.push_back(vector<bool>(col04, col04 + 10));
	tempVec.push_back(vector<bool>(col05, col05 + 10));
	tempVec.push_back(vector<bool>(col06, col06 + 10));
	tempVec.push_back(vector<bool>(col07, col07 + 10));
	tempVec.push_back(vector<bool>(col08, col08 + 10));
	tempVec.push_back(vector<bool>(col09, col09 + 10));
	tempVec.push_back(vector<bool>(col10, col10 + 10));
	tempVec.push_back(vector<bool>(col11, col11 + 10));
	tempVec.push_back(vector<bool>(col12, col12 + 10));
	tempVec.push_back(vector<bool>(col13, col13 + 10));
	tempVec.push_back(vector<bool>(col14, col14 + 10));
	tempVec.push_back(vector<bool>(col15, col15 + 10));
	tempVec.push_back(vector<bool>(col16, col16 + 10));
	tempVec.push_back(vector<bool>(col17, col17 + 10));
	tempVec.push_back(vector<bool>(col18, col18 + 10));
	//printLog2DArray(tempVec,tempVec.size(),tempVec[0].size());
	return tempVec;
}
vector<vector<bool>> BuildHelper::initPylonMaskCannon()
{
	// 16x10, [x][y]
	vector<vector<bool>> tempVec;
	bool col01[10] = {0,0,1,1,1,1,1,0,0,0};
	bool col02[10] = {0,1,1,1,1,1,1,1,0,0};
	bool col03[10] = {0,1,1,1,1,1,1,1,1,0};
	bool col04[10] = {1,1,1,1,1,1,1,1,1,0};
	bool col05[10] = {1,1,1,1,1,1,1,1,1,0};
	bool col06[10] = {1,1,1,1,1,1,1,1,1,1};
	bool col07[10] = {1,1,1,1,1,1,1,1,1,1};
	bool col08[10] = {1,1,1,1,0,0,1,1,1,1};
	bool col09[10] = {1,1,1,1,0,0,1,1,1,1};
	bool col10[10] = {1,1,1,1,1,1,1,1,1,1};
	bool col11[10] = {1,1,1,1,1,1,1,1,1,1};
	bool col12[10] = {1,1,1,1,1,1,1,1,1,0};
	bool col13[10] = {1,1,1,1,1,1,1,1,1,0};
	bool col14[10] = {0,1,1,1,1,1,1,1,1,0};
	bool col15[10] = {0,1,1,1,1,1,1,1,0,0};
	bool col16[10] = {0,0,1,1,1,1,1,0,0,0};
	tempVec.push_back(vector<bool>(col01, col01 + 10));
	tempVec.push_back(vector<bool>(col02, col02 + 10));
	tempVec.push_back(vector<bool>(col03, col03 + 10));
	tempVec.push_back(vector<bool>(col04, col04 + 10));
	tempVec.push_back(vector<bool>(col05, col05 + 10));
	tempVec.push_back(vector<bool>(col06, col06 + 10));
	tempVec.push_back(vector<bool>(col07, col07 + 10));
	tempVec.push_back(vector<bool>(col08, col08 + 10));
	tempVec.push_back(vector<bool>(col09, col09 + 10));
	tempVec.push_back(vector<bool>(col10, col10 + 10));
	tempVec.push_back(vector<bool>(col11, col11 + 10));
	tempVec.push_back(vector<bool>(col12, col12 + 10));
	tempVec.push_back(vector<bool>(col13, col13 + 10));
	tempVec.push_back(vector<bool>(col14, col14 + 10));
	tempVec.push_back(vector<bool>(col15, col15 + 10));
	tempVec.push_back(vector<bool>(col16, col16 + 10));
	//printLog2DArray(tempVec,tempVec.size(),tempVec[0].size());
	return tempVec;
}
const vector<vector<bool>>& BuildHelper::getGatewayPylonMask()
{
	if (gatewayPylonMask.empty())
		gatewayPylonMask = initGatewayPylonMask();
	return gatewayPylonMask;
}
const vector<vector<bool>>& BuildHelper::getTechPylonMask()
{
	if (TechPylonMask.empty())
		TechPylonMask = initTechPylonMask();
	return TechPylonMask;
}
const vector<vector<bool>>& BuildHelper::getPylonMask4x3()
{
	if (pylonMask4x3.empty())
		pylonMask4x3 = initPylonMask4x3();
	return pylonMask4x3;
}
const vector<vector<bool>>& BuildHelper::getPylonMask3x2()
{
	if (pylonMask3x2.empty())
		pylonMask3x2 = initPylonMask3x2();
	return pylonMask3x2;
}
const vector<vector<bool>>& BuildHelper::getPylonMaskCannon()
{
	if (pylonMaskCannon.empty())
		pylonMaskCannon = initPylonMaskCannon();
	return pylonMaskCannon;
}

buildSite BuildHelper::findOptimalBuildSite( BWAPI::UnitType building, vector < vector <tileBuildInfo> > buildMap )
{
	if (building.getName() == "Protoss Gateway" ||
		building.getName() == "Protoss Robotics Facility") // factories
	{ // call specialized function
		//Broodwar->printf("calling findGatewaySite");
		return findGatewaySite(building, buildMap);
	}
	else if (building.getName() == "Protoss Assimilator")
	{
		Broodwar->printf("BuildHelper should not handle Assimilators!");
	}
	else if (building.getName() == "Protoss Pylon")
	{ // special pylon placement. Power unpowered buildings first.
		return findPylonSite(building, buildMap);
	}
	
	return findTechSite(building, buildMap);
}

bool BuildHelper::isAdjacent8( int x, int y, tileBuildInfo::planned_t planned, const vector < vector <tileBuildInfo> >& buildMap )
{
	// get min, max
	int min[2] = {x,y};
	int max[2] = {x,y};
	if ((x + 1) < (int)buildMap.size()) max[0] = x + 1;
	if (x > 0) min[0] = x - 1;
	if ((y + 1) < (int)buildMap[0].size()) max[1] = y + 1;
	if (y > 0) min[1] = y - 1;
	for ( int i = min[0]; i <= max[0]; i++)
	{
		for ( int j = min[1]; j <= max[1]; j++)
		{
			if ( buildMap[i][j].planned == planned )
				return true;
		}
	}
	//logfile << "isAdjacent8(" << x << "," << y << "," << planned << ") max: " << max[0]<<","<<max[1]<<" min: "<<min[0]<<","<<min[1]<<endl;
	return false;
}

bool BuildHelper::isAdjacent4( int x, int y, tileBuildInfo::planned_t planned, const vector < vector <tileBuildInfo> >& buildMap )
{// get min, max
	int min[2] = {x,y};
	int max[2] = {x,y};
	if ((x + 1) < (int)buildMap.size()) max[0] = x + 1;
	if (x > 0) min[0] = x - 1;
	if ((y + 1) < (int)buildMap[0].size()) max[1] = y + 1;
	if (y > 0) min[1] = y - 1;
	if (buildMap[min[0]][y].planned == planned ) return true; // left
	if (buildMap[max[0]][y].planned == planned ) return true; // right
	if (buildMap[x][min[1]].planned == planned ) return true; // top
	if (buildMap[x][max[1]].planned == planned ) return true; // bottom
	return false;
}

unsigned short BuildHelper::buildMatrixScore( int x, int y, int width, int height, const vector < vector <tileBuildScore> >& scoreMap )
{
	unsigned short sum = 0;
	if (x + width >= (int)scoreMap.size()) return 0;
	if (y + height >= (int)scoreMap[0].size()) return 0;
	for ( int i = x; i < x + width; i++)
	{
		for ( int j = y; j < y + height; j++)
		{
			if (scoreMap[i][j].legal == false) return 0;
			sum += scoreMap[i][j].score;
		}
	}
	return sum / (width*height);
}
unsigned short BuildHelper::buildPylonScore( int x, int y, const vector < vector <tileBuildScore> >& scoreMap )
{ // first check if build area is legal
	unsigned short sum = 0;
	int width = 2; // pylon size stats
	int height = 2;
	if (x + width >= (int)scoreMap.size()) return 0;
	if (y + height >= (int)scoreMap[0].size()) return 0;
	if (x < 0) return 0;
	if (y < 0) return 0;
	for ( int i = x; i < x + width; i++)
	{
		for ( int j = y; j < y + height; j++)
		{
			if (scoreMap[i][j].legal == false) return 0;
			sum += scoreMap[i][j].score;
		}
	}// now compare matrix
	int clipX[2]={0,0}; // how much we need to cut off matrix left/right, if out of bounds
	int clipY[2]={0,0}; // how much we need to cut off matrix top/bottom, if out of bounds
	vector<vector<bool>> pylonMask = getPylonMask3x2();
	int radiusX = (pylonMask.size() - width)/2; //should be 8
	int radiusY = (pylonMask[0].size() - height)/2; // should be 4
	if (x - radiusX < 0) clipX[0] = -(x - radiusX);
	if (x + width + radiusX >= scoreMap.size()) clipX[1] = (x + width + radiusX) - (scoreMap.size()-1);
	if (y - radiusY < 0) clipY[0] = -(y - radiusY);
	if (y + width + radiusY >= scoreMap[0].size()) clipY[1] = (y + width + radiusY) - (scoreMap[0].size()-1);
	int matrixOffset[2]; // offset of topleft clipped matrix from build loc, negative
	assert(clipX[0]>=0);
	assert(clipY[0]>=0);
	matrixOffset[0] = clipX[0] - radiusX;
	matrixOffset[1] = clipY[0] - radiusY;
	assert(x+matrixOffset[0] >= 0 && y+matrixOffset[1] >= 0 );
	int matrixDim[2]; // new size of matrix
	matrixDim[0] = width + radiusX-clipX[1] - matrixOffset[0];
	matrixDim[1] = height + radiusY-clipY[1] - matrixOffset[1];
	assert( matrixDim[0]>0 && matrixDim[1] > 0 );
	assert( x+matrixOffset[0]+matrixDim[0] < scoreMap.size());
	assert( y+matrixOffset[1]+matrixDim[1] < scoreMap[0].size());
	int pmX,pmY,smX,smY;
	for (unsigned int i = 0; i < matrixDim[0]; i++)
	{
		for (unsigned int j = 0; j < matrixDim[1]; j++)
		{
			pmX = clipX[0] + i;
			pmY = clipY[0] + j;
			assert( pmX < pylonMask.size() );
			assert( pmY < pylonMask[0].size() );
			smX = x + i + matrixOffset[0];
			smY = y + j + matrixOffset[1];
			assert( smX < scoreMap.size() );
			assert( smY < scoreMap[0].size() );
			if (pylonMask[pmX][pmY])
			{
				sum += scoreMap[smX][smY].score;
			}
		}
	}
	return sum / (pylonMask.size()*pylonMask[0].size());
}
buildSite BuildHelper::findGatewaySite( BWAPI::UnitType building, vector < vector <tileBuildInfo> > buildMap )
{
#if DEBUG > 0
	int startFrame, endFrame;
	Broodwar->printf("findGatewaySite  starting");
	startFrame = Broodwar->getFrameCount();
#endif
	BWAPI::TilePosition bestPos;
	unsigned short bestScore = 0;
	unsigned short buildingSize[2];
	buildingSize[0] = building.tileWidth();
	buildingSize[1] = building.tileHeight();
	//logfile<< "Building Size: " << buildingSize[0] <<", "<<buildingSize[1]<<endl;
	//logWriteBuffer();
	vector < vector <tileBuildScore> > scoreMap(buildMap.size(),vector<tileBuildScore>(buildMap[0].size())); // numColumns, numRows
	//logfile << "BuildMap.planned: \n";
	for (unsigned int j = 0; j < scoreMap[0].size(); j++) // needed so that it prints in right orientation
	{
		for (unsigned int i = 0; i < scoreMap.size(); i++)
		{// scoremap same size as buildmap
			//if ( buildMap[i][j].planned < 10) logfile << " ";
			//logfile << (int)buildMap[i][j].planned << ",";
			if (buildMap[i][j].planned == tileBuildInfo::FRONT)
			{ // allow building on FRONT base
				scoreMap[i][j].legal = true;
				scoreMap[i][j].score = (short)(1/(double)buildMap[i][j].distance * (double)10000);
				if (buildMap[i][j].powered == tileBuildInfo::POWER_PLANNED)
					scoreMap[i][j].score *= 2;
				if (buildMap[i][j].powered == tileBuildInfo::POWERED)
					scoreMap[i][j].score *= 4;
			}
			else if (buildMap[i][j].planned == tileBuildInfo::REAR)
			{ // allow building on REAR base
				scoreMap[i][j].legal = true;
				scoreMap[i][j].score = (short)(1/(double)buildMap[i][j].distance * (double)10000);
				if (buildMap[i][j].powered == tileBuildInfo::POWER_PLANNED)
					scoreMap[i][j].score *= 1.5;
				if (buildMap[i][j].powered == tileBuildInfo::POWERED)
					scoreMap[i][j].score *= 2;
			}
			else if ( buildMap[i][j].planned == tileBuildInfo::RESOURCE &&
				(isAdjacent8(i,j,tileBuildInfo::FRONT,buildMap) ||
				isAdjacent8(i,j,tileBuildInfo::REAR,buildMap)) )
			{ // allow resource, but only for fringes of building
				scoreMap[i][j].legal = true;
				scoreMap[i][j].score = (short)(1/(double)buildMap[i][j].distance * (double)10000);
				if (buildMap[i][j].powered == tileBuildInfo::POWER_PLANNED)
					scoreMap[i][j].score *= 1.4;
				if (buildMap[i][j].powered == tileBuildInfo::POWERED)
					scoreMap[i][j].score *= 1.7;
			}	
			else{
				scoreMap[i][j].legal = false;
			}
		}
		//logfile << endl;
	} // calculate placement score
	//logfile << "Placement: \n";
	for (unsigned int j = 0; j < scoreMap[0].size()-buildingSize[1]; j++) // needed so that it prints in right orientation
	{
		for (unsigned int i = 0; i < scoreMap.size()-buildingSize[0]; i++)
		{
			if (scoreMap[i][j].score > 0)
			{
				scoreMap[i][j].placement = buildMatrixScore(i,j,buildingSize[0],buildingSize[1],scoreMap);
				if (scoreMap[i][j].placement > bestScore)
				{
					bestScore = scoreMap[i][j].placement;
					bestPos.x() = i;
					bestPos.y() = j;
				}
			}
			//if ( scoreMap[i][j].placement < 10) logfile << " ";
			//logfile << scoreMap[i][j].placement << ",";
		}
		//logfile<< endl;
	}
	//logfile << "ScoreMap: \n";
// 	for (unsigned int j = 0; j < scoreMap[0].size(); j++) // needed so that it prints in right orientation
// 	{
// 		for (unsigned int i = 0; i < scoreMap.size(); i++)
// 		{
// 			if (scoreMap[i][j].score>0)
// 			{
// 				if ( (scoreMap[i][j].score) < 10) logfile << " ";
// 				logfile << scoreMap[i][j].score  << ",";
// 			}
// 			else logfile << " 0,";
// 		}
// 		logfile << endl;
// 	}
// 	logfile << "Best Placement for "<< building.getName()<<": " << bestScore<< ": [" << bestPos.x()<<","<<bestPos.y()<<"]"<<endl;
#if DEBUG > 0
	endFrame = Broodwar->getFrameCount();
	Broodwar->printf("findGatewaySite finished. Took %d frames", endFrame-startFrame);
#endif
	buildSite r;
	r.buildPos = bestPos;
	r.optimality = bestScore;
	return r;
}

bool BuildHelper::needsPath( const BWAPI::UnitType& building )
{
	if (building.getName() == "Protoss Gateway") return true;
	else if (building.getName() == "Protoss Robotics Bay") return true;
	else return false;
}

buildSite BuildHelper::findTechSite( BWAPI::UnitType building, vector < vector <tileBuildInfo> > buildMap )
{
#if DEBUG > 0
	int startFrame, endFrame;
	Broodwar->printf("findTechSite  starting");
	startFrame = Broodwar->getFrameCount();
#endif
	buildSite r;
	unsigned short bestScore = 0;
	unsigned short buildingSize[2];
	buildingSize[0] = building.tileWidth();
	buildingSize[1] = building.tileHeight();
	//logfile<< "Building Size: " << buildingSize[0] <<", "<<buildingSize[1]<<endl;
	//logWriteBuffer();
	vector < vector <tileBuildScore> > scoreMap(buildMap.size(),vector<tileBuildScore>(buildMap[0].size())); // numColumns, numRows
//	logfile << "BuildMap.planned: \n";
	for (unsigned int j = 0; j < scoreMap[0].size(); j++) // needed so that it prints in right orientation
	{
		for (unsigned int i = 0; i < scoreMap.size(); i++)
		{// scoremap same size as buildmap
// 			if ( buildMap[i][j].planned < 10) logfile << " ";
// 			logfile << (int)buildMap[i][j].planned << ",";
			if (buildMap[i][j].planned == tileBuildInfo::FRONT ||
				buildMap[i][j].planned == tileBuildInfo::REAR)
			{ // allow building on FRONT OR rear base
				scoreMap[i][j].legal = true;
				scoreMap[i][j].score = buildMap[i][j].safety;
				if (buildMap[i][j].powered == tileBuildInfo::POWER_PLANNED)
					scoreMap[i][j].score *= 2;
				if (buildMap[i][j].powered == tileBuildInfo::POWERED)
					scoreMap[i][j].score *= 4;
			}

			else if ( buildMap[i][j].planned == tileBuildInfo::RESOURCE &&
				(isAdjacent4(i,j,tileBuildInfo::FRONT,buildMap) ||
				isAdjacent4(i,j,tileBuildInfo::REAR,buildMap)) )
			{ // allow resource, but only for fringes of building
				scoreMap[i][j].legal = true;
				scoreMap[i][j].score = buildMap[i][j].safety;
				if (buildMap[i][j].powered == tileBuildInfo::POWER_PLANNED)
					scoreMap[i][j].score *= 2;
				if (buildMap[i][j].powered == tileBuildInfo::POWERED)
					scoreMap[i][j].score *= 4;
			}	
			else{
				scoreMap[i][j].legal = false;
			}
		}
//		logfile << endl;
	} // calculate placement score
//	logfile << "Placement: \n";
	for (unsigned int j = 0; j < scoreMap[0].size()-buildingSize[1]; j++) // needed so that it prints in right orientation
	{
		for (unsigned int i = 0; i < scoreMap.size()-buildingSize[0]; i++)
		{
			if (scoreMap[i][j].score > 0)
			{
				scoreMap[i][j].placement = buildMatrixScore(i,j,buildingSize[0],buildingSize[1],scoreMap);
				if (scoreMap[i][j].placement > bestScore)
				{
					bestScore = scoreMap[i][j].placement;
					r.buildPos.x() = i;
					r.buildPos.y() = j;
				}
			}
// 			if ( scoreMap[i][j].placement < 10) logfile << " ";
// 			logfile << scoreMap[i][j].placement << ",";
		}
//		logfile<< endl;
	}
//	logfile << "ScoreMap: \n";
// 	for (unsigned int j = 0; j < scoreMap[0].size(); j++) // needed so that it prints in right orientation
// 	{
// 		for (unsigned int i = 0; i < scoreMap.size(); i++)
// 		{
// 			if (scoreMap[i][j].score>0)
// 			{
// 				if ( (scoreMap[i][j].score) < 10) logfile << " ";
// 				logfile << scoreMap[i][j].score  << ",";
// 			}
// 			else logfile << " 0,";
// 		}
// 		logfile << endl;
// 	}
// 	logfile << "Best Placement for "<< building.getName()<<": " << bestScore<< ": [" << r.buildPos.x()<<","<<r.buildPos.y()<<"]"<<endl;
#if DEBUG > 0
	endFrame = Broodwar->getFrameCount();
	Broodwar->printf("findTechSite finished. Took %d frames", endFrame-startFrame);
#endif
	r.optimality = bestScore;
	return r;
}

buildSite BuildHelper::findPylonSite( BWAPI::UnitType building, vector < vector <tileBuildInfo> > buildMap )
{
#if DEBUG > 0
	int startFrame, endFrame;
	logfile << "findPylonSite  starting.\n";
	startFrame = Broodwar->getFrameCount();
#endif
	// assume that buildMap is updated with power info
	buildSite r;
	unsigned short bestScore = 0;
	unsigned short buildingSize[2];
	buildingSize[0] = building.tileWidth();
	buildingSize[1] = building.tileHeight();
	vector < vector <tileBuildScore> > scoreMap(buildMap.size(),vector<tileBuildScore>(buildMap[0].size())); // numColumns, numRows
#if DEBUG > 1
	logfile<< "Building Size: " << buildingSize[0] <<", "<<buildingSize[1]<<endl;
	logWriteBuffer();
	logfile << "BuildMap.planned: \n";
	if (BuildHelper::isFirstPylon)
	{
		logfile << "First Pylon In front. \n";
	}
#endif
	for (unsigned int j = 0; j < scoreMap[0].size(); j++) // needed so that it prints in right orientation
	{
		for (unsigned int i = 0; i < scoreMap.size(); i++)
		{// scoremap same size as buildmap
#if DEBUG > 1
			if ( buildMap[i][j].planned < 10) logfile << " ";
			logfile << (int)buildMap[i][j].planned << ",";
#endif
			if (buildMap[i][j].planned == tileBuildInfo::FRONT)
			{ // allow building on FRONT base; prefer first pylon front
				scoreMap[i][j].legal = true;
				scoreMap[i][j].score = buildMap[i][j].safety/100;
				if (BuildHelper::isFirstPylon)
				{
					scoreMap[i][j].score = buildMap[i][j].safety/50;
				}
				if (buildMap[i][j].powered == tileBuildInfo::NO_POWER )
					scoreMap[i][j].score *= 2;
			}
			else if (buildMap[i][j].planned == tileBuildInfo::REAR)
			{
				scoreMap[i][j].legal = true;
				scoreMap[i][j].score = buildMap[i][j].safety/100;
				if (buildMap[i][j].powered == tileBuildInfo::NO_POWER )
					scoreMap[i][j].score *= 2;
			}
			else if ( buildMap[i][j].planned == tileBuildInfo::RESOURCE &&
				(isAdjacent4(i,j,tileBuildInfo::FRONT,buildMap) ||
				isAdjacent4(i,j,tileBuildInfo::REAR,buildMap)) )
			{ // allow resource, but only for fringes of building
				scoreMap[i][j].legal = true;
				scoreMap[i][j].score = buildMap[i][j].safety/100;
				if (buildMap[i][j].powered == tileBuildInfo::NO_POWER )
					scoreMap[i][j].score *= 2;
			}
			else{// not legal building location
				scoreMap[i][j].legal = false;
				if (buildMap[i][j].planned > 6 && buildMap[i][j].planned <=22 &&
					buildMap[i][j].planned != tileBuildInfo::NEXUS &&
					buildMap[i][j].planned != tileBuildInfo::ASSIMILATOR)
				{ // if built structure is unpowered
					scoreMap[i][j].score = buildMap[i][j].safety/100;
					if (buildMap[i][j].powered == tileBuildInfo::NO_POWER )
						scoreMap[i][j].score = 4000;
				}
				else if (buildMap[i][j].planned >= 50) // planned building
				{
					scoreMap[i][j].score = buildMap[i][j].safety/100;
					if (buildMap[i][j].powered == tileBuildInfo::NO_POWER )
						scoreMap[i][j].score = 2000;
				}
				else scoreMap[i][j].score = 0; // geysers and minerals
			}
		}
#if DEBUG > 1
		logfile << endl;
#endif
	} // calculate placement score
#if DEBUG > 1
	logfile << "Placement: \n";
#endif
	for (unsigned int j = 0; j < scoreMap[0].size()-buildingSize[1]; j++) // needed so that it prints in right orientation
	{
		for (unsigned int i = 0; i < scoreMap.size()-buildingSize[0]; i++)
		{
			if (scoreMap[i][j].legal)
			{
				scoreMap[i][j].placement = buildPylonScore(i,j,scoreMap);
				if (scoreMap[i][j].placement > bestScore)
				{
					bestScore = scoreMap[i][j].placement;
					r.buildPos.x() = i;
					r.buildPos.y() = j;
				}
			}
#if DEBUG > 1
			if ( scoreMap[i][j].placement < 10) logfile << " ";
 			logfile << scoreMap[i][j].placement << ",";
#endif
		}
#if DEBUG > 1
		logfile<< endl;
#endif
	}
	BuildHelper::isFirstPylon = false;
#if DEBUG > 1
	logfile << "ScoreMap: \n";
 	for (unsigned int j = 0; j < scoreMap[0].size(); j++) // needed so that it prints in right orientation
 	{
 		for (unsigned int i = 0; i < scoreMap.size(); i++)
 		{
 			if (scoreMap[i][j].score>0)
 			{
 				if ( (scoreMap[i][j].score) < 10) logfile << " ";
 				logfile << scoreMap[i][j].score  << ",";
 			}
 			else logfile << " 0,";
		}
		logfile << endl;
 	}
 	logfile << "Best Placement for "<< building.getName()<<": " << bestScore<< ": [" << r.buildPos.x()<<","<<r.buildPos.y()<<"]"<<endl;
#endif
#if DEBUG > 0
	endFrame = Broodwar->getFrameCount();
	logfile << "findPylonSite finished. Took " << endFrame-startFrame << " frames.\n";
#endif
	r.optimality = bestScore;
	return r;
}

bool BuildHelper::isResearch( const BWAPI::UnitType& building )
{
	if (building == BWAPI::UnitTypes::Protoss_Arbiter_Tribunal ||
		building == BWAPI::UnitTypes::Protoss_Citadel_of_Adun ||
		building == BWAPI::UnitTypes::Protoss_Cybernetics_Core ||
		building == BWAPI::UnitTypes::Protoss_Fleet_Beacon ||
		building == BWAPI::UnitTypes::Protoss_Forge ||
		building == BWAPI::UnitTypes::Protoss_Observatory ||
		building == BWAPI::UnitTypes::Protoss_Robotics_Support_Bay ||
		building == BWAPI::UnitTypes::Protoss_Templar_Archives)
	{
		return true;
	}
	return false;
}

int BuildHelper::checkBuildSite( const plannedBuilding& pb )
{
	TilePosition tBuildPos = pb.gamePos;
	if (!Broodwar->isExplored(tBuildPos))
	{ // tile is black
		return 1;
	}
	Position buildPos = Position(tBuildPos);
	Position botRight = Position(buildPos.x()+pb.building.tileWidth()*32,buildPos.y()+pb.building.tileHeight()*32);
	set< BWAPI::Unit*> blocking = Broodwar->getUnitsInRectangle(buildPos,botRight);
	if (blocking.size() > 1) // something is in the way
	{
		if((*blocking.begin())->getType() == BWAPI::UnitTypes::Protoss_Probe) blocking.erase(blocking.begin()); 
		if((*blocking.begin())->getType() == pb.building) // already built
			return 2;
		else{
			logfile << "BuildHelper.checkBuildSite: blocked by " << (*blocking.begin())->getType().getName() << endl;
			return 3;
		}
	}
	else
		return 0;
}

bool BuildHelper::haveSupplyToTrain( const UnitType& toTrain )
{
	return BB.PS.supplyAvailable > toTrain.supplyRequired();
}

bool BuildHelper::haveResourcesFor( const UnitType& toTrain )
{
	return ( Broodwar->self()->minerals() >= toTrain.mineralPrice() && 
		Broodwar->self()->gas() >= toTrain.gasPrice() );
}

bool BuildHelper::havePrereqsToBuild( const UnitType& toBuild )
{	// get all required units
	bool haveReqs = true;
	map<UnitType, int> unitReq = toBuild.requiredUnits();
	map<UnitType, int>::const_iterator iUnitReq = unitReq.begin();
	// iterate through and test if we got it
	for (iUnitReq; iUnitReq != unitReq.end(); ++iUnitReq)
	{	// just test buildings for now
		if (iUnitReq->first.isBuilding())
		{
			if (BB.ES.getUnitCount(iUnitReq->first) < iUnitReq->second) haveReqs = false;
		}
	}
	return haveReqs;
}

vector<int> BuildHelper::cropToMap( int mapX, int mapY, int top, int left, int bot, int right )
{	// mapY is 1 more than maximum Y value
	vector<int> cropped;
	if( top < 0 ) cropped.push_back(0);
	else cropped.push_back(top);
	if( left < 0 ) cropped.push_back(0);
	else cropped.push_back(left);
	if( bot >= mapY ) cropped.push_back(mapY - 1);
	else cropped.push_back(bot);
	if(right >= mapX ) cropped.push_back(mapX - 1);
	else cropped.push_back(right);
	return cropped;
}
vector<int> BuildHelper::cropToMap( int mapX, int mapY, int posX, int posY, int r )
{
	int top,left,bot,right;
	top = posY - r;
	left = posX - r;
	bot = posY + r;
	right = posX + r;
	return BuildHelper::cropToMap(mapX, mapY, top, left, bot, right);
}