#pragma once
#ifndef INCLUDED_BUILD_HELPER_H
#define INCLUDED_BUILD_HELPER_H
#define DEBUG 0

#include <bwapi.h>
#include <fstream> //logging
#include <math.h> //logging
#include <assert.h>
#include "common/print.h"
#include "tileBuildInfo.h"
#include "Blackboard.h"
#ifndef INCLUDED_PLANNEDBUILDING_H
	#include "plannedBuilding.h"
#endif

using namespace std;
using namespace BBS;
extern ofstream logfile; // logging


struct buildSite
{
	unsigned short optimality;
	BWAPI::TilePosition buildPos;
};


struct tileBuildScore // used for placing buildings
{
	short score;
	short placement; // avg score of buildMatrix tiles, 0 if illegal
	bool legal;
};
class BuildHelper{
private:
	static bool isFirstPylon;
	static vector<vector<bool>> gatewayPylonMask; // placing a pylon to power an existing gateway
	static vector<vector<bool>> TechPylonMask;	// placing pylon to power existing tech 3x2 structure
	static vector<vector<bool>> pylonMask4x3; // power mask for gateway/stargate. Powered if all squares within mask.
	static vector<vector<bool>> pylonMask3x2; // power mask for tech structures. Powered if all squares within mask.
	static vector<vector<bool>> pylonMaskCannon;
	// populates the gatewayPylonMask
	static vector<vector<bool>> initGatewayPylonMask();
	static vector<vector<bool>> initTechPylonMask();
	static vector<vector<bool>> initPylonMask4x3();
	static vector<vector<bool>> initPylonMask3x2();
	static vector<vector<bool>> initPylonMaskCannon();
	// helper function for findOptimalBuildSite
	// true if buildMap[x][y] is adjacent to a square with the specified planned value.
	static bool isAdjacent8(int x, int y, tileBuildInfo::planned_t planned, const vector < vector <tileBuildInfo> >& buildMap);
	// like isAdjecent8, but only for 4 tiles up down left right.
	static bool isAdjacent4(int x, int y, tileBuildInfo::planned_t planned, const vector < vector <tileBuildInfo> >& buildMap);
	// return 0 if unbuildable, else the average of scores across buildMatrix
	static unsigned short buildMatrixScore( int x, int y, int width, int height, const vector < vector <tileBuildScore> >& scoreMap);
	// return 0 if illegal, else the average of scores across techPylonMask
	static unsigned short buildPylonScore( int x, int y, const vector < vector <tileBuildScore> >& scoreMap);
	static buildSite findGatewaySite(BWAPI::UnitType building, vector < vector <tileBuildInfo> > buildMap);
	// finds a build location for tech buildings
	static buildSite findTechSite(BWAPI::UnitType building, vector < vector <tileBuildInfo> > buildMap);
	// gets site that powers the most buildings
	static buildSite findPylonSite(BWAPI::UnitType building, vector < vector <tileBuildInfo> > buildMap);
public:
	static const unsigned short warpInDelay = 70; // how much time for warp animation to play once building is done
	// inverted mask for powering existing Gateway with new Pylon
	static const vector<vector<bool>>& getGatewayPylonMask();
	// inverted mask for powering extant 3x2 building with new pylon
	static const vector<vector<bool>>& getTechPylonMask();
	static const vector<vector<bool>>& getPylonMask4x3();
	static const vector<vector<bool>>& getPylonMask3x2();
	static const vector<vector<bool>>& getPylonMaskCannon();
	// returns tilePosition relative to buildMap
	static buildSite findOptimalBuildSite(BWAPI::UnitType building, vector < vector <tileBuildInfo> > buildMap);
	// true if building creates ground units that can get stuck
	static bool needsPath(const BWAPI::UnitType& building);
	static bool isResearch(const BWAPI::UnitType& building);
	// returns 0 if buildSite clear, 1 unexplored, 2 already built, 3 blocked
	static int checkBuildSite( const plannedBuilding& pb );
	// uses BB to check available supply
	static bool haveSupplyToTrain(const UnitType& toTrain);
	static bool haveResourcesFor(const UnitType& toTrain);
	static bool havePrereqsToBuild(const UnitType& toBuild); // used to check if next item in buildGoalList is ready
	// given map bounds and matrix bounds, returns matrix bounds that lie within the map. [top,left,bot,right]
	static vector<int> cropToMap(int mapX, int mapY, int top, int left, int bot, int right);
	// given map bounds, center, and radius, returns matrix bounds of a square cropped to fit within map [top.left.bot.right]
	static vector<int> cropToMap(int mapX, int mapY, int posX, int posY, int r);
};

#endif