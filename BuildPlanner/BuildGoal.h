#pragma once
#ifndef INCLUDED_BUILDGOAL_H
#define INCLUDED_BUILDGOAL_H

#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include "BWAPI.h"
#endif
#ifndef INCLUDED_VECTOR
	#define INCLUDED_VECTOR
	#include <vector>
#endif

// a BuildGoal is a mid/late game goal to be achieved by Economy.
//TODO: don't think that techs and upgrades implemented yet!
struct BuildGoal
{
	/// mobs and structures, highest priority
	std::vector<std::pair<BWAPI::UnitType,int>> vunGoalUnits_;
	/// tech to be researched asap, when possible, at lower priority than goalUnits
	std::vector<BWAPI::TechType> vGoalTechs_; 
	/// <upgrade,level> to be research right away, at lower priority than goalTechs.
	std::vector<std::pair<BWAPI::UpgradeType,int>> vGoalUpgrades_; 
	inline void clearAll() {
		vunGoalUnits_.clear();
		vGoalTechs_.clear();
		vGoalUpgrades_.clear();
	}
	bool isEmpty()
	{ return vunGoalUnits_.empty()&&vGoalTechs_.empty()&&vGoalUpgrades_.empty(); }
};


#endif