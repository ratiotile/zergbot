#include "BuildGoalPlanner.h"
#include "../common/cpp_includes.h"
#include "../Blackboard.h"
#include "BuildGoal.h"
#include "reqInfo.h"

using namespace BBS;

list<UnitType> BuildGoalPlanner::generateBuildOrder( const BuildGoal& goal )
{
	logfile << "Begin BuildGoalPlanner::generateBuildOrder\n";
	logWriteBuffer();
	// handle morph units Archons in special case
	list<UnitType> buildOrder;
	map<UnitType, int> unitReq;	// required units for one goal
	map<UnitType, ReqInfo> allReqs;	// all required units
	map<UnitType, int>::iterator reqIter;
	logfile << "Getting goal pre-reqs.";
	for (unsigned int i = 0; i < goal.vunGoalUnits_.size(); ++i)
	{ // go though all goal unit types and get list of prereqs
		unitReq = goal.vunGoalUnits_[i].first.requiredUnits();
		allReqs[goal.vunGoalUnits_[i].first].num = goal.vunGoalUnits_[i].second;
		reqIter = unitReq.begin();
		for (reqIter; reqIter!=unitReq.end(); ++reqIter)
		{	// add to allReqs if we don't have it already
			if (!buildingExistsOrPlanned(reqIter->first))
			{	// map access creates object
				allReqs[reqIter->first].num = reqIter->second;
				allReqs[goal.vunGoalUnits_[i].first].requires.insert(reqIter->first);
			}
		}
	} // now allReqs should contain all the required units to make all goal units
	logfile << "Have goal pre-reqs, getting tech reqs.\n";
	logWriteBuffer();
	for (unsigned int i = 0; i < goal.vGoalTechs_.size(); ++i)
	{ // add required building for all techs
		if (!buildingExistsOrPlanned(goal.vGoalTechs_[i].whatResearches())){
			allReqs[goal.vGoalTechs_[i].whatResearches()].num = 1;
		}
	} // now allRequs should contain requirements to make goal units and techs'
	logfile << "Have tech pre-reqs, getting upgrade reqs.\n";
	logWriteBuffer();
	for (unsigned int i = 0; i < goal.vGoalUpgrades_.size(); ++i)
	{ // add required building to research upgrade, and also building that unlocks desired level of upgrade
		int level = goal.vGoalUpgrades_[i].second;
		if (!buildingExistsOrPlanned(goal.vGoalUpgrades_[i].first.whatUpgrades())){
			allReqs[goal.vGoalUpgrades_[i].first.whatUpgrades()].num = 1;
		}
		if (!buildingExistsOrPlanned(goal.vGoalUpgrades_[i].first.whatsRequired(level))){
			allReqs[goal.vGoalUpgrades_[i].first.whatsRequired(level)].num = 1;
		}
	} // now we need to recursively add pre-reqs for these required units
	map<UnitType, ReqInfo>::iterator allReqsIter = allReqs.begin();
	map<UnitType, ReqInfo>::iterator stopIter = allReqs.end();
	map<UnitType, ReqInfo> tempReqs;
	logfile << "Have upgrade pre-reqs, getting recursive reqs.\n";
	logWriteBuffer();
	do 
	{
		tempReqs.clear(); // empty tempReqs
		// check all requrirements
		allReqsIter = allReqs.begin();
		stopIter = allReqs.end();
		while (allReqsIter != stopIter)
		{ // loop over all items in allReqs
			unitReq = allReqsIter->first.requiredUnits();
			reqIter = unitReq.begin();
			//logfile << "allReqs item - " << allReqsIter->first.getName() << endl;
			while (reqIter != unitReq.end())
			{ // loop over all of that item's required units, adding if it is required and not in allreqs
				//logfile << "req - " << reqIter->first.getName() << " ";
				map<UnitType, ReqInfo>::iterator found = allReqs.find(reqIter->first);
				if ( !buildingExistsOrPlanned(reqIter->first))
				{	// add to requires
					//logfile << "Doesn't exist, adding to requires\n";
					allReqsIter->second.requires.insert(reqIter->first);
					if (found == allReqs.end())
					{ // add to tempReqs
						tempReqs[reqIter->first].num = 1;
					}
				}
				++reqIter;
			}
			++allReqsIter;
		}	// now add everything from tempReqs into allReqs
		//logfile << "tempReqs size: " << tempReqs.size() <<endl;
		allReqs.insert(tempReqs.begin(),tempReqs.end());
	} while (!tempReqs.empty()); // all pre-reqs recursively added
	logfile << "Have recursive pre-reqs, finding requiredBy.\n";
	logWriteBuffer();
	// finding RequiredBy values
	for (allReqsIter=allReqs.begin(); allReqsIter!=allReqs.end(); ++allReqsIter)
	{ // allReqsIter looping over all items in allReqs
		map<UnitType, ReqInfo>::iterator allReqsIter2;
		for (allReqsIter2=allReqs.begin(); allReqsIter2!=allReqs.end(); ++allReqsIter2)
		{ // allReqsIter2 also looping over all other items in allReqs
			set<UnitType> unitReqs = allReqsIter2->second.requires;
			set<UnitType>::iterator uriter;
			for (uriter =unitReqs.begin();uriter!=unitReqs.end();++uriter)
			{ // looping over the items that allreqsIter2 requires
				if (*uriter == allReqsIter->first)
				{ // it requires allReqsIter
					allReqsIter->second.requiredBy.insert(allReqsIter2->first);
				}
			}
		}
	}

	// removing zerg larva
	logfile << "Removing Zerg Larva from requirements set.\n";
	logWriteBuffer();
	allReqsIter = allReqs.begin();
	while( allReqsIter != allReqs.end() )
	{
		// look for larva and remove it from reqs
		if (allReqsIter->first == BWAPI::UnitTypes::Zerg_Larva)
		{
			allReqsIter = allReqs.erase(allReqsIter);
		}
		else // remove larva from requires list.
		{
			set<UnitType>::iterator rIter = allReqsIter->second.requires.begin();
			while (rIter != allReqsIter->second.requires.end())
			{
				if ( (*rIter) == BWAPI::UnitTypes::Zerg_Larva )
				{
					set<UnitType>::iterator toErase = rIter;
					rIter++;
					allReqsIter->second.requires.erase(toErase);
				}
				else rIter++;
			}
			allReqsIter++;
		}
	}

	// printing reqs
	allReqsIter = allReqs.begin();
	for (allReqsIter; allReqsIter!=allReqs.end(); allReqsIter++)
	{
		logfile << "- "<<allReqsIter->first.getName()<<"\t\t requires "<<allReqsIter->second.requires.size()
		<<" /requiredBy "<< allReqsIter->second.requiredBy.size() 
		<<" /num= "<<allReqsIter->second.num<<  endl;
		set<UnitType> reqSet = allReqsIter->second.requires;
		set<UnitType>::iterator rIter = reqSet.begin();
		logfile << "req: ";
		while (rIter != reqSet.end())
		{
			logfile << rIter->getName() << " ";
			rIter++;
		}
		logfile << endl;
	}
	// end printing reqs

	//finding buildOrder
	logfile << "Requirements found, now ordering.\n";
	logWriteBuffer();
	
	unsigned int minRequiredBy = 0;
	map<UnitType,ReqInfo>::iterator minReqByIter;
	while(!allReqs.empty())
	{
		minRequiredBy = 10;
		for (allReqsIter=allReqs.begin(); allReqsIter != allReqs.end(); ++allReqsIter)
		{	// find items that require nothing, yet are required by few, 
			//so we can get to goal fast
			if (allReqsIter->second.requires.empty()) // if its reqs would be met
			{
				logfile<< "can now add " << allReqsIter->first.getName() <<endl;
				logWriteBuffer();
				// find the one that is least required by others: a goal would be 0
				if (allReqsIter->second.requiredBy.size() <= minRequiredBy){
					logfile<< allReqsIter->first.getName()<< " is least required.\n";
					logWriteBuffer();
					minRequiredBy = allReqsIter->second.requiredBy.size();
					minReqByIter = allReqsIter;
				}
			}
		}	// add multiples
		logfile << "adding " << minReqByIter->first.getName() <<endl;
		logWriteBuffer();
		for (unsigned int i = 0; i < minReqByIter->second.num; ++i)
		{
			buildOrder.push_back(minReqByIter->first);
		}
		// now go through and erase the picked item from required lists
		for (allReqsIter=allReqs.begin(); allReqsIter != allReqs.end(); ++allReqsIter)
		{
			set<UnitType>::iterator found = allReqsIter->second.requires.find(minReqByIter->first);
			if (found != allReqsIter->second.requires.end())
			{	// found it, erase
				allReqsIter->second.requires.erase(found);
			}
		}
		allReqs.erase(minReqByIter);
	} // now clean up buildOrder by removing extant units
	if (buildOrder.size() == 0) return buildOrder;
	list<UnitType>::iterator iBO = buildOrder.begin();
	while (iBO != buildOrder.end()) // remove probes
	{
		if (*iBO == BWAPI::UnitTypes::Protoss_Probe)
		{
			iBO = buildOrder.erase(iBO);
		} else {
			++iBO;
		}
	}
	printBuildOrder(buildOrder);
	logWriteBuffer();
	return buildOrder;
}

bool BuildGoalPlanner::buildingExistsOrPlanned( const UnitType& qUnit )
{
	// check with blackboard structure count info
	short numComplete = BB.ES.getUnitCount(qUnit)+BB.ES.getUnderConstruction(qUnit)+BB.ES.getUnitsOrdered(qUnit);
	if (numComplete > 0) return true;
	else return false;
}

void BuildGoalPlanner::printBuildOrder( const list<UnitType>& BO )
{
	list<UnitType>::const_iterator ci = BO.begin();
	for (unsigned int i = 0; i < BO.size(); i++)
	{
		logfile << i<<" - "<<ci->getName()<<endl;
		++ci;
	}
}