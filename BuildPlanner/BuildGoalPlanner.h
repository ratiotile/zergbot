#pragma once
#ifndef INCLUDED_BUILDGOALPLANNER_H
#define INCLUDED_BUILDGOALPLANNER_H

#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#include <list>
#include "../common/Singleton.h"

struct BuildGoal;

class BuildGoalPlanner: public Singleton<BuildGoalPlanner>
{
public:
	/// generates an ordered list of unittypes to build
	std::list<BWAPI::UnitType> generateBuildOrder(const BuildGoal& goal);
private:
	bool buildingExistsOrPlanned(const BWAPI::UnitType& qUnit);
	void printBuildOrder(const std::list<BWAPI::UnitType>& BO);
};

#endif