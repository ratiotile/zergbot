#include "BuildOrder.h"
#include <sstream>
#include "../common/cpp_includes.h"
#include "earlyBuildItem.h"

using namespace BWAPI;

namespace BuildOrders
{
	const BuildOrder None( "None" );
}

void BuildOrder::print() const
{
	Broodwar->printf("BuildOrder %s: ", name_.c_str());
	std::ostringstream oss;
	unsigned int n = buildList_.size();
	for (unsigned int i = 0; i < n; i++)
	{
		oss << (int)buildList_[i].supply << "/"
			<< (int)buildList_[i].supplyCap
			<< " - " << buildList_[i].build.getName();
		if (i + 1 < n) oss << ", ";
	}
	Broodwar->printf("%s\n", oss.str().c_str());
}

BuildOrder::BuildOrder()
:utility_(0)
,progress_(0)
,name_("")
,wall(false)
{}

BuildOrder::BuildOrder( const std::string& name )
:name_(name)
,utility_(0)
,progress_(0)
,wall(false)
{}

std::string BuildOrder::toString() const
{	
	std::ostringstream oss;
	oss << "BuildOrder " << name_ << ":\n";
	unsigned int n = buildList_.size();
	for (unsigned int i = 0; i < n; i++)
	{
		oss << (int)buildList_[i].supply << " - " << buildList_[i].build.getName();
		if (i + 1 < n) oss << ", ";
	}
	oss << std::endl;
	return oss.str();
}

vector<BWAPI::UnitType> BuildOrder::getItems() const
{
	vector<BWAPI::UnitType> items(buildList_.size());
	for (unsigned int i = 0; i < buildList_.size(); i++)
		items[i] = buildList_[i].build;
	return items;
}

int BuildOrder::getSupply() const
{
	return buildList_[progress_].supply;
}

int BuildOrder::getSupplyCap() const
{
	return buildList_[progress_].supplyCap;
}

const BWAPI::UnitType BuildOrder::getBuildItem() const
{
	return buildList_[progress_].build;
}

earlyBuildItem BuildOrder::getCurrentItem() const
{
	return buildList_[progress_];
}

ResourceSet BuildOrder::getCurrentCost() const
{
	ResourceSet resourceCost;
	earlyBuildItem& current = getCurrentItem();
	if( current.build != BWAPI::UnitTypes::None )
	{
		resourceCost.supply = current.build.supplyRequired()*current.number;
		resourceCost.minerals = current.build.mineralPrice()*current.number;
		resourceCost.gas = current.build.gasPrice()*current.number;
		// check for unit requirements for morph, workers, factories
		resourceCost.units[current.build.whatBuilds().first]
			= current.build.whatBuilds().second*current.number;
	}
	else if( current.tech != TechTypes::None )
	{
		resourceCost.minerals = current.tech.mineralPrice();
		resourceCost.gas = current.tech.gasPrice();
	}
	else if( current.upgrade != UpgradeTypes::None )
	{
		resourceCost.minerals = current.upgrade.mineralPrice();
		resourceCost.gas = current.upgrade.gasPrice();
	}
	return resourceCost;
}

void BuildOrder::addItem( earlyBuildItem& item )
{
	buildList_.push_back(item);
}

void BuildOrder::printCurrentItem() const
{
	Broodwar->printf("Build item %i/%i: build %s\n", 
		(int)buildList_[progress_].supply,
		(int)buildList_[progress_].supplyCap,
		buildList_[progress_].build.getName().c_str() );
}

void BuildOrder::advance()
{
	progress_++;
	if (progress_ >= buildList_.size())
	{
		progress_ = buildList_.size();
	}
}

WorkerPolicyType::Enum BuildOrder::getWorkerPolicy() const
{
	if (getRemaining() > 0)
	{
		return getCurrentItem().workerPolicy;
	}
	else
	{
		/// return none, default
		return WorkerPolicyType::noBuild;
	}
}