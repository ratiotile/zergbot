// holds build order information for early game play

#ifndef INCLUDED_BUILDORDER_H
#define INCLUDED_BUILDORDER_H

#include <BWAPI.h>
#include <vector>
#include <string>
#include "earlyBuildItem.h"
#include "../common/ResourceSet.h"

class BuildOrder
{
private:
	int utility_ ; // how strong this build order is currently
	std::string name_; // name of buildOrder, used to identify
	int progress_; // which item are we on now?
	std::vector<earlyBuildItem> buildList_;
public: // accessors
	BuildOrder();
	/// new constructor
	BuildOrder(const std::string& name);
	/// add item to back of list
	void addItem(earlyBuildItem& item);
	// check remaining items before trying to get one
	int getSupply() const;
	int getSupplyCap() const; 
	const BWAPI::UnitType getBuildItem() const;
	earlyBuildItem getCurrentItem() const;
	ResourceSet getCurrentCost() const;
	inline const std::string name() const{return name_;}
	inline const int utility() const {return utility_;}
	inline const int getRemaining() const {return buildList_.size() - progress_;}
	inline const std::vector<earlyBuildItem> getBuildList() const{ return buildList_; }
	/// whether to build workers when able to or not
	WorkerPolicyType::Enum getWorkerPolicy() const;


	// mutators
	inline void setBuildList( std::vector<earlyBuildItem>& buildList ) 
		{ buildList_ = buildList; }
	inline void setName( const std::string& name ) { name_ = name; }
	inline int getProgress() const {return progress_;}
	inline void setProgress( const char progress ) { progress_ = progress; }
	// move on to next item in list
	void advance();
	// print to screen the contents of build order
	void print() const;
	void printCurrentItem() const;
	std::string toString() const;
	// get items as vector of BWAPI::UnitTypes::
	std::vector<BWAPI::UnitType> getItems() const;
	/// do wall=in
	bool wall;
};
inline bool operator < (const BuildOrder& lhs, const BuildOrder& rhs)
	{ return lhs.utility() < rhs.utility(); }

namespace BuildOrders
{
	extern const BuildOrder None;
}

#endif