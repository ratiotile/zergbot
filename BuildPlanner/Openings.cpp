#include "Openings.h"
#include "earlyBuildItem.h"
#include "BuildOrder.h"
#include <vector>
#include "../common/cpp_includes.h"

using namespace BWAPI;

Openings::Openings()
{
	isPopulated_ = false;
	populateBuildOrders();
}
void Openings::populateBuildOrders()
{
	if (isPopulated_) return;
	// hardcoding the build orders for now
	//addPvPOpenings();
	//addTerranOpenings();
	//newBuild.print();
	addZergOpenings();
	isPopulated_ = true;
}

const BuildOrder& Openings::getBuildOrder()
{
	//return terran_["Basic"];
	return getBuildOrder("4Pool");
}

const BuildOrder& Openings::getBuildOrder( const std::string& boName )
{
	std::map<std::string,BuildOrder>::iterator found = ZvP_.find(boName);
	if (found == ZvP_.end())
	{
		Broodwar->printf("Error: build order not found!");
		logfile << "Error: build order not found!\n";
		return BuildOrders::None;
	}
	else return found->second;
}

void Openings::addPvPOpenings()
{
	BuildOrder newBuild;
	earlyBuildItem newItem;
	vector<earlyBuildItem> newList;
	// 1 Gate Core
	newBuild.setName("1GateCore");
	newItem.supply = 8;	
	newItem.build = BWAPI::UnitTypes::Protoss_Pylon;
	newList.push_back(newItem); // 8 - Pylon
	newItem.supply = 10;	
	newItem.build = BWAPI::UnitTypes::Protoss_Gateway;
	newList.push_back(newItem); // 10 - Gate
	newItem.supply = 12;	
	newItem.build = BWAPI::UnitTypes::Protoss_Assimilator;
	newList.push_back(newItem); // 12 - Assimilator
	newItem.supply = 13;	
	newItem.build = BWAPI::UnitTypes::Protoss_Cybernetics_Core;
	newList.push_back(newItem); // 13 - Cybernetics Core
	newBuild.setBuildList(newList);
	PvP_.insert(pair<string,BuildOrder>(newBuild.name(),newBuild));
	// 2 Gate Zealot 10/12
	newList.clear();
	newBuild.setName("2GateZealot10_12");
	newItem.supply = 8;	
	newItem.build = BWAPI::UnitTypes::Protoss_Pylon;
	newList.push_back(newItem); // 8 - Pylon
	newItem.supply = 10;	
	newItem.build = BWAPI::UnitTypes::Protoss_Gateway;
	newList.push_back(newItem); // 10 - gate
	newItem.supply = 12;	
	newItem.build = BWAPI::UnitTypes::Protoss_Gateway;
	newList.push_back(newItem); // 12 - gate
	newItem.supply = 13;	
	newItem.build = BWAPI::UnitTypes::Protoss_Zealot;
	newList.push_back(newItem); // 13 - Zeal
	newItem.supply = 15;	
	newItem.build = BWAPI::UnitTypes::Protoss_Pylon;
	newList.push_back(newItem); // 15 - Pylon
	newBuild.setBuildList(newList);
	PvP_.insert(pair<string,BuildOrder>(newBuild.name(),newBuild));
	//
	//newList.clear();
	//newBuild.setName("2GateZealot10_12");
}

void Openings::addTerranOpenings()
{
	// TvP Vulture Pressure or 2Facto Vult/Mines
	string name = "2FactVultMines";
	BuildOrder vultPress(name);
	vultPress.addItem(earlyBuildItem(9,10,BWAPI::UnitTypes::Terran_Supply_Depot));
	vultPress.addItem(earlyBuildItem(11,18, BWAPI::UnitTypes::Terran_Barracks));
	vultPress.addItem(earlyBuildItem(11,18,BWAPI::UnitTypes::Terran_Refinery));
	vultPress.addItem(earlyBuildItem(16,18,BWAPI::UnitTypes::Terran_Factory));
	vultPress.addItem(earlyBuildItem(16,18,BWAPI::UnitTypes::Terran_Machine_Shop));
	vultPress.addItem(earlyBuildItem(16,18,UpgradeTypes::Ion_Thrusters));
	vultPress.addItem(earlyBuildItem(16,18,BWAPI::UnitTypes::Terran_Supply_Depot));
	vultPress.addItem(earlyBuildItem(18,26,BWAPI::UnitTypes::Terran_Factory));
	vultPress.addItem(earlyBuildItem(18,26,BWAPI::UnitTypes::Terran_Machine_Shop));
	vultPress.addItem(earlyBuildItem(18,26,TechTypes::Spider_Mines));
	vultPress.addItem(earlyBuildItem(22,26,BWAPI::UnitTypes::Terran_Supply_Depot));
	vultPress.addItem(earlyBuildItem(22,26,BWAPI::UnitTypes::Terran_Vulture,-1));
	vultPress.addItem(earlyBuildItem(28,34,BWAPI::UnitTypes::Terran_Supply_Depot));
	terran_.insert(make_pair(name,vultPress));
	// TvP Standard Seige Expand
	name = "SeigeExpand";
	BuildOrder SE(name);
	SE.addItem(earlyBuildItem(9,10,BWAPI::UnitTypes::Terran_Supply_Depot));
	SE.addItem(earlyBuildItem(12,18,BWAPI::UnitTypes::Terran_Barracks));
	SE.addItem(earlyBuildItem(12,18,BWAPI::UnitTypes::Terran_Refinery));
	SE.addItem(earlyBuildItem(15,18,BWAPI::UnitTypes::Terran_Supply_Depot));
	SE.addItem(earlyBuildItem(16,26,BWAPI::UnitTypes::Terran_Factory));
	SE.addItem(earlyBuildItem(16,26,BWAPI::UnitTypes::Terran_Machine_Shop));
	SE.addItem(earlyBuildItem(16,26,BWAPI::UnitTypes::Terran_Siege_Tank_Tank_Mode));
	SE.addItem(earlyBuildItem(21,26,BWAPI::UnitTypes::Terran_Command_Center));
	SE.addItem(earlyBuildItem(24,26,BWAPI::UnitTypes::Terran_Supply_Depot));
	SE.addItem(earlyBuildItem(25,34,TechTypes::Tank_Siege_Mode));
	SE.addItem(earlyBuildItem(28,34,BWAPI::UnitTypes::Terran_Engineering_Bay));
	terran_.insert(make_pair(name,SE));
	// TvP 1 Rax Fast Expand
	name = "1RaxFE";
	BuildOrder RFE(name);
	RFE.addItem(earlyBuildItem(9,10,BWAPI::UnitTypes::Terran_Supply_Depot));
	RFE.addItem(earlyBuildItem(11,18,BWAPI::UnitTypes::Terran_Barracks));
	RFE.addItem(earlyBuildItem(15,18,BWAPI::UnitTypes::Terran_Command_Center));
	RFE.addItem(earlyBuildItem(16,18,BWAPI::UnitTypes::Terran_Supply_Depot));
	RFE.addItem(earlyBuildItem(16,18,BWAPI::UnitTypes::Terran_Refinery));
	RFE.addItem(earlyBuildItem(16,18,BWAPI::UnitTypes::Terran_Marine,2));
	RFE.addItem(earlyBuildItem(18,24,BWAPI::UnitTypes::Terran_Bunker));
	RFE.addItem(earlyBuildItem(21,24,BWAPI::UnitTypes::Terran_Factory));
	RFE.addItem(earlyBuildItem(22,24,BWAPI::UnitTypes::Terran_Machine_Shop));
	RFE.addItem(earlyBuildItem(22,24,BWAPI::UnitTypes::Terran_Supply_Depot));
	RFE.addItem(earlyBuildItem(25,32,BWAPI::UnitTypes::Terran_Factory));
	RFE.addItem(earlyBuildItem(26,32,BWAPI::UnitTypes::Terran_Machine_Shop));
	RFE.addItem(earlyBuildItem(28,32,BWAPI::UnitTypes::Terran_Supply_Depot));
	RFE.addItem(earlyBuildItem(30,32,BWAPI::UnitTypes::Terran_Refinery));
	RFE.addItem(earlyBuildItem(31,40,BWAPI::UnitTypes::Terran_Siege_Tank_Tank_Mode));
	RFE.addItem(earlyBuildItem(37,40,BWAPI::UnitTypes::Terran_Supply_Depot));
	RFE.addItem(earlyBuildItem(44,48,BWAPI::UnitTypes::Terran_Engineering_Bay));
	terran_.insert(make_pair(name,RFE));
	// TvP Flash build aka Double Armory
	name = "DoubleArmory";
	BuildOrder DA(name);
	DA.wall = true;
	DA.addItem(earlyBuildItem(9,10,BWAPI::UnitTypes::Terran_Supply_Depot));
	DA.addItem(earlyBuildItem(11,18,BWAPI::UnitTypes::Terran_Barracks));
	DA.addItem(earlyBuildItem(11,18,BWAPI::UnitTypes::Terran_Refinery));
	DA.addItem(earlyBuildItem(15,18,BWAPI::UnitTypes::Terran_Factory));
	DA.addItem(earlyBuildItem(15,18,BWAPI::UnitTypes::Terran_Supply_Depot));
	DA.addItem(earlyBuildItem(16,18,BWAPI::UnitTypes::Terran_Marine));
	DA.addItem(earlyBuildItem(16,26,BWAPI::UnitTypes::Terran_Machine_Shop));
	DA.addItem(earlyBuildItem(16,26,BWAPI::UnitTypes::Terran_Siege_Tank_Tank_Mode,-1));
	DA.addItem(earlyBuildItem(21,26,BWAPI::UnitTypes::Terran_Command_Center));
	DA.addItem(earlyBuildItem(24,26,BWAPI::UnitTypes::Terran_Supply_Depot));
	DA.addItem(earlyBuildItem(25,34,TechTypes::Tank_Siege_Mode));
	DA.addItem(earlyBuildItem(29,34,BWAPI::UnitTypes::Terran_Supply_Depot));
	DA.addItem(earlyBuildItem(33,34,BWAPI::UnitTypes::Terran_Armory));
	DA.addItem(earlyBuildItem(33,34,BWAPI::UnitTypes::Terran_Engineering_Bay));
	DA.addItem(earlyBuildItem(38,42,BWAPI::UnitTypes::Terran_Supply_Depot));
	DA.addItem(earlyBuildItem(38,42,BWAPI::UnitTypes::Terran_Refinery));
	DA.addItem(earlyBuildItem(38,42,UpgradeTypes::Terran_Vehicle_Weapons));
	DA.addItem(earlyBuildItem(38,42,UpgradeTypes::Charon_Boosters));
	DA.addItem(earlyBuildItem(38,42,BWAPI::UnitTypes::Terran_Goliath));
	DA.addItem(earlyBuildItem(46,50,BWAPI::UnitTypes::Terran_Supply_Depot));
	DA.addItem(earlyBuildItem(49,50,BWAPI::UnitTypes::Terran_Supply_Depot));
	DA.addItem(earlyBuildItem(49,50,BWAPI::UnitTypes::Terran_Factory));
	DA.addItem(earlyBuildItem(50,58,BWAPI::UnitTypes::Terran_Command_Center));
	DA.addItem(earlyBuildItem(50,58,BWAPI::UnitTypes::Terran_Starport));
	terran_.insert(make_pair(name,DA));
	// simple basic start
	name = "Basic";
	BuildOrder Basic(name);
	Basic.wall = false;
	Basic.addItem(earlyBuildItem(9,10,BWAPI::UnitTypes::Terran_Supply_Depot));
	Basic.addItem(earlyBuildItem(11,18,BWAPI::UnitTypes::Terran_Barracks));
	terran_.insert(make_pair(name,Basic));
	// BBS general TODO: task should proxy the rax
	name = "BBS";
	BuildOrder BBS(name);
	BBS.wall = false;
	BBS.addItem(earlyBuildItem(7,10,BWAPI::UnitTypes::Terran_Barracks));
	BBS.addItem(earlyBuildItem(8,10,BWAPI::UnitTypes::Terran_Barracks));
	BBS.addItem(earlyBuildItem(9,10,BWAPI::UnitTypes::Terran_Supply_Depot));
	terran_.insert(make_pair(name,BBS));
}

void Openings::addZergOpenings()
{
	BuildOrder TwoHatchMuta("2HatchMuta");
	TwoHatchMuta.addItem(earlyBuildItem(9,9,BWAPI::UnitTypes::Zerg_Overlord));
	earlyBuildItem* buildItem = new earlyBuildItem(12,12,BWAPI::UnitTypes::Zerg_Hatchery);
	buildItem->action = BuildOrderActionType::expandGas;
	TwoHatchMuta.addItem(*buildItem);
	TwoHatchMuta.addItem(earlyBuildItem(11,11,BWAPI::UnitTypes::Zerg_Spawning_Pool));
	TwoHatchMuta.addItem(earlyBuildItem(12,12,BWAPI::UnitTypes::Zerg_Extractor));
	delete buildItem;
	buildItem = new earlyBuildItem(12,17,BWAPI::UnitTypes::Zerg_Zergling);
	buildItem->action = BuildOrderActionType::saveLarva;
	buildItem->workerPolicy = WorkerPolicyType::speed;
	buildItem->extraConditionTarget = BWAPI::UnitTypes::Zerg_Spire;
	buildItem->extraConditionValue = 500;
	buildItem->number = 3;	// makes 6
	TwoHatchMuta.addItem(*buildItem);
	delete buildItem;
	buildItem = new earlyBuildItem(12,16,BWAPI::UnitTypes::Zerg_Lair);
	buildItem->workerPolicy = WorkerPolicyType::greedy;
	TwoHatchMuta.addItem(*buildItem);
	TwoHatchMuta.addItem(earlyBuildItem(15,16,BWAPI::UnitTypes::Zerg_Overlord));
	TwoHatchMuta.addItem(earlyBuildItem(16,16,UpgradeTypes::Metabolic_Boost));
	TwoHatchMuta.addItem(earlyBuildItem(14,16,BWAPI::UnitTypes::Zerg_Spire));
//	delete buildItem;
// 	buildItem = new earlyBuildItem(26,26,BWAPI::UnitTypes::Zerg_Hatchery);
// 	buildItem->action = BuildOrderActionType::expandGas;
// 	TwoHatchMuta.addItem(*buildItem);
	TwoHatchMuta.addItem(earlyBuildItem(25,25,BWAPI::UnitTypes::Zerg_Extractor));
	TwoHatchMuta.addItem(earlyBuildItem(26,26,BWAPI::UnitTypes::Zerg_Overlord,2));
	delete buildItem;
	buildItem = new earlyBuildItem(0,0,BWAPI::UnitTypes::Zerg_Mutalisk,6);
	buildItem->action = BuildOrderActionType::saveLarva;
	buildItem->workerPolicy = WorkerPolicyType::noBuild;
	buildItem->extraCondition = BuildOrderConditionType::buildingProgressHP;
	buildItem->extraConditionTarget = BWAPI::UnitTypes::Zerg_Spire;
	buildItem->extraConditionValue = 350;
	TwoHatchMuta.addItem(*buildItem);
// 	TwoHatchMuta.addItem(earlyBuildItem(0,0,BWAPI::UnitTypes::Zerg_Overlord));
// 	TwoHatchMuta.addItem(earlyBuildItem(0,0,BWAPI::UnitTypes::Zerg_Hatchery));
	delete buildItem;
	ZvP_.insert(make_pair("2HatchMuta",TwoHatchMuta));

	BuildOrder TwelveHatch("12Hatch");
	TwelveHatch.addItem(earlyBuildItem(9,9,BWAPI::UnitTypes::Zerg_Overlord));
	buildItem = new earlyBuildItem(12,17,BWAPI::UnitTypes::Zerg_Hatchery);
	buildItem->action = BuildOrderActionType::expandGas;
	TwelveHatch.addItem(*buildItem);
	delete buildItem;
	TwelveHatch.addItem(earlyBuildItem(11,17,BWAPI::UnitTypes::Zerg_Spawning_Pool));
	ZvP_.insert(make_pair(TwelveHatch.name(),TwelveHatch));

	BuildOrder Overpool("Overpool");
	Overpool.addItem(earlyBuildItem(9,9,BWAPI::UnitTypes::Zerg_Overlord));
	Overpool.addItem(earlyBuildItem(9,9,BWAPI::UnitTypes::Zerg_Spawning_Pool));
	ZvP_.insert(make_pair(Overpool.name(),Overpool));

	BuildOrder FastGas("11Gas10Pool");
	FastGas.addItem(earlyBuildItem(9,9,BWAPI::UnitTypes::Zerg_Overlord));
	FastGas.addItem(earlyBuildItem(11,17,BWAPI::UnitTypes::Zerg_Extractor));
	FastGas.addItem(earlyBuildItem(10,17,BWAPI::UnitTypes::Zerg_Spawning_Pool));
	ZvP_.insert(make_pair(FastGas.name(),FastGas));

	// 4-pool rush opening
	BuildOrder FourPool("4Pool");
	buildItem = new earlyBuildItem(4,9,BWAPI::UnitTypes::Zerg_Spawning_Pool);
	buildItem->workerPolicy = WorkerPolicyType::noBuild;
	FourPool.addItem(*buildItem);
	delete buildItem;
	buildItem = new earlyBuildItem(3,9,BWAPI::UnitTypes::Zerg_Drone);
	buildItem->number = 1;
	buildItem->workerPolicy = WorkerPolicyType::noBuild;
	FourPool.addItem(*buildItem);
	delete buildItem;
	buildItem = new earlyBuildItem(4,9,BWAPI::UnitTypes::Zerg_Zergling);
	buildItem->workerPolicy = WorkerPolicyType::noBuild;
	buildItem->action = BuildOrderActionType::saveLarva;
	buildItem->number = 3;
	FourPool.addItem(*buildItem);
	delete buildItem;
// 	buildItem = new earlyBuildItem(7,9,BWAPI::UnitTypes::Zerg_Zergling);
// 	buildItem->workerPolicy = WorkerPolicyType::speed;
// 	FourPool.addItem(*buildItem);
// 	delete buildItem;
// 	buildItem = new earlyBuildItem(8,9,BWAPI::UnitTypes::Zerg_Zergling);
// 	buildItem->workerPolicy = WorkerPolicyType::speed;
// 	FourPool.addItem(*buildItem);
// 	delete buildItem;
	ZvP_.insert(make_pair(FourPool.name(),FourPool));

	BuildOrder FivePool("5Pool");
	buildItem = new earlyBuildItem(4,9,BWAPI::UnitTypes::Zerg_Spawning_Pool);
	buildItem->workerPolicy = WorkerPolicyType::speed;
	FivePool.addItem(*buildItem);
	delete buildItem;
	buildItem = new earlyBuildItem(3,9,BWAPI::UnitTypes::Zerg_Drone);
	buildItem->workerPolicy = WorkerPolicyType::speed;
	FivePool.addItem(*buildItem);
	delete buildItem;
	buildItem = new earlyBuildItem(4,9,BWAPI::UnitTypes::Zerg_Zergling);
	buildItem->workerPolicy = WorkerPolicyType::speed;
	buildItem->action = BuildOrderActionType::saveLarva;
	buildItem->number = 3;
	FivePool.addItem(*buildItem);
	delete buildItem;
	ZvP_.insert(make_pair(FivePool.name(),FivePool));

	BuildOrder SevenPool("7Pool");
	buildItem = new earlyBuildItem(7,9,BWAPI::UnitTypes::Zerg_Spawning_Pool);
	buildItem->workerPolicy = WorkerPolicyType::greedy;
	SevenPool.addItem(*buildItem);
	delete buildItem;
	buildItem = new earlyBuildItem(7,9,BWAPI::UnitTypes::Zerg_Overlord);
	buildItem->workerPolicy = WorkerPolicyType::greedy;
	SevenPool.addItem(*buildItem);
	delete buildItem;
	buildItem = new earlyBuildItem(8,17,BWAPI::UnitTypes::Zerg_Zergling);
	buildItem->workerPolicy = WorkerPolicyType::greedy;
	buildItem->action = BuildOrderActionType::saveLarva;
	buildItem->number = 3;
	SevenPool.addItem(*buildItem);
	delete buildItem;
	ZvP_.insert(make_pair(SevenPool.name(),SevenPool));
}