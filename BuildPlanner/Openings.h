// holds BuildOrders for "by the book" opening play
#pragma once
#ifndef INCLUDED_OPENINGS_H
#define INCLUDED_OPENINGS_H
#include <map>
#include "../common/Singleton.h"

class BuildOrder;

class Openings: public Singleton<Openings>
{
public:
	Openings();
	// write data to buildorders, call this before trying to get a buildOrder!
	void populateBuildOrders();
	// get best build order for situation.
	const BuildOrder& getBuildOrder();
	/// get specific named build order
	const BuildOrder& getBuildOrder(const std::string& boName);
	inline bool isPopulated() const {return isPopulated_;}
protected:
	
private:
	// have the vectors been filled with buildOrders? 1 = yes
	bool isPopulated_;
	std::map<std::string,BuildOrder> PvP_;
	std::map<std::string,BuildOrder> PvT_;
	std::map<std::string,BuildOrder> PvZ_;
	/// all terran build orders
	std::map<std::string,BuildOrder> terran_;
	// zerg build orders
	std::map<std::string,BuildOrder> ZvP_;
	std::map<std::string,BuildOrder> ZvT_;
	std::map<std::string,BuildOrder> ZvZ_;
	void addTerranOpenings();
	void addPvPOpenings();
	void add2GateZealot10_12();
	void addZergOpenings();
};

#endif