#include "earlyBuildItem.h"
#include "../common/cpp_includes.h"
#include <sstream>

using namespace BWAPI;

std::string earlyBuildItem::toString() const
{
	ostringstream oss;
	if (supply > 0)
	{
		oss << (int)supply << " ";
	}
	if (extraCondition == BuildOrderConditionType::buildingProgressHP)
	{
		oss << "*HP:" << extraConditionValue << " ";
	}
	if (build != BWAPI::UnitTypes::None)
	{
		oss << build.getName() << " ";
		if (number > 1)
		{
			oss << "x" << number << " ";
		}
	}
	if (tech != TechTypes::None)
	{
		oss << tech.getName() << " ";
	}
	if (upgrade != UpgradeTypes::None)
	{
		oss << upgrade.getName() << " ";
	}
	if (action != BuildOrderActionType::build)
	{
		oss << "+";
		if (action == BuildOrderActionType::expand) oss << "expo";
		else if (action == BuildOrderActionType::expandGas) oss << "expoG";
		else if (action == BuildOrderActionType::saveLarva) oss << "saveLarva";
		else oss << "unknown";
		//oss << " ";
	}
	return oss.str();
}

ResourceSet earlyBuildItem::getCost() const
{
	ResourceSet cost;
	if (isBuild())
	{
		cost.supply = build.supplyRequired() * number;
		cost.minerals = build.mineralPrice() * number;
		cost.gas = build.gasPrice() * number;
		cost.units[build.whatBuilds().first] = build.whatBuilds().second * number;
	}
	else if (isTech())
	{
		cost.minerals = tech.mineralPrice();
		cost.gas = tech.gasPrice();
	}
	else if(isUpgrade())
	{
		cost.minerals = upgrade.mineralPrice();
		cost.gas = upgrade.gasPrice();
	}
	return cost;
}