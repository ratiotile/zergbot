#pragma once
#include <BWAPI.h>
#include "../common/ResourceSet.h"

namespace WorkerPolicyType{
	enum Enum{
		greedy, // build workers whenever possible, until we have enough money for buildItem.
				// or condition to build reached. Default.
		speed, // save for buildItem first, build workers while waiting on condition, but only
				// if we have extra resources in excess of what is saved.
		noBuild // build no workers unless specifically called for
	};
}
namespace BuildOrderActionType{
	enum Enum{
		build, // default, used for buildings and units, research, and upgrades
		expand, // take an expo
		expandGas, // take a gas expo
		saveLarva // stop making units as Zerg until next action.
	};
}
namespace BuildOrderConditionType{
	enum Enum{
		none,	// no extra conditions
		buildingProgressHP, // when specified structure is at so many HP
	};
}
/// for opening build order only, tracked by supply.
struct earlyBuildItem{
	earlyBuildItem()
		:supply(0), supplyCap(0), build(BWAPI::UnitTypes::None), number(1)
		,tech(BWAPI::TechTypes::None), upgrade(BWAPI::UpgradeTypes::None)
		,extraCondition(BuildOrderConditionType::none)
		,extraConditionValue(0)
		,extraConditionTarget(BWAPI::UnitTypes::None)
		,action(BuildOrderActionType::build)
		,workerPolicy(WorkerPolicyType::greedy)
	{}
	earlyBuildItem(BWAPI::UnitType item, int num = 1 )
		:supply(0), supplyCap(0), build(item), number(1)
		,tech(BWAPI::TechTypes::None), upgrade(BWAPI::UpgradeTypes::None)
		,extraCondition(BuildOrderConditionType::none)
		,extraConditionValue(0)
		,extraConditionTarget(BWAPI::UnitTypes::None)
		,action(BuildOrderActionType::build)
		,workerPolicy(WorkerPolicyType::greedy)
	{}
	earlyBuildItem(int s, int cap, BWAPI::UnitType item, int num = 1)
		:supply(s), supplyCap(cap), build(item), number(num)
		,tech(BWAPI::TechTypes::None), upgrade(BWAPI::UpgradeTypes::None)
		,extraCondition(BuildOrderConditionType::none)
		,extraConditionValue(0)
		,extraConditionTarget(BWAPI::UnitTypes::None)
		,action(BuildOrderActionType::build)
		,workerPolicy(WorkerPolicyType::greedy)
	{}
	earlyBuildItem(int s, int cap, BWAPI::TechType item, int num = 1)
		:supply(s), supplyCap(cap), build(BWAPI::UnitTypes::None), number(num)
		,tech(item), upgrade(BWAPI::UpgradeTypes::None)
		,extraCondition(BuildOrderConditionType::none)
		,extraConditionValue(0)
		,extraConditionTarget(BWAPI::UnitTypes::None)
		,action(BuildOrderActionType::build)
		,workerPolicy(WorkerPolicyType::greedy)
	{}
	earlyBuildItem(int s, int cap, BWAPI::UpgradeType item, int num = 1)
		:supply(s), supplyCap(cap), build(BWAPI::UnitTypes::None), number(num)
		,tech(BWAPI::TechTypes::None), upgrade(item)
		,extraCondition(BuildOrderConditionType::none)
		,extraConditionValue(0)
		,extraConditionTarget(BWAPI::UnitTypes::None)
		,action(BuildOrderActionType::build)
		,workerPolicy(WorkerPolicyType::greedy)
	{}
	/// number of supply to have consumed before building this item
	int supply; 
	/// target amount of total supply available TODO:not used yet
	int supplyCap;
	/// the item to be built when supply_ reached
	BWAPI::UnitType build; 
	BWAPI::TechType tech;
	BWAPI::UpgradeType upgrade;
	/// only applies to units, -1 = pump, 0 = stop pump
	int number;
	/// extra condition to start build
	BuildOrderConditionType::Enum extraCondition;
	int extraConditionValue;
	/// structure to trigger extraCondition off of.
	BWAPI::UnitType extraConditionTarget;
	/// how should workers be built while waiting for this item?
	WorkerPolicyType::Enum workerPolicy;
	/// additional actions to take on reaching conditions
	BuildOrderActionType::Enum action;
	/// returns buildItem in string representation
	std::string toString() const;
	bool isBuild() const {return build != BWAPI::UnitTypes::None;}
	bool isTech() const { return tech != BWAPI::TechTypes::None;}
	bool isUpgrade() const {return upgrade != BWAPI::UpgradeTypes::None;}
	ResourceSet getCost() const;
};

