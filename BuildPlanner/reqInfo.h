#include "BWAPI.h"
#include <set>

// ReqInfo is used as a temporary data storage device in BuildGoalPlanner
struct ReqInfo
{
	unsigned short num;
	set<BWAPI::UnitType> requires;	// immediate prereqs this unit needs 
	set<BWAPI::UnitType> requiredBy;	// other units that have this unit as prereq.
};