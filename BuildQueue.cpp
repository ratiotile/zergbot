#include "BuildQueue.h"
#include "common/cpp_includes.h"
#include "Tasks/Task.h"
#include "Blackboard.h"
#include "Tasks/T_BuildUnit.h"
#include "Tasks/T_TakeExpo.h"
#include "Tasks/T_BuildStructure.h"
#include "Budget.h"
#include "common/ResourceSet.h"
#include <sstream>
#include "MapInfo.h"
#include "HarvestManager.h"

using namespace BWAPI::UnitTypes;
using BWAPI::TechType;

BuildQueue::BuildQueue()
:Manager("BuildQueue")
, autoBuildWorkers(true)
, autoBuildSupply(true)
{
	mResourcesRequired.clear();
}

void BuildQueue::pushBack( const BWAPI::UnitType& buildType )
{
	pushBack(earlyBuildItem(buildType));
}

void BuildQueue::pushBack( const BuildOrder buildOrder )
{
	BOOST_FOREACH(earlyBuildItem buildItem, buildOrder.getBuildList() )
	{
		pushBack(buildItem);
	}
}

void BuildQueue::pushFront( const BWAPI::UnitType& buildType )
{
	pushFront(earlyBuildItem(buildType));
}



void BuildQueue::processTasks()
{
	list<pTask>::iterator it = mTasks.begin();
	for (it; it != mTasks.end(); )
	{
		pTask task = *it;
		if (task->isComplete() )
		{
			logfile << Broodwar->getFrameCount() << " Task is complete: "
				<< task->toString() << endl;
			task->terminate();
			it = mTasks.erase(it);
		}
		else if (task->isFailed())
		{
			// TODO: handle failed items here, perhaps re-add to front of list?
			// use events to handle instead, downcast not certain to be BuildUnit
			task->terminate();
			it = mTasks.erase(it);
		}
		else
		{
			task->update();
			++it;
		}
	}
}

void BuildQueue::update()
{
	processTasks();
	// check conditions and add top item task
	if (!mBuildList.empty() && frontItemConditionsMet())
	{
		Broodwar->printf("Starting Build: %s",mBuildList.front().toString().c_str());
		if (mBuildList.front().isBuild())
		{
			initiateBuildTask(); // account for fail
		}
		// TODO: upgrades, tech
		mBuildList.pop_front();
	}
	else
	{
		// build workers/overlords if necessary
		if (autoBuildWorkers)
		{
			buildWorker();
		}
		buildSupply();
		
		
	}
}

bool BuildQueue::frontItemConditionsMet() const
{
	if (mBuildList.empty()) return false;
	if ( mBuildList.front().supply > Broodwar->self()->supplyUsed()/2 ||
		mBuildList.front().supplyCap > Broodwar->self()->supplyTotal()/2 )
	{
		return false;
	}
	// check extra conditions
	if (mBuildList.front().extraCondition == BuildOrderConditionType::buildingProgressHP)
	{
		UnitType targetType = mBuildList.front().extraConditionTarget;
		int triggerHP = mBuildList.front().extraConditionValue;
		// look in UnitManager or Use Broodwar.getUnits?
		const set< BWAPI::Unit*>& allUnits = Broodwar->self()->getUnits();
		BOOST_FOREACH(  BWAPI::Unit* unit, allUnits )
		{
			if (unit->getType() == targetType && unit->getHitPoints() > triggerHP)
			{
				return true;
			}
		}
		// didn't find it, return false
		return false;
	}
	// no extra conditions
	return true;
}

int BuildQueue::itemTechRequirementWait( const earlyBuildItem buildItem ) const
{
	int longestWait = 0;
	if (buildItem.isBuild())
	{
		// test for required units
		map<UnitType,int> requiredUnits = buildItem.build.requiredUnits();
		for (map<UnitType,int>::iterator iter = requiredUnits.begin();
			iter != requiredUnits.end(); ++iter)
		{
			UnitType reqType = (*iter).first;
			int reqNum = (*iter).second;
			int unfinished = max( reqNum - BBS::BB.ES.getUnitCount(reqType), 0 );
			if (unfinished > 0)
			{
				int unstarted = max( unfinished - BBS::BB.ES.getUnderConstruction(reqType), 0);
				if (unstarted > 0)
				{
					int unplanned = max( unstarted - BBS::BB.ES.getUnitsOrdered(reqType), 0);
					if (unplanned > 0) return -1;
					else
					{
						if (longestWait < reqType.buildTime()) longestWait = reqType.buildTime();
					}
				}
				else // all required units at least have been started for #unstarted under construction
				{// find how long till complete
					list< BWAPI::Unit*> unfinishedUnits = BWAPIext::getUnfinishedUnits(reqType);
					assert(unfinished > 0 && unfinishedUnits.size() >= (unsigned int)unfinished);
					list< BWAPI::Unit*>::iterator iter = unfinishedUnits.begin();
					for (int i = 0; i < unfinished; ++i)
					{
						++iter;
					}
					if( longestWait < (*iter)->getRemainingBuildTime() ) 
						longestWait = (*iter)->getRemainingBuildTime();
				}
				
			} // all required of this type are finished
		}
		// test for required tech
		TechType requiredTech = buildItem.build.requiredTech();
		if (requiredTech != BWAPI::TechTypes::None)
		{
			if(!Broodwar->self()->hasResearched(requiredTech))
			{
				if (Broodwar->self()->isResearching(requiredTech))
				{
					UnitType researcher = requiredTech.whatResearches();
					set< BWAPI::Unit*> myunits = Broodwar->self()->getUnits();
					int researchTime = -1;
					BOOST_FOREACH( BWAPI::Unit* unit, myunits)
					{
						if (unit->getType() == researcher)
						{
							if (unit->isResearching())
							{
								researchTime = unit->getRemainingResearchTime();
								if( longestWait < researchTime ) 
									longestWait = researchTime;
							}
						}
					}
				}
				else return -1;
			}
		}
	}
	return longestWait;
}

bool BuildQueue::buildWorker()
{
	UnitType workerType = Broodwar->self()->getRace().getWorker();
	if (canBuildWorker() && !HarvestManager::get().areAllBasesSaturated())
	{
		if (mBuildList.empty())
		{
			mBuildList.push_back(workerType);
		}
		else if (mBuildList.front().workerPolicy == WorkerPolicyType::greedy)
		{
			// build a worker!
			logfile << Broodwar->getFrameCount() << " Can build worker, greedy policy!\n";
			mBuildList.push_front(workerType);
		}
		else if (mBuildList.front().workerPolicy == WorkerPolicyType::speed)
		{
			if (Budget::get().enoughResourcesFor(Broodwar->self()->getRace().getWorker()))
			{
				mBuildList.push_front(workerType);
			}
		}
		else // don't build workers
		{
			return false;
		}
	}
	else return false;
	return true;
}

bool BuildQueue::canBuildWorker() const
{
	if(!autoBuildWorkers) return false;
	// check to be sure we are able to make
	UnitType wkrType = Broodwar->self()->getRace().getWorker();
	if (!Broodwar->canMake(NULL,wkrType)) return false;
	if (Budget::get().mineralsAvailable() < 50 && Budget::get().supplyAvailable() < 2) return false;
	// check that there are no other build worker tasks waiting
	BOOST_FOREACH( pTask task, mTasks )
	{
		if (task->getName() == "BuildUnit" && task->status() == TaskStatus::inactive)
		{
			pTBU buildTask = boost::static_pointer_cast<T_BuildUnit>(task);
			if (buildTask->getBuildType().isWorker())
			{
				return false; // we still waiting on another buildWorker task to start
			}
		}
	}
	// also check no other workers in queue
	for each (earlyBuildItem item in mBuildList)
	{
		if (item.build.isWorker())
		{
			return false;
		}
	}
	// if cleared all conditions
	return true;
}

void BuildQueue::buildSupply()
{
	static int lastAdded = 0;
	int supplyThreshold = Broodwar->self()->supplyTotal()/10 - 1;
	if (supplyThreshold < 0) supplyThreshold = 0;
	if(Broodwar->self()->supplyTotal() > 350) supplyThreshold = 1;
	if(Broodwar->self()->supplyTotal() == 400) return;
	
	if(lastAdded > 0) lastAdded--;
	if (Broodwar->self()->getRace() == BWAPI::Races::Zerg && lastAdded <= 0)
	{
		int supplyTotal = BB.ES.supplyTotal;
		int supplyUsed = BB.ES.supplyUsed;
		int futureSupply = supplyTotal - supplyUsed;
		if ( futureSupply <= supplyThreshold && (mBuildList.empty()))
		{
			Broodwar->printf("%d planned supplyTotal, %d used, %d overlords, %d bw supplyTotal"
				,supplyUsed,Broodwar->self()->supplyUsed(),BBS::BB.ES.getUnitTotal(Zerg_Overlord),
				supplyTotal);
			logfile << Broodwar->getFrameCount() << "["<<Broodwar->self()->supplyUsed()<<
				"|"<< Broodwar->self()->supplyTotal()<<"] Build Overlord: " << supplyUsed << 
				" planned supply, " << futureSupply << " future supply " << 
				BB.ES.getUnitCountStr(Zerg_Overlord) << " PlannedSupply Provided: " << 
				BB.ES.getPlannedSupplyProvided() << endl;
			logWriteBuffer();
			mBuildList.push_front(Zerg_Overlord);
			lastAdded = 10;
		}
	}
	// TODO: terran and protoss
}

void BuildQueue::updateResources()
{
	ResourceSet previous = mResourcesRequired;
	mResourcesRequired.clear();
	BOOST_FOREACH(pTask task, mTasks)
	{
		if (task->status() == TaskStatus::inactive || task->status() == TaskStatus::active)
		{
			mResourcesRequired = mResourcesRequired + task->resourcesOutstanding();
		}
	}
	BOOST_FOREACH(earlyBuildItem item, mBuildList)
	{
		mResourcesRequired = mResourcesRequired + item.getCost();
	}
}

std::string BuildQueue::toString() const
{
	ostringstream oss;
	oss << "Build Queue";
	oss << " [R: "<< mResourcesRequired.minerals
		<<","<< mResourcesRequired.gas<<","<<mResourcesRequired.supply;
	return oss.str();
}

WorkerPolicyType::Enum BuildQueue::getWorkerPolicy() const
{
	if (mBuildList.empty()) return WorkerPolicyType::greedy;
	else return mBuildList.front().workerPolicy;
}
int BuildQueue::displayInfo( int x, int y, int s ) const
{
	Broodwar->drawTextScreen(x,y,"%s _Tasks_ S:%i",toString().c_str(),
		Broodwar->self()->supplyTotal() - Broodwar->self()->supplyUsed() - BB.ES.getPlannedSupplyUsed());
	y += s;
	// display build tasks
	BOOST_FOREACH(pTask task, mTasks)
	{
		y = task->displayTaskInfo(x,y,s);
	}
	string wp = "greedy";
	if (getWorkerPolicy() == WorkerPolicyType::speed) wp = "speed";
	else if (getWorkerPolicy() == WorkerPolicyType::noBuild) wp="noBuild";
	Broodwar->drawTextScreen(x,y,"Wkr Policy: %s",wp.c_str());
	y += s;
	Broodwar->drawTextScreen(x,y,"_Queued Items_");
	y += s;
	BOOST_FOREACH(earlyBuildItem item, mBuildList)
	{
		Broodwar->drawTextScreen(x,y,"%s",item.toString().c_str());
		y+=s;
	}
	return y;
}

bool BuildQueue::initiateBuildTask()
{
	// check extra actions
	baseLocInfo* targetExpo = NULL;
	assert(!mBuildList.empty());
	if (mBuildList.front().action == BuildOrderActionType::expandGas)
	{
		// find gas expo
		targetExpo = MapInfo::get().getNextExpo(true);
		if (targetExpo)
		{
			pTTE takeExpo( new T_TakeExpo(targetExpo) );
			mTasks.push_back(takeExpo);
		}
		else
		{
			logfile << "BQ initiate failed, no gas expo location found!\n";
			return false;
		}
	}
	else if (mBuildList.front().action == BuildOrderActionType::expand)
	{
		// any expo
		targetExpo = MapInfo::get().getNextExpo();
		if (targetExpo)
		{
			pTTE takeExpo( new T_TakeExpo(targetExpo) );
			mTasks.push_back(takeExpo);
		}
		else
		{
			logfile << "BQ initiate failed, no expo location found!\n";
			return false;
		}
	}
	else
	{
		logfile << "BQ creating new BuildUnit task\n";
		for (int i = 0; i < mBuildList.front().number; i++)
		{
			if (mBuildList.front().build.isBuilding())
			{
				pTBS buildStruct( new T_BuildStructure(mBuildList.front().build) );
				mTasks.push_back(buildStruct);
			}
			else
			{
				pTBU buildUnit( new T_BuildUnit(mBuildList.front().build) );
				mTasks.push_back(buildUnit);
			}
		}
	}

	return true;
}
/// also count number inactive tasks
int BuildQueue::getNumQueued( UnitType ut ) const
{
	int count = 0;
	for each ( earlyBuildItem item in mBuildList )
	{
		if (item.build == ut)
		{
			count++;
		}
	}
	for each ( pTask task in mTasks )
	{
		if (task->getName() == "BuildUnit" && task->isInactive())
		{
			pTBU buildTask = boost::static_pointer_cast<T_BuildUnit>(task);
			if (buildTask->getBuildType() == ut)
			{
				++count;
			}
		}
	}
	return count;
}

int BuildQueue::getNumTasked( BWAPI::UnitType ut ) const
{
	int count = 0;
	for each ( pTask task in mTasks )
	{
		if (task->getName() == "BuildUnit" && task->isActive())
		{
			pTBU buildTask = boost::static_pointer_cast<T_BuildUnit>(task);
			if (buildTask->getBuildType() == ut)
			{
				++count;
			}
		}
	}
	return count;
}

int BuildQueue::getQueuedSupply() const
{
	int supply = 0;
	for each ( earlyBuildItem item in mBuildList )
	{
		if (item.number > 0)
		{
			supply += item.build.supplyRequired() * item.number;
		}
	}
	for each ( pTask task in mTasks )
	{
		if (task->getName() == "BuildUnit" && task->isInactive())
		{
			pTBU buildTask = boost::static_pointer_cast<T_BuildUnit>(task);
			if (buildTask && buildTask->getBuildType() != BWAPI::UnitTypes::None)
			{
				supply += buildTask->getBuildType().supplyRequired();
			}
		}
	}
	return supply;
}