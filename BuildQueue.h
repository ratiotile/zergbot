/************************************************************************/
/* Build Queue holds all the planned and currently under construction	*/
/* units. It is accessed through Blackboard.							*/
/************************************************************************/
#pragma once
#include <list>
#include <BWAPI.h>
#include "BuildPlanner/earlyBuildItem.h"
#include "Manager.h"
#include "BuildPlanner/BuildOrder.h"

class BuildQueue: public Manager
{
public:
	BuildQueue();

	const std::list<earlyBuildItem>& get() const { return mBuildList; }
	bool frontItemConditionsMet() const;
	/// number of frames to wait for tech requirement of item to complete, -1 is never
	int itemTechRequirementWait( const earlyBuildItem buildItem ) const;
	WorkerPolicyType::Enum getWorkerPolicy() const;
	int displayInfo( int x, int y, int s ) const;
	bool canBuildWorker() const;
	int getNumQueued( BWAPI::UnitType ut ) const;
	/// number already started task for
	int getNumTasked( BWAPI::UnitType ut ) const;
	/// supply in inactive and queued items
	int getQueuedSupply() const;

	void pushBack( const BWAPI::UnitType& buildType );
	void pushBack( const earlyBuildItem buildItem ) {mBuildList.push_back(buildItem);}
	void pushBack( const BuildOrder buildOrder );

	void pushFront( const BWAPI::UnitType& buildType );
	void pushFront( const earlyBuildItem buildItem ) {mBuildList.push_front(buildItem);}

	void processTasks();
	void update();

	bool autoBuildWorkers;
	bool autoBuildSupply;
private:
	std::list<earlyBuildItem> mBuildList;
	ResourceSet mResourcesRequired;
	// from Manager:
	//  list<pTask> mTasks;
	//  string mName
	/// true if worker task added this frame
	
	std::string toString() const;
	

	bool buildWorker();
	void buildSupply();
	void updateResources();
	bool initiateBuildTask();
};
