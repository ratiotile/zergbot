#include "ChokeInfo.h"
#include "common/cpp_includes.h"
#include "TABW/Chokepoint.h"


chokeInfo::chokeInfo(TABW::pChoke choke) : 
controller(NULL), 
frameLastVisible(0), 
priority(0),
isClear(true),
mCenter(choke->center()),
mWidth(choke->clearance()),
mSides(choke->sides()),
// has to be initialized later once regions finish
mRegions(pair<regionInfo*,regionInfo*>(NULL,NULL)),
mID(choke->ID())
{

}

void chokeInfo::setRegions( regionInfo* r1, regionInfo* r2 )
{
	mRegions.first = r1;
	mRegions.second = r2;
}