#pragma once
#include "common/typedefs.h"
#include <BWAPI.h>
#include "TABW/TABW.h"

class regionInfo;

class chokeInfo
{
public:
	chokeInfo( TABW::pChoke choke );
	// interface
	BWAPI::Position center() const {return mCenter;}
	int width() const {return mWidth;}
	std::pair<BWAPI::Position, BWAPI::Position> sides()const{return mSides;}
	std::pair<regionInfo*,regionInfo*> regions() {return mRegions;}
	int ID() const {return mID;}

	void setRegions( regionInfo* r1, regionInfo* r2 );

	// data members
	BWAPI::Player* controller;
	/// last frame visible
	unsigned int frameLastVisible;	
	/// how important it is to scout, based on time last scouted
	int priority;
	/// true if no enemy units near point
	bool isClear;
private: // for data that is constant
	BWAPI::Position mCenter;
	/// how wide in walk tiles
	int mWidth;
	int mID;
	std::pair<BWAPI::Position, BWAPI::Position> mSides;
	std::pair<regionInfo*,regionInfo*> mRegions;
};