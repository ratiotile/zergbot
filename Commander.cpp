#include "Commander.h"
#include "common/cpp_includes.h"
#include "Tasks/Task_PumpUnit.h"
#include "Tasks/T_BuildOrder.h"
#include "BuildPlanner/Openings.h"
#include "Tasks/T_TakeExpo.h"
#include "Budget.h"
#include "MapInfo.h"
#include "Tasks/T_BBS.h"
#include "UnitAgent.h"
#include "typedef_UnitAgent.h"
#include "UnitManager.h"
#include <algorithm>
#include "HarvestManager.h"
#include "WorkerAgent.h"
#include "common/ResourceSet.h"
#include "Tasks/T_BuildDefenses.h"


Commander::Commander()
:Manager("Commander")
,mExpoCount(0)
{

}

void Commander::onGameStart()
{
// 	mOutstandingTasks.push_front(pTask( new Task("Economy Start")));
 	logfile << "Commander onGameStart. Task added.\n";
	mTasks.push_back( pTask( new Task_PumpUnit(BWAPI::UnitTypes::Terran_SCV)) );
	//mTasks.push_back( pTask( new T_BuildOrder(Openings::get().getBuildOrder()) ));
	mTasks.push_back( pTask( new T_BBS() ) );
}

void Commander::update()
{
	processTasks();
	displayTaskInfo();
	updateResources();
// 	if (Budget::get().mineralsAvailable() > 400)
// 	{
// 		int remaining = MapInfo::get().remainingExpoCount();
// 		if (remaining > 0)
// 		{
// 			mExpoCount++;
// 			Broodwar->printf("Starting expo %i/%i",mExpoCount,remaining);
// 			mTasks.push_back( pTask( new T_TakeExpo() ) );
// 		}
// 	}
}

void Commander::processTasks()
{
	// eliminate completed or failed tasks
	list<pTask>::iterator it = mTasks.begin();
	for (it; it != mTasks.end(); )
	{
		pTask task = *it;
		if (task->isComplete() )
		{
			// 			logfile << Broodwar->getFrameCount() << " "<< mName <<": "
			// 				<< task->getName()
			// 				<< " task complete, terminating. "
			// 				<< mTasks.size()-1 << " tasks remain\n";
			task->terminate();
			it = mTasks.erase(it);
		}
		else if (task->isFailed() )
		{
			logfile << Broodwar->getFrameCount() << " "<< mName <<": "
				<< task->getName()
				<< " task failed, terminating. "
				<< mTasks.size()-1 << " tasks remain\n";
			task->terminate();
			it = mTasks.erase(it);
		}
		else
		{
			task->update();
			++it;
		}
	}
}

void Commander::displayTaskInfo() const
{
	int x = 15;	// x-coordinate
	int y = 20; // starting line
	int s = 10; // line spacing
	BOOST_FOREACH(pTask task, mTasks)
	{
		y = task->displayTaskInfo(x,y,s);
	}
	BBS::BB.BQ.displayInfo(x,y,s);
}

void Commander::updateResources()
{
	ResourceSet requiredResources;
	requiredResources.clear();
	BOOST_FOREACH(pTask task, mTasks)
	{
		requiredResources = requiredResources + task->resourcesOutstanding();
	}
	BBS::BB.resourcesRequired = requiredResources;
}

void Commander::buildDefenses()
{
	mTasks.push_back( pTask( new T_BuildDefenses() ) );
}