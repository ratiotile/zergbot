/** ZergBot
Right now the commander has only 2 objectives: build econ, and expand.
*/
#pragma once
#include <list>
#include "typedef_Task.h"
#include "Manager.h"

class Commander: public Manager
{
public:
	Commander();
	/// perform starting task
	virtual void onGameStart();
	/// call each frame
	virtual void update();
	void processTasks();
	void displayTaskInfo() const;
	void buildDefenses();
protected:
	/// tasks that have been stopped, for debugging
	std::list<pTask> mFinishedTasks;
	/// temp for expo testing
	int mExpoCount;
	/// add up resource requirements of all tasks and update bb
	void updateResources();
};