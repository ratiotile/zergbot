/** ConstructionManager handles the construction of buildings, and is 
granted control of workers temporarily to do so. It should only be invokec
by UnitManager and not higher level managers.
*/
#pragma once
#include <BWAPI.h>
#include "Singleton.h"

class ConstructionManager: Singleton<ConstructionManager>
{
public:
	ConstructionManager();
};