/**
Handles Contracts, which describe an action to take. Contractor responsible
for marking Contract as completed or failed when done. The issuer holds a
pointer and so does the Contractor. Contractor should discard pointer after
marking done.
*/
#pragma once
#include <boost/enable_shared_from_this.hpp>
#include "typedef_Contract.h"
#include <list>


class Contractor
{
public:
	virtual ~Contractor(){}
	///for Managers to directly push tasks, return false if not accepted
	virtual bool addContract(pContract job) = 0;
	/// called by manager to stop a task when finished or to interrupt.
	virtual void cancel(pContract job) = 0;
protected:
	/// jobs this contractor is responsible to accomplish, in order
	std::list<pContract> mContracts;
};
