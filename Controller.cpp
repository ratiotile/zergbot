#include "Controller.h"
#include "common/cpp_includes.h"
#include "UnitAgent.h"

void Controller::addUnit( UAptr unit )
{
	if(!mUnits.insert(unit).second)
		logfile << "Error adding unit to Controller!\n";
}

bool Controller::removeUnit( UAptr unit )
{
	if (mUnits.erase(unit) != 1)
	{
		logfile << "Error removing unit from Controller!\n";
		return false;
	}

	return true;
}

bool Controller::hasUnits( unitQuantity units ) const
{
	// iterate through mUnits, subtracting unit quantity.
	BOOST_FOREACH( UAptr unit, mUnits )
	{
		units[unit->getType()] -= 1;
	}
	BOOST_FOREACH( unitCount uc, units )
	{
		if( uc.second > 0 ) return false;
	}
	// finally if all quantities are 0 or under, success.
	return true;
}