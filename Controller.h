/************************************************************************/
/* Controller classes can own units, and give them up when ordered.     */
/* Tasks and Commander are Controllers.									*/
/************************************************************************/
#pragma once
#include "typedef_UnitAgent.h"
#include "common/typedefs.h"


class Controller
{
public:
	virtual void addUnit(UAptr unit);
	virtual bool removeUnit(UAptr unit);
	/// given map of unit types and quantities, return true if they are contained in mUnits.
	virtual bool hasUnits( unitQuantity units ) const;
protected:
	UAset mUnits;
};