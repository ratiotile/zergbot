#include "DisplayManager.h"
#include "common/UnitInfo.h"
#include "MapInfo.h"
#include "common/BWAPIext.h"
#include "UnitManager.h"
#include "common/Config.h"
#include "TABW/TerrainAnalyzer.h"
#include "Budget.h"
#include "HarvestManager.h"
#include "OverlordPositions/OverlordPositions.h"

using namespace std;
using namespace BWAPI;

DisplayManager::DisplayManager():
show_health_(false),
show_targetLines_(true),
show_unitBox_(false),
show_coverLines_(BB.Cfg().get<bool>("DisplayManager.show_clusterLines")),
show_clusterIDs_(BB.Cfg().get<bool>("DisplayManager.show_clusterIDs")),	// deprecated -- why??
show_mapPaths_(BB.Cfg().get<bool>("DisplayManager.show_mapPaths")),
show_unitActions_(true),
show_unit_counts_(BB.Cfg().get<bool>("DisplayManager.show_unit_counts")),
draw_unitInfo_(BB.Cfg().get<bool>("DisplayManager.draw_unitInfo")),
draw_enhancedUnitInfo_(BB.Cfg().get<bool>("DisplayManager.draw_enhancedUnitInfo")),
draw_unitAgentInfo_(BB.Cfg().get<bool>("DisplayManager.draw_unitAgentInfo")),
show_reserve_(BB.Cfg().get<bool>("DisplayManager.show_reserve")),
show_buildable_(BB.Cfg().get<bool>("DisplayManager.show_buildable")),
show_resourceAreas_(BB.Cfg().get<bool>("DisplayManager.show_resourceAreas")),
show_baseLocOwners_(BB.Cfg().get<bool>("DisplayManager.show_baseLocOwners")),
show_regionInfo_(BB.Cfg().get<bool>("DisplayManager.show_regionInfo")),
show_overlordPositions_(BB.Cfg().get<bool>("DisplayManager.show_overlordPositions"))
{
	pTimeDisplay = new TimeDisplay;
	show_BWAPI_regions_ = BB.Cfg().get<bool>(
		"DisplayManager.show_BWAPI_regions");
	show_resource_clusters_ = BB.Cfg().get<bool>(
		"DisplayManager.show_resource_clusters");
}

DisplayManager::~DisplayManager()
{
	delete pTimeDisplay;
}
/** Run all draw functions. Called before frame time computed.
*/
void DisplayManager::onFrameEnd()
{
	if (show_buildable_) MapInfo::get().drawBuildability();
	if (show_resourceAreas_) HarvestManager::get().drawResourceAreas();
	if (show_BWAPI_regions_) drawRegions();
	//GameState::getInstance().TM().drawScreen();
	//TODO:fix draw functions
	if( show_mapPaths_ ) MapInfo::get().drawRegionPaths();
	if( show_regionInfo_) MapInfo::get().drawRegionInfo();
	if( show_resource_clusters_ && TABW::TerrainAnalyzer::getInstance().isMapAnalyzed())
	{
		drawResources();
		drawResourceClusters();
	}
	if ( show_baseLocOwners_ )
	{
		MapInfo::get().drawBaseLocInfo();
	}
	if(draw_unitInfo_) drawUnitInfo();
	if(draw_enhancedUnitInfo_) drawEnhancedUnitInfo();
//	GameState::getInstance().Regions().drawRegionOwners();
	if( !Broodwar->isReplay() && draw_unitAgentInfo_ ) drawUnitAgentInfo();
	if ( show_unit_counts_ ) BB.displayUnitCount();
	if(show_reserve_) Budget::get().displayReserve();
	if (show_overlordPositions_) OverlordPositions::get().drawPositions();
}
 
void DisplayManager::drawTime()
{
	pTimeDisplay->onFrameEnd();
	pTimeDisplay->draw();
}

void DisplayManager::drawRegions()
{
	set<Region*> allRegions = Broodwar->getAllRegions();
	BOOST_FOREACH(Region* bwRegion, allRegions)
	{
		if (bwRegion->getDefensePriority() > 0)
		{
			Broodwar->drawBoxMap(bwRegion->getBoundsLeft(), bwRegion->getBoundsTop(),
				bwRegion->getBoundsRight(), bwRegion->getBoundsBottom(), BWAPI::Colors::Yellow);
			Broodwar->drawTextMap(bwRegion->getCenter().x(),bwRegion->getCenter().y(),
				"Region [%d,%d] %d", bwRegion->getRegionGroupID(), bwRegion->getID()
				,bwRegion->getDefensePriority());
		}
	}
}
void DisplayManager::drawUnitInfo()
{
	set< BWAPI::Unit*> allUnits = Broodwar->getAllUnits();
	set<const  BWAPI::Unit*> covered;	// units that have cover drawn placed in here
	BOOST_FOREACH(  BWAPI::Unit* pUnit, allUnits )
	{
		if (pUnit->exists() && !pUnit->getPlayer()->isNeutral())
		{
			if (show_unitBox_) drawBox(pUnit);
			if (show_health_) drawHealth(pUnit);
			/// somehow causes LAG
 			if (show_targetLines_) showTargets(pUnit);
			if ( pUnit->getPlayer() != Broodwar->self() )
			{
				if (show_coverLines_) 
				{
					covered.insert(pUnit);
					showCoverLines(pUnit, covered);
				}
				if (show_clusterIDs_) drawClusterID(pUnit);
			}
		}
	}
}
/** Draw unit HP and Shields as a stacked bar at the top of collision box
*/
void DisplayManager::drawHealth( const BWAPI::Unit* pUnit ) const
{
	UnitType ut = pUnit->getType();
	int barWidth = ut.dimensionRight() + ut.dimensionLeft() - 2; // account for border
	int barHeight = 2;	// replace with configurable value
	int barTop = pUnit->getTop();
	int barLeft = pUnit->getLeft() + 1;
	int hpMax = ut.maxHitPoints();
	int hpPx = 0;
	int spMax = ut.maxShields();
	int spPx = 0;
	int healthMax = hpMax + spMax;
	double hpPercent = (double)pUnit->getHitPoints() / (double)healthMax;
	hpPx = (int)floor(barWidth * hpPercent + 0.5 );
	Broodwar->drawBoxMap( barLeft, barTop,
		barLeft + barWidth, barTop + barHeight, BWAPI::Colors::Black, true );
	if (spMax > 0) 
	{
		double spPercent = (double)pUnit->getShields() / (double)healthMax;
		spPx = (int)floor(barWidth * spPercent + 0.5 );
		Broodwar->drawBoxMap( barLeft + hpPx, barTop,
			barLeft + hpPx + spPx, barTop + barHeight, BWAPI::Colors::Blue, true );
	}
	Broodwar->drawBoxMap( barLeft, barTop,
		barLeft + hpPx, barTop + barHeight, BWAPI::Colors::Green, true );
}
/** Draws a box of the player's color around unit.
*/
void DisplayManager::drawBox( const BWAPI::Unit* pUnit ) const
{
	Broodwar->drawBoxMap( pUnit->getLeft(), pUnit->getTop(), pUnit->getRight(), 
		pUnit->getBottom(), pUnit->getPlayer()->getColor() );
}

void DisplayManager::showTargets( const BWAPI::Unit* pUnit ) const
{
	Position unitPos = pUnit->getPosition();
	if (!unitPos.isValid())
	{
		return;
	}
	if (pUnit->getType().canMove())
	{
		Position targetPos = pUnit->getTargetPosition();
		if (targetPos.isValid())
		{
			Broodwar->drawLineMap(pUnit->getPosition().x(),pUnit->getPosition().y(),
				targetPos.x(),targetPos.y(),BWAPI::Colors::White);
		}
	}
	Player* pPlayer = pUnit->getPlayer();
	 BWAPI::Unit* pTarget = pUnit->getTarget();
	if (pTarget && pTarget->exists() && pTarget->getPosition().isValid())
	{
		Broodwar->drawLineMap( pUnit->getPosition().x() - 1, pUnit->getPosition().y() - 1,
			pTarget->getPosition().x() - 1, pTarget->getPosition().y() - 1, pPlayer->getColor() );
	}
	else
	{
// 		 BWAPI::Unit* pOrderTarget = pUnit->getOrderTarget();
// 		if (pOrderTarget && pOrderTarget->exists() && pOrderTarget->getPosition().isValid())
// 		{
// 			Broodwar->drawLineMap( pUnit->getPosition().x(),
// 				pUnit->getPosition().y(), pOrderTarget->getPosition().x(), 
// 				pOrderTarget->getPosition().y(),
// 				pPlayer->getColor() );
// 		}
	}
	if (pUnit->isSelected())
	{
		Broodwar->drawTextMap(pUnit->getPosition().x(),pUnit->getPosition().y(),
			"{%i}",pUnit->getID());
	}
	else
	{
		string statename = pUnit->getOrder().getName();
		if (pUnit->isIdle())
		{
			statename += ":Idle";
		}
		Broodwar->drawTextMap(pUnit->getPosition().x(),pUnit->getPosition().y(),"%s",
			statename.c_str() );
	}
}

void DisplayManager::drawEnhancedUnitInfo()
{	// displays when unit is covered by FOW
	GameState::get().drawLastSeenInfo();
	UnitManager::get().drawAttackers();
}

void DisplayManager::showCoverLines( const BWAPI::Unit* pUnit, set<const  BWAPI::Unit*> &covered )
{	// cyan for one-way protection, blue for both ways. Circle around covered.
	UIptr pUI = GameState::get().getUnitInfo(pUnit);
	if ( !pUI ) return; // in case invalid pUI is returned
	if ( pUnit != pUI->getUnit() )
	{
		logfile << "GameState returned invalid UnitInfo!\n";
		return;
	}
	int nSize = 0;
	if ( !pUI->coveredBy().empty() )
	{
		BOOST_FOREACH( UIptr cb, pUI->coveredBy() )
		{
			if (cb == UnitInfo::Null)
			{
				logfile << "Encountered undetected Null UnitInfo!\n";
				return;
			}
			if (pUI->covers(cb))
			{	// prevent doubling up
				if ( covered.find(cb->getUnit()) == covered.end() )
				{
					Broodwar->drawLineMap(pUI->getPosition().x(),
					pUI->getPosition().y(), cb->getPosition().x(),
					cb->getPosition().y(), BWAPI::Colors::Blue);
				}
				nSize++;
			}
			else	// pUI is covered by cb but doesn't cover back
			{
				Broodwar->drawLineMap(pUI->getPosition().x(),
					pUI->getPosition().y(), cb->getPosition().x(),
					cb->getPosition().y(), BWAPI::Colors::Cyan);
				Broodwar->drawCircleMap(pUI->getPosition().x(),
					pUI->getPosition().y(),14,BWAPI::Colors::Cyan,false);
			}
		}
		if (nSize >= 2)
		{
			Broodwar->drawCircleMap( pUI->getPosition().x(), 
				pUI->getPosition().y(), 16, BWAPI::Colors::Blue, false);
		}
	}
}

void DisplayManager::drawClusterID( const BWAPI::Unit* pUnit ) const
{	// displaying CID in drawLastSeenInfo when unit not visible
	UIptr pUI = GameState::get().getUnitInfo(pUnit);
	if(!pUI)
	{
		logfile << "DisplayManager::drawClusterID: UnitInfo not found for " << 
			BWAPIext::getUnitNameID(pUnit) << endl;
		logWriteBuffer();
		return;
	}
	assert( pUI != NULL );
	if (!pUnit->getType().isBuilding() && pUI->clusterID() != 0 )
	{
		Broodwar->drawTextMap(pUI->rightPx(), pUI->bottomPx(),
			"(%d)", pUI->clusterID());
	}
	// temp test for Lurker Visibility
	/*
	if (pUnit->getType().getName() == "Zerg Lurker")
	{
		Player* terranPlayer = NULL;
		BOOST_FOREACH(Player* player, Broodwar->getPlayers())
		{
			if (player->getRace() == BWAPI::Races::Terran)
			{
				terranPlayer = player;
			}
		}
		if(!terranPlayer) return;
		if (pUnit->isVisible(terranPlayer))
		{
			Broodwar->drawTextMap(pUI->rightPx(), pUI->bottomPx()-16,
				"Lurker [visible]");
		}
	}
	*/
}

void DisplayManager::drawUnitAgentInfo() const
{	// UnitManager implementation has changed!

// 	list<UnitAgent*> unitList = UnitManager::getInstance().getActiveUnitsList();
// 	BOOST_FOREACH( UnitAgent* UA, unitList )
// 	{
// 		if(show_unitActions_) drawUnitAction(UA);
// 	}
}

void DisplayManager::drawUnitAction( const UnitAgent* pUnitAgent ) const
{
	Position pos = pUnitAgent->getPosition();
	string actionName = pUnitAgent->getActiveActionName();
	assert( pos.isValid() );
	assert( !actionName.empty() );
	Broodwar->drawTextMap( pos.x(), pos.y(), 
		actionName.c_str() );
}

void DisplayManager::drawResourceClusters()
{
	TABW::TerrainAnalyzer::getInstance().drawResourceClusters();
}

void DisplayManager::drawResources()
{
	MapInfo::get().drawResources();
}