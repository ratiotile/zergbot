#pragma once
#ifndef INCLUDED_DISPLAYMANAGER_H
#define INCLUDED_DISPLAYMANAGER_H

#include <boost/foreach.hpp>
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#include "TimeDisplay.h"
#include <math.h>
#include <set>
#include "common/UnitInfo.h"

class UnitAgent;
// owned by Module
class DisplayManager
{
public:
	DisplayManager(); // only create 1 instance
	~DisplayManager();
	void onFrameEnd();
	void drawTime();
	void drawRegions();
	/// display HP and other indicators over units
	void drawUnitInfo();
	/// if it is a replay, must provide different functionality
	//void setReplay();
	/// display information in GameState
	void drawEnhancedUnitInfo();
	/// display player unit internal information. for non-replay games
	void drawUnitAgentInfo() const;
private:
	TimeDisplay* pTimeDisplay;
	/// graphically displays health/shield/mana over each unit
	void drawHealth(const BWAPI::Unit* pUnit) const;
	/// show unit collision box
	void drawBox(const BWAPI::Unit* pUnit) const;
	/// print BWAPI order given to unit
	void showOrders(const BWAPI::Unit* pUnit) const;
	/// draw lines to target units or move position
	void showTargets(const BWAPI::Unit* pUnit) const;
	/// draw lines to show covering relationships between units
	void showCoverLines( const BWAPI::Unit* pUnit, set<const  BWAPI::Unit*> &covered );
	/// print unit group ID number
	void drawClusterID( const BWAPI::Unit* pUnit ) const;
	/// print current active action.
	void drawUnitAction( const UnitAgent* pUnitAgent ) const;
	/// draw resource cluster number above each resource unit
	void drawResourceClusters();
	/// show resource outlines and depot positions
	void drawResources();

	// config settings
	bool show_health_;
	bool show_targetLines_;
	bool show_unitBox_;
	bool show_coverLines_;
	bool show_clusterIDs_;
	bool show_mapPaths_;	// draw lines from region centers to chokes
	bool show_unitActions_;	
	bool show_BWAPI_regions_;
	bool show_resource_clusters_;
	bool show_unit_counts_;
	bool draw_unitInfo_;
	bool draw_enhancedUnitInfo_;
	bool draw_unitAgentInfo_;
	bool show_reserve_;
	bool show_buildable_;
	bool show_resourceAreas_;
	bool show_baseLocOwners_;
	bool show_regionInfo_;
	bool show_overlordPositions_;
};

#endif //INCLUDED_DISPLAYMANAGER_H