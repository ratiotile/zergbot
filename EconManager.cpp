#include "EconManager.h"
#include "common/cpp_includes.h"
#include "Commander.h"
#include "Tasks/Task.h"
#include "Tasks/Contract.h"
#include "Budget.h"
#include "Tasks/T_BuildUnit.h"
#include "Tasks/C_BuildUnit.h"
#include "Tasks/T_BuildStructure.h"
#include "Tasks/T_AutoSupply.h"

EconManager::EconManager()
:Manager("EconManager")
{

}
void EconManager::update()
{
	processTasks();
	Budget::get().update();
}

void EconManager::handleEconomyStart()
{

}

bool EconManager::addContract( pContract job )
{
	if ( !job )
	{
		logfile << "Error in EconManager addContract: bad ptr\n";
		return false;
	}
	if ( job->name == "BuildUnit" )
	{
		pCBU buildJob = boost::static_pointer_cast<C_BuildUnit>(job);
		assert(buildJob);
		mContracts.push_back(job);
		// segregate mob units from buildings
		if (buildJob->buildType.isBuilding())
		{
			logfile << "EconManager accepts BuildStructure contract\n";
			//mTasks.push_back(pTBS(new T_BuildStructure(buildJob)));
		}
		else
		{
			//logfile << "EconManager accepts BuildUnit contract.\n";
			//mTasks.push_back(pTBU(new T_BuildUnit(buildJob)));
		}
		return true;
	}
	logfile << "Warning in EconManager addContract: unable to handle\n";
	return false;
}

void EconManager::cancel( pContract job )
{

}

void EconManager::enableAutoSupply()
{
	// look for existing AutoSupply task
	bool alreadyExists = false;
	BOOST_FOREACH( pTask task, mTasks )
	{
		if (task->getName() == T_AutoSupply::name)
		{
			alreadyExists = true;
		}
	}
	if (!alreadyExists)
	{
		logfile << "Econmanager autosupply enabled\n";
		pTAS autoSupply( new T_AutoSupply() );
		assert(autoSupply);
		mTasks.push_back(autoSupply);
	}
	else
	{
		logfile << "Warning in Econmanager enableAutoSupply: task already exists.\n";
	}
}

void EconManager::disableAutoSupply()
{
	list<pTask>::iterator it = mTasks.begin();
	for (it; it != mTasks.end(); )
	{
		if ( (*it)->getName() == T_AutoSupply::name )
		{
			(*it)->terminate();
			it = mTasks.erase(it);
		}
		else ++it;
	}
}