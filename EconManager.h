/**
EconManager handles constructing things and allocating funds.
*/
#include "common/Singleton.h"
#include "Contractor.h"
#include "typedef_Contract.h"
#include "Manager.h"

class EconManager: public Singleton<EconManager>, public Contractor, public
	Manager
{
public:
	EconManager();
	
	void update();
	bool addContract(pContract job);
	void cancel(pContract job);
	//void processTasks();
	/// create new autosupply task if not exist
	void enableAutoSupply();
	/// remove autosupply task if it exists
	void disableAutoSupply();

private:
	// std::list<pContract> mContracts; inherited
	
	void handleEconomyStart();
};