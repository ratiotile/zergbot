#include "EconState.h"
#include "common/cpp_includes.h"

EconState::EconState()
: supplyTotal(0)
, supplyUsed(0)
{

}
int EconState::getCombatUnitCount() const
{
	map<UnitType,UnitCountInfo>::const_iterator citer;
	int count = 0;
	for (citer = unitCount_.begin(); citer != unitCount_.end(); ++citer)
	{
		if (citer->first.canAttack())
		{ // accounts for all but high templar
			count += citer->second.numActive + citer->second.numOrdered + citer->second.numTraining;
		}
	}
	return count;
}
short EconState::getUnitCount( const UnitType &bt ) const
{
	if (bt.isBuilding())
	{
		map<UnitType,BuildingCountInfo>::const_iterator found = buildingCount_.find(bt);
		if ( found != buildingCount_.end() ) // if found
		{
			return found->second.numComplete;
		} else {	// not found
			return 0;
		}
	} else
	{
		map<UnitType,UnitCountInfo>::const_iterator found = unitCount_.find(bt);
		if ( found != unitCount_.end() ) // if found
		{
			return found->second.numActive;
		} else {	// not found
			return 0;
		}
	}
}
short EconState::getUnderConstruction( const UnitType &bt ) const
{
	if (bt.isBuilding())
	{
		map<UnitType,BuildingCountInfo>::const_iterator found = buildingCount_.find(bt);
		if ( found != buildingCount_.end() ) // if found
		{
			return found->second.numConstructing;
		} else {	// not found
			return 0;
		}
	} else
	{
		map<UnitType,UnitCountInfo>::const_iterator found = unitCount_.find(bt);
		if ( found != unitCount_.end() ) // if found
		{
			return found->second.numTraining;
		} else {	// not found
			return 0;
		}
	}
}
short EconState::getUnitsOrdered( const UnitType &bt ) const
{
	if (bt.isBuilding())
	{
		map<UnitType,BuildingCountInfo>::const_iterator found = buildingCount_.find(bt);
		if ( found != buildingCount_.end() ) // if found
		{
			return found->second.numOrdered;
		} else {	// not found
			return 0;
		}
	}else
	{
		map<UnitType,UnitCountInfo>::const_iterator found = unitCount_.find(bt);
		if ( found != unitCount_.end() ) // if found
		{
			return found->second.numOrdered;
		} else {	// not found
			return 0;
		}
	}
}

int EconState::getUnitTotal( const UnitType &bt ) const
{
	if (bt.isBuilding())
	{
		map<UnitType,BuildingCountInfo>::const_iterator found = buildingCount_.find(bt);
		if ( found != buildingCount_.end() ) // if found
		{
			return found->second.numOrdered + found->second.numComplete + found->second.numConstructing;
		} else {	// not found
			return 0;
		}
	}else
	{
		map<UnitType,UnitCountInfo>::const_iterator found = unitCount_.find(bt);
		if ( found != unitCount_.end() ) // if found
		{
			return found->second.numOrdered + found->second.numTraining + found->second.numActive;
		} else {	// not found
			return 0;
		}
	}
}

void EconState::onNewUnitOrder( const UnitType &bt )
{
	
	if (bt.isBuilding())
	{
		map<UnitType,BuildingCountInfo>::iterator found = buildingCount_.find(bt);
		if ( found != buildingCount_.end() ) // if found
		{
			found->second.numOrdered += 1;
		} else { // not found, create new
			buildingCount_[bt].numComplete = 0;
			buildingCount_[bt].numConstructing = 0;
			buildingCount_[bt].numOrdered = 1;
		}
	}
	else
	{
		
		map<UnitType,UnitCountInfo>::iterator found = unitCount_.find(bt);
		if ( found != unitCount_.end() ) // if found
		{
			found->second.numOrdered += 1;
		} else { // not found, create new
			unitCount_[bt].numActive = 0;
			unitCount_[bt].numTraining = 0;
			unitCount_[bt].numOrdered = 1;

		}
		logfile << "onNewUnitOrder " << bt.getName() 
			<< "["<<unitCount_[bt].numActive<<"|"<<unitCount_[bt].numTraining
			<< "|"<<unitCount_[bt].numOrdered << "]" << endl;
	}
}
void EconState::onNewUnit( const UnitType &bt )
{
	//Broodwar->printf("onNewUnit, %s",bt.getName().c_str());
	//
	if (bt.isBuilding())
	{
		map<UnitType,BuildingCountInfo>::iterator found = buildingCount_.find(bt);
		if ( found != buildingCount_.end() ) // if found
		{
			found->second.numConstructing += 1;
			if ( found->second.numOrdered > 0 ) found->second.numOrdered -= 1;
		} else { // not found, create new
			buildingCount_[bt].numComplete = 0;
			buildingCount_[bt].numConstructing = 1;
			buildingCount_[bt].numOrdered = 0;
// 			logfile << "Warning in EconState.onNewUnit: building " 
// 				<< bt.getName() << " not found in plan!\n";
		}
	} else
	{
		map<UnitType,UnitCountInfo>::iterator found = unitCount_.find(bt);
		if ( found != unitCount_.end() ) // if found
		{
			found->second.numTraining += 1;
			if ( found->second.numOrdered > 0 ) found->second.numOrdered -= 1;
		} else { // not found, create new
			unitCount_[bt].numActive = 0;
			unitCount_[bt].numTraining = 1;
			unitCount_[bt].numOrdered = 0;
// 			logfile << "Warning in EconState.onNewUnit: unit " 
// 				<< bt.getName() << " not found in plan!\n";
		}
		logfile << "onNewUnit " << bt.getName() 
			<< "["<<unitCount_[bt].numActive<<"|"<<unitCount_[bt].numTraining
			<< "|"<<unitCount_[bt].numOrdered << "]"
			<< endl;
	}
}
void EconState::onUnitComplete( const UnitType &bt )
{
	//Broodwar->printf("onUnitComplete, %s",bt.getName().c_str());
	//logfile << "onUnitComplete " << bt.getName() <<endl;
	if (bt.isBuilding())
	{
		map<UnitType,BuildingCountInfo>::iterator found = buildingCount_.find(bt);
		if ( found != buildingCount_.end() ) // if found
		{
			found->second.numComplete += 1;
			if (found->second.numConstructing > 0 ) found->second.numConstructing -= 1;
		} else { // not found, create new
			buildingCount_[bt].numComplete += 1;
			//buildingCount_[bt].numConstructing = 0;
			//buildingCount_[bt].numOrdered = 0;
			logfile << "Warning in EconState.onUnitComplete: building " << bt.getName() << " not found!\n";
		}
	}else
	{
		map<UnitType,UnitCountInfo>::iterator found = unitCount_.find(bt);
		if ( found != unitCount_.end() ) // if found
		{
			found->second.numActive += 1;
			if (found->second.numTraining > 0 ) found->second.numTraining -= 1;
		} else { // not found, create new
			unitCount_[bt].numActive += 1;
			//unitCount_[bt].numTraining = 0;
			//unitCount_[bt].numOrdered = 0;
			logfile << "Warning in EconState.onUnitComplete: unit " << bt.getName() << " not found!\n";
		}
		logfile << "onUnitComplete " << bt.getName() 
			<< "["<<unitCount_[bt].numActive<<"|"<<unitCount_[bt].numTraining
			<< "|"<<unitCount_[bt].numOrdered << "]"
			<< endl;
		
		
		
	}

}
void EconState::onUnitDestroyed( const UnitType &bt )
{
	if (bt.isBuilding())
	{
		map<UnitType,BuildingCountInfo>::iterator found = buildingCount_.find(bt);
		if ( found != buildingCount_.end() ) // if found
		{
			if (found->second.numComplete > 0) found->second.numComplete -= 1;
		} else { // not found, error
			buildingCount_[bt].numComplete = 0;
			buildingCount_[bt].numConstructing = 0;
			buildingCount_[bt].numOrdered = 0;
			logfile << "Error in EconState.onUnitDestroyed: building " << bt.getName() << " not found!\n";
		}
	}else
	{
		map<UnitType,UnitCountInfo>::iterator found = unitCount_.find(bt);
		if ( found != unitCount_.end() ) // if found
		{
			if (found->second.numActive > 0) found->second.numActive -= 1;
		} else { // not found, create new
			unitCount_[bt].numActive = 0;
			unitCount_[bt].numTraining = 0;
			unitCount_[bt].numOrdered = 0;
			logfile << "Error in EconState.onUnitDestroyed: unit " << bt.getName() << " not found!\n";
		}
	}
}

void EconState::onUnitMorphed( const  BWAPI::Unit* pUnit )
{
	UnitType ut = pUnit->getType();
	using namespace BWAPI::UnitTypes;
	// handle previous units
	if ( ut == Zerg_Egg )
	{
		onUnitDestroyed(Zerg_Larva);
	} 
	else if ( BWAPIext::isDroneMorphed(ut) )
	{
		onUnitDestroyed(Zerg_Drone);
	}
	else if ( BWAPIext::isEggHatched(ut) )
	{
		if (ut.isTwoUnitsInOneEgg())
		{
			static int doubleCount = 0;
			doubleCount++;
			if (doubleCount >=2)
			{
				logfile << "2 zerglings done, subtacting egg.\n";
				onUnitDestroyed(Zerg_Egg);
				doubleCount = 0;
			}
		}
		else onUnitDestroyed(Zerg_Egg);
	}
	else
	{
		logfile << "ES: Unit " << BWAPIext::getUnitNameID(pUnit) << " no prev unit!\n";
	}
	// if unit morphed into an egg type or building
	if (pUnit->getBuildType() && pUnit->getBuildType()!= BWAPI::UnitTypes::None)
	{
		logfile << "Econstate +new " << pUnit->getBuildType().getName() << " +complete:"
			<< pUnit->getType().getName() << "\n";
		onNewUnit(pUnit->getBuildType());
		if (pUnit->getBuildType().isTwoUnitsInOneEgg())
		{
			onNewUnit(pUnit->getBuildType());
		}
		if (pUnit->getType() == BWAPI::UnitTypes::Zerg_Egg ||
			pUnit->getType() == BWAPI::UnitTypes::Zerg_Cocoon ||
			pUnit->getType() == BWAPI::UnitTypes::Zerg_Lurker_Egg)
		{
			onUnitComplete(pUnit->getType());
		}
	}
	// for morphing into finished unit
	else if (pUnit->getRemainingBuildTime() == 0)
	{
		logfile << "Econstate morph +complete: " << pUnit->getType().getName()<<endl;
		onUnitComplete(pUnit->getType());
	}
	else
	{
		logfile << "Econstate warning morph unhandled condition, ";
		if (pUnit->isBeingConstructed())
		{
			logfile << "is being constructed, remain time: "<<pUnit->getRemainingBuildTime()
				<<endl;
		}
	}
}
void EconState::onUnitCancel( const UnitType &bt )
{
	if (bt.isBuilding())
	{
		map<UnitType,BuildingCountInfo>::iterator found = buildingCount_.find(bt);
		if ( found != buildingCount_.end() ) // if found
		{
			if (found->second.numConstructing > 0) found->second.numConstructing -= 1;
		} else { // not found, error
			buildingCount_[bt].numComplete = 0;
			buildingCount_[bt].numConstructing = 0;
			buildingCount_[bt].numOrdered = 0;
			logfile << "Error in EconState.onUnitCancel: building " << bt.getName() << " not found!\n";
		}
	}else
	{
		map<UnitType,UnitCountInfo>::iterator found = unitCount_.find(bt);
		if ( found != unitCount_.end() ) // if found
		{
			if (found->second.numTraining > 0) found->second.numTraining -= 1;
		} else { // not found, create new
			unitCount_[bt].numActive = 0;
			unitCount_[bt].numTraining = 0;
			unitCount_[bt].numOrdered = 0;
			logfile << "Error in EconState.onUnitCancel: unit " << bt.getName() << " not found!\n";
		}
		logfile << Broodwar->getFrameCount() << " onUnitCancel " << bt.getName() 
			<< "["<<unitCount_[bt].numActive<<"|"<<unitCount_[bt].numTraining
			<< "|"<<unitCount_[bt].numOrdered << "]"
			<< endl;
	}
}
void EconState::onUnitOrderCancel( const UnitType &bt )
{
	if (bt.isBuilding())
	{
		map<UnitType,BuildingCountInfo>::iterator found = buildingCount_.find(bt);
		if ( found != buildingCount_.end() ) // if found
		{
			if (found->second.numOrdered > 0) found->second.numOrdered -= 1;
		} else { // not found, error
			buildingCount_[bt].numComplete = 0;
			buildingCount_[bt].numConstructing = 0;
			buildingCount_[bt].numOrdered = 0;
			logfile << "Error in EconState.onUnitOrderCancel: building " << bt.getName() << " not found!\n";
		}
	}else
	{
		map<UnitType,UnitCountInfo>::iterator found = unitCount_.find(bt);
		if ( found != unitCount_.end() ) // if found
		{
			if (found->second.numOrdered > 0) found->second.numOrdered -= 1;
		} else { // not found, create new
			unitCount_[bt].numActive = 0;
			unitCount_[bt].numTraining = 0;
			unitCount_[bt].numOrdered = 0;
			logfile << "Error in EconState.onUnitOrderCancel: unit " << bt.getName() << " not found!\n";
		}
		logfile << Broodwar->getFrameCount() << " onUnitOrderCancel " << bt.getName() 
			<< "["<<unitCount_[bt].numActive<<"|"<<unitCount_[bt].numTraining
			<< "|"<<unitCount_[bt].numOrdered << "]"
			<< endl;
	}
}

void EconState::onFrame()
{
	const static int interval = 101; // frames
	static int frameCount = 0;
	frameCount++;
	if (frameCount > interval)
	{// update counts on units
		// set to 0
		frameCount = 0;
		for (map<UnitType,BuildingCountInfo>::iterator iter = buildingCount_.begin();
			iter != buildingCount_.end(); ++iter)
		{
			iter->second.numComplete = 0;
		}
		for (map<UnitType,UnitCountInfo>::iterator iter = unitCount_.begin();
			iter != unitCount_.end(); ++iter)
		{
			iter->second.numActive = 0;
			iter->second.numTraining = 0;
		}
		BOOST_FOREACH( BWAPI::Unit* unit, Broodwar->getAllUnits())
		{
			if (unit->getPlayer() == Broodwar->self() &&
				unit->isCompleted())
			{
				if (unit->getType().isBuilding())
				{
					buildingCount_[unit->getType()].numComplete++;
				}
				else
				{
					unitCount_[unit->getType()].numActive++;
				}
			}
			else if (BWAPIext::isEggType(unit->getType()))
			{
				unitCount_[unit->getType()].numActive++;
				unitCount_[unit->getBuildType()].numTraining++;
			}
		}
		logfile << Broodwar->getFrameCount() <<  " Onframe update " << getUnitCountStr(Zerg_Overlord) << endl;
	}
	// update supply counts
	supplyTotal = getPlannedSupplyProvided();
	supplyUsed = getPlannedSupplyUsed();
	displaySupply();
}

int EconState::getPlannedSupplyUsed() const
{
	
	// go through buildings list, add planned
	int supplyUsed = 0;
	for (map<UnitType,UnitCountInfo>::const_iterator iter = unitCount_.begin();
		iter != unitCount_.end(); ++iter)
	{
		supplyUsed += iter->first.supplyRequired() * 
			(iter->second.numOrdered + iter->second.numTraining + iter->second.numActive);
	}
	supplyUsed += BB.BQ.getQueuedSupply();
	return supplyUsed;
}

int EconState::getPlannedSupplyProvided() const
{
	int plannedSuppy = 0;
	for (map<UnitType,BuildingCountInfo>::const_iterator iter = buildingCount_.begin();
		iter != buildingCount_.end(); ++iter)
	{
		UnitType ut = (*iter).first;
		plannedSuppy += ut.supplyProvided() * (iter->second.numOrdered + iter->second.numConstructing
			+ iter->second.numComplete);
	}
	for (map<UnitType,UnitCountInfo>::const_iterator iter = unitCount_.begin();
		iter != unitCount_.end(); ++iter)
	{
		plannedSuppy += iter->first.supplyProvided() * 
			(iter->second.numOrdered + iter->second.numTraining + iter->second.numActive);
	}
	if (Broodwar->self()->getRace() == Zerg)
	{
		plannedSuppy += BB.BQ.getNumQueued(Zerg_Overlord)*Zerg_Overlord.supplyProvided();
		plannedSuppy += BB.BQ.getNumTasked(Zerg_Overlord)*Zerg_Overlord.supplyProvided();
		plannedSuppy += BB.BQ.getNumQueued(Zerg_Hatchery)*Zerg_Hatchery.supplyProvided();
	}
	return plannedSuppy;
}

std::string EconState::getUnitCountStr( UnitType ut )
{
	stringstream oss;
	oss << ut.getName() << "[" << unitCount_[ut].numActive<<"|"<<unitCount_[ut].numTraining
		<< "|" << unitCount_[ut].numOrdered << "]";
	return oss.str();
}

void EconState::displaySupply() const
{
	Broodwar->drawTextScreen(600,12,"%i/%i",supplyUsed,supplyTotal);
}