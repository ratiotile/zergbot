#ifndef INCLUDED_ECONSTATE_H
#define INCLUDED_ECONSTATE_H

#include <BWAPI.h>
#include <map>
#ifndef INCLUDED_LOGGING
	#define INCLUDED_LOGGING
	#include <fstream>
#endif

using namespace std;
using BWAPI::UnitType;
extern ofstream logfile;

struct UnitCountInfo
{
	short numOrdered;	// number planned, but not started yet
	short numTraining;	// number under construction
	short numActive;	// number that are alive
};
struct BuildingCountInfo // will be used to better represent building counts, perhaps with a map
{
	short numOrdered;	// how many have been ordered, but construction not yet begun
	short numConstructing;		// how many in progress warping
	short numComplete;		// how many finished, structureAgent has responsiblilty to update
};
struct EconState // only Econ should update!
{
	EconState();
	unsigned short wkrsOnMin;
	unsigned short wkrsOnGas;
	double minRate; // min per frame we are gathering
	double gasRate; // gas per frame
	map<UnitType,BuildingCountInfo> buildingCount_; // hold numBuild/complete
	map<UnitType,UnitCountInfo> unitCount_; // for non-building units
	bool getGas; // set to true after building first cybernetics core
	int supplyTotal;
	int supplyUsed;

	// accessors
	int getCombatUnitCount() const; // total number active, training, and ordered
	short getUnitCount( const UnitType &bt ) const; // number completed
	short getUnderConstruction( const UnitType &bt )const; // number unfinished
	short getUnitsOrdered( const UnitType &bt )const; // number ordered but not yet begun to construct
	int getUnitTotal( const UnitType &bt )const; // number complete, building, and planned.
	/// supply consumption(+) of all planned units, incl supply providers(-) constructing
	/// since in-game supply counts constructing units but not providers
	int getPlannedSupplyUsed() const;
	/// replacement for getSupplyTotal, as it lags behind a few frames
	int getPlannedSupplyProvided() const;
	// mutators
	void onNewUnitOrder( const UnitType &bt ); // call when new unit is ordered, before construction begins
	void onNewUnit( const UnitType &bt );	// call when new unit of ours detected, training begins
	void onUnitComplete( const UnitType &bt ); // call when building finishes warping in/Unit done training
	void onUnitDestroyed( const UnitType &bt ); // call when our unit has been destroyed
	void onUnitMorphed( const BWAPI::Unit* pUnit ); // call when unit has morphed
	void onUnitCancel( const UnitType &bt ); // call when an incomplete unit cancel or destroyed
	void onUnitOrderCancel( const UnitType &bt );	// call when a building order has been canceled
	std::string getUnitCountStr(UnitType ut);

	/// update, call every few seconds or so
	void onFrame();

	void displaySupply() const;
};
#endif