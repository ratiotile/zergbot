#include "EventManager.h"
#include "common/cpp_includes.h"

boost::signals2::connection EventManager::addSignalOnUnitCreate( const UnitSignal::slot_type& slot )
{
	return fSigOnUnitCreate.connect(slot);
}

boost::signals2::connection EventManager::addSignalOnUnitMorph( const UnitSignal::slot_type& slot )
{
	return fSigOnUnitMorph.connect(slot);
}
boost::signals2::connection EventManager::addSignalOnUnitDestroyed( const UnitSignal::slot_type& slot )
{
	return fSigOnUnitDestroyed.connect(slot);
}
void EventManager::onUnitCreate( BWAPI::Unit* unit )
{
	logfile << "Event: new unit created - " << BWAPIext::getUnitNameID(unit) << endl;
	fSigOnUnitCreate(unit);
}

void EventManager::onUnitMorph( BWAPI::Unit* unit )
{
	logfile << Broodwar->getFrameCount() << " Event: unit morphed - " 
		<< BWAPIext::getUnitNameID(unit) << ": "<< BB.ES.getUnitCountStr(unit->getType()) <<endl;
	fSigOnUnitMorph(unit);
}

void EventManager::onUnitDestroyed( BWAPI::Unit* unit )
{
	logfile << "Event: unit was destroyed - " << BWAPIext::getUnitNameID(unit) << endl;
	fSigOnUnitDestroyed(unit);
}
