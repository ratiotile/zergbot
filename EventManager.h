/************************************************************************/
/* EventManager handles all event messaging using Boost::Signals        */
/************************************************************************/
#pragma once
#include <BWAPI.h>
#include "common/Singleton.h"
#include <boost/signals2.hpp>

class EventManager: public Singleton<EventManager>
{
public:
	typedef boost::signals2::signal<void (BWAPI::Unit*)> UnitSignal;
	boost::signals2::connection addSignalOnUnitCreate( const UnitSignal::slot_type& slot);
	boost::signals2::connection addSignalOnUnitMorph( const UnitSignal::slot_type& slot);
	boost::signals2::connection addSignalOnUnitDestroyed( const UnitSignal::slot_type& slot);
	void onUnitCreate(BWAPI::Unit* unit);
	void onUnitMorph(BWAPI::Unit* unit);
	void onUnitDestroyed(BWAPI::Unit* unit);
protected:
private:
	UnitSignal fSigOnUnitCreate;
	UnitSignal fSigOnUnitMorph;
	UnitSignal fSigOnUnitDestroyed;
};

