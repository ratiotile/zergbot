#include "ExampleAIModule.h"
#include "common/Config.h"
#include "TABW/TerrainAnalyzer.h"
#include "Commander.h"
#include "EconManager.h"
#include "MapInfo.h"
#include "common/BWAPIext.h"
#include "ZergCommander.h"
#include "EventManager.h"
#include "OverlordPositions/OverlordPositions.h"
#include "common/WalkTile.h"

using namespace BWAPI;

bool analyzed;
bool analysis_just_finished;

//temporary for test ONLY
 //==========================================================

DisplayManager* pDisplayManager;
Commander* CMDR;
ofstream logfile; // logging
bool showEconomy = false;	// disable for UMS mode
bool analyzeOnStart = false;
bool showTerrain = false;
bool show_stats = false;
bool ai_enable = true; // can disable through config file

void ExampleAIModule::onStart()
{
	srand(time(NULL));
	TimeManager::getInstance().startTimer(TimeManager::startFrame);
	TimeManager::getInstance().startTimer(TimeManager::sfInit);
	logfile.open("ai_log.txt");
	logfile << "logging begun.\n";
	BBS::BB.init();
	if ( !BBS::BB.Cfg().get<bool>("AI_enable") )
	{
		logfile << "AI disabled.\n";
		ai_enable = false;
	}
	analyzeOnStart = BBS::BB.Cfg().get<bool>("AnalyzeMapOnStart");
	int gamespeed = BBS::BB.Cfg().get<int>("GameSpeed");
	

	pDisplayManager = new DisplayManager();
	Broodwar->printf("The map is %s, a %d player map",Broodwar->mapName().c_str(),Broodwar->getStartLocations().size());
	// Enable some cheat flags
	Broodwar->enableFlag(Flag::UserInput);
	//Broodwar->sendText("power overwhelming");
	//Broodwar->sendText("operation cwal");
	//Broodwar->sendText("show me the money");
// 	BOOST_FOREACH( UnitType ut, BWAPI::UnitTypes::allUnitTypes() )
// 	{
// 		if ( ut.canAttack() && !ut.isHero() )
// 		{
// 			GameState::getInstance().unitStrength(ut,Broodwar->self(),
// 				BWAPI::UnitTypes::Protoss_Dragoon, NULL);
// 		}
// 	}

	
	GameState::get();
	TimeManager::getInstance().stopTimer(TimeManager::sfInit);

	TimeManager::getInstance().startTimer(TimeManager::sfTA);
	TABW::TerrainAnalyzer::getInstance().analyzeTerrain();
	TimeManager::getInstance().stopTimer(TimeManager::sfTA);

	GameState::get().mapAnalyzed();

	TimeManager::getInstance().startTimer(TimeManager::sfMapInfo);
	MapInfo::get().onMapAnalyzed(); 
	TimeManager::getInstance().stopTimer(TimeManager::sfMapInfo);

	if ( BBS::BB.Cfg().get<bool>("BlackSheepWall") )
	{
		Broodwar->sendText("black sheep wall");
	}

	analyzed=false;
	analysis_just_finished=false;

	show_bullets=false;
	show_visibility_data=false;
	Broodwar->setLocalSpeed(gamespeed);
	if (Broodwar->isReplay() || !ai_enable)
	{
		Broodwar->printf("The following players are in this replay:");
		for(std::set<Player*>::iterator p=Broodwar->getPlayers().begin();p!=Broodwar->getPlayers().end();p++)
		{
			if (!(*p)->getUnits().empty() && !(*p)->isNeutral())
			{
			Broodwar->printf("%s, playing as a %s",(*p)->getName().c_str(),(*p)->getRace().getName().c_str());
			}
		}
		// add all units to GameState, since initial units dont trigger discover
// 		for(std::set< BWAPI::Unit*>::const_iterator i=Broodwar->getAllUnits().begin()
// 			;i!=Broodwar->getAllUnits().end();i++)
// 		{
// 			if (!(*i)->getPlayer()->isNeutral())
// 			{
// 				GameState::getInstance().onBanditSighted(*i);
// 			}
// 		}
	}
	else // not replay
	{
		// add all our units to UnitManager
		UnitManager::get().init();
		// add all other visible units to GameState
		
		TimeManager::getInstance().startTimer(TimeManager::sfCmdr);
		// start Commander
		if (Broodwar->self()->getRace().getName() == "Zerg")
		{
			logfile << "Zerg player race detected.\n";
			CMDR = new ZergCommander();
		}
		else CMDR = new Commander(); // Terran Commander

		CMDR->onGameStart();
		TimeManager::getInstance().stopTimer(TimeManager::sfCmdr);

		if (Broodwar->getGameType() == 
			BWAPI::GameTypes::getGameType("Use Map Settings"))
		{
	
		}
		else
		{
			Broodwar->printf("The match up is %s v %s",
				Broodwar->self()->getRace().getName().c_str(),
				Broodwar->enemy()->getRace().getName().c_str());
			Broodwar->printf("...");	//newline, doesn't work?
		}
	}
	TimeManager::getInstance().stopTimer(TimeManager::startFrame);
	TimeManager::getInstance().logStartFrameInfo();
}

void ExampleAIModule::onEnd(bool isWinner)
{
  if (isWinner)
  {
    //log win to file
  }
  logfile << "end of game.\n";
  logfile.close();
  delete pDisplayManager;
}

void ExampleAIModule::onFrame()
{
	TimeManager::getInstance().onFrameStart();
	
	if (Broodwar->isReplay() || !ai_enable)
	{
		if (analysis_just_finished)
		{
			Broodwar->printf("Finished analyzing map.");
			GameState::get().mapAnalyzed();
			analysis_just_finished=false;
		}
 		GameState::get().onFrameBegin();
 		TimeManager::getInstance().onFrameEnd();
 		//pDisplayManager->onFrameEnd();
 		pDisplayManager->drawTime();
		return;
	}
	GameState::get().onFrameBegin();
	// TEST ==================================================================================
	BBS::BB.update();
	//GameState::getInstance().update();
	Perceptor::getInstance().update();	// after GameState
	CMDR->update();
	EconManager::get().update();
	TimeManager::getInstance().processActions(); //	last
	logWriteBuffer();
	if (showEconomy)
	{
		//cmdr->econ->displayEconomy();
		//nexus1.displayAcquisitionRange();
	}
	
  if (show_visibility_data)
    drawVisibilityData();

  if (show_bullets)
    drawBullets();

  if (Broodwar->isReplay())
    return;
	if(show_stats)
		drawStats();

  

  if (analysis_just_finished)
  {
    Broodwar->printf("Finished analyzing map.");
	//cmdr->setMapAnalyzed(true);
	//cmdr->init();
	GameState::get().mapAnalyzed();
    analysis_just_finished=false;
  }
  ScoutManager::getInstance().onFrame();
  
  pDisplayManager->onFrameEnd();
  TimeManager::getInstance().onFrameEnd();
  pDisplayManager->drawTime();
}

void ExampleAIModule::onSendText(std::string text)
{
  if (text=="/show bullets")
  {
    show_bullets = !show_bullets;
  } else if (text=="/show players")
  {
    showPlayers();
  } else if (text=="/show forces")
  {
    showForces();
  } else if (text=="/show visibility")
  {
    show_visibility_data=!show_visibility_data;
  } else if (text=="/analyze")
  {
    if (analyzed == false)
    {
		Broodwar->printf("Analyzing map... this may take a minute");
		//CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)AnalyzeThread, NULL, 0, NULL);
    }
  }
  else if (text == "/show terrain")
  {
	  if (showTerrain)
		  showTerrain = false;
	  else
		  showTerrain = true;

  }
  else if (text=="/show economy")
  {
	  if (showEconomy)
		showEconomy = false;
	  else
		  showEconomy = true;
  }else if (text=="/info")
  {
// 	cmdr->printNewUnits();
// 	cmdr->econ->printStatus();
	  //nexus1.printStatus();
  }else if (text=="/computeBaseLocations")
  {
	  //cmdr->recon->computeNearestStartLocations();
  }else if (text=="/load shuttle")
  {
	  //cmdr->testAction1();
  }else if (text=="/rdist")
  {
	  //display region dist from spawn to mouse loc
	  Position mp = Broodwar->getMousePosition() + Broodwar->getScreenPosition();
	  BWAPIext::WalkTile wp(mp);
	  BWAPIext::WalkTile spawn(Broodwar->self()->getStartLocation());
	  int dist = MapInfo::get().getRegionPathDistance(spawn,wp);
	  Broodwar->printf("RegionDist: %d",dist);
  }else if (text == "/bd")
  {
	  CMDR->buildDefenses();
  }
  else 
  {
	//parse text
	std::stringstream os(text);
	std::string temp;
	std::vector<std::string> sParameters;
	while (getline(os,temp,' '))
	{
		//Broodwar->printf("token: %s",temp.c_str());
		sParameters.push_back(temp);
	}
	if (sParameters[0] == "add")
	{
		Position mp = Broodwar->getMousePosition() + Broodwar->getScreenPosition();
		Broodwar->printf("MousePos: %d,%d",mp.x(),mp.y());
		OverlordPositions::get().addPosition(mp);
	}	
	else if (sParameters[0] == "rem")
	{
		Position mp = Broodwar->getMousePosition() + Broodwar->getScreenPosition();
		Broodwar->printf("MousePos: %d,%d",mp.x(),mp.y());
		OverlordPositions::get().removePosition(mp);
	}	
	else if (sParameters[0] == "m" && sParameters.size() == 2)
	{
		//Broodwar->printf("move unit, direction %s", sParameters[1].c_str());
		//cmdr->moveSelectedUnits(sParameters[1]);
	}
	else if (sParameters[0] == "MB" && sParameters.size() == 3)
	{	// "MB seek 1" enables seek on selected units
		bool b = atoi(sParameters[2].c_str()) != 0;
		UnitManager::get().selectedUnitsSetBehavior(sParameters[1],b);
	}
	else{
		Broodwar->printf("You typed '%s'!",text.c_str());
		Broodwar->sendText("%s",text.c_str());
	}
  }
}

void ExampleAIModule::onReceiveText(BWAPI::Player* player, std::string text)
{
	Broodwar->printf("%s said '%s'", player->getName().c_str(), text.c_str());
}

void ExampleAIModule::onPlayerLeft(BWAPI::Player* player)
{
  Broodwar->sendText("%s left the game.",player->getName().c_str());
}

void ExampleAIModule::onNukeDetect(BWAPI::Position target)
{
  if (target!=Positions::Unknown)
    Broodwar->printf("Nuclear Launch Detected at (%d,%d)",target.x(),target.y());
  else
    Broodwar->printf("Nuclear Launch Detected");
}

void ExampleAIModule::onUnitDiscover(BWAPI::Unit* unit)
{
 	if (Broodwar->getFrameCount()>1){
	//	Broodwar->sendText("A %s [%x] has been discovered at (%d,%d)",unit->getType().getName().c_str(),unit,unit->getPosition().x(),unit->getPosition().y());
		//cmdr->onUnitDiscover(unit);
		if (!Broodwar->isReplay())
		{
			if (unit->getPlayer()->isEnemy(Broodwar->self()))
			{
				logfile << unit->getType().getName() << "[" << unit->getID() 
					<< "] has been discovered.\n";
				GameState::get().onBanditSighted(unit);
			}
		}
		else
		{
			logfile << unit->getType().getName() << "[" << unit->getID() 
				<< "] has been discovered.\n";
			GameState::get().onBanditSighted(unit);
		}
		if (BWAPIext::isResourceDepot(unit->getType()))
		{
			MapInfo::get().onDepotFound(unit);
		}
 	}
}

void ExampleAIModule::onUnitEvade(BWAPI::Unit* unit)
{
	if (!Broodwar->isReplay() && Broodwar->getFrameCount()>1){}
// 		Broodwar->sendText("A %s [%x] was last accessible at (%d,%d)",unit->getType().getName().c_str(),unit,unit->getPosition().x(),unit->getPosition().y());
		//cmdr->onUnitEvade(unit);
}

void ExampleAIModule::onUnitShow(BWAPI::Unit* unit)
{
// 	if (!Broodwar->isReplay() && Broodwar->getFrameCount()>1){
// 		Broodwar->sendText("A %s [%x] has been spotted at (%d,%d)",unit->getType().getName().c_str(),unit,unit->getPosition().x(),unit->getPosition().y());
// 	}
}

void ExampleAIModule::onUnitHide(BWAPI::Unit* unit)
{
//   if (!Broodwar->isReplay() && Broodwar->getFrameCount()>1)
//     Broodwar->sendText("A %s [%x] was last seen at (%d,%d)",unit->getType().getName().c_str(),unit,unit->getPosition().x(),unit->getPosition().y());
}

void ExampleAIModule::onUnitCreate(BWAPI::Unit* unit)
{
	if (!Broodwar->isReplay())
	{
		//Broodwar->sendText("A %s [%x] has been created at (%d,%d)",unit->getType().getName().c_str(),unit,unit->getPosition().x(),unit->getPosition().y());
		 // is it our unit?
		if (unit->getPlayer() == Broodwar->self())
		{
			if (unit->isCompleted())
			{
				BB.ES.onUnitComplete(unit->getType());
			}
			else BB.ES.onNewUnit(unit->getType());
			UnitManager::get().addUnitAgent(unit);
			EventManager::get().onUnitCreate(unit);
			if (BWAPIext::isResourceDepot(unit->getType()))
			{
				MapInfo::get().onDepotFound(unit);
			}
		}
		else
		{
		}
	}
	else
	{

	}

}

void ExampleAIModule::onUnitDestroy(BWAPI::Unit* unit)
{
	if (Broodwar->getFrameCount()>1)
	{
		EventManager::get().onUnitDestroyed(unit); // should go before UnitManager, so can lookup
		//Broodwar->sendText("A %s [%x] has been destroyed at (%d,%d)",unit->getType().getName().c_str(),unit,unit->getPosition().x(),unit->getPosition().y());
		if (!Broodwar->isReplay())
		{
			if(unit->getPlayer() != Broodwar->self())
			{
				GameState::get().onBanditDestroyed(unit);
				Perceptor::getInstance().onBanditDestroyed(unit);
				UnitManager::get().onEnemyDestroyed(unit);
			}
			else
			{// one of ours lost
				Broodwar->printf("One of our units, %s was destroyed.", unit->getType().getName().c_str());
				logfile << "One of our units, " << unit->getType().getName()
					<< "[" << unit->getID() << "] was destroyed.\n";
				logWriteBuffer();
				BB.ES.onUnitDestroyed(unit->getType());
				UnitManager::get().onUnitLost(unit);
			}
		}
		else	// REPLAY USE
		{
			GameState::get().onBanditDestroyed(unit);
		}
	}
}

void ExampleAIModule::onUnitMorph(BWAPI::Unit* unit)
{
// 	Broodwar->sendText("A %s [%x] has been morphed at (%d,%d)",
// 		unit->getType().getName().c_str(),unit,unit->getPosition().x(),
// 		unit->getPosition().y());
	if (Broodwar->getFrameCount()>1){
// 		logfile << unit->getType().getName() << "[" << unit->getID() 
// 			<< "] has Morphed.\n";
		GameState::get().onBanditMutate(unit);
	}
	if (!Broodwar->isReplay()){
		
		if (unit->getPlayer() == Broodwar->self())
		{
			// update Blackboard
			BBS::BB.ES.onUnitMorphed(unit);
			UnitManager::get().onUnitMorph(unit);
			EventManager::get().onUnitMorph(unit); // make sure eventManager processes after UnitManager!
			if (unit->getType() == BWAPI::UnitTypes::Protoss_Assimilator)
			{
				//cmdr->econ->newUnit(unit);
			}
		}
	}
	else
  {
    /*if we are in a replay, then we will print out the build order
    (just of the buildings, not the units).*/
//     if (unit->getType().isBuilding() && unit->getPlayer()->isNeutral()==false)
//     {
//       int seconds=Broodwar->getFrameCount()/24;
//       int minutes=seconds/60;
//       seconds%=60;
//       Broodwar->sendText("%.2d:%.2d: %s morphs a %s",minutes,seconds,unit->getPlayer()->getName().c_str(),unit->getType().getName().c_str());
//     }
  }
}

void ExampleAIModule::onUnitRenegade(BWAPI::Unit* unit)
{
  if (!Broodwar->isReplay())
    Broodwar->sendText("A %s [%x] is now owned by %s",unit->getType().getName().c_str(),unit,unit->getPlayer()->getName().c_str());
}

void ExampleAIModule::onSaveGame(std::string gameName)
{
  Broodwar->printf("The game was saved to \"%s\".", gameName.c_str());
}

DWORD WINAPI AnalyzeThread()
{
  //BWTA::analyze();
  analyzed   = true;
  analysis_just_finished = true;
  return 0;
}

void ExampleAIModule::drawStats()
{
  std::set< BWAPI::Unit*> myUnits = Broodwar->self()->getUnits();
  Broodwar->drawTextScreen(5,0,"I have %d units:",myUnits.size());
  std::map<UnitType, int> unitTypeCounts;
  for(std::set< BWAPI::Unit*>::iterator i=myUnits.begin();i!=myUnits.end();i++)
  {
    if (unitTypeCounts.find((*i)->getType())==unitTypeCounts.end())
    {
      unitTypeCounts.insert(std::make_pair((*i)->getType(),0));
    }
    unitTypeCounts.find((*i)->getType())->second++;
  }
  int line=1;
  for(std::map<UnitType,int>::iterator i=unitTypeCounts.begin();i!=unitTypeCounts.end();i++)
  {
    Broodwar->drawTextScreen(5,16*line,"- %d %ss",(*i).second, (*i).first.getName().c_str());
    line++;
  }
}

void ExampleAIModule::drawBullets()
{
  std::set<Bullet*> bullets = Broodwar->getBullets();
  for(std::set<Bullet*>::iterator i=bullets.begin();i!=bullets.end();i++)
  {
    Position p=(*i)->getPosition();
    double velocityX = (*i)->getVelocityX();
    double velocityY = (*i)->getVelocityY();
    if ((*i)->getPlayer()==Broodwar->self())
    {
      Broodwar->drawLineMap(p.x(),p.y(),p.x()+(int)velocityX,p.y()+(int)velocityY,BWAPI::Colors::Green);
      Broodwar->drawTextMap(p.x(),p.y(),"\x07%s",(*i)->getType().getName().c_str());
    }
    else
    {
      Broodwar->drawLineMap(p.x(),p.y(),p.x()+(int)velocityX,p.y()+(int)velocityY,BWAPI::Colors::Red);
      Broodwar->drawTextMap(p.x(),p.y(),"\x06%s",(*i)->getType().getName().c_str());
    }
  }
}

void ExampleAIModule::drawVisibilityData()
{
  for(int x=0;x<Broodwar->mapWidth();x++)
  {
    for(int y=0;y<Broodwar->mapHeight();y++)
    {
      if (Broodwar->isExplored(x,y))
      {
        if (Broodwar->isVisible(x,y))
          Broodwar->drawDotMap(x*32+16,y*32+16,BWAPI::Colors::Green);
        else
          Broodwar->drawDotMap(x*32+16,y*32+16,BWAPI::Colors::Blue);
      }
      else
        Broodwar->drawDotMap(x*32+16,y*32+16,BWAPI::Colors::Red);
    }
  }
}

void ExampleAIModule::drawTerrainData()
{

}

void ExampleAIModule::showPlayers()
{
  std::set<Player*> players=Broodwar->getPlayers();
  for(std::set<Player*>::iterator i=players.begin();i!=players.end();i++)
  {
    Broodwar->printf("Player [%d]: %s is in force: %s",(*i)->getID(),(*i)->getName().c_str(), (*i)->getForce()->getName().c_str());
  }
}

void ExampleAIModule::showForces()
{
  std::set<Force*> forces=Broodwar->getForces();
  for(std::set<Force*>::iterator i=forces.begin();i!=forces.end();i++)
  {
    std::set<Player*> players=(*i)->getPlayers();
    Broodwar->printf("Force %s has the following players:",(*i)->getName().c_str());
    for(std::set<Player*>::iterator j=players.begin();j!=players.end();j++)
    {
      Broodwar->printf("  - Player [%d]: %s",(*j)->getID(),(*j)->getName().c_str());
    }
  }
}
