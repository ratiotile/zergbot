#include "GameState.h"
#include "StrengthEstimator.h"
#include "TargetManager.h"
#include "common/cpp_includes.h"
#include <stdio.h>
#include "HarvestManager.h"
#include "MapInfo.h"

// using namespace BWAPI;
// using namespace std;
// using boost::shared_ptr;

GameState* GameState::pInstance_ = NULL;

GameState::GameState():
// set startLocation_ at middle of screen
startLocation_(Broodwar->getScreenPosition().x()+320,Broodwar->getScreenPosition().y()+240)
,isMapAnalyzed_(false)
,initialized_(false)
,threatMap_( new ThreatMap(Broodwar->mapWidth(),Broodwar->mapHeight()) )
,pClusterMan_( new ClusterManager( ClusterManager::alg_ANC ) )
,pStrengthEstimator_( new StrengthEstimator() )
,mMyStrength(0)
,mEnemyStrength(0)
{
	logfile << "GameState Constructor\n";
	Broodwar->printf("Starting ScreenPos: [%d,%d]",startLocation_.x(),startLocation_.y());
}
GameState::~GameState()
{
	delete threatMap_;
	delete pClusterMan_;
}
GameState& GameState::get()
{
	if (!pInstance_)
	{
		pInstance_ = new GameState();
		pInstance_->init();
	}
	return *pInstance_;
}

void GameState::onBanditSighted(  BWAPI::Unit* pUnit )
{
	if (pUnit->getPlayer()->isNeutral() ||
		pUnit->getPlayer() == Broodwar->self())
	{
		return;
	}
	// check first if we already have it
	UUImap::iterator found = mBandits_.find(pUnit);
	if (found == mBandits_.end())
	{
		UIptr InpTempUI( new UnitInfo(pUnit) );
 		char buf[100];
 		sprintf_s(buf, "%s sighted, added to mBandits_:%p",
 			InpTempUI->getType().getName().c_str(),InpTempUI->getUnit());
// 		logfile << buf << endl;
		mBandits_.insert(make_pair(pUnit,InpTempUI));
		threatMap_->onEnemyUnitDiscovered(*pUnit); // update threatMap
		if ( !pUnit->getType().isBuilding() &&
			!pUnit->getType().isSpell() &&
			!(pUnit->getType().getName() == "Terran Vulture Spider Mine") &&
			!(pUnit->getType().getName() == "Zerg Egg") )
		{
			//pSPM_->addNewUnit( InpTempUI );	// add unit to SPM
			pClusterMan_->addUnitInfo(InpTempUI);
			// also add the unit to TargetManager
			TargetManager::get().addUnit(InpTempUI);
		}
	}
}


void GameState::onBanditDestroyed(  BWAPI::Unit* pUnit )
{
	if (pUnit->getPlayer()->isNeutral() ||
		pUnit->getPlayer() == Broodwar->self())
	{
		return;
	}

	UUImap::iterator found = mBandits_.find(pUnit);
	if (found == mBandits_.end())
	{
		Broodwar->printf("%s reported destroyed but not in mBandits_",
			pUnit->getType().getName().c_str());
	} else{
		//Broodwar->printf("Bogey %s [%x] has been destroyed at (%d,%d). Erased",pUnit->getType().getName().c_str(),pUnit,pUnit->getPosition().x(),pUnit->getPosition().y());
		logfile << "Unit "<<pUnit->getType().getName()<<" ["<< pUnit->getID() <<"] "
			<< "has been destroyed at ("<<pUnit->getPosition().x()<<
			","<<pUnit->getPosition().y()<<"). Erased\n";
		logWriteBuffer();
		threatMap_->onEnemyUnitDestroyed(*pUnit);	// remove from threatMap
		if ( !pUnit->getType().isBuilding() )
		{
			//pSPM_->removeUnit( (*found).second );	// remove unit from SPM
			pClusterMan_->removeUnitInfo( (*found).second );
		}
		if ( pUnit->getType().isMineralField() )
		{
			HarvestManager::get().onMineralDestroyed((*found).second);
		}
		mBandits_.erase(found);
	}
}
void GameState::update()
{
	for ( UUImap::iterator i = mBandits_.begin(); i != mBandits_.end(); i++)
	{
		(*i).second->update();
	}
	//temp draw

	// put non replay-safe code here
	if ( !Broodwar->isReplay() )
	{
		//updateRegionInfo();
	}
	calculateTotalStrengths();
}
/// call once map is analyzed
void GameState::mapAnalyzed()
{
	isMapAnalyzed_ = true;
	//pRegionMan_->onMapAnalyzed();
}

const map<const  BWAPI::Unit*,UIptr>& GameState::getBandits() const
{
	return mBandits_;
}

void GameState::drawMapPaths() const
{
	if (!isMapAnalyzed_)
	{
		return;
	}/*
	typedef pair<BWTA::Region*, shared_ptr<regionInfo>> mapType;
	BOOST_FOREACH( const mapType& regionPair , mRegions_)
	{
		Position cPos = regionPair.first->getCenter();
		BOOST_FOREACH( baseLocInfo& BLI, regionPair.second->baseLocs)
		{
			Position iPos = BLI.baseLoc->getPosition();
			Broodwar->drawLineMap(cPos.x(),cPos.y(),iPos.x(),iPos.y(),BWAPI::Colors::Green);
		}
		
		BOOST_FOREACH( BWTA::Chokepoint* choke, regionPair.first->getChokepoints() )
		{
			Position iPos = choke->getCenter();
			Broodwar->drawLineMap(cPos.x(),cPos.y(),iPos.x(),iPos.y(),BWAPI::Colors::Grey);
		}
	}*/
}

// unsigned int GameState::getUnexploredLocations(BWTA::Region* region) const
// {
// 	return pRegionMan_->countUnexploredLocations(region);
// }

// void GameState::updateRegionInfo()
// {	
// 	pRegionMan_->updateRegionInfo();
// }

bool GameState::isMapAnalyzed() const
{
	return isMapAnalyzed_;
}

// const std::map<BWTA::Region*,boost::shared_ptr<regionInfo>>& GameState::getRegionInfos() const
// {
// 	return pRegionMan_->getRegionInfos();
// }

// const baseLocInfo* GameState::getBaseLocInfo( BWTA::BaseLocation* bloc ) const
// {
// 	return pRegionMan_->getBaseLocInfo(bloc);
// }

void GameState::updateFrameLastSeen( ScoutLocation& sl )
{
	//pRegionMan_->updateFrameLastSeen(sl);
}

BWAPI::Position GameState::getBanditPosition( const  BWAPI::Unit* bandit ) const
{
	UUImap::const_iterator found = mBandits_.find(bandit);
	if (found != mBandits_.end())
	{
		return found->second->getPosition();
	}
	return BWAPI::Positions::None;
}

void GameState::drawLastSeenInfo() const
{
	typedef pair<const  BWAPI::Unit*,UIptr> tPair;
	BOOST_FOREACH(tPair banditP, mBandits_ )
	{
		if (!banditP.first->isVisible())
		{	// draw
			Broodwar->drawBoxMap(banditP.second->leftPx(), banditP.second->topPx(),
				banditP.second->rightPx(), banditP.second->bottomPx(), BWAPI::Colors::Grey );
			Broodwar->drawTextMap(banditP.second->getPosition().x(), 
				banditP.second->getPosition().y(),
				"%s", banditP.second->getType().getName().c_str());
			// also display cID
			Broodwar->drawTextMap(banditP.second->rightPx(), banditP.second->bottomPx(),
				"(%d)", banditP.second->clusterID());
		}
	}
}

void GameState::drawClusterIDs() const
{
	typedef pair<const  BWAPI::Unit*,UIptr> tPair;
	BOOST_FOREACH(tPair banditP, mBandits_ )
	{
		if (!banditP.first->getType().isBuilding())
		{
			Broodwar->drawTextMap(banditP.second->rightPx(), banditP.second->bottomPx(),
				"(%d)", banditP.second->clusterID());
		}
	}
}

void GameState::onFrameBegin()
{
	update();
	// no longer needed
	//SpatialPositionManager::getInstance().onFrameBegin();
	pClusterMan_->atFrameBegin();
}

void GameState::getBanditsInRadius( BWAPI::Position center, int rPx, 
								   setUI& results)
{
	Player* targetPlayer = Broodwar->self();
	if (Broodwar->isReplay())
	{
		targetPlayer = BWAPIext::getTerranPlayer();
	}
	// dub the UI from mBandits into a vector
	UUImap::const_iterator unitMapIter = mBandits_.begin();
	vector<UIptr> tempVector;
	for (unitMapIter; unitMapIter != mBandits_.end(); ++unitMapIter)
	{
	//	if ( targetPlayer->isEnemy(unitMapIter->first->getPlayer() ))
	//	{
			tempVector.push_back(unitMapIter->second);
	//	}
	}
	SpatialPositionManager::getInstance().getUnitsInRadius( 
		tempVector, center, rPx, results );
}

void GameState::getBanditsInRadius( const setUI& unitPopulation,
	BWAPI::Position center, int rPx, setUI& results)
{
	SpatialPositionManager::getInstance().getUnitsInRadius( 
		unitPopulation, center, rPx, results );
}

void GameState::onBanditMoveTile( UIptr pUI )
{
	pClusterMan_->onUnitMoveTile( pUI );
}

UIptr GameState::getUnitInfo( const BWAPI::Unit* unit )
{
	UUImap::iterator found = mBandits_.find(unit);
	if ( found != mBandits_.end() )
	{
		return found->second;
	}
	return UnitInfo::Null;
}

void GameState::onBanditMutate(  BWAPI::Unit* pUnit )
{
	if (pUnit->getType().isNeutral() )
	{
		return;
	}
	UUImap::iterator found = mBandits_.find(pUnit);
	if (found != mBandits_.end())
	{
		if ( pUnit->getType().isBuilding() )
		{	// remove from clusterManager
			//pSPM_->removeUnit( found->second );
			pClusterMan_->removeUnitInfo(found->second);
		}
	}
	
	else
	{
		onBanditSighted(pUnit);
	}
}

double GameState::unitStrength( BWAPI::UnitType u1, BWAPI::Player* p1, BWAPI::UnitType u2, BWAPI::Player* p2 )
{
	return pStrengthEstimator_->getStrength(u1, p1, u2, p2);
}

void GameState::init()
{
	if (!initialized_)
	{
		addStaticUnitInfo();
		initialized_ = true;
		logfile << "GameState running init()\n";
		TargetManager::get().init();
	}
}

void GameState::addStaticUnitInfo()
{
	set< BWAPI::Unit*> staticNeutrals = Broodwar->getStaticNeutralUnits();
	BOOST_FOREACH( BWAPI::Unit* snu, staticNeutrals)
	{
		UUImap::iterator found = mBandits_.find(snu);
		if (found == mBandits_.end())
		{
			UIptr InpTempUI( new UnitInfo(snu) );
			mBandits_.insert(make_pair(snu,InpTempUI));
		}
		else
		{
			logfile << "Warning in GameState addStaticUnitInfo: static neutral already added!\n";
		}
	}
}

std::set<const regionInfo*> GameState::getEnemyPresence() const
{
	set<const regionInfo*> presence;
	// iterate over all enemy unitInfo
	typedef pair<const  BWAPI::Unit*,UIptr> UUIpair;
	BOOST_FOREACH(UUIpair UUI, mBandits_)
	{
		UIptr UI(UUI.second);
		if (UI->getOwner()->isEnemy(Broodwar->self()))
		{
			const regionInfo* ri = MapInfo::get().getRegion(UI->getPosition());
			presence.insert(ri);
		}
	}
	return presence;
}

int GameState::numEnemyUnitsInRegion( regionInfo* region )
{
	if (!region)
	{
		return 0;
	}

	int unitsInRegion(0);
	BOOST_FOREACH(BWAPI::Unit * unit, BWAPI::Broodwar->enemy()->getUnits()) 
	{
		if (unit->isVisible() 
			&& MapInfo::get().getRegion(unit->getPosition()) == region) 
		{
			unitsInRegion++;
		}
	}

	return unitsInRegion;
}

int GameState::numEnemyFlyingUnitsInRegion( regionInfo* region )
{
	if (!region)
	{
		return 0;
	}

	int unitsInRegion(0);
	BOOST_FOREACH(BWAPI::Unit * unit, BWAPI::Broodwar->enemy()->getUnits()) 
	{
		if (unit->isVisible() 
			&& MapInfo::get().getRegion(unit->getPosition()) == region
			&& unit->getType().isFlyer()) 
		{
			unitsInRegion++;
		}
	}

	return unitsInRegion;
}

void GameState::calculateTotalStrengths()
{
	// first calculate our strength
	mMyStrength = 0;
	set<BWAPI::Unit*> combatUnits;
	for each( BWAPI::Unit* unit in Broodwar->self()->getUnits() )
	{
		if (BWAPIext::isCombatUnit(unit->getType()))
		{
			combatUnits.insert(unit);
		}
	}
	mMyStrength += strengthEstimator().getScore(combatUnits);
	// then calculate enemy strength
	
	set<UIptr> enemyCombatUnits = getEnemyCombatUnits();
	mEnemyStrength = strengthEstimator().getScore(enemyCombatUnits);

	// draw somewhere on screen.
	Broodwar->drawTextScreen(300, 40, "S: %d", (int)mMyStrength);
	Broodwar->drawTextScreen(300, 50, "E: %d", (int)mEnemyStrength);
}

set<UIptr> GameState::getEnemyCombatUnits() const
{
	set<UIptr> combatUnits;
	for each ( pair<const BWAPI::Unit*,UIptr> unitPair in mBandits_ )
	{
		if ( BWAPIext::isCombatUnit(unitPair.second->getType())
			&& !unitPair.second->getType().isBuilding())
		{
			combatUnits.insert(unitPair.second);
		}
	}
	return combatUnits;
}

void GameState::getEnemyInRegion( const regionInfo* region, setUI& results )
{
	results.clear();
	for each ( pair<const BWAPI::Unit*,UIptr> unitPair in mBandits_ )
	{
		UIptr unit = unitPair.second;
		if ( unit->exists() && Broodwar->self()->isEnemy(unit->getOwner()) &&
			MapInfo::get().getRegion(unit->getPosition()) == region )
		{
			results.insert(unit);
		}
	}
}