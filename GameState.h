/************************************************************************/
/* GameState contains information about enemy units that need to be		*/
/* globally accessible. It is updated by callback on appearance.		*/
/************************************************************************/
#pragma once

#include <list>
#include <map>
#include <boost/smart_ptr.hpp>
#include <boost/foreach.hpp>
#ifndef INCLUDED_LOGGING
	#define INCLUDED_LOGGING
	#include <fstream>
#endif
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#ifndef INCLUDED_UNITINFO_H
	#include "common/UnitInfo.h"
#endif
#ifndef INCLUDED_REGIONINFO_H
	#include "RegionInfo.h"
#endif
#ifndef INCLUDED_PRINT_H
	#include "common/print.h"
#endif
#ifndef INCLUDED_SCOUTLOCATION_H
	#include "ScoutLocation.h"
#endif
#ifndef INCLUDED_THREATMAP_H
	#include "ThreatMap.h"
#endif
#ifndef INCLUDED_SPATIALPOSITIONMANAGER_H
	#include "clustering/SpatialPositionManager.h"
#endif
#ifndef INCLUDED_CLUSTERMANAGER_H
	#include "clustering/ClusterManager.h"
#endif

extern ofstream logfile;

class StrengthEstimator;
class TargetManager;
// singleton
class GameState
{
public:
	typedef boost::shared_ptr<UnitInfo> UIptr;
	typedef std::map<const BWAPI::Unit*,UIptr> UUImap;
	typedef std::set<UIptr> setUI;

	static GameState& get();
	~GameState();
	/// update game state on enemy unit sighted
	void onBanditSighted(BWAPI::Unit* pUnit);
	/// update game state when enemy unit killed
	void onBanditDestroyed(BWAPI::Unit* pUnit);	// remove from mBandits_
	/// called in onFrameBegin
	void update(); // updates unit information
	void mapAnalyzed();	// call when map analysis finished
	const UUImap& getBandits() const;
	set<UIptr> getEnemyCombatUnits() const;
	
	/// call at beginning each frame
	void onFrameBegin();
	/// call whenever a unitInfo has moved full tile
	void onBanditMoveTile( UIptr pUI );
	/// call when unit mutates
	void onBanditMutate( BWAPI::Unit* pUnit );

	// information functions

	bool isMapAnalyzed() const;

	/// called by ScoutManager, on scoutMissionSuccess
	void updateFrameLastSeen(ScoutLocation& sl);
	
	/// use SPM to calculate, returns by reference set of units in radius
	void getBanditsInRadius( 
		BWAPI::Position center, int rPx, setUI& results );
	/// same as above, but pass in set of units to search
	void getBanditsInRadius( const setUI& unitPopulation,
		BWAPI::Position center, int rPx, setUI& results );
	/// return set of enemy in region
	void getEnemyInRegion( const regionInfo* region, setUI& results );
	/// returns NONE if enemy dead, otherwise last known position
	BWAPI::Position getBanditPosition( const BWAPI::Unit* bandit ) const;
	/// draws the hitboxes and names of units in database that are not visible
	void drawLastSeenInfo() const;
	/// draws clusterID labels on units
	void drawClusterIDs() const;
	/// returns reference to ThreatMap
	ThreatMap& TM() {return *threatMap_;}
	/// returns reference to RegionManager
	//RegionManager& Regions() {return *pRegionMan_;}
	/// return reference to ClusterManager
	ClusterManager& ClusterMan() {return *pClusterMan_;}
	/// returns NULL if not found!
	UIptr getUnitInfo( const BWAPI::Unit* unit ); 

	/// estimate strength
	double unitStrength( BWAPI::UnitType u1, BWAPI::Player* p1,
		BWAPI::UnitType u2, BWAPI::Player* p2 );
	StrengthEstimator& strengthEstimator() {return *pStrengthEstimator_;}
	void calculateTotalStrengths();
	double getMyCombatStrength() const { return mMyStrength; }
	double getEnemyCombatStrength() const { return mEnemyStrength; }

	/// return set of regions where enemy players have units
	std::set<const regionInfo*> getEnemyPresence() const;

	
	int	numEnemyUnitsInRegion(regionInfo* region);
	int	numEnemyFlyingUnitsInRegion(regionInfo* region);
	//getUnitInfo(BWAPI::Broodwar->enemy())

	// display
	/// draw lines from regions to chokes
	void drawMapPaths() const; 
private:
	GameState();
	static GameState* pInstance_;
	bool initialized_;
	double mMyStrength;
	double mEnemyStrength;
	
	/// other players' units 
	UUImap mBandits_; 

	/// where the camera starts the game.
	BWAPI::Position startLocation_;	
	bool isMapAnalyzed_;
	/// Records threatened tiles
	ThreatMap* threatMap_;
	/// finds clusters in unitInfos
	ClusterManager* pClusterMan_;
	/// abstracts out map region information
	//RegionManager* pRegionMan_;
	/// estimate combat strength
	StrengthEstimator* pStrengthEstimator_;

	// helpers
	/// call after constructor complete
	void init();

	//void updateRegionInfo(); // exploration information
	
	/// enters static unit information, call in init
	void addStaticUnitInfo();
};