#include "HarvestManager.h"
#include "common/cpp_includes.h"
#include "MapInfo.h"
#include "HarvestNode.h"
#include "UnitAgent.h"
#include "common/Config.h"
#include "WorkerAgent.h"
#include "UnitManager.h"
#include <algorithm>
#include <math.h>

HarvestManager::HarvestManager()
:mMaxDistHarvestNode(BB.Cfg().get<int>("MaxDistFromDepotLocation"))
{
	//logfile << "HarvestManager constructor\n";
}

void HarvestManager::update()
{
	BOOST_FOREACH( HarvestNode* hn, mNodes )
	{
		hn->update();
		// pull workers from mined out bases
		if (hn->mineralFieldsRemaining() == 0)
		{
			WAset transferWorkers = hn->pullIdleWorkers();
			if( !transferWorkers.empty() ) 
				evenlyAssignWorkers(transferWorkers);
		}
	}
}

void HarvestManager::onNewDepotComplete( UAptr depot )
{
	if (!depot)
	{
		logfile << "Error in HarvestManager onNewDepotComplete: bad ptr.\n";
		return;
	}
	if (!BWAPIext::isResourceDepot(depot->getType()))
	{
		logfile << "Error in HarvestManager onNewDepotComplete: not a depot.\n";
		return;
	}
	// test for proximity to baseLocation
	pair<baseLocInfo*,int> blocDist = 
		MapInfo::get().getNearestRegionalBaseLoc( depot->getPosition() );
	if (blocDist.second <= mMaxDistHarvestNode)
	{
		//Broodwar->printf("depot dist: %d",blocDist.second);
		HarvestNode* tempNode = new HarvestNode(blocDist.first,depot);
		mNodes.push_back(tempNode);
		if (mNodes.size() > 1)
		{
			logfile << "HarvestManager created new HarvestNode for depot, maynarding\n";
			maynard(tempNode);
		}
		else logfile << "HarvestManager placed main depot\n";
		return;
	} // fail too far
	logfile << "Warning: HarvestManager did not create new HarvestNode: distance too far\n";
}

void HarvestManager::addWorker( WAptr worker )
{
	if (mNodes.empty())
	{
		logfile << "Warning in HarvestManager addWorker: no HarvestNodes!\n";
		return;
	}
	//add to nearest HarvestNode; unsaturated
	Position testPos = worker->getPosition();
	HarvestNode* closestNode = NULL;
	int shortestDist = -1;
	BOOST_FOREACH( HarvestNode* node, mNodes )
	{
		// do not add workers to mined out bases.
		if ( node->mineralFieldsRemaining() == 0 ) continue;

		int dist = node->getPosition().getApproxDistance(testPos);
		
		if ( shortestDist == -1 || (dist < shortestDist 
			&& !node->isSaturated()))
		{
			closestNode = node;
			shortestDist = dist;
		}
	}
	if (closestNode)
	{
		closestNode->addWorker(worker);
	}
	else
	{	// if all else fails, just add them to the first node
		(*mNodes.begin())->addWorker(worker);
	}
}

bool HarvestManager::eraseWorker( WAptr worker )
{
	BOOST_FOREACH( HarvestNode* node, mNodes )
	{
		if (node->eraseWorker(worker))
		{
			return true;
		}
	}
	return false;
}
void HarvestManager::drawResourceAreas()
{
	BOOST_FOREACH(HarvestNode* node, mNodes)
	{
		node->displayResourceArea();
	}
}

WAptr HarvestManager::pullNearestWorker( BWAPI::Position pos )
{
	// first get closest harvestNode to pull from
	HarvestNode* closest = NULL;
	int minDist = -1;
	BOOST_FOREACH( HarvestNode* node, mNodes )
	{
		if ( node->workerCount() > 0 )
		{
			int dist = pos.getApproxDistance(node->getPosition());
			if (minDist == -1 || dist < minDist)
			{
				minDist = dist;
				closest = node;
			}
		}
	}
	if (!closest)
	{
		logfile << "Error in HarvestManager pullNearestWorker: no workers!\n";
		WAptr null;
		return null;
	}
	// pull worker and check
	WAptr worker = closest->pullNearestWorker(pos);
	if (!worker)
	{
		logfile << "Error in HarvestManager pullNearestWorker: failed to pull\n";
	}
	else
	{
		logfile << Broodwar->getFrameCount() << " ###pulled worker " << worker->getID() << "\n";
	}
	return worker;
}

void HarvestManager::maynard( HarvestNode* recv )
{
	vector<HarvestNode*> donors = mNodes;
	vector<HarvestNode*>::iterator toErase = std::find(donors.begin(),donors.end(),recv);
	if (toErase != donors.begin())
	{
		donors.erase(toErase);
	}
	
	if (mNodes.empty())
	{
		return;
	}
	list<WAptr> donated;
	vector<HarvestNode*>::iterator di = donors.begin();
	int toDonate = 2;
	for (di; di != donors.end(); ++di)
	{
		HarvestNode* node = *di;
		int workerCount = node->workerCount();
		int minerals = node->mineralFieldsRemaining();
		double workerRatio = (double)workerCount/(double)minerals;
		if (workerCount < 2)
		{
			toDonate = 0;
		}
		else if (workerRatio > workersPerMineral())
		{
			int excess = workerCount - (int)(minerals*workersPerMineral());
			if( excess > 0 )
			{
				toDonate = excess;
			}
		}
		for (int i = 0; i < toDonate; i++)
		{	// pull closest workers to depot; less likely to get stuck
			donated.push_back((*di)->pullNearestWorker((*di)->getPosition()));
		}
	}
	// should now have all donated workers, add to receiving node
	BOOST_FOREACH(WAptr wkr, donated)
	{
		recv->addWorker(wkr);
	}
	donated.clear();
}

void HarvestManager::onMineralDestroyed( UIptr mineral )
{
	BOOST_FOREACH(HarvestNode* node, mNodes)
	{
		node->onMineralDestroyed(mineral);
	}
}

void HarvestManager::reassignWorkersIfMinedOut( HarvestNode* node )
{
	// determine if node is mined out
	if (node->mineralFieldsRemaining() > 0)
	{
		return;
	}
	Broodwar->printf("Reassigning workers from mined-out bases");
	std::set<WAptr> toReassign = node->pullIdleWorkers();
	evenlyAssignWorkers(toReassign);
}

void HarvestManager::evenlyAssignWorkers( std::set<WAptr> workers )
{
	WAset::iterator witer = workers.begin();
	while( witer != workers.end() )
	{
		// find base with least workers per mineral and add a worker
		double minWPM = -1;
		HarvestNode* toAdd = NULL;
		BOOST_FOREACH( HarvestNode* node, mNodes )
		{	// skip over mined out nodes
			if (node->mineralFieldsRemaining() == 0)
				continue;
			double wpm = node->workersPerMineral();
			if ( wpm < minWPM || minWPM == -1 )
			{
				minWPM = wpm;
				toAdd = node;
			}
		}
		if (toAdd)
		{
			WAset::iterator toErase = witer;
			toAdd->addWorker(*witer);
			++witer;
			workers.erase(toErase);
		}
		else // no mining bases, add workers to UnitManager
		{
			logfile << "Warning in HarvestManager: No mining bases remaining!\n";
			BOOST_FOREACH(WAptr worker, workers)
			{
				UnitManager::get().giveUnit(worker);
			}
			workers.clear();
			return;
		}
	}
	workers.clear();
}

bool HarvestManager::haveMiningDepot() const
{
	BOOST_FOREACH( const HarvestNode* node, mNodes )
	{
		if (node->mineralFieldsRemaining() > 0)
		{
			return true;
		}
	}
	return false;
}

void HarvestManager::onDepotDestroyed( UAptr depot )
{
	for (int i = 0; i < mNodes.size(); )
	{
		if (mNodes[i]->getDepot() == depot)
		{
			logfile << "Depot " << i << " was destroyed, transferring workers to ";
			// clear out all workers to UnitManager for now
			set<WAptr> workers = mNodes[i]->pullAllWorkers();
			if (i != 0)
			{
				logfile << "base 0\n";
				BOOST_FOREACH(WAptr worker, workers)
				{
					mNodes[0]->addWorker(worker);
				}
			}
			else
			{
				logfile << "UnitManager\n";
				BOOST_FOREACH(WAptr worker, workers)
				{
					UnitManager::get().giveUnit(worker);
				}
			}
			// remove destroyed depot
			delete mNodes[i];
			mNodes.erase(mNodes.begin() + i);
		}
		else ++i;
	}
}

std::list<Position> HarvestManager::getDepotPositions() const
{
	list<Position> dpos;
	BOOST_FOREACH(HarvestNode* node, mNodes)
	{
		dpos.push_back(node->getPosition());
	}
	return dpos;
}

int HarvestManager::getWorkersNeeded() const
{
	int workerCount = 0;
	int mineralCount = 0;
	for each (HarvestNode* node in mNodes)
	{
		workerCount += node->workerCount();
		mineralCount += node->mineralFieldsRemaining();
	}
	int workersMax = ceil(mineralCount * HarvestManager::workersPerMineral());
	workerCount += BBS::BB.ES.getUnitsOrdered(Broodwar->self()->getRace().getWorker());
	workerCount += BBS::BB.ES.getUnderConstruction(Broodwar->self()->getRace().getWorker());
	int workersNeeded = workersMax - workerCount;
	return workersNeeded;
}

bool HarvestManager::areAllBasesSaturated() const
{
	for each( HarvestNode* node in mNodes )
	{
		if(!node->isSaturated()) return false;
	}
	return true;
}