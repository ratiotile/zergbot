#pragma once
#include <vector>
#include "typedef_UnitAgent.h"
#include "common/Singleton.h"
#include <BWAPI.h>
#include "common/typedefs.h"


class HarvestNode;

class HarvestManager: public Singleton<HarvestManager>
{
public:
	HarvestManager();
	/// call each frame in UnitManager update
	void update();
	/// pass in new Depot unit to check and create HarvestNode
	/// also start worker transfer
	void onNewDepotComplete(UAptr depot);
	/// erase destroyed nodes and reallocate workers
	void onDepotDestroyed(UAptr depot);
	/// pass worker over to HarvestNode control
	void addWorker(WAptr worker);
	bool eraseWorker(WAptr worker);
	/// number of resource bases, UnitManager dont give workers when 0
	int nodeCount()const {return mNodes.size();}
	/// remove the closest worker from mining duties
	WAptr pullNearestWorker(BWAPI::Position pos);
	/// transfer 1/n workers from each expo, where n is number of expos
	void maynard(HarvestNode* recv);
	/// call when mineral is mined out to remove from HarvestNodes
	void onMineralDestroyed(UIptr mineral);

	/// true if we have at least one base that is still mining
	bool haveMiningDepot() const;
	std::list<BWAPI::Position> getDepotPositions() const;
	/// counting any planned and building, how many more workers to reach saturation.
	int getWorkersNeeded() const;
	bool areAllBasesSaturated() const;

	static double workersPerMineral() { return 1.5; }

	//debug
	void drawResourceAreas();
private:
	/// max distance in pixels from depot location to create HarvestNode
	int mMaxDistHarvestNode;
	std::vector<HarvestNode*> mNodes;
	/// if mined out, move workers to other nodes.
	void reassignWorkersIfMinedOut(HarvestNode* node);
	/// splits workers between bases to bring even saturation
	void evenlyAssignWorkers(std::set<WAptr> workers);
};