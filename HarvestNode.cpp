#include "HarvestNode.h"
#include "common/cpp_includes.h"
#include "baseLocInfo.h"
#include "UnitAgent.h"
#include "WorkerAgent.h"
#include "common/typedefs.h"
#include "Algorithms/BnB.h"
#include "MapInfo.h"
#include "common/UnitInfo.h"
#include <limits>
#include "Budget.h"
#include "Blackboard.h"
#include "HarvestManager.h"

HarvestNode::HarvestNode( baseLocInfo* targetBase, UAptr pDepot )
:mBase(targetBase)
,mDepot(pDepot)
,mSplitOffset(0)
, mWorkerRatio(0)
{
	if (!mBase || !pDepot)
	{
		logfile << "Error in HarvestNode constructor: bad pointers!\n";
	}
	SetUI resources = mBase->resources();
	BOOST_FOREACH( UIptr res, resources )
	{
		// must translate into GameState UnitInfo
		UIptr resInfo = GameState::get().getUnitInfo(res->getUnit());
		assert(resInfo);
		if (resInfo->getType() == BWAPI::UnitTypes::Resource_Vespene_Geyser)
			mGeysers.insert(resInfo);
		else mMinerals.insert(resInfo);
	}
	calcResourceArea();
}

void HarvestNode::addWorker( WAptr worker )
{
	if (worker)
	{
		//logfile << "Added worker to HarvestNode\n";
		worker->halt();
		mWorkers.insert(worker);
	}
	else logfile << "Error in HarvestNode: bad worker pointer\n";
}

bool HarvestNode::eraseWorker( WAptr worker )
{
	if (worker)
	{
		if (mWorkers.count(worker) > 0)
		{
			mWorkers.erase(worker);
			return true;
		}
	}
	return false;
}
/// if any workers are idle, make them mine minerals
void HarvestNode::update()
{
	WAlist idleWorkers;
	BOOST_FOREACH(WAptr worker, mWorkers)
	{
		if (!worker->isHarvesting() && !worker->getUnit()->isSelected())
		{
			idleWorkers.push_back(worker);
		}
	}
	if (!idleWorkers.empty())
	{
		splitWorkers(idleWorkers);
	}
	if (isGasNeeded() && workersOnGas() < numRefineries()*3)
	{
		harvestGas();
	}
	// remove consumed minerals
	for each( UIptr mineral in mMinerals )
	{
		if (mineral->getUnit()->getResources() == 0)
		{
			mMinerals.erase(mineral);
			break; // iterator invalidated
		}
	}
	mWorkerRatio = (double)workerCount()/(double)mineralFieldsRemaining();
}

void HarvestNode::splitWorkers( WAlist toBeSplit )
{
	set<UIptr>::iterator imin = mMinerals.begin();
	if (imin == mMinerals.end())
	{
		logfile << "Error in HarvestNode splitWorkers: no resources in set.\n";
		return;
	}
	// cycle through mineral fields to reach previous position
	if ( mSplitOffset >= mMinerals.size() ) mSplitOffset = 0;
	assert(mSplitOffset < mMinerals.size());
	for (int i = 0; i < mSplitOffset; ++i )
	{
		++imin;
	}
	BOOST_FOREACH(WAptr worker, toBeSplit)
	{
		worker->gather((*imin)->getUnit());
		++imin;
		++mSplitOffset;
		if( imin == mMinerals.end() )
		{
			imin = mMinerals.begin();
			mSplitOffset = 0;
		}
	}
}

BWAPI::Position HarvestNode::getPosition() const
{
	return mDepot->getPosition();
}

void HarvestNode::calcResourceArea()
{
	vector<BWAPI::Position> points;
	BWAPI::Position tempPos;
	set<UIptr>::iterator ci = mMinerals.begin();
	for (ci; ci != mMinerals.end();ci++) // add all the mineral patches locations
		points.push_back((*ci)->getPosition());
	for (ci = mGeysers.begin(); ci != mGeysers.end(); ci++ ) // add all the geyeser locations
		points.push_back((*ci)->getPosition());
	//points.push_back(mDepot->getPosition()); // add nexus location, all done adding
	// old approach, generating shortest path, not needed
	/*
	// generate adjacency matrix
	int** W = new int*[points.size()];
	for (unsigned int i = 0; i < points.size(); i++){
		W[i] = new int[points.size()];
		for (unsigned int j = 0; j < points.size(); j++){
			if ( i == j ) W[i][j] = 0;
			else W[i][j] = points[i].getApproxDistance(points[j]);	
		}
	}
	//	print2DArray(W,points.size());
	resourceAreaBoundary.clear();
	BnB branchBound;
	vector<unsigned int> hamilton;
	if (points.size() < 13) // small enough
	{
		branchBound.travel(points.size(),W);
		hamilton = branchBound.getTour();
		for (unsigned int i = 0; i < hamilton.size()-1; i++ ) // last index is start
			resourceAreaBoundary.push_back(points[hamilton[i]]);
	}
	else
	{
		branchBound.greedy(points.size(),W);
		hamilton = branchBound.getGreedyTour();
		for (unsigned int i = 0; i < hamilton.size()-1; i++ ) // last index is start
			resourceAreaBoundary.push_back(points[hamilton[i]]);
		simplifyResourceArea();
	}
	*/
	MapInfo::get().addResourceArea(points,mDepot->getPosition());
	// delete W
// 	for (unsigned int i = 0; i < points.size(); i++){
// 		delete[] W[i];}
// 	delete[] W;
}

void HarvestNode::displayResourceArea() const
{
	for(int j=0;j<(int)resourceAreaBoundary.size();j++)
	{
		Position point1=resourceAreaBoundary[j];
		Position point2=resourceAreaBoundary[(j+1) % resourceAreaBoundary.size()];
		Broodwar->drawLine(BWAPI::CoordinateType::Map,point1.x(),point1.y(),point2.x(),point2.y(),
			BWAPI::Colors::Green);
	}
}

void HarvestNode::simplifyResourceArea()
{
	int firstN;
	int secondN;
	int dist_iTo1;
	int dist_1To2;
	int dist_iTo2;
	for( int i = 0; i < resourceAreaBoundary.size(); )
	{
		firstN = i + 1;
		if(firstN >= resourceAreaBoundary.size()) 
			firstN -= resourceAreaBoundary.size();
		secondN = i + 2;
		if(secondN >= resourceAreaBoundary.size())
			secondN -= resourceAreaBoundary.size();
		assert( i < resourceAreaBoundary.size() );
		assert( firstN < resourceAreaBoundary.size() );
		assert( secondN < resourceAreaBoundary.size() );
		// condition to eliminate neighbor 1
		dist_iTo1 = resourceAreaBoundary[i].getApproxDistance(
			resourceAreaBoundary[firstN]);
		dist_1To2 = resourceAreaBoundary[firstN].getApproxDistance(
			resourceAreaBoundary[secondN]);
		dist_iTo2 = resourceAreaBoundary[i].getApproxDistance(
			resourceAreaBoundary[secondN]);
		if (dist_iTo1 + dist_1To2 > 2.9*dist_iTo2)
		{
			resourceAreaBoundary.erase(resourceAreaBoundary.begin()+firstN);
		}
		else ++i;
	}
}

WAptr HarvestNode::pullNearestWorker( BWAPI::Position pos )
{
	WAptr closest = findNearestWorker(pos);
	assert(closest);
	mWorkers.erase(closest);
	return closest;
}

void HarvestNode::onMineralDestroyed( UIptr mineral )
{
	if (mMinerals.count(mineral))
	{
		mMinerals.erase(mineral);
	}
}

double HarvestNode::workersPerMineral() const
{
	if (mineralFieldsRemaining() == 0)
	{
		return numeric_limits<double>::infinity();
	}
	return (double)workerCount()/mineralFieldsRemaining();
}

std::set<WAptr> HarvestNode::pullIdleWorkers()
{
	set<WAptr> idleWorkers;
	if (mWorkers.empty()) return idleWorkers;
	WAset::iterator witer = mWorkers.begin();
	while( witer != mWorkers.end() )
	{
		if ((*witer)->isIdle())
		{
			idleWorkers.insert(*witer);
			WAset::iterator toErase = witer;
			++witer;
			mWorkers.erase(toErase);
		}
		else ++witer;
	}
	return idleWorkers;
}

std::set<WAptr> HarvestNode::pullAllWorkers()
{
	set<WAptr> allWorkers = mWorkers;
	mWorkers.clear();
	return allWorkers;
}

int HarvestNode::numRefineries() const
{
	if (mGeysers.empty())
	{
		return 0;
	}
	// count number of finished geysers
	int count = 0;
	BOOST_FOREACH(UIptr ref, mGeysers)
	{
		if (ref->getOwner() == Broodwar->self() && ref->getUnit()->isCompleted())
		{
			count++;
		}
	}
	return count;
}

bool HarvestNode::isGasNeeded() const
{
	return true; // temp to get harvesting faster.
	if (Budget::get().isGasShortfall())
	{
		return true;
	}
	return false;
}

void HarvestNode::harvestGas()
{
	if (numRefineries() < 1) return;
	int onGas = workersOnGas();
	int optimum = numRefineries()*3;
	int needed = optimum - onGas;
	int available = mWorkers.size() - onGas;
	needed = min(needed, available);
	logfile << "Sending " << needed << " Workers to harvest gas\n";
	BOOST_FOREACH(UIptr refinery, mGeysers)
	{
		if (refinery->getOwner() == Broodwar->self() && refinery->getUnit()->isCompleted())
		{
			WAset gatheringHere = findWorkersGathering(refinery);
			int neededHere = std::min(needed, (int)(3 - gatheringHere.size()));
			logfile << "Need " << neededHere << " workers here.\n";
			
			for (int i = 0; i < needed; i++)
			{
				WAptr worker = findNearestWorker(refinery->getPosition());
				if (worker)
				{
					worker->gather(refinery->getUnit());
					logfile << "Ordering worker to gather gas\n";
				}
				else
				{
					logfile << "Warning: HarvestNode.HarvestGas - bad Worker ptr\n";
				}
			}
			needed -= neededHere;
		}
	}
	
}

int HarvestNode::workersOnGas() const
{
	int count = 0;
	BOOST_FOREACH(WAptr worker, mWorkers)
	{
		if (worker->getUnit()->isGatheringGas())
		{
			count++;
		}
	}
	return count;
}

WAptr HarvestNode::findNearestWorker( BWAPI::Position pos ) const
{
	WAptr closest;
	if(mWorkers.empty()) return closest;

	int minDist = -1;
	int carrymod = 5*32;
	int gasmod = 10*32;
	// try for minerals gatherers noncarry first
	BOOST_FOREACH( WAptr wkr, mWorkers )
	{
		int dist = pos.getApproxDistance(wkr->getPosition());
		if(wkr->getUnit()->isGatheringGas()) dist += gasmod;
		if(wkr->getUnit()->isCarryingGas() || wkr->getUnit()->isCarryingMinerals()) dist += carrymod;
		if (minDist == -1 || dist < minDist)
		{
			minDist = dist;
			closest = wkr;
		}
	}
	assert(closest);
	return closest;
}

WAset HarvestNode::findWorkersGathering( UIptr res ) const
{
	WAset gatherers;
	BOOST_FOREACH(WAptr worker, mWorkers)
	{
		if (worker->getGatherTarget() == res->getUnit())
		{
			gatherers.insert(worker);
		}
	}
	return gatherers;
}

bool HarvestNode::isSaturated() const
{
	if (mWorkerRatio >
		HarvestManager::workersPerMineral())
	{
		return true;
	}
	return false;
}