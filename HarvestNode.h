#pragma once
#include "typedef_UnitAgent.h"
#include <BWAPI.h>
#include <vector>
#include "common/typedefs.h"
class baseLocInfo;

class HarvestNode
{
public:
	HarvestNode(baseLocInfo* targetBase, UAptr pDepot );
	/// call each frame from HarvestManager
	void update();
	void addWorker(WAptr worker);
	bool eraseWorker(WAptr worker);
	/// remove all idle workers
	std::set<WAptr> pullIdleWorkers();
	/// remove all workers
	std::set<WAptr> pullAllWorkers();
	WAptr pullNearestWorker( BWAPI::Position pos );
	/// remove the mineral from mMinerals
	void onMineralDestroyed(UIptr mineral);

	/// center position of Resource depot
	BWAPI::Position getPosition() const;
	UAptr getDepot() const { return mDepot; }
	int workerCount() const {return mWorkers.size();}
	/// are there any mineral fields left?
	int mineralFieldsRemaining() const {return mMinerals.size();}

	/// get workers per mineral field
	double workersPerMineral() const;
	/// how many workers on gas
	int workersOnGas() const;
	/// do we have gas, and a completed extractor on it?
	int numRefineries() const;
	bool isSaturated() const;



	/// draw polygon around resource area
	void displayResourceArea() const;
private:
	std::set<UIptr> mMinerals;
	std::set<UIptr> mGeysers;
	/// increments on each worker so they spread out among minerals better.
	int mSplitOffset;
	WAset mWorkers;
	baseLocInfo* mBase;
	UAptr mDepot;
	double mWorkerRatio;
	/// holds points making a polygon around Depot and resources
	std::vector<BWAPI::Position> resourceAreaBoundary; 

	void splitWorkers(WAlist toBeSplit);
	void calcResourceArea();
	void simplifyResourceArea();
	
	/// is gas needed?
	bool isGasNeeded() const;
	
	/// redirect up to 3 workers to mine gas
	void harvestGas();
	/// return WAptr to nearest worker
	WAptr findNearestWorker( BWAPI::Position pos ) const;
	/// return set containing all workers gathering that resource
	WAset findWorkersGathering( UIptr res ) const;
};