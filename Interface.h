#include <BWAPI.h>
#include <boost/shared_ptr.hpp>

typedef BWAPI::TilePosition TilePosition;
typedef BWAPI::Position Position;
using BWAPI::Player;
using BWAPI::Broodwar;

class UnitClass;
typedef boost::shared_ptr<UnitClass> Unit;

#include "common/WalkTile.h"
#include "Unit.h"

using std::string;
using std::vector;