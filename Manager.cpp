#include "Manager.h"
#include "common/cpp_includes.h"
#include <list>
#include "Tasks/Task.h"

Manager::~Manager(){}

void Manager::processTasks()
{
	// eliminate completed or failed tasks
	list<pTask>::iterator it = mTasks.begin();
	for (it; it != mTasks.end(); )
	{
		pTask task = *it;
		if (task->isComplete() )
		{
// 			logfile << Broodwar->getFrameCount() << " "<< mName <<": "
// 				<< task->getName()
// 				<< " task complete, terminating. "
// 				<< mTasks.size()-1 << " tasks remain\n";
			task->terminate();
			it = mTasks.erase(it);
		}
		else if (task->isFailed() )
		{
			logfile << Broodwar->getFrameCount() << " "<< mName <<": "
				<< task->getName()
				<< " task failed, terminating. "
				<< mTasks.size()-1 << " tasks remain\n";
			task->terminate();
			it = mTasks.erase(it);
		}
		else
		{
			task->update();
			++it;
		}
	}
}

Manager::Manager()
:mName("Manager")
{

}

Manager::Manager( std::string name )
:mName(name)
{

}