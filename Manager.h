/**
Manager class is for abstract agents that control higher level functions.
Managers can create tasks and run them.
*/

#pragma once
#ifndef __MANAGER_H__
#define __MANAGER_H__


#include <list>
#include "typedef_Task.h"

class Manager
{

public:
	Manager();
	Manager(std::string name);
	virtual ~Manager();

	virtual void processTasks();
	
protected:
	std::list<pTask> mTasks;
	std::string mName;
};


#endif