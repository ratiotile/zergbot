#include "MapInfo.h"
#include "common/cpp_includes.h"
#include "TABW/Region.h"
#include "TABW/TABW.h"
#include "TABW/ResourceCluster.h"
#include "TABW/Chokepoint.h"
#include "TABW/TABW_typedefs.h"
#include "regionInfo.h"
#include "chokeInfo.h"
#include "baseLocInfo.h"
#include <map>
//#include <BWTA.h>
#include "Blackboard.h"
#include "common/Config.h"
#include "GameState.h"
// for exit paths
#include "Pathfinder.h"
#include "pathfinder/BuildTilePath.h"
#include <boost/lexical_cast.hpp>
#include "TimeManager.h"
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include "OverlordPositions/OverlordPositions.h"
#include <limits>

// for UAB scouting
#define SELF_INDEX 0
#define ENEMY_INDEX 1

using namespace BWAPIext;
using namespace BBS;

MapInfo::MapInfo():
mIsMapAnalyzed(false),
mStartLocation(Broodwar->self()->getStartLocation()),
mEnemyStart(BWAPI::TilePositions::Unknown),
mDepotDistToOwnBaseLoc(BB.Cfg().get<int>("MaxDistFromDepotLocation")),
rows(Broodwar->mapHeight()),
cols(Broodwar->mapWidth()),
mIsBaseLocDistCalculated(false)
{
	map			= std::vector<bool>(rows*cols, false);
	fringe		= std::vector<int> (rows*cols, 0);
}

void MapInfo::init()
{
	TimeManager::getInstance().startTimer(TimeManager::MI_convert);
	mIsMapAnalyzed = true;
	set<TABW::pRegion> TABW_regions = TABW::getRegions();
	BOOST_FOREACH(TABW::pRegion region, TABW_regions)
	{
		regionInfo* pRegionInfo = new regionInfo(region);

		mTABW_regionToRegionInfo.insert(make_pair(region,pRegionInfo));
		mRegions.insert(pRegionInfo);
		// fill base locations(resource clusters)
		set<TABW::pResourceCluster> tempClusters = region->resourceClusters();
		BOOST_FOREACH(TABW::pResourceCluster cluster, tempClusters)
		{
			baseLocInfo* pBLI = new baseLocInfo(cluster);
			pBLI->region = pRegionInfo;

			pRegionInfo->baseLocs.insert(pBLI);
			mBaseLocs.insert(pBLI);
			pRegionInfo->numLocations++;
		}
		// add chokes
		set<TABW::wpChoke> tempChokes = region->chokepoints();
		std::map<TABW::pChoke,chokeInfo*>::iterator found;
		BOOST_FOREACH(TABW::wpChoke wChoke, tempChokes)
		{
			// first try to find in mTABW_chokeToChokeInfo
			if (TABW::pChoke choke = wChoke.lock() )
			{
				//map<TABW::pChoke,chokeInfo*>::iterator found;
				found = mTABW_chokeToChokeInfo.find(choke);
				// if not found, create new chokeInfo and add
				if (found == mTABW_chokeToChokeInfo.end())
				{
					chokeInfo* pCI = new chokeInfo(choke);

					mChokes.insert(pCI);
					mTABW_chokeToChokeInfo.insert(make_pair(choke,pCI));
					pRegionInfo->chokes.insert(pCI);
					pRegionInfo->numLocations++;
				}
				else // choke already added to master set
				{// simply add to regionInfo
					chokeInfo* foundCInfo = (*found).second;
					pRegionInfo->chokes.insert(foundCInfo);
					pRegionInfo->numLocations++;
				}
			} 
			// if invalid can do nothing so skip this one
		}
	}
	// finish init chokes
	set<TABW::pChoke> tempChokes = TABW::getChokepoints();
	std::map<TABW::pChoke,chokeInfo*>::iterator found;
	BOOST_FOREACH( TABW::pChoke choke, tempChokes )
	{
		// find corresponding chokeInfo
		//TABW::pChoke choke = wChoke.lock();
		
		if (choke)
		{
			found = mTABW_chokeToChokeInfo.find(choke);
			if (found != mTABW_chokeToChokeInfo.end())
			{
				TABW::pRegion pR1 = choke->getRegions().first;
				TABW::pRegion pR2 = choke->getRegions().second;
				assert( pR1 && pR2 );
				regionInfo* r1 = mTABW_regionToRegionInfo.find(pR1)->second;
				regionInfo* r2 = mTABW_regionToRegionInfo.find(pR2)->second;
				(*found).second->setRegions(r1,r2);
			}
		}
	}
	
	// now that regions are established, fill mWalkTileToRegion
	mMapWidthWalkTile = TABW::mapWidthWT();
	mMapHeightWalkTile = TABW::mapHeightWT();
	mWalkTileToRegion.resize(mMapWidthWalkTile,mMapHeightWalkTile,0);
	for (int x = 0; x < mMapWidthWalkTile; ++x)
	{
		for (int y = 0; y < mMapHeightWalkTile; ++y)
		{
			WalkTile wt(x,y);
			TABW::pRegion tr = TABW::getRegion(wt);
			// should be NULL if not part of any region
			mWalkTileToRegion[x][y] = TABWtoRegionInfo(tr);
		}
	}
	mStartLocation = Broodwar->self()->getStartLocation();
	// initialize mBuildable
	mBuildable.resize(Broodwar->mapWidth(), Broodwar->mapHeight(), TileBuildablity::yes);
	for (int x = 0; x < Broodwar->mapWidth(); x++)
	{
		for (int y = 0; y< Broodwar->mapHeight(); y++)
		{
			if ( !Broodwar->isBuildable(x,y) )
			{
				mBuildable[x][y] = TileBuildablity::walkonly;
				// if cannot walk either
				int wx = BWAPIext::BtoW(x);
				int wy = BWAPIext::BtoW(y);
				if ( !Broodwar->isWalkable(wx,wy) )
				{
					mBuildable[x][y] = TileBuildablity::nowalk;
				}
			}
		}
	}
	// add all static resource positions
	set< BWAPI::Unit*> neutrals = Broodwar->getStaticNeutralUnits();
	BOOST_FOREACH(  BWAPI::Unit* unit, neutrals )
	{
		if (!unit->getType().canMove())
		{
			TilePosition TL(unit->getInitialTilePosition());
			TilePosition BR(TL.x()+unit->getType().tileWidth()
				,TL.y()+unit->getType().tileHeight());
			for (int i = TL.x(); i < BR.x(); i++)
			{
				for (int j = TL.y(); j < BR.y(); j++)
				{
					mBuildable[i][j] = TileBuildablity::nowalk;
				}
			}
		}
	}
	TimeManager::getInstance().stopTimer(TimeManager::MI_convert);
	TimeManager::getInstance().startTimer(TimeManager::MI_RD);
	calculateRegionDistances();
	TimeManager::getInstance().stopTimer(TimeManager::MI_RD);
	TimeManager::getInstance().startTimer(TimeManager::MI_BLD);
	// start thread TODO: synchronization
 	assert(!mThread);
	mThread = boost::shared_ptr<boost::thread>(new boost::thread(
		boost::bind(&MapInfo::calculateBaseLocDistances, this)));
	TimeManager::getInstance().stopTimer(TimeManager::MI_BLD);
	// figure out where the enemy is on 2-player maps
	mMainBaseLocations[Broodwar->enemy()] = NULL;
	mMainBaseLocations[Broodwar->self()] = NULL;
	set<TilePosition> startLocations = Broodwar->getStartLocations();
	if (startLocations.size() == 2)
	{
		startLocations.erase(mStartLocation);
		mEnemyStart = *startLocations.begin();
		OverlordPositions::get().onEnemyStartLocFound(mEnemyStart);
		BOOST_FOREACH(baseLocInfo* base, mBaseLocs)
		{
			if (getRegion(base->center()) == getRegion(mEnemyStart))
			{
				mMainBaseLocations[Broodwar->enemy()] = base;
			}
			if (getRegion(base->center()) == getRegion(mStartLocation))
			{
				mMainBaseLocations[Broodwar->self()] = base;
			}
		}
	}
	setBWAPIMapData();
	calculateNaturals();
	if (mEnemyStart)
	{
		const regionInfo* startRegion = getRegion(mStartLocation);
		const regionInfo* enemyRegion = getRegion(mEnemyStart);
		assert( startRegion && enemyRegion );
		mPathToEnemyMain = getRegionPath(startRegion,enemyRegion);
		if (!mPathToEnemyMain.empty() && mPathToEnemyMain.size() > 3)
		{
			BBS::BB.rallyPoint = mPathToEnemyMain[2]->center();
		}
	}
}

BWAPI::TilePosition MapInfo::getNearestSafePosition( BWAPI::Position pos ) const
{
	return Broodwar->self()->getStartLocation();
}

void MapInfo::drawRegionPaths() const
{
	if (!mIsMapAnalyzed)
	{
		return;
	}
	if (!mPathToEnemyMain.empty())
	{
		for (vector<const regionInfo*>::const_iterator iter = mPathToEnemyMain.begin();
			iter != mPathToEnemyMain.end(); ++iter )
		{
			// if there is a next, draw line from one to next along choke
			vector<const regionInfo*>::const_iterator next = iter;
			next++;
			if (next != mPathToEnemyMain.end())
			{
				const regionInfo* r1 = *iter;
				assert ( r1 );
				const regionInfo* r2 = *next;
				assert (r2);
				chokeInfo* c = NULL;
				for each ( chokeInfo* choke in r1->chokes )
				{
					if (choke->regions().first == r1 && choke->regions().second == r2 ||
						choke->regions().first == r2 && choke->regions().second == r1 )
					{
						c = choke;
						break;
					}
				}
				assert( c != NULL );
				
				Broodwar->drawLineMap(c->center().x(),c->center().y(),r1->center().x(),r1->center().y(),
					BWAPI::Colors::Red);
				Broodwar->drawLineMap(c->center().x(),c->center().y(),r2->center().x(),r2->center().y(),
					BWAPI::Colors::Red);
			}
		}
		return;
	}
	BOOST_FOREACH( regionInfo* region , mRegions)
	{
		assert(region);
		Position cPos = region->center();
		if (region->isIsland() || region->ID() > 20)
		{
			continue;
		}
		/*
		BOOST_FOREACH( baseLocInfo& BLI, regionPair.second->baseLocs)
		{
			Position iPos = BLI.depotPosition->getPosition();
			Broodwar->drawLineMap(cPos.x(),cPos.y(),iPos.x(),iPos.y(),
				BWAPI::Colors::Green);
		}
		*/
		BOOST_FOREACH( chokeInfo* choke, region->chokes )
		{
			assert(choke);
			Position iPos = choke->center();
			BWAPI::Color lineColor = BWAPI::Colors::Orange;
			if (choke->isClear)
			{
				lineColor = BWAPI::Colors::Blue;
			}
			
			Broodwar->drawLineMap(cPos.x(),cPos.y(),iPos.x(),iPos.y(),
				lineColor);
			// test region distances
		}
		
	}
	// draw region distances
	BOOST_FOREACH( chokeInfo* choke, mChokes )
	{
		std::pair<regionInfo*,regionInfo*> rPair(choke->regions().first,choke->regions().second);
		int dist = mRPaths.getRDist(rPair.first->ID(),rPair.second->ID());
		Broodwar->drawTextMap(choke->center().x(),choke->center().y(),"%d:%d",
			choke->ID(), dist);
// 		regionInfo* r1 = choke->regions().first;
// 		regionInfo* r2 = choke->regions().second;
// 		BWAPI::Color lineColor = BWAPI::Colors::Orange;
// 		
// 		BOOST_FOREACH( chokeInfo* c2, r1->chokes )
// 		{
// 			if (choke->ID() == c2->ID()) continue;
// 			if (choke->isClear && c2->isClear)
// 			{
// 				lineColor = BWAPI::Colors::Blue;
// 			}
// 			Broodwar->drawLineMap(choke->center().x(),choke->center().y(),
// 				c2->center().x(),c2->center().y(), lineColor);
// 		}
// 		BOOST_FOREACH( chokeInfo* c2, r2->chokes )
// 		{
// 			if (choke->ID() == c2->ID()) continue;
// 			if (choke->isClear && c2->isClear)
// 			{
// 				lineColor = BWAPI::Colors::Blue;
// 			}
// 			Broodwar->drawLineMap(choke->center().x(),choke->center().y(),
// 				c2->center().x(),c2->center().y(), lineColor);
// 		}
	}
}

void MapInfo::drawRegionInfo() const
{
	BOOST_FOREACH( regionInfo* r, mRegions )
	{
		assert(r);
		r->drawRegionInfo();
	}
}

regionInfo* MapInfo::TABWtoRegionInfo( TABW::pRegion pr )
{
	if (!pr) return NULL;
	else return mTABW_regionToRegionInfo[pr];
}


const regionInfo* MapInfo::getRegion( BWAPIext::WalkTile walkPos )const
{
	return mWalkTileToRegion[walkPos.x()][walkPos.y()];
}
const regionInfo* MapInfo::getRegion( BWAPI::TilePosition tilePos )const
{
	WalkTile wt(tilePos);
	return getRegion(wt);
}

const regionInfo* MapInfo::getRegion( BWAPI::Position pos )const
{
	WalkTile wt(pos);
	return getRegion(wt);
}

const regionInfo* MapInfo::getRegion( int ID ) const
{
	for each ( regionInfo* region in mRegions )
	{
		if (region->ID() == ID)
		{
			return region;
		}
	}
	return NULL;
}

void MapInfo::drawResources() const
{
	BOOST_FOREACH(baseLocInfo* bloc, mBaseLocs)
	{
		int left =  BtoP(bloc->depotPosition().x());
		int top = BtoP(bloc->depotPosition().y());
		int right = left + 4*32;
		int bot = top + 3*32;
		Broodwar->drawBoxMap(left,top,right,bot,BWAPI::Colors::Orange);
		BOOST_FOREACH(UIptr resource, bloc->resources())
		{
			left = resource->leftPx();
			top = resource->topPx();
			right = resource->rightPx();
			bot = resource->bottomPx();
			Broodwar->drawBoxMap(left,top,right,bot,BWAPI::Colors::Cyan);
		}
	}
}

void MapInfo::onMapAnalyzed()
{
	init();
}

std::pair<baseLocInfo*,int> MapInfo::getNearestRegionalBaseLoc( BWAPI::Position pos ) const
{
	const regionInfo* region = getRegion(pos);
	if (!region)
	{
		return pair<baseLocInfo*,int>(NULL,0);
	}
	assert(region);
	baseLocInfo* closest = NULL;
	int closestDist = -1;
	BOOST_FOREACH( baseLocInfo* bloc, region->baseLocs )
	{
		int d = bloc->center().getApproxDistance(pos);
		if ( closestDist == -1 || d < closestDist )
		{
			closestDist = d;
			closest = bloc;
		}
	}
	return make_pair(closest,closestDist);
}

void MapInfo::drawBuildability() const
{
	// window the draw so not to waste resources
	Position screenPos = Position(Broodwar->getScreenPosition());
	if (screenPos == BWAPI::Positions::Unknown || screenPos == BWAPI::Positions::Invalid)
	{ return;}
	TilePosition tScreenPos = TilePosition(screenPos);
	int screenW = 640/32;
	int screenH = 384/32;
	if(screenW + tScreenPos.x() >= Broodwar->mapWidth()) screenW -= 1;
	if(screenH + tScreenPos.y() >= Broodwar->mapHeight()) screenH -= 1;
	assert(screenW + tScreenPos.x() < Broodwar->mapWidth() );
	assert(screenH + tScreenPos.y() < Broodwar->mapHeight() );
	for (size_t i = tScreenPos.x(); i < screenW + tScreenPos.x(); i++)
	{
		for (size_t j = tScreenPos.y(); j < screenH + tScreenPos.y(); j++)
		{
			if (mBuildable[i][j] == TileBuildablity::resourcing)
			{
				Broodwar->drawBoxMap(i*32,j*32,i*32+33,j*32+33,BWAPI::Colors::Grey);
				Broodwar->drawTextMap(i*32+16,j*32+16,"R");
			}
			else if (mBuildable[i][j] == TileBuildablity::walkonly)
			{
				Broodwar->drawBoxMap(i*32,j*32,i*32+33,j*32+33,BWAPI::Colors::Grey);
				Broodwar->drawTextMap(i*32+16,j*32+16,"W");
			}
			else if (mBuildable[i][j] == TileBuildablity::nowalk)
			{
				Broodwar->drawBoxMap(i*32,j*32,i*32+33,j*32+33,BWAPI::Colors::Grey);
				Broodwar->drawTextMap(i*32+16,j*32+16,"X");
			}
			else if (mBuildable[i][j] == TileBuildablity::bib)
			{
				Broodwar->drawBoxMap(i*32,j*32,i*32+33,j*32+33,BWAPI::Colors::Grey);
				Broodwar->drawTextMap(i*32+16,j*32+16,"B");
			}
			else if (mBuildable[i][j] == TileBuildablity::reserved)
			{
				Broodwar->drawBoxMap(i*32,j*32,i*32+33,j*32+33,BWAPI::Colors::Grey);
				Broodwar->drawTextMap(i*32+16,j*32+16,"p");
			}
		}
	}
}
// to be replaced with new method without using polygon
void MapInfo::addResourceArea( std::vector<BWAPI::Position> points )
{/*
	BWTA::Polygon resAreaPoly;
	int minY = -1;
	int minX = -1;
	int xMax = -1;
	int yMax = -1;
	for (unsigned int i = 0; i < points.size(); i++)
	{
		resAreaPoly.push_back(points[i]);
		if (minY == -1 || points[i].y() < minY) minY = points[i].y();
		if (minX == -1 || points[i].x() < minX) minX = points[i].x();
		if (yMax == -1 || points[i].y() > yMax) yMax = points[i].y();
		if (xMax == -1 || points[i].x() > xMax) xMax = points[i].x();
	}
	// transform  to tile coordinates
	minY = BWAPIext::PtoB(minY);
	minX = BWAPIext::PtoB(minX);
	yMax = BWAPIext::PtoB(yMax);
	xMax = BWAPIext::PtoB(xMax);
	assert(xMax < Broodwar->mapWidth());
	assert(minX >= 0);
	assert(yMax < Broodwar->mapHeight());
	assert(minY >= 0);
	for (int x = minX; x < xMax; x++)
	{
		for (int y = minY; y< yMax; y++)
		{
			TilePosition tPos(x,y);
			if ( resAreaPoly.isInside(BWAPIext::getCenter(tPos)) )
			{
				mBuildable[x][y] = TileBuildablity::resourcing;
			}
		}
	}*/
}

void MapInfo::addResourceArea( std::vector<BWAPI::Position> resources, BWAPI::Position depot )
{
	markExitPaths(TilePosition(depot));
	logfile << "adding Resource Area, "<< resources.size() << " Depot:" <<depot.x() <<", "<<depot.y() <<"\n";
	BOOST_FOREACH(BWAPI::Position resPos, resources)
	{
		//logfile << "resource loc: "<< resPos.x() << ", " << resPos.y() << std::endl;
		int dx = depot.x() - resPos.x(); // dx
		int dy = depot.y() - resPos.y(); // dy
		double am = abs( (double)dy/(double)dx );
		// make line from resPos to depot
		int ix = 1;
		int iy = 1;
		if (dx < 0) ix = -1;
		if (dy < 0) iy = -1;
		//logfile << "dx:"<<dx<<" dy:"<<dy<<" am:" <<am<<" ix:"<< ix<<" iy:"<<iy <<std::endl;
		int x = resPos.x();
		int y = resPos.y();
		// make tilePosition
		BWAPI::TilePosition lastTile = BWAPI::TilePosition(BWAPI::Position(x,y));
		while( x != depot.x() || y != depot.y() )
		{
			dx = depot.x() - x;
			dy = depot.y() - y;
			//logfile <<"d:" <<dx<<","<<dy<<" p:"<< x << "," << 
			//y << std::endl;
			// check and mark tile.
			BWAPI::TilePosition tile = BWAPI::TilePosition(BWAPI::Position(x,y));
			if ( tile != lastTile && tile.isValid() )
			{
				lastTile = tile;
				if ( mBuildable[tile.x()][tile.y()] != TileBuildablity::nowalk )
				{
					mBuildable[tile.x()][tile.y()] = TileBuildablity::resourcing;
				}
			}
			if ( y != depot.y() )
			{
				if (abs( (double)dy/(double)dx ) > am )
				{
					y += iy;
					continue;
				}
			}
			else if( x != depot.x()) x += ix;
			if ( x != depot.x() ) x += ix;
			else if( y != depot.y() )y += iy;
		}
	}
	
}

void MapInfo::markExitPaths( BWAPI::TilePosition depot )
{
	depot.x() += 1;
	depot.y() += 3;
	// get region
	const regionInfo* region = getRegion(depot);
	BOOST_FOREACH( chokeInfo* choke, region->chokes )
	{
		BuildTilePath exitPath = Pathfinder::getInstance().CreateTilePath(depot, 
			TilePosition(choke->center()));
		logfile << "Path found, length: " << exitPath.path.size() <<"\n"; 
		// for each tile in path, mark as bib
		BOOST_FOREACH( TilePosition tp, exitPath.path )
		{
			if (mBuildable[tp.x()][tp.y()] == TileBuildablity::yes)
			{
				mBuildable[tp.x()][tp.y()] = TileBuildablity::bib;
			}
		}
	}
}
// if zerg structure, check for creep
bool MapInfo::canBuildHere( BWAPI::TilePosition buildPos, BWAPI::UnitType building ) const
{
	TilePosition TL(buildPos);
	TilePosition BR(buildPos.x()+building.tileWidth(), 
		buildPos.y()+building.tileHeight());
	// check out of bounds
	if (buildPos.x()<0 || buildPos.y()<0 
		|| BR.x()>=Broodwar->mapWidth() || BR.y()>=Broodwar->mapHeight())
	{
		return false;
	}
	// check BWAPI's builtin
	if ( !Broodwar->canBuildHere(NULL,buildPos, building ) )
	{
		return false;
	}
	// amount of open space around building

	int bibX = 0;
	int bibY = 0;
	TilePosition b_BR(BR.x()+bibX, BR.y()+bibY);
	TilePosition b_TL(buildPos.x()-bibX, buildPos.y()-bibY);

	if (b_BR.x() >= Broodwar->mapWidth()) b_BR.x()--;
	if (b_BR.y() >= Broodwar->mapHeight()) b_BR.y()--;
	if (b_TL.x() < 0) b_TL.x() = 0;
	if (b_TL.y() < 0) b_TL.y() = 0;

	assert(b_BR.isValid() && b_TL.isValid());

	// check buildmap for bib
	for( int x = b_TL.x(); x < b_BR.x(); x++ )
	{
		for ( int y = b_TL.y(); y < b_BR.y(); y++)
		{
			if (mBuildable[x][y] != TileBuildablity::yes)
			{
				return false;
			}
		}
	}

	
	// check for units in the way
	Position ptl(b_TL);
	Position pbr(b_BR);
	set< BWAPI::Unit*> obs = Broodwar->getUnitsInRectangle(ptl,pbr);
	// remove all our mobile units
	set< BWAPI::Unit*>::iterator oi = obs.begin();
	for (oi; oi != obs.end(); )
	{
		 BWAPI::Unit* u = *oi;
		if (u->getPlayer() == Broodwar->self() && u->getType().canMove())
		{
			oi = obs.erase(oi);
		}
		else ++oi;
	}
	if (!obs.empty()) // something in the way
	{
		return false;
	}

	return true;
}

bool MapInfo::isTileWalkable( BWAPI::TilePosition position ) const
{
	if ( !position.isValid() )
	{
		logfile << "Warning: isTileWalkable invalid position ("<<position.x()<<","<<position.y()
			<<"), map dim:[" << BWAPIext::WtoB(mMapWidthWalkTile)
			<< "," << BWAPIext::WtoB(mMapHeightWalkTile) << "]\n";
		return false;
	}
	if( mBuildable[position.x()][position.y()] == TileBuildablity::nowalk )
		return false;
	return true;
}
/// if testReachable is invalid or unwalkable, just test if position is walkable.
bool MapInfo::isTileReachable( BWAPI::TilePosition position, BWAPI::TilePosition testReachable ) const
{
	if (!position.isValid())
	{
		return false;
	}
	if (!isTileWalkable(position)) return false;
	if (testReachable.isValid() && isTileWalkable(testReachable))
	{
		 if(!isConnected(position, testReachable))
		{
			return false;
		}
	}
	return true;
}

/// simple spiral building placer with 1 tile surround bib.
BWAPI::TilePosition MapInfo::getBuildingPlacement( BWAPI::UnitType building )
{
	return getBuildingPlacement(building, mStartLocation );
}

BWAPI::TilePosition MapInfo::getBuildingPlacement( BWAPI::UnitType building, BWAPI::TilePosition pos )
{
	if (!pos.isValid())
	{
		pos = mStartLocation;
	}
	if (building.isRefinery())
	{
		return getRefineryPlacement();
	}
	int x = pos.x();
	int y = pos.y();
	int length = 1;
	int j = 0;
	int dx = 0;
	int dy = 1;
	bool first = true; // control spiral rate

	while (length < Broodwar->mapWidth() )
	{
		if (x >= 0 && x < Broodwar->mapWidth() && 
			y >= 0 && y < Broodwar->mapHeight())
		{
			TilePosition tp(x,y);
			bool canBuild = canBuildHere(tp, building);
			if (canBuild)
			{
				return tp;
			}
		}
		// try next spot
		x = x + dx;
		y = y + dy;
		// length of this leg of spiral
		j++;
		if (j == length) //if we've reached the end, its time to turn
		{
			//reset step counter
			j = 0;

			//Spiral out. Keep going.
			if (!first)
				length++; //increment step counter if needed

			//first=true for every other turn so we spiral out at the right rate
			first =! first;

			//turn counter clockwise 90 degrees:
			if (dx == 0)
			{
				dx = dy;
				dy = 0;
			}
			else
			{
				dy = -dx;
				dx = 0;
			}
		}
	}
	// fail
	return BWAPI::TilePositions::None;
}
/// call when one of our buildings is created or dies. Or liftoff/lands.
void MapInfo::updateBuildability( const BWAPI::Unit* pUnit )
{
	if ( !pUnit->getType().isBuilding() )
	{
		logfile << "Warning: MapInfo.UpdateBuildability " 
			<< BWAPIext::getUnitNameID(pUnit) << " - not a building!\n";
		return;
	}
	if ( pUnit->getPlayer() != Broodwar->self() )
	{
		logfile << "Warning: MapInfo.UpdateBuildability "
			<< BWAPIext::getUnitNameID(pUnit) << " - is not ours!\n";
		return;
	}
	assert( pUnit->getPlayer() == Broodwar->self() 
		&& pUnit->getType().isBuilding() );
	if ( pUnit->exists() ) // set unbuildable and unwalkable
	{
		TilePosition TL, BR;
		BWAPIext::getTileBounds( *pUnit, TL, BR );
		assert( TL.isValid() && BR.isValid() );
		for ( int x = TL.x(); x < BR.x(); x++ )
		{
			for ( int y = TL.y(); y < BR.y(); y++ )
			{
				mBuildable[x][y] = TileBuildablity::nowalk;
			}
		}
	}
	else	// unit has died, rescan buildability
	{
		TilePosition TL, BR;
		BWAPIext::getTileBounds( *pUnit, TL, BR );
		assert( TL.isValid() && BR.isValid() );
		for ( int x = TL.x(); x < BR.x(); x++ )
		{
			for ( int y = TL.y(); y < BR.y(); y++ )
			{
				mBuildable[x][y] = TileBuildablity::yes;
			}
		}
	}
}

void MapInfo::reserveBuildability( BWAPI::UnitType ut, BWAPI::TilePosition TL, bool clear /*= false */ )
{
	// validation
	if ( !ut.isBuilding() )
	{
		logfile << "Warning: MapInfo.reserveBuildability " 
			<< ut.getName() << " - not a building!\n";
		return;
	}
	if ( !TL.isValid() )
	{
		logfile << "Warning: MapInfo.reserveBuildability " 
			<< ut.getName() << " - invalid position!\n";
		return;
	}
	// reserve tiles
	TilePosition BR;
	BWAPIext::getTileBounds( ut, TL, BR );
	assert( TL.isValid() && BR.isValid() );
	TileBuildablity::Enum bv = TileBuildablity::reserved;
	if (clear) bv = TileBuildablity::yes;
	for ( int x = TL.x(); x < BR.x(); x++ )
	{
		for ( int y = TL.y(); y < BR.y(); y++ )
		{
			mBuildable[x][y] = bv;
		}
	}
}

void MapInfo::drawBaseLocInfo() const
{
	if (!mIsMapAnalyzed)
	{
		return;
	}
	BOOST_FOREACH(baseLocInfo* bloc, mBaseLocs)
	{
		string ownerName = "None ";
		
		if (bloc->controller)
		{
			ownerName = bloc->controller->getName();
		}
		if (bloc->reachableFromStart)
		{
			ownerName += boost::lexical_cast<string>(bloc->groundDistFromStart*32);
		}
		
		Broodwar->drawTextMap(bloc->center().x(),bloc->center().y(),"[%s]",ownerName.c_str());
		Position start = BWAPIext::getCenter(Broodwar->self()->getStartLocation());
		int regionDist = getRegionPathDistance( start, bloc->center() );
		Broodwar->drawTextMap(bloc->center().x(),bloc->center().y()+10,"rd: %d", regionDist);
		if (bloc->isNat)
		{
			printf("Nat");
			Broodwar->drawTextMap(bloc->center().x(),bloc->center().y()+20,"Natural");
		}
	}
}

void MapInfo::onDepotFound( BWAPI::Unit* depot )
{
	// make certain unit is depot
	if ( !depot || !isResourceDepot(depot->getType()) )
	{
		return;
	}
	if (!mIsMapAnalyzed)
	{
		logfile << "Error in MapInfo onDepotFound: map is not analyzed!\n";
	}
	// check distances from baselocations
	BOOST_FOREACH(baseLocInfo* bloc, mBaseLocs)
	{
		if (Position(bloc->center()).getApproxDistance(depot->getPosition()) <= mDepotDistToOwnBaseLoc)
		{
			bloc->controller = depot->getPlayer();
			//Broodwar->printf("depot dist: %d", Position(bloc->depotPosition()).getApproxDistance(depot->getPosition()) );
		}
	}
}

int MapInfo::getRegionPathDistance( BWAPIext::WalkTile source, BWAPIext::WalkTile dest ) const
{
	return getRegionPathDistance(source.getCenterPosition(),dest.getCenterPosition());
}

int MapInfo::getRegionPathDistance( Position source, Position dest ) const
{
	const regionInfo* sourceRegion = getRegion(source);
	const regionInfo* destRegion = getRegion(dest);

	if (!sourceRegion || !destRegion )
	{
		return -1;
	}
	if (sourceRegion == destRegion)
	{
		return source.getApproxDistance(dest);
	}
	// else the points are in two separate regions, check connectivity
	if (!isConnected(source,dest) )
	{
		return -1;
	}

	set<chokeInfo*> sourceChokes = sourceRegion->chokes;
	set<chokeInfo*> destChokes = destRegion->chokes;
#undef max
	int shortest = numeric_limits<int>::max();
	// check choke distance between each pair of chokes.
	BOOST_FOREACH(chokeInfo* sc, sourceChokes)
	{
		int sID = sc->ID();
		int dc1 = source.getApproxDistance(sc->center());
		BOOST_FOREACH(chokeInfo* dc, destChokes)
		{
			int dID = dc->ID();
			int dc2 = dest.getApproxDistance(dc->center());
			int d = mRPaths.getCDist(sID,dID) + dc1 + dc2;
			if (d < shortest)
			{
				shortest = d;
			}
		}
	}

	return shortest;
}

vector<const regionInfo*> MapInfo::getRegionPath( const regionInfo* begin, const regionInfo* end ) const
{
	int startID = begin->ID();
	int endID = end->ID();
	list<int> rp = mRPaths.getPath(startID,endID);
	vector<const regionInfo*> regionPath;
	for each ( int i in rp )
	{
		const regionInfo* region = getRegion(i);
		if (region)
		{
			regionPath.push_back(region);
		}
		else
		{
			logfile << "getRegion by ID returned NULL\n";
		}
	}
	return regionPath;
}

bool MapInfo::isConnected( BWAPIext::WalkTile source, BWAPIext::WalkTile dest ) const
{
	return TABW::isConnected(source,dest);
}

bool MapInfo::isConnected( BWAPI::TilePosition source, BWAPI::TilePosition dest ) const
{
	return TABW::isConnected(BWAPIext::WalkTile(source), BWAPIext::WalkTile(dest));
}
std::set<regionInfo*> MapInfo::getAdjacentRegions( const regionInfo* region )
{
	set<regionInfo*> neighbors;
	BOOST_FOREACH(TABW::pRegion tr,region->adjacentRegions)
	{
		regionInfo* neighbor = mTABW_regionToRegionInfo[tr];
		neighbors.insert(neighbor);
	}
	return neighbors;
}

void MapInfo::calculateRegionDistances()
{
	set<regionInfo*> connectedRegions; // act as unvisited
	BOOST_FOREACH(regionInfo* region, mRegions)
	{
		if (!region->isIsland())
		{
			connectedRegions.insert(region);
			logfile << "Connected region: " << region->ID() <<endl;
			mRPaths.dist[region->ID()][region->ID()] = 0;
		}
	}
	// now have all connected regions, Floyd-Warshall algorithm
	int V = connectedRegions.size();
	logfile << "num Connected Regions: " << V << endl;
	// initialize distance array, set to max=infinity

	BOOST_FOREACH(chokeInfo* choke, mChokes)
	{
		regionInfo* r1 = choke->regions().first;
		regionInfo* r2 = choke->regions().second;
		int dist = r1->center().getApproxDistance(choke->center())
			+ choke->center().getApproxDistance(r2->center());
		
		mRPaths.dist[r1->ID()][r2->ID()] = dist;
		mRPaths.dist[r2->ID()][r1->ID()] = dist;
		logfile << "dist: " << mRPaths.dist[r1->ID()][r2->ID()]  << endl;
		logfile << "getRDIST: " << mRPaths.getRDist(r1->ID(),r2->ID()) << endl;
	}
	// loop through k,i,j
	BOOST_FOREACH(regionInfo* k, connectedRegions)
	{
		BOOST_FOREACH(regionInfo* i, connectedRegions)
		{
			BOOST_FOREACH(regionInfo* j, connectedRegions)
			{
				int ikDist = mRPaths.getRDist(i->ID(),k->ID());
				int kjDist = mRPaths.getRDist(k->ID(),j->ID());
				int ijDist = ikDist + kjDist;
#undef max
				if (ikDist == numeric_limits<int>::max() ||
					kjDist == numeric_limits<int>::max() )
				{
					ijDist = numeric_limits<int>::max();
				}
				if ( ijDist < mRPaths.getRDist(i->ID(),j->ID()))
				{
					mRPaths.dist[i->ID()][j->ID()] = ijDist;
					mRPaths.next[i->ID()][j->ID()] = k->ID();
				}
			}
		}
	}
	// calculate choke paths: shortest distance between each chokepoint
	// init array
	int nc = mChokes.size()+1;
	mRPaths.distC.resize(nc,nc,numeric_limits<int>::max());
	mRPaths.predecessor.resize(nc,nc,numeric_limits<int>::max());
	// now set adjacent choke distances, for each choke in bordering region
	// straight line between chokepoints approximation.
	BOOST_FOREACH(chokeInfo* c, mChokes)
	{
		mRPaths.distC[c->ID()][c->ID()] = 0;
		regionInfo* r1 = c->regions().first;
		regionInfo* r2 = c->regions().second;
		BOOST_FOREACH(chokeInfo* c2, mChokes)
		{
			if( c->ID() == c2->ID() ) continue;
			int dist = c->center().getApproxDistance(c2->center());
			mRPaths.distC[c->ID()][c2->ID()] = dist;
		}
		BOOST_FOREACH(const chokeInfo* c2, r2->chokes)
		{
			if( c->ID() == c2->ID() ) continue;
			int dist = c->center().getApproxDistance(c2->center());
			mRPaths.distC[c->ID()][c2->ID()] = dist;
		}
	}
	// loop through k,i,j
	BOOST_FOREACH(chokeInfo* k, mChokes)
	{
		BOOST_FOREACH(chokeInfo* i, mChokes)
		{
			BOOST_FOREACH(chokeInfo* j, mChokes)
			{
				int ikDist = mRPaths.getCDist(i->ID(),k->ID());
				int kjDist = mRPaths.getCDist(k->ID(),j->ID());
				int ijDist = ikDist + kjDist;
#undef max
				if (ikDist == numeric_limits<int>::max() ||
					kjDist == numeric_limits<int>::max() )
				{
					ijDist = numeric_limits<int>::max();
				}
				if ( ijDist < mRPaths.getCDist(i->ID(),j->ID()))
				{
					mRPaths.distC[i->ID()][j->ID()] = ijDist;
					mRPaths.predecessor[i->ID()][j->ID()] = k->ID();
				}
			}
		}
	}
}

int MapInfo::remainingExpoCount() const
{
	int remaining = 0;
	BOOST_FOREACH(baseLocInfo* bloc, mBaseLocs)
	{
		// eliminate claimed
		if (bloc->controller)
		{
			continue;
		}
		// eliminate islands
		else if (bloc->region->isIsland())
			continue;
		// eliminate different landmasses
		else if (!isConnected(WalkTile(bloc->depotPosition()),
			WalkTile(Broodwar->self()->getStartLocation()) ) )
		{
			continue;;
		}
		remaining++;
	}
	return remaining;
}

void MapInfo::updateExplorationInfo()
{
	set<chokeInfo*>::iterator citer = mChokes.begin();
	for (citer; citer != mChokes.end(); ++citer)
	{
		chokeInfo* choke = *citer;
		if (Broodwar->isVisible(TilePosition(choke->center())))
		{
			choke->frameLastVisible = Broodwar->getFrameCount();
		}
	}
	set<baseLocInfo*>::iterator biter = mBaseLocs.begin();
	for (biter; biter != mBaseLocs.end(); ++biter)
	{
		baseLocInfo* bloc = *biter;
		if (Broodwar->isVisible(TilePosition(bloc->center())))
		{
			bloc->frameLastVisible = Broodwar->getFrameCount();
		}
	}
	// check centers
	set<regionInfo*>::iterator riter = mRegions.begin();
	for (riter; riter != mRegions.end(); ++riter)
	{
		regionInfo* reg = *riter;
		if (Broodwar->isVisible(TilePosition(reg->center())))
		{
			reg->lastSeenCenter = Broodwar->getFrameCount();
		}
	}
}

void MapInfo::update()
{
	updateExplorationInfo();
	updateEnemyPresence();
	static bool msg = false;
	if (!msg && mIsBaseLocDistCalculated)
	{
		msg = true;
		Broodwar->printf("Calculation of BaseLocDistances finished!");
	}
}

void MapInfo::updateEnemyPresence()
{
	set<const regionInfo*> enemyPresence = 
		GameState::get().getEnemyPresence();
	
	set<regionInfo*>::iterator riter = mRegions.begin();
	for (riter; riter!=mRegions.end(); ++riter)
	{
		if (enemyPresence.count(*riter))
		{
			(*riter)->enemyPresence = true;
		}
		else (*riter)->enemyPresence = false;
	}
}

void MapInfo::calculateBaseLocDistances()
{
	set<baseLocInfo*>::iterator blit = mBaseLocs.begin();
	for (blit; blit != mBaseLocs.end(); ++blit)
	{
		BuildTilePath pathToStart = Pathfinder::getInstance().CreateTilePath(mStartLocation, 
			TilePosition((*blit)->center()));
		if (pathToStart.isComplete)
		{
			(*blit)->groundDistFromStart = pathToStart.path.size();
			logfile << "baseloc distance from start = " << (*blit)->groundDistFromStart << endl;
			(*blit)->reachableFromStart = true;
		}
	}
	mIsBaseLocDistCalculated = true;
	//Broodwar->printf("Calculation of BaseLocDistances complete.");
}

baseLocInfo* MapInfo::getNextExpo( bool gasExpo )
{
	list<baseLocInfo*> potentialExpos( mBaseLocs.begin(),mBaseLocs.end() );
	// remove all occupied positions, and unconnected/islands for now
	list<baseLocInfo*>::iterator pei = potentialExpos.begin();
	while (pei != potentialExpos.end())
	{
		if ((*pei)->controller || (*pei)->region->isIsland()
			|| !MapInfo::get().isConnected( WalkTile((*pei)->depotPosition()), 
			WalkTile(Broodwar->self()->getStartLocation()) ) )
		{
			pei = potentialExpos.erase(pei);
		}
		else if (gasExpo && !(*pei)->hasGas() )
		{
			pei = potentialExpos.erase(pei);
		}
		else ++pei;
	}
	// now select the closest one, use startlocation for now
	int closestDist = -1;
	baseLocInfo* closestLoc = NULL;
	BOOST_FOREACH( baseLocInfo* bloc, potentialExpos )
	{

		int dist = bloc->groundDistFromStart;
		if (dist < closestDist || closestDist == -1)
		{
			closestDist = dist;
			closestLoc = bloc;
		}
	}
	if (!closestLoc)
	{
		logfile << "MapInfo: no more expo locations remain\n";
	}
	return closestLoc;
}

BWAPI::TilePosition MapInfo::getRefineryPlacement()
{
	TilePosition target = BWAPI::TilePositions::None;
	list<baseLocInfo*> potentialExpos( mBaseLocs.begin(),mBaseLocs.end() );
	// remove all we don't control, or have no gas
	list<baseLocInfo*>::iterator pei = potentialExpos.begin();
	while (pei != potentialExpos.end())
	{
		if ((*pei)->controller != Broodwar->self() 
			|| !(*pei)->hasGas())
		{
			pei = potentialExpos.erase(pei);
			logfile << "Erasing, due to controller/gas, " << potentialExpos.size() << " remain\n";
		}
		else 
		{
			// check state of geyser
			baseLocInfo* bli = *pei;
			if (bli->geyser->getType() != BWAPI::UnitTypes::Resource_Vespene_Geyser)
			{
				pei = potentialExpos.erase(pei);
				logfile << "Erasing, due to no geyser found, " << potentialExpos.size() << " remain\n";
			}
			else
			{
				++pei;
			}
			
		}
	}
	// get closest one
	int closestDist = -1;
	BOOST_FOREACH( baseLocInfo* bloc, potentialExpos )
	{
		int dist = bloc->groundDistFromStart;
		if (dist < closestDist || closestDist == -1)
		{
			closestDist = dist;
			target = TilePosition(bloc->geyser->getTilePosition());
		}
	}
	if (!target)
	{
		logfile << "MapInfo: no more refinery locations remain\n";
	}
	else
	{
		logfile << "Mapinfo found possible refinery locations\n";
	}

	return target;
}

std::set<regionInfo*> MapInfo::getOccupiedRegions( BWAPI::Player* player )
{
	set<regionInfo*> occupied;
	BOOST_FOREACH(regionInfo* region, mRegions)
	{
		if (region->areUnitsPresent(player))
		{
			occupied.insert(region);
		}
	}
	return occupied;
}

baseLocInfo* MapInfo::getMainBaseLocation( BWAPI::Player* player )
{
	return mMainBaseLocations[player];
}
// from UAB
int MapInfo::getGroundDistance(BWAPI::Position origin, BWAPI::Position destination)
{
	// if we haven't yet computed the distance map to the destination
	if (allMaps.find(destination) == allMaps.end())
	{
		BWAPI::Broodwar->printf("Computing DistanceMap for new destination");

		// add the map and compute it
		allMaps[destination] = DistanceMap();
		computeDistance(allMaps[destination], destination);
	}

	// get the distance from the map
	return allMaps[destination][origin];
}
// computes walk distance from Position P to all other points on the map
void MapInfo::computeDistance(DistanceMap & dmap, const BWAPI::Position p)
{
	search(dmap, p.y() / 32, p.x() / 32);
}
// does the dynamic programming search
void MapInfo::search(DistanceMap & dmap, const int sR, const int sC)
{
	// reset the internal variables
	resetFringe();

	// set the starting position for this search
	dmap.setStartPosition(sR,sC);

	// set the distance of the start cell to zero
	dmap[getIndex(sR, sC)] = 0;

	// set the fringe variables accordingly
	int fringeSize(1);
	int fringeIndex(0);
	fringe[0] = getIndex(sR,sC);

	// temporary variables used in search loop
	int currentIndex, nextIndex;
	int newDist;

	// the size of the map
	int size = rows*cols;

	// while we still have things left to expand
	while (fringeIndex < fringeSize)
	{
		// grab the current index to expand from the fringe
		currentIndex = fringe[fringeIndex++];
		newDist = dmap[currentIndex] + 1;

		// search up
		nextIndex = (currentIndex > cols) ? (currentIndex - cols) : -1;
		if (unexplored(dmap, nextIndex))
		{
			// set the distance based on distance to current cell
			dmap.setDistance(nextIndex, newDist);
			dmap.setMoveTo(nextIndex, 'D');

			// put it in the fringe
			fringe[fringeSize++] = nextIndex;				
		}

		// search down
		nextIndex = (currentIndex + cols < size) ? (currentIndex + cols) : -1;
		if (unexplored(dmap, nextIndex))
		{
			// set the distance based on distance to current cell
			dmap.setDistance(nextIndex, newDist);
			dmap.setMoveTo(nextIndex, 'U');

			// put it in the fringe
			fringe[fringeSize++] = nextIndex;
		}

		// search left
		nextIndex = (currentIndex % cols > 0) ? (currentIndex - 1) : -1;
		if (unexplored(dmap, nextIndex))
		{
			// set the distance based on distance to current cell
			dmap.setDistance(nextIndex, newDist);
			dmap.setMoveTo(nextIndex, 'R');

			// put it in the fringe
			fringe[fringeSize++] = nextIndex;
		}

		// search right
		nextIndex = (currentIndex % cols < cols - 1) ? (currentIndex + 1) : -1;
		if (unexplored(dmap, nextIndex))
		{
			// set the distance based on distance to current cell
			dmap.setDistance(nextIndex, newDist);
			dmap.setMoveTo(nextIndex, 'L');

			// put it in the fringe
			fringe[fringeSize++] = nextIndex;
		}
	}
}

void MapInfo::resetFringe()
{
	std::fill(fringe.begin(), fringe.end(), 0);
}
inline int MapInfo::getIndex(int row, int col)
{
	return row * cols + col;
}

bool MapInfo::unexplored( DistanceMap & dmap, const int index ) const
{
	return (index != -1) && dmap[index] == -1 && map[index];
}

void MapInfo::setBWAPIMapData()
{
	// for each row and column
	for (int r(0); r < rows; ++r)
	{
		for (int c(0); c < cols; ++c)
		{
			bool clear = false;

			// check each walk tile within this TilePosition
			for (int i=0; i<4; ++i)
			{
				for (int j=0; j<4; ++j)
				{
					if (BWAPI::Broodwar->isWalkable(c*4 + i, r*4 + j))
					{
						clear = true;
						break;
					}

					if (clear)
					{
						break;
					}
				}
			}

			// set the map as binary clear or not
			map[getIndex(r,c)] = clear;
		}
	}
}

void MapInfo::calculateNaturals()
{
	set<TilePosition> startLocs =  Broodwar->getStartLocations();
	for( set<TilePosition>::iterator iter = startLocs.begin();
		iter != startLocs.end(); ++iter)
	{
		TilePosition startLoc = *iter;
		const regionInfo* startRegion = getRegion(Position(startLoc));
		baseLocInfo* closest = NULL;
		int closestDist = numeric_limits<int>::max();
		for(set<baseLocInfo*>::iterator biter = mBaseLocs.begin();
			biter != mBaseLocs.end(); ++biter)
		{
			baseLocInfo* base = *biter;
			if (base->region == startRegion)
			{
				continue;
			}
			if (base->hasGas())
			{
				int rd = getRegionPathDistance(BWAPIext::getCenter(startLoc), base->center());
				if( closest==NULL || (rd != -1 && closestDist > rd) )
				{
					closestDist = rd;
					closest = base;
				}
			}
		}
		if (closest)
		{
			closest->isNat = true;
			logfile << "Found Natural!\n";
		}
	}
}

BWAPI::TilePosition MapInfo::getEnemyStartLocation() const
{
// 	baseLocInfo* enemyBase = mMainBaseLocations[Broodwar->enemy()];
// 	return TilePosition(enemyBase->center());
	return mEnemyStart;
}

BWAPI::Position MapInfo::mapCenter() const
{
	return Position(Broodwar->mapWidth()*16,Broodwar->mapHeight()*16);
}

int MapInfo::countUnitsInRegion( BWAPI::UnitType ut, const regionInfo* region ) const
{
	int count = 0;
	BOOST_FOREACH(  BWAPI::Unit* unit, Broodwar->self()->getUnits() )
	{
		if ( unit->getType()== ut && getRegion(unit) == region )
		{
			count++;
		}
	}
	return count;
}

TilePosition MapInfo::getNearestWalkableTile( Position pos, Unit unit /*= StaticUnits::nullunit*/ ) const
{
	TilePosition tile(pos);
	TilePosition unitTile = BWAPI::TilePositions::None;;
	if (unit && unit != StaticUnits::nullunit )
	{
		unitTile = unit->getTilePosition();
		if (!isTileWalkable(unitTile)) unitTile = BWAPI::TilePositions::None;
	}
	if (isTileWalkable(tile)) return tile;
	else
	{
		// spiral search out
		tile =  tileSpiralSearch(tile, &MapInfo::isTileReachable, unitTile);
	}
	if (!tile.isValid())
	{
		logfile << "Warning: getNearestWalkableTile isn't supposed to be able to fail.\n";
	}
	return tile;
}

TilePosition MapInfo::tileSpiralSearch( TilePosition startTile, 
									   bool (MapInfo::*eval)(TilePosition tile, TilePosition tile2) const
									   , TilePosition compAgainst /*= BWAPI::TilePositions::None*/) const
{
	if (!startTile.isValid())
	{
		logfile << "Warning: startTile " <<  BWAPIext::getString(startTile) << " invalid, making valid: ";
		startTile.makeValid();
		logfile << BWAPIext::getString(startTile) << endl;
	}
	assert(startTile.isValid());
	int x = startTile.x();
	int y = startTile.y();
	int length = 1;
	int j = 0;
	int dx = 0;
	int dy = 1;
	bool first = true; // control spiral rate

	while (length < Broodwar->mapWidth() )
	{
		if (x >= 0 && x < Broodwar->mapWidth() && 
			y >= 0 && y < Broodwar->mapHeight())
		{
			TilePosition tp(x,y);
			bool passEval = (this->*eval)(tp, compAgainst); // using member function pointer
			if (passEval)
			{
				return tp;
			}
		}
		// try next spot
		x = x + dx;
		y = y + dy;
		// length of this leg of spiral
		j++;
		if (j == length) //if we've reached the end, its time to turn
		{
			//reset step counter
			j = 0;

			//Spiral out. Keep going.
			if (!first)
				length++; //increment step counter if needed

			//first=true for every other turn so we spiral out at the right rate
			first =! first;

			//turn counter clockwise 90 degrees:
			if (dx == 0)
			{
				dx = dy;
				dy = 0;
			}
			else
			{
				dy = -dx;
				dx = 0;
			}
		}
	}
	// fail
	return BWAPI::TilePositions::None;
}
