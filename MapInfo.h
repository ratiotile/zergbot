/** MapInfo holds all map analysis information and plots building locations
*/

#pragma once
#include "common/Singleton.h"
#include "TABW/RectangleArray.h"
#include <BWAPI.h>
#include "TABW/TABW.h"
#include <boost/thread.hpp>
#include "DistanceMap.hpp"
#include "RegionPaths.h"
#include "Interface.h"

class regionInfo;
class chokeInfo;
class baseLocInfo;

namespace TileBuildablity
{
	enum Enum{
		yes,		// can build here
		walkonly,	// no build but can walk
		nowalk,		// can't walk or build
		reserved,	// planned to build on
		bib,		// space around building for access
		resourcing	// space between Depot and resources
	};
}

class MapInfo: public Singleton<MapInfo>
{
public:
	MapInfo();
	/// call once map analyzed by TABW
	void onMapAnalyzed();
	/// const reference to mRegions
	const std::set<regionInfo*>& regions(){return mRegions;}
	/// const reference to mChokes
	const std::set<chokeInfo*>& chokes() {return mChokes;}
	const std::set<baseLocInfo*>& baseLocs() {return mBaseLocs;}
	
	const regionInfo* getRegion( BWAPIext::WalkTile walkPos ) const;
	const regionInfo* getRegion( BWAPI::TilePosition tilePos ) const;
	const regionInfo* getRegion( BWAPI::Position pos ) const;
	const regionInfo* getRegion( BWAPI::Unit* unit ) const
		{return getRegion(unit->getPosition());}
	const regionInfo* getRegion( int ID ) const;
	// backwards compatability with RegionManager
	/// returns unknown if not known
	BWAPI::TilePosition getEnemyStartLocation() const;
	/// gives Start Location for now
	BWAPI::TilePosition getNearestSafePosition( BWAPI::Position pos ) const;
	/// returns closest depotLocation in the region
	std::pair<baseLocInfo*,int> getNearestRegionalBaseLoc( BWAPI::Position pos ) const;
	/// number of own units in region
	int countUnitsInRegion( BWAPI::UnitType ut, const regionInfo* region ) const;

	
	//Buildability Functions/ Tile checks
	/// for HarvestNodes to add Resource area to buildability
	void addResourceArea(std::vector<BWAPI::Position> points);
	/// fills tiles in a line between resources and depot with resourceArea buildability
	void addResourceArea(std::vector<BWAPI::Position> resources, BWAPI::Position depot);
	/// marks path tiles to each choke from depot
	void markExitPaths(BWAPI::TilePosition depot);
	/// search outwards in spiral around Start.
	BWAPI::TilePosition getBuildingPlacement( BWAPI::UnitType building);
	BWAPI::TilePosition getBuildingPlacement( BWAPI::UnitType building, BWAPI::TilePosition pos);
	bool canBuildHere( BWAPI::TilePosition buildPos, BWAPI::UnitType building ) const;
	/// buildability update for our structures
	void updateBuildability( const BWAPI::Unit* pUnit );
	void reserveBuildability( BWAPI::UnitType ut, BWAPI::TilePosition TL, bool clear = false );
	/// get placement for refineries
	BWAPI::TilePosition getRefineryPlacement();
	BWAPI::Position mapCenter() const;

	/// is tile walkable?
	bool isTileWalkable( BWAPI::TilePosition position ) const;
	/// is tile walkable and on same landmass?
	bool isTileReachable( BWAPI::TilePosition position, BWAPI::TilePosition testReachable ) const;
	/// return spiral search result of nearest walkable tile.
	TilePosition getNearestWalkableTile( Position pos, Unit unit = StaticUnits::nullunit) const;
	/// spiral search out from starting tiles given comparator
	TilePosition tileSpiralSearch( TilePosition startTile, 
		bool (MapInfo::*eval)(TilePosition tile, TilePosition tile2) const, 
		TilePosition compAgainst = BWAPI::TilePositions::None ) const;
	
	/// call when depot discovered, to set BaseLoc ownership
	void onDepotFound( BWAPI::Unit* depot );
	/// RegionPathDist is from point to choke, going through chokes from each region. -1 if no path
	int getRegionPathDistance( BWAPIext::WalkTile source, BWAPIext::WalkTile dest ) const;
	int getRegionPathDistance( BWAPI::Position source, BWAPI::Position dest ) const;
	vector<const regionInfo*> getRegionPath( const regionInfo* begin, const regionInfo* end ) const;
	/// can a unit walk from one tile to the next?
	bool isConnected( BWAPIext::WalkTile source, BWAPIext::WalkTile dest ) const;
	bool isConnected( BWAPI::TilePosition source, BWAPI::TilePosition dest) const;
	/// get set of regions directly reachable
	std::set<regionInfo*> getAdjacentRegions(const regionInfo* region);
	/// number of remaining expansions that we know how to take
	int remainingExpoCount() const;
	/// call each frame
	void update();
	/// get nearest unclaimed baseLoc. or NULL if none left
	baseLocInfo* getNextExpo(bool gasExpo = false);
	/// returns natural to specified start location.
	baseLocInfo* getNatural(BWAPI::TilePosition startLocation);


	// needed to interface with UAB's Combat system
	/// regions where that player has units present
	std::set<regionInfo*> getOccupiedRegions(BWAPI::Player* player);
	baseLocInfo* getMainBaseLocation(BWAPI::Player* player);
	int getGroundDistance(BWAPI::Position from, BWAPI::Position to);
	// computes walk distance from Position P to all other points on the map
	void computeDistance(DistanceMap & dmap, const BWAPI::Position p);
	// does the dynamic programming search
	void search(DistanceMap & dmap, const int sR, const int sC);
	
	// debug draw functions
	/// display RegionPaths, blue for clear, else orange
	void drawRegionPaths() const;
	/// draws the name of the controlling player if exists, at region center 
	void drawRegionInfo() const;
	/// draws resource locations and depot placement
	void drawResources() const;
	/// displays buildability information
	void drawBuildability() const;
	/// draws if player owns each base location
	void drawBaseLocInfo() const;
	
private:
	/// has map been analyzed yet?
	bool mIsMapAnalyzed;
	/// have groundDistances for baseLocs been calculated?
	bool mIsBaseLocDistCalculated;
	int mMapWidthWalkTile;
	int mMapHeightWalkTile;
	/// how close a depot has to be to control a base location, in pixels
	int mDepotDistToOwnBaseLoc;
	// use regular pointers since this class as owner lives forever
	std::set<regionInfo*> mRegions;
	std::map<TABW::pRegion,regionInfo*> mTABW_regionToRegionInfo;
	std::set<chokeInfo*> mChokes;
	std::map<TABW::pChoke,chokeInfo*> mTABW_chokeToChokeInfo;
	std::set<baseLocInfo*> mBaseLocs;
	/// walk tile to regionInfo, NULL for no region(unwalkables)
	RectangleArray<regionInfo*> mWalkTileToRegion;

	/// buildability map, BuildTile resolution
	RectangleArray<TileBuildablity::Enum> mBuildable;
	/// distance from one region center to the next, straight line through chokes
	std::map<std::pair<regionInfo*,regionInfo*>,int> mRegionAdjacency; //remove
	RegionPaths mRPaths;
	// temp
	vector<const regionInfo*> mPathToEnemyMain;

	boost::shared_ptr<boost::thread> mThread;

	/// for UAB stuff, store distance searches
	std::map<BWAPI::Position, DistanceMap>	allMaps;
	// the map stored at TilePosition resolution 
	// values are 0/1 for walkable or not walkable
	std::vector<bool>	map;
	// the fringe vector which is used as a sort of 'open list'
	std::vector<int>	fringe;
	int rows, cols;
	// reset the fringe
	void resetFringe();
	// return the index of the 1D array from (row,col)
	inline int getIndex(int row, int col);
	bool unexplored(DistanceMap & dmap, const int index) const;
	void setBWAPIMapData();
	std::map<BWAPI::Player*,baseLocInfo*> mMainBaseLocations;
	/// own start position, for convenience
	BWAPI::TilePosition mStartLocation;
	BWAPI::TilePosition mEnemyStart;

	/// call to initialize data from TerrainAnalyzer
	void init();
	/// return NULL if no region
	regionInfo* TABWtoRegionInfo( TABW::pRegion pr );
	/// calculate distances between all regions
	void calculateRegionDistances();
	/// calculate the distances to each baseloc
	void calculateBaseLocDistances();
	/// if chokes and baselocs are visible, set lastScouted
	void updateExplorationInfo();
	/// updates presence field in regionInfos and baselocs
	void updateEnemyPresence();
	/// updates baseLocs isNat field, find nat for all start locs
	void calculateNaturals();
	
};