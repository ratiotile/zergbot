#include "MovementBehaviors.h"
#include "UnitAgent.h"

using namespace std;
using namespace BWAPI;
/// constructor
MovementBehaviors::MovementBehaviors( UnitAgent* pUnitAgent ):
	m_pUnitAgent(pUnitAgent),
	m_behavior(none)
{
	m_pUnit = m_pUnitAgent->getUnit();
}
/// destructor
MovementBehaviors::~MovementBehaviors()
{

}

void MovementBehaviors::printBehavior()
{
	if (isOn(seek))
	{
		Broodwar->printf("seek is on.");
	}
	if (isOn(flee))
	{
		Broodwar->printf("flee is on.");
	}
	if (isOn(waypoints))
	{
		Broodwar->printf("waypoints is on.");
	}
	if (isOn(pursue))
	{
		Broodwar->printf("pursue is on");
	}
}

void MovementBehaviors::processActions()
{
	if (isOn(none))
	{
		return;
	}
	if (isOn(hold))
	{
		if( m_lWaypoints_px.empty() ) m_lWaypoints_px.push_back( m_pUnit->getPosition() );
		else if( m_pUnit->getPosition() != m_lWaypoints_px.front() )
			m_lWaypoints_px.front() = m_pUnit->getPosition();
	}
	else	// all other behaviors
	{
	if (isOn(seek))
	{	// always have a moveTarget
		if (!m_lWaypoints_px.empty()&& m_lWaypoints_px.front() != m_pUnit->getPosition())
		{	
			if (m_pUnit->isMoving())
			{
				if (m_targetPos_px != m_lWaypoints_px.front())
				{
					m_targetPos_px = m_lWaypoints_px.front();
					m_pUnit->move(m_targetPos_px);
				}
				else if (m_pUnit->getTargetPosition() != m_targetPos_px)
				{
					m_pUnit->move(m_targetPos_px);
				}
			} 
			else // choose temporary step and move
			{
				Vector2D vDest(m_lWaypoints_px.front().x() - m_pUnit->getPosition().x(),
					m_lWaypoints_px.front().y() - m_pUnit->getPosition().y() );
				vDest.normalize();
				m_targetPos_px = Position( int(vDest.x), int(vDest.y) );
				m_pUnit->move(m_targetPos_px);
			}
		}
	}
	}
}

void MovementBehaviors::setDestination( const BWAPI::Position& dest )
{
	m_lWaypoints_px.clear();
	m_lWaypoints_px.push_back(dest);
}

void MovementBehaviors::setWaypoints( const std::list<BWAPI::Position> nWP )
{
	m_lWaypoints_px = nWP;
}

void MovementBehaviors::enable( behavior_type bt )
{
	m_behavior = bt;
}

Position MovementBehaviors::getDestination() const
{
	if (m_lWaypoints_px.empty())
	{
		return BWAPI::Positions::None;
	}
	else
	{
		return m_lWaypoints_px.front();
	}
}

void MovementBehaviors::move( BWAPI::Position pos )
{
	enable(seek);
	// todo: detect re-move?
	m_targetPos_px = pos;
	m_lWaypoints_px.clear();
	m_lWaypoints_px.push_back(pos);
}