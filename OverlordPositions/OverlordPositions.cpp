#include "OverlordPositions.h"
#include "../common/cpp_includes.h"
#include "../MapInfo.h"
#include <sys/stat.h>
#include <fstream>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/map.hpp>
#include "SerializeTilePosition.hpp"


OverlordPositions::OverlordPositions()
: mStartPos(Broodwar->self()->getStartLocation())
,mEnemyStartPos(BWAPI::TilePositions::Unknown)
{

}

void OverlordPositions::addPosition( unsigned int x, unsigned int y )
{
	// check to avoid overwriting
	Broodwar->printf("Adding position %d,%d",x,y);
	int h = hash(x,y);
	WalkTile uh = unhash(h);
	Broodwar->printf("Hash: %d, unhash: %d,%d", h, uh.x(), uh.y() );
	if (data[mStartPos][mEnemyStartPos][hash(x,y)] != 0)
	{

	}
	save();
}
void OverlordPositions::addPosition( BWAPI::Position pos )
{
	unsigned int x = BWAPIext::PtoW(pos.x());
	unsigned int y = BWAPIext::PtoW(pos.y());
	addPosition(x,y);
}

void OverlordPositions::drawPositions()
{
	if (mEnemyStartPos == BWAPI::TilePositions::Unknown)
	{
		mEnemyStartPos = MapInfo::get().getEnemyStartLocation();
	}
	if ( mEnemyStartPos != BWAPI::TilePositions::Unknown)
	{
		typedef pair<unsigned int, unsigned int> intPair;
		BOOST_FOREACH(intPair ip, data[mStartPos][mEnemyStartPos].mapPosScore)
		{
			WalkTile tl = unhash(ip.first);
			int x1 = BWAPIext::WtoP( tl.x() );
			int y1 = BWAPIext::WtoP( tl.y() );
			int x2 = BWAPIext::WtoP( tl.x() +1 );
			int y2 = BWAPIext::WtoP( tl.y() +1 );
			Broodwar->drawBoxMap(x1,y1,x2,y2,BWAPI::Colors::Green);
		}
	}
	else
	{
		Broodwar->drawTextScreen(280,400,"Enemy Location unknown!");
	}
}

unsigned int OverlordPositions::hash( unsigned int x, unsigned int y ) const
{
	return x * 10000 + y;
}

WalkTile OverlordPositions::unhash( unsigned int h ) const
{
	int x = h/10000;
	int y = h - x*10000;
	return WalkTile(x,y);
}
void OverlordPositions::onEnemyStartLocFound( BWAPI::TilePosition enemy )
{
	//mStartPos = Broodwar->self()->getStartLocation();
	mEnemyStartPos = enemy;
	mFileName = "bwapi-data/OverlordPositions/";
	mFileName += Broodwar->mapHash();
	mFileName += ".op";
	// try to load
	attemptLoad();
}
bool OverlordPositions::attemptLoad()
{
	// check if file exists
	struct stat stFileInfo;
	if( stat(mFileName.c_str(),&stFileInfo) != 0 ) return false;
	logfile << "OverlordPositions: Map data file exists.\n";
	ifstream fi;
	fi.open( mFileName.c_str(), ios::binary );
	boost::archive::binary_iarchive ia(fi);
	ia >> data;
	fi.close();
	return true;
}

void OverlordPositions::save()
{
	ofstream fo;
	fo.open( mFileName.c_str(), ios::binary );
	boost::archive::binary_oarchive oa(fo);
	oa << data;
	fo.close();
}

void OverlordPositions::removePosition( unsigned int x, unsigned int y )
{
	unsigned int h = hash(x,y);
	map<unsigned int, unsigned int>& mapPosScore = data[mStartPos][mEnemyStartPos].mapPosScore;
	map<unsigned int, unsigned int>::iterator mpi =
		mapPosScore.find(h);
	if (mpi != mapPosScore.end())
	{
		mapPosScore.erase(mpi);
	}
	save();
}

void OverlordPositions::removePosition( BWAPI::Position pos )
{
	unsigned int x = BWAPIext::PtoW(pos.x());
	unsigned int y = BWAPIext::PtoW(pos.y());
	removePosition(x,y);
}