/************************************************************************/
/* Data structure holding positions for overlords to hide while scouting*/
/* in the time after discovering enemy's start location, but before		*/
/* overlord speed is upgraded											*/
/************************************************************************/
#pragma once
#include <boost/serialization/access.hpp>
#include <map>
#include <BWAPI.h>
#include "../common/WalkTile.h"
#include "../common/Singleton.h"

struct PositionData 
{
	PositionData() {}
	std::map<unsigned int,unsigned int> mapPosScore;
	unsigned int operator[](unsigned int q){return mapPosScore[q];}
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & mapPosScore;
	}
};
class OverlordPositions: public Singleton<OverlordPositions>
{
public:
	OverlordPositions();
	typedef std::map<BWAPI::TilePosition,std::map<BWAPI::TilePosition,PositionData>> startPosMap;
	/// x*10000 + y
	unsigned int hash(unsigned int x, unsigned int y) const;
	BWAPIext::WalkTile unhash(unsigned int h) const;
	/// add a position, giving initial score of 0
	void addPosition(unsigned int x, unsigned int y);
	void addPosition(BWAPI::Position pos);
	void removePosition(unsigned int x, unsigned int y);
	void removePosition(BWAPI::Position pos);
	void onEnemyStartLocFound( BWAPI::TilePosition enemy);
	
	/// draw to screen
	void drawPositions();
	
private:
	BWAPI::TilePosition mStartPos;
	BWAPI::TilePosition mEnemyStartPos;
	startPosMap data;
	std::string mFileName;

	bool attemptLoad();
	void save();
};


