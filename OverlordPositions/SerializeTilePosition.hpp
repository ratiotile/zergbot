#pragma once
#include <BWAPI.h>


namespace boost {
	namespace serialization {

		template<class Archive>
		void serialize(Archive & ar, BWAPI::TilePosition & g, const unsigned int version)
		{
			ar & g.x();
			ar & g.y();
		}

	} // namespace serialization
} // namespace boost