/** Path is a container holding one of several types of paths for UnitActions
to steer their UnitAgents across. 0: only holds destination point, rely on 
BW internal pathing. 1: use Region Paths, waypoints based on region centers
and chokes. 2: a path with tile precision.
*/
#pragma once
#ifndef INCLUDED_PATH_H
#define INCLUDED_PATH_H
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif

class Path
{
public:
	enum PathType{
		direct = 0,
		region = 1,
		tile = 2
	};
	Path( PathType pathLevel, std::list<BWAPI::Position>& wp );
private:
	/// last one is the destination
	std::list<BWAPI::Position> waypoints_;
	PathType type_;
};
#endif