#include "Pathfinder.h"
#include "common/cpp_includes.h"
#include "GameState.h"
#include "MapInfo.h"
#include "Path.h"
#include "pathfinder/BuildTilePath.h"
#include "pathfinder/Heap.h"

Pathfinder* Pathfinder::pInstance_ = NULL;

Pathfinder::Pathfinder()
{

}
Pathfinder::~Pathfinder()
{

}
Pathfinder& Pathfinder::getInstance()
{
	if (!pInstance_) pInstance_ = new Pathfinder();
	return *pInstance_;
}

bool Pathfinder::isDetailedPathingAvailable() const
{
	return false;
}

bool Pathfinder::isRegionInfoAvailable() const
{
	return false;
	//return GameState::getInstance().Regions().isReady();
}

Path* Pathfinder::findFleePath( BWAPI::Unit* pUnit )
{	// determine level of path to find: 0 none, 2 tile, 3 by region.
	//RegionManager& RegionMgr = GameState::getInstance().Regions();
	Path* fleePath = NULL;
	list<Position> wp;
	int level = 0;
	if (isDetailedPathingAvailable())
	{
		level = 2;
	}
	else if( isRegionInfoAvailable() )
	{
		level = 1;
		Position sp = Position(
			MapInfo::get().getNearestSafePosition(pUnit->getPosition()));
		wp.push_back(sp);
		fleePath = new Path( Path::direct, wp );
	}
	return fleePath;
}

BuildTilePath Pathfinder::CreateTilePath(TilePosition start, TilePosition target,
											  std::tr1::function<bool (TilePosition)> tileTest, 
											  std::tr1::function<int (TilePosition, TilePosition, int)> gFunction, 
											  std::tr1::function<int (TilePosition, TilePosition)> hFunction, 
											  int maxGValue, bool diaganol)
{
	BuildTilePath path;

	Heap<TilePosition, int> openTiles(true);
	std::map<TilePosition, int> gmap;
	std::map<TilePosition, TilePosition> parent;
	std::set<TilePosition> closedTiles;

	openTiles.push(std::make_pair(start, 0));
	gmap[start] = 0;
	parent[start] = start;

	while(!openTiles.empty())
	{
		TilePosition p = openTiles.top().first;
		int gvalue = gmap[p];

		if(p == target || (maxGValue != 0 && gvalue >= maxGValue))
			break;

		int fvalue = openTiles.top().second;

		openTiles.pop();
		closedTiles.insert(p);

		int minx = std::max(p.x() - 1, 0);
		int maxx = std::min(p.x() + 1, BWAPI::Broodwar->mapWidth()-1);
		int miny = std::max(p.y() - 1, 0);
		int maxy = std::min(p.y() + 1, BWAPI::Broodwar->mapHeight()-1);

		for(int x = minx; x <= maxx; x++)
		{
			for(int y = miny; y <= maxy; y++)
			{
				if (x != p.x() && y != p.y() && !diaganol)
					continue;

				TilePosition t(x, y);

				if (closedTiles.find(t) != closedTiles.end())
					continue;

				if(!tileTest(t))
					continue;

				int g = gFunction(t, p, gvalue);

				int f = g + hFunction(t, target);
				if (gmap.find(t) == gmap.end() || g < gmap.find(t)->second)
				{
					gmap[t] = g;
					openTiles.set(t, f);
					parent[t] = p;
				}
			}
		}
	}

	if(!openTiles.empty())
	{
		TilePosition p = openTiles.top().first;
		while(p != parent[p])
		{
			path.addNode(p);
			p = parent[p];
		}
		path.addNode(start);
		path.isComplete = true;
	}

	return path;
}