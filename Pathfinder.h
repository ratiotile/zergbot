/**
Pathfinder constructs paths using Map and Region information from GameState.
It is a singleton that is always globally accessible.
*/
#pragma once
#ifndef INCLUDED_PATHFINDER_H
#define INCLUDED_PATHFINDER_H

#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif

#include "pathfinder/BuildTilePath.h"
#include "MapInfo.h"

class DefaultTileTest
{
public:
	bool operator()(BWAPI::TilePosition position)
	{
		//return BuildingPlacer::Instance().isTileWalkable(position);
		return MapInfo::get().isTileWalkable(position);
	}
};
class DefaultHValue
{
public:
	int operator()(BWAPI::TilePosition position, BWAPI::TilePosition target)
	{
		int dx = abs(position.x() - target.x());
		int dy = abs(position.x() - target.y());
		return abs(dx - dy) * 10 + std::min(dx, dy) * 14;
	}
};
class DefaultGValue
{
public:
	int operator()(BWAPI::TilePosition currentTile, BWAPI::TilePosition previousTile, int gTotal)
	{
		gTotal += 10;
		if (currentTile.x() != previousTile.x() && currentTile.y() != previousTile.y())
			gTotal += 4;

		return gTotal;
	}
};

class Path;
class Pathfinder
{
public:
	static Pathfinder& getInstance();
	~Pathfinder();

	/// returns a path to safe area from unit's location
	Path* findFleePath( BWAPI::Unit* pUnit );

	BuildTilePath CreateTilePath(BWAPI::TilePosition start, BWAPI::TilePosition target, 
		std::tr1::function<bool (BWAPI::TilePosition)> tileTest = DefaultTileTest(), 
		std::tr1::function<int (BWAPI::TilePosition, BWAPI::TilePosition, int)> gFunction = DefaultGValue(), 
		std::tr1::function<int (BWAPI::TilePosition, BWAPI::TilePosition)> hFunction = DefaultHValue(), 
		int maxGValue = 0, bool diaganol = false);
private:
	Pathfinder();
	static Pathfinder* pInstance_;

	// helpers
	/// true if RegionManager is initiated (map analyzed by BWTA)
	bool isRegionInfoAvailable() const;
	/// true if we can use threat aware A* - not completed yet!
	bool isDetailedPathingAvailable() const;
};


#endif