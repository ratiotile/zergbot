#include "PerceptUnit.h"

PerceptUnit::PerceptUnit()
{
	triggerPosition = BWAPI::Positions::None;
}

PerceptUnit::PerceptUnit( const  BWAPI::Unit* unit )
{
	triggerPosition = unit->getPosition();
}