/**
* PerceptUnit - Tracks last position of unit, used to trigger an update.    
*/

#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif

class PerceptUnit
{
public:
	PerceptUnit();
	PerceptUnit( const BWAPI::Unit* unit);
	BWAPI::Position triggerPosition;
protected:
private:
};