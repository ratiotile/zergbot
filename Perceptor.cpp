#include "Perceptor.h"

using namespace std;
using namespace boost;

Perceptor* Perceptor::pInstance_ = NULL;
Perceptor::Perceptor() {}
Perceptor::~Perceptor() {}
Perceptor& Perceptor::getInstance()
{	
	if (!pInstance_)
	pInstance_ = new Perceptor();
	return *pInstance_;
}

void Perceptor::update()
{	// for all unitInfos in GameState bandits.
	for (map<const  BWAPI::Unit*,UIptr>::const_iterator ci = 
		GameState::get().getBandits().begin();
		ci != GameState::get().getBandits().end(); ci++)
	{
		// filter out neutral units like minerals
		if ((*ci).first->getType().isNeutral())
		{
			continue;
		}
		map<const  BWAPI::Unit*,shared_ptr<PerceptUnit>>::iterator found = 
			mUnitPerceptData_.find(ci->first);
		if (found != mUnitPerceptData_.end()) // unit in database, detect movement
		{
			if (found->first->exists() == false)
			{
				logfile << Broodwar->getFrameCount()<< 
					": Perceptor Update found unit that doesn't exist(probably not visible): " 
					<< found->first->getType().getName()
					<< "[" << found->first->getID()<<"] / "
					<< ci->second->getType().getName() << "["
					<<  ci->second->getID()
					<< "]. Erasing\n";
				//logWriteBuffer();
				mUnitPerceptData_.erase(found);
			}
			else if (found->second->triggerPosition.getApproxDistance(
				ci->second->getPosition()) > 32)
			{ // unit did move, check for proximity to our units and update
				found->second->triggerPosition = ci->second->getPosition();
				checkHostileProximity(ci->second);
			}
		} else {	// Bogey Not in database, enter and check!
			if (ci->first->exists()) // only add if unit is visible!
			{
				shared_ptr<PerceptUnit> tempPerceptUnit(new PerceptUnit(ci->first));
				pair< const  BWAPI::Unit*, shared_ptr<PerceptUnit>> tempPair(ci->first,tempPerceptUnit);
				mUnitPerceptData_.insert(tempPair);
				checkHostileProximity(ci->second);
			}
		}
	}

}
/** If the enemy unit is within a certain range of our units, add the enemy to our units'
	watchlists.
*/
void Perceptor::checkHostileProximity( const UIptr eUnit )
{	// set to 1 tile more than weapon range
	// only ground unit for now
	if(eUnit->getOwner()== Broodwar->self()) return;
	if (eUnit->getType().groundWeapon() != BWAPI::WeaponTypes::None)
	{
		int rangePx = BB.groundThreatRange(eUnit->getType());
		set< BWAPI::Unit*> ourUnits = Broodwar->getUnitsInRadius(eUnit->getPosition(), rangePx);
		// add enemy to watch lists
		for (set< BWAPI::Unit*>::iterator iOurUnits = ourUnits.begin();
			iOurUnits != ourUnits.end(); iOurUnits++)
		{
			if ((*iOurUnits)->getPlayer() == Broodwar->self())
			{
				UnitManager::get().perceptNearbyEnemy(*iOurUnits, eUnit);
			}

		} 
	}
	
	
}
/** Find the unit in mUnitPerceptData_ and remove the entry
*/
void Perceptor::onBanditDestroyed( const BWAPI::Unit* pUnit )
{
	map< const  BWAPI::Unit*,shared_ptr<PerceptUnit>>::iterator found = 
		mUnitPerceptData_.find(pUnit);
	if (found != mUnitPerceptData_.end()) // if found
	{
		mUnitPerceptData_.erase(found);
	}else {
		Broodwar->printf("Bandit %s destroyed, not found in Perceptor.",
			pUnit->getType().getName().c_str());
	}
}