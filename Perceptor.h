/**
* Perceptor filters game information so that each unit receives the    
* local information that is important to let it make decisions. Uses	
* PerceptUnit objects for memory of unit movements.					
*/

#ifndef INCLUDED_PERCEPTOR_H
#define INCLUDED_PERCEPTOR_H
#include <boost/smart_ptr.hpp>
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#include "PerceptUnit.h"
#include "GameState.h"
#include "UnitManager.h"

// singleton
class Perceptor
{
public:
	static Perceptor& getInstance();
	~Perceptor();
	/// process moved/new units
	void update();
	/// remove dead units from percepts list
	void onBanditDestroyed( const BWAPI::Unit* pUnit);
private:
	Perceptor();
	static Perceptor* pInstance_;
	std::map<const  BWAPI::Unit*,boost::shared_ptr<PerceptUnit>> mUnitPerceptData_;

	//helpers
	/// Alert nearby units if enemy is within a certain radius.
	void checkHostileProximity( const UIptr eUnit );
};
#endif