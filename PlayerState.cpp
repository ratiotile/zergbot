#include "PlayerState.h"
using BWAPI::UnitType;

int PlayerState::getProductionTarget( UnitType ut ) const
{
	map<UnitType,int>::const_iterator found = unitProductionTargets_.find(ut);
	if (found != unitProductionTargets_.end())
	{
		return found->second;
	} else	{
		return 0;
	}
}
void PlayerState::setProductionTarget( UnitType ut, int num )
{
	unitProductionTargets_[ut] = num;
}

void PlayerState::increaseProductionTarget( UnitType ut, int num )
{
	unitProductionTargets_[ut] = unitProductionTargets_[ut] + num;
}