#ifndef INCLUDED_PLAYERSTATE_H
#define INCLUDED_PLAYERSTATE_H

#include <BWAPI.h>
#include <map>
using namespace std;
using BWAPI::UnitType;
extern ofstream logfile;

/* production targets cause factories to produce until they are met. To build additional
	Units, must set a higher target as it counts total units in the game. This way it 
	automatically replenishes the army as it takes losses.
*/
struct PlayerState // only CMDR allowed to update
{
	enum devState_t{EARLY_GAME, // pick and finish a build order
		MID_GAME,	// pick and finish a transition, activate policies
		LATE_GAME	// able to pick none for buildgoal.
	};
	devState_t devState; // current development level, tech tier MID = 2
	bool isMapAnalyzed; // has BWTA been run?
	unsigned short supplyAvailable; // store how much free supply is available
	map<UnitType,int> unitProductionTargets_; // used by autotrain, sets army composition
	//accessors
	int getProductionTarget(UnitType ut) const; // returns target number of that unitType
	//mutators
	void setProductionTarget(UnitType ut, int num); // cmdr uses this to initialize production targets
	void increaseProductionTarget(UnitType ut, int num); // num is added to current target
};
#endif
