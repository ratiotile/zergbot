#include "ProposedTarget.h"
#include "common/cpp_includes.h"

ProposedTarget::ProposedTarget( TGptr target, UAptr proposer ):
target_(target),
frameProposed_(Broodwar->getFrameCount()),
gatherPoint_(BWAPI::Positions::None)
{
	pledgeUnit( proposer );
}

bool ProposedTarget::pledgeUnit( UAptr pledge )
{
	bool success = pledged_.insert( pledge ).second;
	return success;
}