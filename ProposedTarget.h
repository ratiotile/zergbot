#pragma once
#ifndef INCLUDED_PROPOSEDTARGET_H
#define INCLUDED_PROPOSEDTARGET_H
#include "common/typedefs.h"
#include "typedef_UnitAgent.h"
#include <BWAPI.h>

class TargetGroup;
class ProposedTarget
{
public:
	ProposedTarget( TGptr target, UAptr proposer );
	/// add a pledged unit, and this PT to unitAgent
	bool pledgeUnit( UAptr pledge );
private:
	TGptr target_; // proposed target to attack
	int frameProposed_;
	int framesToLive_; // frames to get min pledges before killing proposal
	BWAPI::Position gatherPoint_; // for units to gather while waiting
	UAset pledged_;	// set of UAptr for pledged units.
};
#endif