#include "RegionInfo.h"
#include "common/cpp_includes.h"
#include "common/typedefs.h"
#include "TABW/Region.h"
#include "ChokeInfo.h"
#include "baseLocInfo.h"

regionInfo::regionInfo( TABW::pRegion region ):
controller(NULL),
numLocations(1), // for center
mID(region->ID()),
priority(0),
lastScoutedBase(0),
lastMissionFrame(0),
lastSeenCenter(0),
explorationStatus(0),
mCenter(region->center()),
mSize(region->size()),
mConnectivity(region->connectivity()),
enemyPresence(false)
{
	
}
BWAPI::Position regionInfo::center() const
{
	return mCenter;
}

int regionInfo::countUnexploredLocations() const
{
	int count = 0;
	BOOST_FOREACH(chokeInfo* choke, chokes)
	{
		if (choke->frameLastVisible == 0)
		{
			count++;
		}
	}
	BOOST_FOREACH(baseLocInfo* bloc, baseLocs)
	{
		if (bloc->frameLastVisible == 0)
		{
			count++;
		}
	}
	if (lastSeenCenter == 0)
	{
		count++;
	}
	return count;
}

void regionInfo::drawRegionInfo() const
{
	// exclude small regions
	if (isIsland() && mID>20)
	{
		return;
	}
	// draw owner
	if ( controller )
	{
		Position c = center();
		Broodwar->drawTextMap( c.x(), c.y(),
			BWAPIext::getPlayerNameID(controller).c_str() );
	} // draw exploration info
	Broodwar->drawTextMap(mCenter.x(),mCenter.y()+14,
		"ID:%i Exploration:%i/%i",mID,numLocations-countUnexploredLocations(),numLocations);
	string status = "";
	if (getExplorationLevel() == 2 && !enemyPresence)
	{
		status = "Clear";
	}
	if (enemyPresence)
	{
		status = "Enemy Present";
	}
	Broodwar->drawTextMap(mCenter.x(),mCenter.y()+28,"%s",status.c_str());
}

int regionInfo::getExplorationLevel() const
{
	int numUnexplored = countUnexploredLocations();
	if (numUnexplored == numLocations) return 0;
	else if (numUnexplored > 0) return 1;
	else return 2;
}

bool regionInfo::areUnitsPresent( Player* player ) const
{
	return playerPresence.find(player) != playerPresence.end();
}
// const baseLocInfo* regionInfo::getBaseLocInfo( BWTA::BaseLocation* bloc) const
// {
// 	// iterate through baseLocs Don't use BOOST_FOREACH!
// 	for (int i = 0; i < baseLocs.size(); i++)
// 	{
// 		if (baseLocs[i].depotPosition == bloc)
// 		{
// 			return &baseLocs[i];
// 		}
// 	}
// 	logfile << "regionInfo::getBaseLocInfo - bloc not found!\n";
// 	return NULL;
// }
/*
void regionInfo::addChokepoint( wpChoke chokepoint )
{
	mChokepoints.insert(chokepoint);
}

void regionInfo::addResourceCluster( pResourceCluster cluster )
{
	mResourceClusters.insert(cluster);
}*/