#pragma once
#ifndef INCLUDED_REGIONINFO_H
#define INCLUDED_REGIONINFO_H

#ifndef INCLUDED_LOGGING
	#define INCLUDED_LOGGING
	#include <fstream>
	#endif
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#include <boost/foreach.hpp>
#include "common/typedefs.h"
#include "TABW/TABW.h"

using namespace std;
using BWAPI::TilePosition;

extern ofstream logfile;

class ResourceCluster;
class TABW::Region;
class chokeInfo;
class baseLocInfo;

/**	structure to hold information about a region. ReconManager keeps a vector of regionInfo, to store information
	about each region of the map. Tracks how long since one of our units has been to the region's center, and
	also who owns the region.
*/
class regionInfo
{
public:
	regionInfo(TABW::pRegion region);
	// interface
	BWAPI::Position center() const;
	// new code
	/// going to use resource cluster instead of baselocs
// 	std::set<pResourceCluster> mResourceClusters;
// 	void addResourceCluster( pResourceCluster cluster );
// 	std::set<pResourceCluster>& resourceClusters() 
// 		{ return mResourceClusters; }
	TABW::pRegion region;
	std::set<TABW::pRegion> adjacentRegions;
	std::set<chokeInfo*> chokes;
	std::set<baseLocInfo*> baseLocs;

	/// last frame baseLocation of region was scouted. If more than 1, the longer time.
	unsigned int lastScoutedBase; 
	/// frame that last mission was sent on.
	unsigned int lastMissionFrame; 
	/// last frame center of the region was visible
	unsigned int lastSeenCenter;
	/// importance of scouting this region
	int priority;	
	/// player that owns this region. NULL if Neutral
	BWAPI::Player* controller;
	/// 0: unexplored, 1: visited, 2: explored
	char explorationStatus; 
	/// total of chokes, baseLocs
	unsigned int numLocations; 
	/// does enemy have units in region?
	bool enemyPresence;
	/// which players have units here
	std::set<BWAPI::Player*> playerPresence;
	bool areUnitsPresent(BWAPI::Player* player) const;
	bool isConnected( const regionInfo* other ) const { return mID == other->ID(); }

	/// connectivity ID from TABW
	int ID() const {return mID;}
	/// is this region connected to any others?
	bool isIsland() const{ return chokes.empty(); }
	/// chokes and baselocs not explored
	int countUnexploredLocations() const;
	/// 0 = unexplored, 1 = visited, 2 = explored
	int getExplorationLevel() const;

	/// draw owner and exploration info
	void drawRegionInfo() const;

private:
	int mID;
	BWAPI::Position mCenter;
	/// in walk tiles
	int mSize;
	/// same numbers are connected
	int mConnectivity;
};

typedef boost::shared_ptr<regionInfo> pRInfo;
typedef boost::weak_ptr<regionInfo> wpRInfo;

#endif