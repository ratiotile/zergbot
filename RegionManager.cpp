#include "RegionManager.h"
#include "cpp_includes.h"
#include "ScoutLocation.h"
#include "UnitInfo.h"
#include "GameState.h"
#include "ChokeInfo.h"
#include "Blackboard.h"
#include "Config.h"
#include "TABW/Region.h"
#include "TABW/ResourceCluster.h"
#include "TABW/Chokepoint.h"
#include "baseLocInfo.h"

using boost::shared_ptr;
// test
#include "img/CImg.h"
using namespace cimg_library;

RegionManager::RegionManager():
isMapAnalyzed_(false)
{

}
void RegionManager::onMapAnalyzed()
{
	if (!isMapAnalyzed_)
	{
		init();
		// image draw code
		// get map dim in walk tiles
		if ( BBS::BB.Cfg().get<bool>("RegionManager.save_bmp_BWTA") )
		{
			int mapWidthWT = Broodwar->mapWidth() * 4;
			int mapHeightWT = Broodwar->mapHeight() * 4;
			int numRegions = BWTA::getRegions().size();
			std::map<BWTA::Region*,int> regionToID;
			int count = 0;
			BOOST_FOREACH(BWTA::Region* r, BWTA::getRegions() )
			{
				++count;
				regionToID.insert( std::make_pair(r,count) );
			}
			BWTA::Region* pRegion;
			CImg<unsigned char> img(mapWidthWT, mapHeightWT, 1, 3);
			img.fill(0);
			unsigned char heatmapcolor[3];
			for(int x = 0; x < mapWidthWT; ++x)
			{
				for(int y = 0; y < mapHeightWT; ++y)
				{
					pRegion = BWTA::getRegion(TilePosition(BWAPIext::WalkTile(x ,y)));
					int rid = regionToID[pRegion];
					heatmapcolor[0] = 50 * (rid%5);
					heatmapcolor[2] = 50 * ((rid/5)%5);
					heatmapcolor[1] = 50 * ((rid/25)%5);
					img.draw_point(x, y, heatmapcolor);
				}
			}
			unsigned char white[] = {255,255,255};
			BOOST_FOREACH( BWTA::Chokepoint* c, BWTA::getChokepoints() )
			{
				img.draw_line(c->getSides().second.x()/8, c->getSides().second.y()/8,
					c->getSides().first.x()/8, c->getSides().first.y()/8, white );
				img.draw_circle(c->getCenter().x()/8, c->getCenter().y()/8, 2, 
					white, 1);
			}
			string imgName = "BWTA choke map - ";
			imgName += Broodwar->mapFileName();
			imgName += ".bmp";
			img.save_bmp(imgName.c_str());
		}
	}
}

void RegionManager::init()
{
	isMapAnalyzed_ = true;
	// populate RegionInfos
	//set<BWTA::Region*> allRegions = BWTA::getRegions();
	set<TABW::pRegion> TABW_regions = TABW::getRegions();
	boost::shared_ptr<regionInfo> pRegionInfo;
	//BOOST_FOREACH(BWTA::Region* region, allRegions)
	BOOST_FOREACH(TABW::pRegion region, TABW_regions)
	{	// make new regionInfo
		pRegionInfo.reset(new regionInfo());
		pRegionInfo->ID = region->ID();
		pRegionInfo->priority = 0;
		pRegionInfo->lastScoutedBase = 0;
		pRegionInfo->lastMissionFrame = 0;
		pRegionInfo->region = region;
		pRegionInfo->explorationStatus = 0;
		//fill baseLocs
		//set<BWTA::BaseLocation*> tempBaseLocs = region->getBaseLocations();
		set<TABW::pResourceCluster> tempClusters = region->resourceClusters();
		BOOST_FOREACH(TABW::pResourceCluster cluster, tempClusters)
		{
			baseLocInfo nBLI;
			nBLI.depotPosition = cluster->depotPosition();
			nBLI.controller = NULL;
			nBLI.lastScouted = 0;
			nBLI.priority = 0;
			pRegionInfo->baseLocs.push_back(nBLI);
			pRegionInfo->numLocations++;
		}
		// find adjacent regions
		//set<BWTA::Chokepoint*> tempChokes = region->getChokepoints();
		set<TABW::wpChoke> tempChokes = region->chokepoints();
		BOOST_FOREACH(TABW::wpChoke choke, tempChokes)
		{
			if (TABW::pChoke cPtr = choke.lock())
			{
				pRegionInfo->numLocations++;
				pair<TABW::pRegion,TABW::pRegion> bRegions = cPtr->getRegions();
				if (bRegions.first == region)
				{
					pRegionInfo->adjacentRegions.insert(bRegions.second);
				}
				else //(bRegions.second == region)
				{
					pRegionInfo->adjacentRegions.insert(bRegions.first);
				}
			}
		}
		TABW_regionToRInfo.insert(make_pair(region,pRegionInfo));
		mRegions.insert(pRegionInfo);
	}
	// populate chokes
	BOOST_FOREACH(TABW::pChoke choke, TABW::getChokepoints())
	{
		pCInfo cInfo( new chokeInfo());
		cInfo->choke = choke;
		cInfo->controller = NULL;
		cInfo->lastScouted = 0;
		cInfo->priority = 0;
		mChokes.insert(cInfo);
		TABW_chokeToCInfo.insert(make_pair(choke,wpCInfo(cInfo)));
	}
}

unsigned int RegionManager::countUnexploredLocations( BWTA::Region* region ) const
{
	unsigned int exploredLocations = 0;
	map< BWTA::Region*,shared_ptr<regionInfo>>::const_iterator found 
		= mRegions_.find(region);
	if (found == mRegions_.end())
	{
		logfile << "ERROR: GameState::getUnexploredLocations - region not found!\n";
		Broodwar->printf("ERROR: GameState::getUnexploredLocations - region not found!");
		return 0;
	}
	shared_ptr<regionInfo> rInfo = found->second;
	BOOST_FOREACH(baseLocInfo& bli, rInfo->baseLocs)
	{
		if (bli.lastScouted > 0)
		{
			exploredLocations++;
		}
	}
	typedef pair<BWTA::Chokepoint*,chokeInfo> ChokePair;
	BOOST_FOREACH(ChokePair cp, mChokepoints_) // fine because not modifying
	{
		pair<BWTA::Region*,BWTA::Region*> chokeRegions = cp.first->getRegions();
		if (chokeRegions.first == region || chokeRegions.second == region)
		{
			exploredLocations++;
		}
	}
	if (exploredLocations > rInfo->numLocations)
	{
		logfile << "ERROR: GameState::getUnexploredLocations - exploredLocations greater than total!\n";
		Broodwar->printf("ERROR: GameState::getUnexploredLocations - exploredLocations greater than total!");
		return 0;
	} 
	else return rInfo->numLocations - exploredLocations;
}

void RegionManager::updateRegionInfo()
{
	// scan chokes and baseLocations for change.
	typedef pair<BWTA::Region*, shared_ptr<regionInfo>> mapType;
	BOOST_FOREACH( const mapType regionPair , mRegions_)
	{
		shared_ptr<regionInfo> rInfo = regionPair.second;
		BOOST_FOREACH(baseLocInfo& bli, rInfo->baseLocs)
		{
			if (Broodwar->isVisible(bli.depotPosition))
			{
				bli.lastScouted = Broodwar->getFrameCount();
			}
		}
	}
	// BOOST foreach messes up here by copying...
	map<BWTA::Chokepoint*,chokeInfo>::iterator cpi = mChokepoints_.begin();
	for (cpi; cpi != mChokepoints_.end(); cpi++)
	{
		BWTA::Chokepoint* choke = cpi->first;
		if (Broodwar->isVisible(TilePosition(choke->getCenter())))
		{
			cpi->second.lastScouted = Broodwar->getFrameCount();
		}
	}
}

char RegionManager::getRegionExplorationLevel( BWTA::Region* region ) const
{// 0 = unexplored, 1 = visited, 2 = explored
	map<BWTA::Region*,shared_ptr<regionInfo>>::const_iterator found 
		= mRegions_.find(region);
	if (found == mRegions_.end())
	{
		logfile << "ERROR: RegionManager::getRegionExplorationLevel - region not found!\n";
		Broodwar->printf("ERROR: RegionManager::getUnexploredLocations - region not found!");
		return 0;
	}
	unsigned int numUnexploredLocations = countUnexploredLocations(region);
	if (numUnexploredLocations == found->second->numLocations) return 0;
	else if (numUnexploredLocations > 0) return 1;
	else return 2;
}

// const baseLocInfo* RegionManager::getBaseLocInfo( BWTA::BaseLocation* bloc ) const
// {
// 	BWTA::Region* bregion = bloc->getRegion();
// 	std::map<BWTA::Region*,boost::shared_ptr<regionInfo>>::const_iterator found =
// 		mRegions_.find(bregion);
// 	if (found != mRegions_.end())
// 	{
// 		return found->second->getBaseLocInfo(bloc);
// 	}
// 	else
// 	{
// 		logfile << "RegionManager::getBaseLocInfo - Region not found.\n";
// 		return NULL;
// 	}
// }

void RegionManager::updateFrameLastSeen( ScoutLocation& sl )
{
	if (sl.getType() == ScoutLocation::CHOKEPOINT)
	{
		std::map<BWTA::Chokepoint*,chokeInfo>::iterator found =
			mChokepoints_.find( sl.getChoke() );
		if (found != mChokepoints_.end())
		{
			found->second.lastScouted = Broodwar->getFrameCount();
			return;
		} 
		else
		{
			logfile << "RegionManager::updateFrameLastSeen - Choke not found.\n";
		}
	}
	if (sl.getType() == ScoutLocation::BASELOCATION)
	{
		std::map<BWTA::Region*,boost::shared_ptr<regionInfo>>::iterator found 
			= mRegions_.find(sl.getARegion()); // there is only one in this case
		if (found != mRegions_.end())
		{	// Can't use BOOST_FOREACH HERE!
			for(int i = 0; i < found->second->baseLocs.size(); i++)
			{
// 				if (found->second->baseLocs[i].depotPosition == sl.getBase())
// 				{
// 					found->second->baseLocs[i].lastScouted = Broodwar->getFrameCount();
// 					return;
// 				}

			}
		}
		logfile << "RegionManager::update FrameLastSeen - Bloc not found.\n";
	}
}

const chokeInfo* RegionManager::getChokeInfo( BWTA::Chokepoint* choke ) const
{
	std::map<BWTA::Chokepoint*,chokeInfo>::const_iterator found = 
		mChokepoints_.find(choke);
	if(found != mChokepoints_.end())	// success
		return &(found->second);
	else
	{
		logfile << "RegionManager::getChokeInfo - cannot find choke\n";
		return NULL;
	}
}

void RegionManager::drawRegionPaths() const
{
	if (!isMapAnalyzed_)
	{
		return;
	}
	typedef pair<BWTA::Region*, shared_ptr<regionInfo>> mapType;
	BOOST_FOREACH( const mapType& regionPair , mRegions_)
	{
		Position cPos = regionPair.first->getCenter();
		/*
		BOOST_FOREACH( baseLocInfo& BLI, regionPair.second->baseLocs)
		{
			Position iPos = BLI.depotPosition->getPosition();
			Broodwar->drawLineMap(cPos.x(),cPos.y(),iPos.x(),iPos.y(),
				Colors::Green);
		}
		*/
		BOOST_FOREACH( BWTA::Chokepoint* choke, 
			regionPair.first->getChokepoints() )
		{
			Position iPos = choke->getCenter();
			BWAPI::Color lineColor = Colors::Orange;
			tChokeMap::const_iterator cFound = mChokepoints_.find(choke);
			assert(cFound != mChokepoints_.end());
			if (cFound->second.isClear)
			{
				lineColor = Colors::Blue;
			}
			Broodwar->drawLineMap(cPos.x(),cPos.y(),iPos.x(),iPos.y(),
				lineColor);
		}
	}
	
}

void RegionManager::update()
{
	if( !isMapAnalyzed_ ) return;
	// scan for enemy units near each choke, and set isClear
	Player* targetPlayer = Broodwar->self();
	if (Broodwar->isReplay())
	{
		targetPlayer = BWAPIext::getTerranPlayer();
	}
	if (!targetPlayer) return;
	typedef std::set<UIptr> setUI;
	tChokeMap::iterator chokeIter = mChokepoints_.begin();
	for ( chokeIter; chokeIter != mChokepoints_.end(); ++chokeIter)
	{
		chokeIter->second.isClear = true;
		setUI nearbyUnits;
		GameState::getInstance().getBanditsInRadius( 
			chokeIter->first->getCenter(), 32*12, nearbyUnits );
		if (nearbyUnits.size() > 0);
		{

		}
		// strip out units that are not enemies of targetPlayer
		setUI::iterator NI = nearbyUnits.begin();
		while(NI != nearbyUnits.end())
		{
			if( !targetPlayer->isEnemy( (*NI)->getOwner() ) )
			{
				setUI::iterator toDelete = NI;
				++NI;
				nearbyUnits.erase(toDelete);
			}
			else ++NI;
		}
		if ( !nearbyUnits.empty() ) 
		{
			chokeIter->second.isClear = false;
			continue;
		}
	}
	// update ownership of each region
	BOOST_FOREACH( regionsPair rp, mRegions_ )
	{
		calculateRegionOwnership( rp.second );
	}
}

void RegionManager::calculateRegionOwnership(shared_ptr<regionInfo> spRegion)
{
// 	// grab all players and build presence map
// 	set<Player*> players = Broodwar->getPlayers();
// 	map<Player*,bool> playerPresence;
// 	for ( set<Player*>::iterator pi = players.begin(); pi != players.end();
// 		++pi )
// 	{
// 		if ( !(*pi)->isNeutral() )
// 		{
// 			playerPresence[*pi] = false;
// 		}
// 	}
// 	int search_radius = 12*32; // 12 tiles
// 	// use BWAPIext to strip Neutral units from BWAPI functionality
// 	set<Unit*> myUnits = BWAPIext::getUnitsInRadius(
// 		spRegion->region->getCenter(), search_radius );
// 	// iterate through
// 	for ( set<Unit*>::iterator mui = myUnits.begin(); mui != myUnits.end(); 
// 		++mui )
// 	{	// check ownership, since it is least expensive test
// 		Unit* u = *mui;
// 		Player* unitOwner = (*mui)->getPlayer();
//  		assert( playerPresence.find( unitOwner ) != playerPresence.end() );
// 		if ( playerPresence[unitOwner] == true 
// 			|| BWAPIext::isUnitNonCombat(u) /* unit cannot attack */ )
// 		{	// presence already detected, no need to go farther
// 			continue;
// 		}
// 		// filter out units that are not inside region; for small regions
// 		assert( u->getTilePosition().isValid() );
// 		if ( spRegion->region == BWTA::getRegion( u->getTilePosition() ) )
// 		{	
// 			playerPresence[unitOwner] = true;
// 		}
//  	}
// 	// if only single player presence, that player owns, else no one does
// 	int count = 0;
// 	Player* controller = NULL;
// 	for ( map<Player*,bool>::const_iterator ci = playerPresence.begin();
// 		ci != playerPresence.end(); ++ci )
// 	{
// 		if (ci->second == true)
// 		{ 
// 			++count;
// 			controller = ci->first;
// 		}
// 	}
// 	spRegion->controller = NULL;
// 	if (count == 1)
// 	{
// 		spRegion->controller = controller;
// 	}
}

void RegionManager::drawRegionOwners() const
{
	BOOST_FOREACH( regionsPair rp, mRegions_ )
	{
		Player* controller = rp.second->controller;
		if ( controller != NULL )
		{
			Position c = rp.first->getCenter();
			Broodwar->drawTextMap( c.x(), c.y(),
				BWAPIext::getPlayerNameID(controller).c_str() );
		}
	}
}

BWAPI::Position RegionManager::getNearestSafePosition( BWAPI::Position pos ) const
{
	return Position( Broodwar->self()->getStartLocation() );
}