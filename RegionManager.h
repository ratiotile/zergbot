#pragma once
#ifndef INCLUDED_REGIONMANAGER_H
#define INCLUDED_REGIONMANAGER_H

#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#ifndef INCLUDED_BWTA_H
	#define INCLUDED_BWTA_H
	#include <BWTA.h>
#endif
#include <boost/smart_ptr.hpp>
#include "RegionInfo.h"
#include "TABW/TABW.h"

/**		Region Manager - component of GameState
Handles regions using RegionInfo.
Computes and tracks Region Paths.
Tracks Region exploration, and control.
Tracks locations of our and enemy bases.

Can't be initialized until map is analyzed.
*/
struct baseLocInfo;
struct chokeInfo;
class ScoutLocation;


class RegionManager
{
	typedef std::map<BWTA::Region*,boost::shared_ptr<regionInfo>> tRegionMap;
	typedef pair<BWTA::Region*,boost::shared_ptr<regionInfo>> regionsPair;
	typedef std::map<BWTA::Chokepoint*,chokeInfo> tChokeMap;

	typedef boost::shared_ptr<regionInfo> pRInfo;
	typedef boost::weak_ptr<regionInfo> wpRInfo;
	typedef boost::shared_ptr<chokeInfo> pCInfo;
	typedef boost::weak_ptr<chokeInfo> wpCInfo;
	typedef boost::shared_ptr<baseLocInfo> pBInfo;
	
public:
	/// constructor, called on gameState initiation
	RegionManager();
	/// called by GameState once map has been analyzed by BWTA
	void onMapAnalyzed();

	/// port from GameState
	unsigned int countUnexploredLocations(BWTA::Region* region) const;
	void updateRegionInfo(); // exploration information
	char getRegionExplorationLevel(BWTA::Region* region) const;
	/// will be empty before map analyzed
	const tRegionMap& getRegionInfos() const {return mRegions_;}
	/*const baseLocInfo* getBaseLocInfo( BWTA::BaseLocation* bloc) const;*/
	void updateFrameLastSeen(ScoutLocation& sl);
	const std::map<BWTA::Chokepoint*,chokeInfo>& getChokeInfos() const
	{return mChokepoints_;}
	const chokeInfo* getChokeInfo( BWTA::Chokepoint* choke) const;

	// every frame updates?
	/// 
	void update();
	// draw
	/// display RegionPaths, blue for clear, else orange
	void drawRegionPaths() const;
	/// draws the name of the controlling player if exists, at region center 
	void drawRegionOwners() const;
	/// returns if RegionMgr has been initialized
	bool isReady() const { return isMapAnalyzed_; }
	/// gives Start Location for now
	BWAPI::Position getNearestSafePosition( BWAPI::Position pos ) const;
protected:
private:
	/// has map been analyzed by BWTA?
	bool isMapAnalyzed_;
	/// call when map is analyzed; populates region data
	void init();
	// old members BWTA
	/// BWTA::Regions info
	tRegionMap mRegions_;
	/// BWTA::Chokepoint Info
	std::map<BWTA::Chokepoint*,chokeInfo> mChokepoints_;
	/// List of startLocations for quick reference; currently unused!
	std::list<boost::shared_ptr<regionInfo>> lStartLocations_;
	// new members, post TABW
	/// holds the regioninfos
	std::set<pRInfo> mRegions;
	/// map from TABW region to regionInfo
	std::map<TABW::pRegion,wpRInfo> TABW_regionToRInfo;
	/// holds the chokeInfos
	std::set<pCInfo> mChokes;
	/// map from TABW choke to chokeInfo
	std::map<TABW::pChoke,wpCInfo> TABW_chokeToCInfo;

	/// where the camera starts the game.
	BWAPI::Position startLocation_;	

	//helpers
	/// compute Region Ownership - units within regions
	void calculateRegionOwnership( boost::shared_ptr<regionInfo> spRegion );
};

#endif