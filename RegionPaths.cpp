#include "RegionPaths.h"
#include <limits>
#include "common/cpp_includes.h"

int RegionPaths::getRDist( int id1, int id2 ) const
{
	std::map<int, std::map<int,int>>::const_iterator found1 = dist.find(id1);
	std::map<int,int>::const_iterator found2;
	if (found1 != dist.end())
	{
		found2 = found1->second.find(id2);
		if (found2 != (*found1).second.end())
		{
			return found2->second;
		}
	}
	return std::numeric_limits<int>::max();
}

int RegionPaths::getCDist( int i, int j ) const
{
	if ( i < distC.sizeX() )
	{
		if (j < distC.sizeY() )
		{
			return distC[i][j];
		}
	}
	return std::numeric_limits<int>::max();
}

int RegionPaths::getNext( int i, int j ) const
{
	std::map<int, std::map<int,int>>::const_iterator found1 = next.find(i);
	std::map<int,int>::const_iterator found2;
	if (found1 != next.end())
	{
		found2 = found1->second.find(j);
		if (found2 != (*found1).second.end())
		{
			return found2->second;
		}
	}
	return 0;
}

int RegionPaths::getPredecessor( int i, int j ) const
{
	return predecessor[i][j];
}

/// region path including start and end regions.
std::list<int> RegionPaths::getPath( int source, int dest ) const
{
	list<int> path = getPath2(source,dest);
	path.push_front(source);
	return path;
}

/// returns from one step past beginning to the end.
std::list<int> RegionPaths::getPath2( int source, int dest ) const
{
	list<int> path;
	if( getRDist(source,dest) == 0 )
	{
		//path.push_back(source);
		return path;
	}
	int k = getNext(source,dest);
	if ( k == 0 )
	{
		path.push_back(dest);
		return path;
	}
	else
	{
		path = getPath2(source,k);
		list<int> b = getPath2(k,dest);
		//path.push_back(k);
		path.splice(path.end(),b);
		return path;
	}
}

std::list<int> RegionPaths::getChokePath( int fromChoke, int toChoke ) const
{
	list<int> path;
	if( getCDist(fromChoke,toChoke) == 0 )
	{
		return path;
	}
	int k = getPredecessor(fromChoke,toChoke);
	if ( k == 0 )
	{
		path.push_back(toChoke);
		return path;
	}
	else
	{
		path = getChokePath(fromChoke,k);
		list<int> b = getChokePath(k,toChoke);
		path.push_back(k);
		path.splice(path.end(),b);
		return path;
	}
}
