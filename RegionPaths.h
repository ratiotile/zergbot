#include <map>
#include <list>
#include "TABW/RectangleArray.h"

struct RegionPaths
{
	std::map<int, std::map<int,int>> dist;
	std::map<int, std::map<int,int>> next; // last intermediate for path reconstruction
	int getRDist( int id1, int id2 ) const;
	int getNext( int i, int j ) const; // needed for const
	// returns list of Region IDs.
	std::list<int> getPath(int source, int dest) const;
	std::list<int> getPath2(int source, int dest) const;


	// choke paths
	RectangleArray<int> distC;
	/// next choke
	RectangleArray<int> predecessor;
	int getCDist( int i, int j ) const;
	int getPredecessor( int i, int j ) const;
	// returns a list of choke IDs.
	std::list<int> getChokePath(int fromChoke, int toChoke) const;
	
};