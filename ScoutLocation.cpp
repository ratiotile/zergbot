#include "ScoutLocation.h"
#include "ChokeInfo.h"
#include "RegionInfo.h"
#include "baseLocInfo.h"

using namespace std;

ScoutLocation::ScoutLocation( chokeInfo* pChoke ):
choke_(pChoke), base_(NULL), type_(CHOKEPOINT), pos_(pChoke->center()),
assignedScout(NULL)
{

}

ScoutLocation::ScoutLocation( baseLocInfo* pBase ):
choke_(NULL), base_(pBase), type_(BASELOCATION), pos_(pBase->center()),
assignedScout(NULL)
{

}

bool ScoutLocation::isGreaterThan( const ScoutLocation& first, const ScoutLocation& second )
{
	return first.score > second.score;
}

BWAPI::Position ScoutLocation::getPosition() const
{
	return pos_;
}

ScoutLocation::location_t ScoutLocation::getType() const
{
	return type_;
}

chokeInfo* ScoutLocation::getChoke() const
{
	return choke_;
}

baseLocInfo* ScoutLocation::getBase() const
{
	return base_;
}

std::list<regionInfo*> ScoutLocation::getRegions() const
{
	list<regionInfo*> regList;
	if (choke_)
	{
		regList.push_back(choke_->regions().first);
		regList.push_back(choke_->regions().second);
	}
	else if (base_)
	{
		regList.push_back(base_->region );
	}
	return regList;
}

bool ScoutLocation::isInRegion( const regionInfo* r ) const
{
	list<regionInfo*> regions = getRegions();
	BOOST_FOREACH(regionInfo* slr, regions)
	{
		if (slr == r)
			return true;
	}
	return false;
}

regionInfo* ScoutLocation::getARegion() const
{
	if (choke_) return choke_->regions().first;
	if (base_) return base_->region;
	logfile << "ScoutLocation::getARegion - region not found!\n";
	return NULL;
}