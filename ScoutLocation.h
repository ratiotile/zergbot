/************************************************************************/
/* ScoutLocation encapsulates a chokepoint or baseLocation              */
/************************************************************************/
#ifndef INCLUDED_SCOUTLOCATION_H
#define INCLUDED_SCOUTLOCATION_H
#pragma once

#include <list>
#include <boost/foreach.hpp>
#ifndef INCLUDED_LOGGING
	#define INCLUDED_LOGGING
	#include <fstream>
#endif
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#ifndef INCLUDED_PRINT_H
	#include "common/print.h"
#endif

extern ofstream logfile;
class UnitAgent;
class chokeInfo;
class baseLocInfo;
class regionInfo;
class ScoutLocation
{
public:
	ScoutLocation(chokeInfo* pChoke);
	ScoutLocation(baseLocInfo* pBase);
	enum location_t{
		BASELOCATION,
		CHOKEPOINT
	};
	BWAPI::Unit* assignedScout;
	int score;	// used in evaluation
	static bool isGreaterThan(const ScoutLocation& first, const ScoutLocation& second);
	BWAPI::Position getPosition() const;
	location_t getType() const;
	chokeInfo* getChoke() const;
	baseLocInfo* getBase() const;
	std::list<regionInfo*> getRegions() const;
	regionInfo* getARegion() const; // returns first region of choke
	bool isInRegion(const regionInfo* r) const;
private:
	location_t type_;
	chokeInfo* choke_;	// hold one of these
	baseLocInfo* base_;
	BWAPI::Position pos_;
};

#endif