#include "ScoutManager.h"
#include "ChokeInfo.h"
#include "MapInfo.h"


using BWAPI::Position;

ScoutManager* ScoutManager::pInstance_ = NULL;
ScoutManager::ScoutManager(): isInitialized(false)
{
}
ScoutManager::~ScoutManager()
{
}
ScoutManager& ScoutManager::getInstance()
{
	if (!pInstance_)
		pInstance_ = new ScoutManager();
	return *pInstance_;
}

void ScoutManager::initScoutLocations()
{
	typedef pair<chokeInfo*, chokeInfo> ChokePair;
	BOOST_FOREACH(chokeInfo* choke, MapInfo::get().chokes() )
	{
		vScoutLocations_.push_back(ScoutLocation(choke));
	}
	BOOST_FOREACH(baseLocInfo* bloc, MapInfo::get().baseLocs() )
	{
		vScoutLocations_.push_back(ScoutLocation(bloc));
	}
	isInitialized = true;
}

void ScoutManager::sortScoutLocations()
{
	sort(vScoutLocations_.begin(), vScoutLocations_.end(), ScoutLocation::isGreaterThan);
}

void ScoutManager::evaluateScoutLocation( ScoutLocation& sl, const  BWAPI::Unit* pScout )
{
// 	logfile << "evaluateScoutLocation: enter\n";
// 	logWriteBuffer();
	const regionInfo* scoutRegion = 
		MapInfo::get().getRegion( pScout->getTilePosition() );
// 	logfile << "evaluateScoutLocation: got scoutRegion\n";
// 	logWriteBuffer();
	int dist = pScout->getPosition().getApproxDistance(sl.getPosition());
	int frames = Broodwar->getFrameCount() - getFramesLastSeen(sl);
	bool isSameRegionScout = false;
	if (!scoutRegion)
	{
		logfile << Broodwar->getFrameCount() << 
			": evaluateScoutLocation: getRegion returned NULL."
			<< " evaluating based solely on last visited.\n";
		logWriteBuffer();
	}
	else isSameRegionScout = sl.isInRegion(scoutRegion);	// +1000
	char expState = 0;
	if (isSameRegionScout)
	{
		expState = scoutRegion->explorationStatus;
	}
	else
	{
		expState = sl.getARegion()->explorationStatus;
	}
	sl.score = frames - dist/6 + 1000*isSameRegionScout - 500*expState;
}

int ScoutManager::getFramesLastSeen( const ScoutLocation& sl ) const
{
	if (sl.getType() == ScoutLocation::CHOKEPOINT)
		return sl.getChoke()->frameLastVisible;
// 	else if (sl.getType() == ScoutLocation::BASELOCATION)
// 		return GameState::getInstance().Regions().
// 		getBaseLocInfo(sl.getBase())->frameLastVisible;
	else
	{
		return 0;
		logfile << "ScoutManager::getFramesLastSeen - ScoutLocation is unknown!\n";
	}
}

BWAPI::Position ScoutManager::getNextScoutLocation( BWAPI::Unit* pScout )
{	// ensure GameState is initialized
	//logfile << "getNextScoutLocation: entering\n";
	if (!GameState::get().isMapAnalyzed())
	{
		logfile << "ScoutManager::getNextScoutLocation - GameState says map not analyzed!\n";
		return BWAPI::Positions::None;
	}	// evaluate all positions
	BOOST_FOREACH(ScoutLocation& sl, vScoutLocations_)
	{
		evaluateScoutLocation(sl, pScout);
	}
	//logfile << "getNextScoutLocation: Sorting scoutLocations\n";
	//logWriteBuffer();
	sortScoutLocations();
	//logfile << "getNextScoutLocation: selecting scoutLocation\n";
	//logWriteBuffer();
	// skip over locations already assigned to scouts
	BOOST_FOREACH(ScoutLocation& sl, vScoutLocations_)
	{
// 		if (sl.assignedScout && !sl.assignedScout->exists()) // check if unit is dead
// 		{
// 			sl.assignedScout = NULL;
// 		}
		if (sl.assignedScout == NULL && sl.getPosition().getApproxDistance(pScout->getPosition()) > 8*32)
		{
			sl.assignedScout = pScout;
// 			logfile << "getNextScoutLocation: Found location, returning\n";
// 			logWriteBuffer();
			return sl.getPosition();
		}
	}
	logfile << "ScoutManager::getNextScoutLocation - failed to find any locations!\n";
	logWriteBuffer();
	return BWAPI::Positions::None;
}

void ScoutManager::scoutMissionSuccess( BWAPI::Unit* pScout, BWAPI::Position loc )
{
	BOOST_FOREACH(ScoutLocation& sl, vScoutLocations_)
	{
		if (sl.assignedScout == pScout)
		{
			if (sl.getPosition() == loc)
			{	// update frame seen
				updateFrameLastSeen(sl);
			}
			sl.assignedScout = NULL;
		}
	}
}

void ScoutManager::updateFrameLastSeen( ScoutLocation& sl )
{
	//GameState::getInstance().updateFrameLastSeen(sl);
	// no need to update, MapInfo should do it automatically
}

void ScoutManager::scoutMissionAbort( BWAPI::Unit* pScout )
{
	BOOST_FOREACH(ScoutLocation& sl, vScoutLocations_)
	{
		if (sl.assignedScout == pScout)
		{
			sl.assignedScout = NULL;
		}
	}
}

void ScoutManager::drawScoutLocations() const
{
// 	if (GameState::getInstance().isMapAnalyzed())
// 	{
// 		BOOST_FOREACH(ScoutLocation sl, vScoutLocations_)
// 		{
// 			unsigned int frames = 0;
// 			if (sl.getType() == ScoutLocation::BASELOCATION)
// 			{
// 				frames = 
// 					GameState::getInstance().getBaseLocInfo(
// 					sl.getBase() )->frameLastVisible;
// 			}
// 			if (sl.getType() == ScoutLocation::CHOKEPOINT)
// 			{
// 				frames =
// 					GameState::getInstance().Regions().
// 					getChokeInfo(sl.getChoke())->frameLastVisible;
// 			}
// 			Broodwar->drawTextMap(sl.getPosition().x(),sl.getPosition().y()-16,
// 				"ScoutLocation: %d",Broodwar->getFrameCount() - frames);
// 			if (sl.assignedScout)
// 			{
// 				Broodwar->drawTextMap(sl.getPosition().x(),sl.getPosition().y(),
// 					"Assigned: %d",sl.assignedScout->getID());
// 			}
// 		}
// 	}
}

void ScoutManager::onFrame()
{
	if (!isInitialized && GameState::get().isMapAnalyzed())
	{
		initScoutLocations();
	}
	if (isInitialized)
	{
		drawScoutLocations();
	}
	
}

void ScoutManager::setScoutMax( BWAPI::UnitType scoutType, int numMax )
{
	// validate input
	if (numMax < 0)
	{
		numMax = 0;
		logfile << "setScoutMax: invalid input: " << numMax <<endl;
	}
	maxScouts[scoutType] = numMax;
	Broodwar->printf("Max number of %s scouts set to %d",scoutType.c_str(),numMax);
}