/************************************************************************/
/* ScoutManager tracks good places to explore and which units are		*/
/* exploring where.														*/
/************************************************************************/

#ifndef INCLUDED_SCOUTMANAGER_H
#define INCLUDED_SCOUTMANAGER_H
#pragma once

#include <vector>
#include <map>
#include <algorithm>
#include <boost/foreach.hpp>
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#ifndef INCLUDED_GAMESTATE_H
	#include "GameState.h"
#endif
#ifndef INCLUDED_SCOUTLOCATION_H
	#include "ScoutLocation.h"
#endif
#ifndef INCLUDED_REGIONINFO_H
	#include "RegionInfo.h"
#endif

class ScoutManager
{
public:
	static ScoutManager& getInstance();
	~ScoutManager();
	void initScoutLocations();
	BWAPI::Position getNextScoutLocation(BWAPI::Unit* pScout);
	void scoutMissionSuccess(BWAPI::Unit* pScout, BWAPI::Position loc);
	void scoutMissionAbort(BWAPI::Unit* pScout);
	void drawScoutLocations() const;
	void onFrame();

	/// allows a unit type to be used for scouting
	void setScoutMax(BWAPI::UnitType scoutType, int numMax);
	int getScoutMax(BWAPI::UnitType scoutType) {return maxScouts[scoutType];}
private:
	ScoutManager();
	static ScoutManager* pInstance_;
	vector<ScoutLocation> vScoutLocations_;
	bool isInitialized;
	/// max number of scouts of that type
	std::map<BWAPI::UnitType,int> maxScouts;

	// helpers
	void evaluateScoutLocation(ScoutLocation& sl, const BWAPI::Unit* pScout); //sets score on the ScoutLocation
	void sortScoutLocations(); //descending order of score
	int getFramesLastSeen(const ScoutLocation& sl) const;
	void updateFrameLastSeen(ScoutLocation& sl);
};

#endif