#include "Squad.h"
#include "common/cpp_includes.h"
#include "MapInfo.h"
#include "UnitAgent.h"
#include "TargetManager.h"
#include "UnitManager.h"
#include "Vector2D.h"


Squad::Squad()
:mIsUnderAttack(false)
,mState(SquadState::None)
{

}
void Squad::update()
{
// 	if (Broodwar->getFrameCount() % cUpdateInterval != 0)
// 	{
// 		return;
// 	}
	// gather units if they are too far away and not in right region.
	checkIfUnderAttack();
	eraseDead();
	computeStatistics();
	if (mBehavior == UnitBehaviors::Rush ) Rush();
	else if (mBehavior == UnitBehaviors::Observe) Observe();
	else if (mBehavior == UnitBehaviors::Defend) DefendRegion();
	executeState();
	drawDebug();
}

void Squad::setTargetPosition( BWAPI::Position targetPos )
{
	mTargetPosition = targetPos;
}

void Squad::setBehavior( UnitBehaviors::Enum behavior )
{
	mBehavior = behavior;
}

void Squad::onUnitDestroy( BWAPI::Unit* unit )
{
	if (unit->getPlayer() != Broodwar->self()) return;

	UAptr pUnit = UnitManager::get().getUAptr(unit);
	if (pUnit) // just erase from set
	{
		UAset::iterator found = mUnits.find(pUnit);
		if (found != mUnits.end())
		{
			mUnits.erase(found);
		}
	}
	else // iterate through set
	{
		for (UAset::iterator iter = mUnits.begin(); iter != mUnits.end(); ++iter)
		{
			if ((*iter)->getUnit() == unit)
			{
				mUnits.erase(iter);
				return;
			}
		}
	}
}

void Squad::Rush()
{
	if (mState == SquadState::None)
	{
		mState = SquadState::Attack;
	}
	if (mState == SquadState::Retreat)
	{
		mState = SquadState::Regroup;
	}
	else if (mState == SquadState::Regroup && mSpread < getMaxSpread())
	{
		mState = SquadState::Attack;
	}
}

void Squad::Observe()
{
	if (mState == SquadState::None)
	{
		mState = SquadState::Attack;
	}
	if (mIsUnderAttack) // retreat!
	{
		mState = SquadState::Retreat;
	}
	else
	{	// regroup to attack
		if (mState == SquadState::Retreat)
		{
			mState = SquadState::Regroup;
		}
		else if (mState == SquadState::Regroup && mSpread < getMaxSpread())
		{
			mState = SquadState::Attack;
		}
	}
}

void Squad::DefendRegion()
{
	mState = SquadState::None;
	const regionInfo* defendRegion = MapInfo::get().getRegion(mTargetPosition);
	set<UIptr> enemyInRegion; 
	GameState::get().getEnemyInRegion(defendRegion, enemyInRegion);
	if (!enemyInRegion.empty())
	{
		mState = SquadState::Defend;
	}
	else if (mSpread > getMaxSpread())
	{
		mState = SquadState::Regroup;
	}
	else if (mCenter.getApproxDistance(mTargetPosition) > getMaxSpread()/3)
	{
		mState = SquadState::Move;
	}
	else
	{
		mState = SquadState::None;
	} 
	// draw enemy in region
	for each (UIptr enemy in enemyInRegion)
	{
		Broodwar->drawCircleMap(enemy->getPosition().x(),enemy->getPosition().y(),16,BWAPI::Colors::Red);
	}
}

void Squad::checkIfUnderAttack()
{
	//static int attackTimer = 0;
	//if(attackTimer > 0) attackTimer--;
	mIsUnderAttack = false;

	BOOST_FOREACH( UAptr unit, mUnits )
	{
		if (unit->isThreatened() )
		{
			//attackTimer = 3;
			mIsUnderAttack = true;
			return;
		}
	}
}

void Squad::drawDebug() const
{
	// draw lines to center
	Position center = getCenter();
// 	for each( UAptr unit in mUnits )
// 	{
// 		Broodwar->drawLineMap(unit->getPosition().x(), unit->getPosition().y(), center.x(), center.y(), BWAPI::Colors::Blue);
// 	}
	// draw state near center
	string stateString = stateName();
	Broodwar->drawTextMap(center.x(),center.y(), stateString.c_str() );
	// draw spread
	Broodwar->drawCircleMap(getCenter().x(),getCenter().y(), (int)mSpread, BWAPI::Colors::White);
	Broodwar->drawCircleMap(getCenter().x(),getCenter().y(), getMaxSpread(), BWAPI::Colors::Teal);
	// draw center to targetPosition
	Broodwar->drawLineMap(mCenter.x(), mCenter.y(), mTargetPosition.x(), mTargetPosition.y(), BWAPI::Colors::Teal);
}

Position Squad::getCenter() const
{
	return mCenter;
}

void Squad::executeState()
{
	if (mState == SquadState::None)
	{
		stateNone();
	}
	else if (mState == SquadState::Retreat)
	{
		stateRetreat();
	}
	else if (mState == SquadState::Attack)
	{
		stateAttack();
	}
	else if (mState == SquadState::Regroup)
	{
		stateRegroup();
	}
	else if (mState == SquadState::Defend)
	{
		stateDefend();
	}
	else if (mState == SquadState::Move)
	{
		stateMove();
	}
}

void Squad::stateRetreat()
{
	for (UAset::iterator iter = mUnits.begin(); iter != mUnits.end(); ++iter)
	{
		UAptr unit = *iter;
		assert(unit && unit->getUnit()->exists());
		Position unitPos = unit->getPosition();
		if (mIsUnderAttack) // retreat!
		{
			mState = SquadState::Retreat;
			Position destination = BWAPIext::getCenter(Broodwar->self()->getStartLocation());
			if (!unit->isMovingTo(destination) &&
				MapInfo::get().getRegion(unitPos) != MapInfo::get().getRegion(destination) &&
				unitPos.getApproxDistance(destination) > cAttractRange)
			{
				unit->move(destination);
			}
		}
	}
}

void Squad::eraseDead()
{
	for (UAset::iterator iter = mUnits.begin(); iter != mUnits.end(); )
	{
		UAptr unit = *iter;
		if (!unit || !unit->getUnit()->exists())
		{
			UAset::iterator toErase = iter;
			iter++;
			mUnits.erase(toErase);
			continue;
		} 
		++iter;
	}
}

void Squad::stateAttack()
{
	for (UAset::iterator iter = mUnits.begin(); iter != mUnits.end(); ++iter)
	{
		UAptr unit = *iter;
		Position unitPos = unit->getPosition();
		BWAPI::Unit* target = TargetManager::get().getTarget(unit->getUnit());
		// want to get target first, otherwise we do stupid running through zealots
		if (target && unit->isIdle())
		{
			unit->attackMove(target->getPosition());
		}
		else if (!unit->isMovingTo(mTargetPosition) &&
			MapInfo::get().getRegion(unitPos) != MapInfo::get().getRegion(mTargetPosition) &&
			unitPos.getApproxDistance(mTargetPosition) > cAttractRange)
		{
			unit->attackMove(mTargetPosition);
		}
	}
}

void Squad::stateRegroup()
{
	Position regroupPoint = BWAPIext::getCenter(MapInfo::get().getNearestWalkableTile(getCenter()));
	for (UAset::iterator iter = mUnits.begin(); iter != mUnits.end(); ++iter)
	{
		UAptr unit = *iter;
		unit->move(regroupPoint);
	}
}

string Squad::stateName() const
{
	if (mState == SquadState::None) return "None";
	else if (mState == SquadState::Attack) return "Attack";
	else if (mState == SquadState::Retreat) return "Retreat";
	else if (mState == SquadState::Regroup) return "Regroup";
	else if (mState == SquadState::Defend) return "Defend";
	else if (mState == SquadState::Move) return "Move";
	else return "name unknown";
}

void Squad::computeStatistics()
{
	mCenter = Position(0,0);
	for each(UAptr unit in mUnits)
	{
		mCenter += unit->getPosition();
	}
	if (!mUnits.empty())
	{
		mCenter = Position(mCenter.x()/mUnits.size(), mCenter.y()/mUnits.size());
	}
	mSpread = 0.0;
	for each(UAptr unit in mUnits)
	{
		mSpread += unit->getPosition().getApproxDistance(mCenter);
	}
	if (!mUnits.empty())
	{
		mSpread /= mUnits.size();
	}
}

void Squad::stateDefend()
{
	
	BWAPI::Unit* target = NULL;

		// get unit closest to region center
	target = TargetManager::get().getTarget(mTargetPosition);

	for (UAset::iterator iter = mUnits.begin(); iter != mUnits.end(); ++iter)
	{
		UAptr unit = *iter;
		if (target && unit->isIdle())
		{
			unit->attackMove(target->getPosition());
		}
	}
}

void Squad::stateMove()
{
	for (UAset::iterator iter = mUnits.begin(); iter != mUnits.end(); ++iter)
	{
		UAptr unit = *iter;
		unit->move(mTargetPosition);
	}
}

void Squad::stateNone()
{
	for (UAset::iterator iter = mUnits.begin(); iter != mUnits.end(); ++iter)
	{
		UAptr unit = *iter;
		if (unit->getUnit()->isMoving() && unit->getPosition().getApproxDistance(mTargetPosition) < getMaxSpread())
		{
			unit->halt();
		}	
	}
}