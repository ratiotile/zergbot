/************************************************************************/
/* Squad                                                                  
/************************************************************************/
#pragma once
#include <BWAPI.h>
#include "UnitBehavior.h"
#include "Controller.h"
#include "Interface.h"
#include "TypeSafeEnum.h"

struct SquadStateDef
{
	enum type
	{
		Regroup,
		Retreat,
		Attack,
		None,
		Defend,
		Move
	};
};
typedef SafeEnum<SquadStateDef> SquadState;

class Squad: public Controller
{
public:
	// inherited:
	// void addUnit(UAptr unit)
	// bool removeUnit(UAptr unit)
	// bool hasUnits(unitQuantity units) const
	Squad();
	void update();

	void onUnitDestroy( BWAPI::Unit* unit );

	BWAPI::Position getTargetPosition() const {return mTargetPosition;}
	UnitBehaviors::Enum getBehavior() const {return mBehavior;}
	int getUnitCount() const {return mUnits.size();}
	Position getCenter() const;
	bool empty() const { return mUnits.empty(); }
	int getMaxSpread() const 
		{ return MAX_SPREAD_BASE + MAX_SPREAD_PER_UNIT*mUnits.size();}

	void setTargetPosition(BWAPI::Position targetPos);
	void setBehavior(UnitBehaviors::Enum behavior);

	void drawDebug() const;
	string stateName() const;

	const static int MAX_SPREAD_BASE = 300;
	const static int MAX_SPREAD_PER_UNIT = 2;
protected:
	// UAset mUnits
	BWAPI::Position mTargetPosition;
	UnitBehaviors::Enum mBehavior;
	SquadState mState;
	Position mCenter;
	double mSpread;

	static const int cUpdateInterval = 2; // update every x frames
	static const int cAttractRange = 16*32; // tiles away to start gathering stray units
	boost::signals2::connection mOnUnitDestroyConnection;
	bool mIsUnderAttack; /// true if any unit in squad is being targeted.

	void computeStatistics();

	void checkIfUnderAttack();
	void Rush();
	void Observe();
	void DefendRegion();

	void eraseDead();
	void executeState(); // perform actions of state

	void stateRetreat();
	void stateAttack();
	void stateDefend();
	void stateRegroup();
	void stateMove();
	void stateNone();
};