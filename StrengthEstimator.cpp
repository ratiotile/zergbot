#include "StrengthEstimator.h"
#include "common/cpp_includes.h"

using BWAPI::WeaponType;
using BWAPI::DamageType;

double StrengthEstimator::getDPS( BWAPI::UnitType u1, BWAPI::Player* p1, 
	BWAPI::UnitType u2 /*= BWAPI::UnitTypes::None */, 
	BWAPI::Player* p2 /*= NULL */ )
{
	assert( u1 != BWAPI::UnitTypes::None );
	int armor = 0; // how much armor unit2 has
	if ( u2 != BWAPI::UnitTypes::None )
	{
		armor = u2.armor();
		if ( p2 )
		{
			armor += p2->getUpgradeLevel(u2.armorUpgrade());
		}
	}
	assert( p1 != NULL );
	double damage = 0;
	int cooldown = 0;
	int hits = 1;
	WeaponType weap = u1.groundWeapon();
	if ( u2 != BWAPI::UnitTypes::None && u2.isFlyer() )
	{
		weap = u1.airWeapon();
		if ( u1.airWeapon() == BWAPI::WeaponTypes::None )
		{
			return 0; // cannot harm air units
		}
		hits = u1.maxAirHits();
		cooldown = u1.airWeapon().damageCooldown();
	}
	else // if u2 not given, or ground unit.
	{
		if ( weap == BWAPI::WeaponTypes::None )
		{
			return 0; // cannot harm ground units
		}
		hits = u1.maxGroundHits();
		cooldown = u1.groundWeapon().damageCooldown();
	}
	assert( cooldown > 0 );
	damage = weap.damageAmount() + ( weap.damageFactor() *
		p1->getUpgradeLevel(weap.upgradeType()) );
	// calculate damage types
	assert( weap != BWAPI::WeaponTypes::None );
	DamageType wdt = weap.damageType();
	double shieldPercentage = u2.maxShields()/getToughness(u2);
	if ( u2 != BWAPI::UnitTypes::None && wdt != BWAPI::DamageTypes::Normal )
	{
		BWAPI::UnitSizeType u2size = u2.size();
		if( wdt == BWAPI::DamageTypes::Explosive )
		{
			if( u2size == BWAPI::UnitSizeTypes::Small ) 
				damage = shieldPercentage*damage + 
					(1-shieldPercentage)*damage/2 ;
			else if( u2size == BWAPI::UnitSizeTypes::Medium ) 
				damage = shieldPercentage*damage + 
					(1-shieldPercentage)*damage*3/4;
		}
		if( wdt == BWAPI::DamageTypes::Concussive )
		{
			if( u2size == BWAPI::UnitSizeTypes::Large ) 
				damage = shieldPercentage*damage + 
				(1-shieldPercentage)*damage/4;
			else if( u2size == BWAPI::UnitSizeTypes::Medium ) 
				damage = shieldPercentage*damage + 
				(1-shieldPercentage)*damage/2;
		}
	}
	// apply armor
	damage = damage - armor;
	// apply hits
	if( damage <= 0 ) damage = 0.5;
	damage = damage * hits;

	double dps = damage * 24/cooldown;
	// range advantage
	WeaponType weap2 = u2.groundWeapon();
	if ( u1.isFlyer() )
	{
		weap2 = u2.airWeapon();
	}
	int rangeAdvantage = (weap.maxRange() - weap2.maxRange())/32;
	if (rangeAdvantage > 0) dps *= (1 + 0.1*rangeAdvantage);
	return dps;
}

double StrengthEstimator::getToughness( BWAPI::UnitType ut )
{
	return ut.maxHitPoints() + ut.maxShields();
}

double StrengthEstimator::getToughness( BWAPI::Unit* unit )
{
	return unit->getHitPoints() + unit->getShields();
}

double StrengthEstimator::getStrength( BWAPI::UnitType u1,
	BWAPI::Player* p1, BWAPI::UnitType u2 /*= BWAPI::UnitTypes::None */, 
	BWAPI::Player* p2 /*= NULL */ )
{
	double dps = getDPS(u1, p1, u2, p2);
	double toughness = getToughness(u1);
	double strength = dps * toughness;
	logfile <<" DPS: "<<dps<<"\t toughness: "<<toughness<<"\tStrength: "
		<<strength <<"\t\t"<< u1.getName()<<endl;
	return strength;
}

int StrengthEstimator::getScore( std::set<BWAPI::Unit*> units ) const
{
	int total = 0;
	BOOST_FOREACH( BWAPI::Unit* unit, units)
	{
		total += unit->getType().destroyScore();
	}
	return total;
}

int StrengthEstimator::getScore( std::set<UIptr> units ) const
{
	int total = 0;
	BOOST_FOREACH(UIptr unit, units)
	{
		total += unit->getType().destroyScore();
	}
	return total;
}