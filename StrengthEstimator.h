#pragma once
#ifndef INCLUDED_STRENGTHESTIMATOR_H
#define INCLUDED_STRENGTHESTIMATOR_H

#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#include "common/UnitInfo.h"

class StrengthEstimator
{
public:
	/// returns strength of unit with player's upgrades, base if NULL player
	double getStrength( BWAPI::UnitType u1, BWAPI::Player* p1,
		BWAPI::UnitType u2 = BWAPI::UnitTypes::None , 
		BWAPI::Player* p2 = NULL );
	/// returns dps of first unit against second
	double getDPS( BWAPI::UnitType u1, BWAPI::Player* p1,
		BWAPI::UnitType u2 = BWAPI::UnitTypes::None , 
		BWAPI::Player* p2 = NULL );
	/// returns max hp+shields
	double getToughness( BWAPI::UnitType ut );
	/// returns current hp+shields
	double getToughness( BWAPI::Unit* unit );
	/// really quick estimate using in-game score
	int getScore( std::set<BWAPI::Unit*> units ) const;
	int getScore( std::set<UIptr> units ) const;
private:
	
};
#endif