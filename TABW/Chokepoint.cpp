#include "Chokepoint.h"

namespace TABW
{
	int Chokepoint::count = 0;

	Chokepoint::Chokepoint( 
		BWAPI::Position side1, BWAPI::Position side2, int clearance )
		: mSides(side1, side2)
		, clearance_(clearance)
		, mCenter((side1.x() + side2.x()) / 2, (side1.y() + side2.y()) / 2)
		, mID(nextID())
	{
	}

	std::pair<pRegion, pRegion> Chokepoint::getRegions()
	{
		return std::make_pair(regions_.first.lock(), regions_.second.lock());
	}
}
