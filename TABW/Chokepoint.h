#pragma once
#include "TABW_typedefs.h"
#include <BWAPI.h>

namespace TABW
{
	class Chokepoint
	{
	public:
		Chokepoint();
		Chokepoint(BWAPI::Position side1, BWAPI::Position side2, int clearance);
		std::pair<pRegion, pRegion> getRegions();
		void setRegion1(wpRegion region) { regions_.first = region; }
		void setRegion2(wpRegion region) { regions_.second = region; }

		const std::pair<BWAPI::Position, BWAPI::Position>& sides() const
			{return mSides;}

		int clearance() const { return clearance_; }
		void clearance(int newClearance) {clearance_ = newClearance;}

		const BWAPI::Position& center() const {return mCenter;}

		int ID() const { return mID; }
		void ID( int newID) {mID = newID;}
	private:
		static int count;
		int nextID() { return ++Chokepoint::count; }
		int mID;
		int clearance_;
		std::pair<wpRegion, wpRegion> regions_;
		std::pair<BWAPI::Position, BWAPI::Position> mSides;
		BWAPI::Position mCenter;
	};
}
