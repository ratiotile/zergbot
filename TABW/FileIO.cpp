#include "FileIO.h"
#include <fstream>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include "MapArchive.h"
#include <sys/stat.h>

using namespace std;

void save_data( const std::string& filename, const MapArchive& data )
{
	ofstream fo;
	fo.open( filename.c_str(), ios::binary );
	boost::archive::binary_oarchive oa(fo);
	oa << data;
	fo.close();
}

void load_data( const std::string& filename, MapArchive& data )
{
	ifstream fi;
	fi.open( filename.c_str(), ios::binary );
	boost::archive::binary_iarchive ia(fi);
	ia >> data;
	fi.close();
}

bool fileExists( std::string filename )
{
	struct stat stFileInfo;
	return stat(filename.c_str(),&stFileInfo) == 0;
}