#pragma once
#include <string>

struct MapArchive;

void save_data( const std::string& filename, const MapArchive& data );
void load_data( const std::string& filename, MapArchive& data );
// taken from BWTA
bool fileExists(std::string filename);