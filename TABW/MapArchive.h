#pragma once
#include <list>
#include <boost/serialization/list.hpp>
#include <BWAPI.h>
#include "RectangleArray.h"

struct RegionArchive;
struct ChokeArchive;
struct ClusterArchive;
struct PosA
{
	PosA(): x(0), y(0)
	{}
	PosA( int ix, int iy ): x(ix), y(iy) {}
	int x;
	int y;
	void set( BWAPI::Position pos ) 
	{
		x = pos.x(); 
		y = pos.y(); 
	}
	template<class Archive>
	void serialize(Archive& ar, const unsigned int file_version)
	{
		ar & x & y;
	}
};
struct WalkTileA
{
	WalkTileA(): x(0), y(0)
	{}
	int x;
	int y;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int file_version )
	{
		ar & x & y;
	}
};
struct BuildTileA
{
	BuildTileA(): x(0), y(0)
	{}
	int x;
	int y;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int file_version )
	{
		ar & x & y;
	}
};

struct MapArchive
{
	MapArchive(): version(0), mapWidthBT(0), mapHeightBT(0)
	{}
	int version;
	int mapWidthBT;
	int mapHeightBT;
	/// walktile resolution all below
	RectangleArray<int> wtileConnectivity;
	RectangleArray<int> wtileClearance;
	RectangleArray<int> wtileToRegionID;
	std::list<RegionArchive> regions;
	std::list<ChokeArchive> chokepoints;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int file_version )
	{
		ar & version & mapWidthBT & mapHeightBT 
			& wtileConnectivity & wtileClearance & wtileToRegionID 
			& regions & chokepoints;
	}
};

struct RegionArchive
{
	RegionArchive(): id(0), clearance(0), size(0), connectivity(0)
	{}
	int id;
	PosA center;
	int clearance;
	int size;
	/// the ID of the walkable tile group this region connects to
	int connectivity;
	std::list<ClusterArchive> clusters;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int file_version )
	{
		ar & id & center & clearance & size & connectivity & clusters;
	}
};

struct ChokeArchive
{
	ChokeArchive(): id(0), region1(0), region2(0), clearance(0)
	{}
	int id;
	int region1;
	int region2;
	PosA center;
	PosA side1;
	PosA side2;
	int clearance;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int file_version)
	{
		ar & id & region1 & region2 & center & side1 & side2 & clearance;
	}
};

struct ClusterArchive
{
	ClusterArchive(): id(0)
	{}
	int id;
	BuildTileA baseLocation;
	std::list<PosA> minerals;
	std::list<PosA> geysers;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int file_version)
	{
		ar & id & baseLocation & minerals & geysers;
	}
};

