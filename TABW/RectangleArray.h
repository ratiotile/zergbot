// from Skynet
#pragma once

#include <vector>
#include <boost/serialization/vector.hpp>

template <typename T>
class RectangleArray
{
public:
	RectangleArray(unsigned int width, unsigned int height, T val = T())
		: mData(width, std::vector<T>(height, val))
	{}

	RectangleArray()
	{}

	void resize(unsigned int width, unsigned int height, T val = T())
	{
		mData.resize(width);
		for(unsigned int i = 0; i < width; ++i)
			mData[i].resize(height, val);
	}

	const std::vector<T> &operator[](int i) const
	{
		return mData[i];
	}

	std::vector<T> &operator[](int i)
	{
		return mData[i];
	}
	template<class Archive>
	void serialize(Archive& ar, const unsigned int file_version)
	{
		ar & mData;
	}
	int sizeX() const {return mData.size();}
	int sizeY() const {return mData[0].size();}
private:
	std::vector<std::vector<T>> mData;
};