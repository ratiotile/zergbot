#include "Region.h"
#include "ResourceCluster.h"

namespace TABW
{
	unsigned int Region::count = 0;
	void Region::addChokepoint( wpChoke chokepoint )
	{
		mChokepoints.insert(chokepoint);
	}

	void Region::addResourceCluster( pResourceCluster cluster )
	{
		mResourceClusters.insert(cluster);
	}

	Region::Region( BWAPI::Position center, int clearance, int connectivity ) 
		: center_(center), 
		clearance_(clearance),
		mID( Region::nextID() ),
		mConnectivity(connectivity)
	{
	}

	unsigned int Region::nextID()
	{
		return ++count;
	}
}
