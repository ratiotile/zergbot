#pragma once

#include "TABW_typedefs.h"
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif

namespace TABW
{
	class Region
	{
	public:
		BWAPI::Position getCenter() const {return center_;}
		
		std::set<wpChoke> mChokepoints;
		std::set<pResourceCluster> mResourceClusters;
	
		Region() {}

		Region(BWAPI::Position center, int clearance, int connectivity);

		void addChokepoint(wpChoke chokepoint);

		void addResourceCluster( pResourceCluster cluster );

		int size() const {return mSize;}
		void size(int size){ mSize = size; }

		std::set<pResourceCluster>& resourceClusters() { return mResourceClusters; }
		std::set<wpChoke>& chokepoints() { return mChokepoints; }

		unsigned int ID() const {return mID;}
		void ID( int newID ) {mID = newID; }

		int connectivity() const {return mConnectivity;}
		void connectivity(int newID) {mConnectivity = newID;}

		int clearance() const {return clearance_;}
		void clearance(int newClearance) {clearance_ = newClearance;}

		const BWAPI::Position& center() const {return center_;}
		void center(const BWAPI::Position& newCenter) {center_ = newCenter;}
	private:
		BWAPI::Position center_;
		static unsigned int count;
		static unsigned int nextID();
		/// which landmass is it part of?
		int mConnectivity;
		/// number of tiles in region
		int mSize;
		/// how close is nearest obstacle from center
		int clearance_;

		unsigned int mID;
	};
}