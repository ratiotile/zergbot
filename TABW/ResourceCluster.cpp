#include "ResourceCluster.h"
#include "../common/typedefs.h"
#include "../common/UnitInfo.h"
#include <boost/foreach.hpp>

typedef boost::shared_ptr<UnitInfo> UIptr;

namespace TABW
{
	ResourceCluster::ResourceCluster( setUI& resources, pRegion region )
		:mRegion(region), 
		mResources(resources), 
		mDepotPos(BWAPI::TilePositions::Unknown)
	{
		BOOST_FOREACH( UIptr resource, resources )
		{
			if ( resource->getType().isMineralField() )
			{
				mMineralPositions.insert( resource->getPosition() );
			}
			else if ( resource->getType() == BWAPI::UnitTypes::Resource_Vespene_Geyser )
			{
				mGeyserPositions.insert( resource->getPosition() );
			}
			else
			{
				//logfile << "ResourceCluster Error: non resource unit!\n";
			}
		}
	}
}
