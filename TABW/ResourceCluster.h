#pragma once
#include "TABW_typedefs.h"
#include "Region.h"
#include "../common/UnitInfo.h"

namespace TABW
{
	class ResourceCluster
	{
	public:
		ResourceCluster( setUI& resources, pRegion region );
		/// NOT Compatible with UnitINfo from GameState.
		setUI& resources() { return mResources; }
		/// returns depot position in Build Tiles
		const BWAPI::TilePosition& depotPosition() const
		{ return mDepotPos; }
		/// expects tilePosition coordinates for optimal depot placement
		void depotPosition( const BWAPI::TilePosition& bestPos )
		{ mDepotPos = bestPos; }
		BWAPI::TilePosition depotPosition() { return mDepotPos; }
		/// holds mineral center positions, not top left
		const std::set<BWAPI::Position>& mineralPositions() const
			{ return mMineralPositions; }
		const std::set<BWAPI::Position>& geyserPositions() const
			{ return mGeyserPositions; }
	private:
		/// the region that this cluster is contained in
		pRegion mRegion;
		/// set of UnitInfo containing the resources in this cluster
		setUI mResources;
		/// holds positions of the minerals for serialization
		std::set<BWAPI::Position> mMineralPositions;
		/// holds positions of the geysers for serialization
		std::set<BWAPI::Position> mGeyserPositions;
		/// Top Left buildtile to place resource depot to gather cluster
		BWAPI::TilePosition mDepotPos;
	};

	typedef boost::shared_ptr<ResourceCluster> pResourceCluster;
}

