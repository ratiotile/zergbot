#pragma once
#include "TABW.h"
#include "TABW_typedefs.h"
#include "TerrainAnalyzer.h"

using BWAPIext::WalkTile;

std::set<TABW::pRegion> TABW::getRegions()
{
	return TerrainAnalyzer::getInstance().getRegions();
}

std::set<TABW::pChoke> TABW::getChokepoints()
{
	return TerrainAnalyzer::getInstance().getChokepoints();
}

int TABW::mapHeightWT(){ return TerrainAnalyzer::getInstance().mapHeightWT();}

int TABW::mapWidthWT() { return TerrainAnalyzer::getInstance().mapWidthWT();}

TABW::pRegion TABW::getRegion( WalkTile wt )
{
	return TerrainAnalyzer::getInstance().getRegion(wt);
}

bool TABW::isConnected( BWAPIext::WalkTile wt1,BWAPIext::WalkTile wt2 )
{
	return TerrainAnalyzer::getInstance().getConnectivityID(wt1)
		== TerrainAnalyzer::getInstance().getConnectivityID(wt2);
}