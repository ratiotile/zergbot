#pragma once
#include <set>
#include "TABW_typedefs.h"
#include "../common/BWAPIext.h"

class BWAPIext::WalkTile;

namespace TABW
{
	std::set<pRegion> getRegions();
	std::set<pChoke> getChokepoints();
	int mapHeightWT();
	int mapWidthWT();
	pRegion getRegion(BWAPIext::WalkTile wt);
	bool isConnected(BWAPIext::WalkTile wt1,BWAPIext::WalkTile wt2);
}