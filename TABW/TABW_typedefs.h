#pragma once

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

namespace TABW
{
	class Chokepoint;
	typedef boost::shared_ptr<Chokepoint> pChoke;
	typedef boost::weak_ptr<Chokepoint> wpChoke;

	class Region;
	typedef boost::shared_ptr<Region> pRegion;
	typedef boost::weak_ptr<Region> wpRegion;

	class ResourceCluster;
	typedef boost::shared_ptr<ResourceCluster> pResourceCluster;
}
