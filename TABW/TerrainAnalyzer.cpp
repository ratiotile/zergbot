#include "TerrainAnalyzer.h"
#include "Heap.h"
#include "ResourceCluster.h"
#include "Chokepoint.h"
#include "Region.h"
#include "FileIO.h"

#include "../common/cpp_includes.h"
#include "../common/Vector2D.h"
#include "../common/UnitInfo.h"
#include "../common/Config.h"
#include "../clustering/ClusterManager.h"
#include "../TimeManager.h"

// resolve by making own config file!
//#include "../Blackboard.h"


// try to make own instance of clusterer and delete it when done.
//#include "../GameState.h"


using namespace BWAPIext;
// test
#include "../img/CImg.h"
using namespace cimg_library;

namespace TABW
{
	TerrainAnalyzer* TerrainAnalyzer::pInstance_ = NULL;

	TerrainAnalyzer::TerrainAnalyzer()
		:isMapAnalyzed_(false),
		mArchive( NULL ),
		config_(NULL),
		mVersion( 0 )
	{
		logfile << "TerrainAnalyzer constructor\n";
		logWriteBuffer();
		mFileName = "bwapi-data/TABW/";
		mFileName += Broodwar->mapHash();
		mFileName += ".tabw";
		config_ = new Config("TABW.info");
	}

	TerrainAnalyzer::~TerrainAnalyzer()
	{
		if (config_)
		{
			delete config_;
			config_ = NULL;
		}
		if (mArchive)
		{
			delete mArchive;
			mArchive = NULL;
		}
	}


	TerrainAnalyzer& TerrainAnalyzer::getInstance()
	{
		if (!pInstance_)
		{
			pInstance_ = new TerrainAnalyzer();
		}
		return *pInstance_;
	}

	void TerrainAnalyzer::analyzeTerrain()
	{
		if ( config_->get<bool>("TerrainAnalyzer.do_analysis") )
		{
			if (!load())
			{
				logfile << "TerrainAnalyzer starting map analysis.\n";
				logWriteBuffer();
				calculateConnectivity();
				calculateWalkTileClearance();
				createRegions();
				createBases();
				save();
			}
			isMapAnalyzed_ = true;
		}
	}

	// source: Skynet
	// step 1, assigns connectivity numbers also to contiguous impassable areas
	// store small impassable region ids in mSmallObstacles.
	void TerrainAnalyzer::calculateConnectivity()
	{
		mMapWidth = BWAPI::Broodwar->mapWidth() * 4;
		mMapHeight = BWAPI::Broodwar->mapHeight() * 4;

		int currentRegion = 1;
		mTileConnectivity.resize(mMapWidth, mMapHeight, 0);
		for(int x = 0; x < mMapWidth; ++x)
		{
			for(int y = 0; y < mMapHeight; ++y)
			{
				if(mTileConnectivity[x][y] != 0)
					continue;

				bool walkable = BWAPI::Broodwar->isWalkable(x, y);
				int tileCount = 0;

				std::set<WalkTile> unvisitedTiles;
				unvisitedTiles.insert(WalkTile(x, y));

				while(!unvisitedTiles.empty())
				{
					const std::set<WalkTile>::iterator &tile = unvisitedTiles.begin();

					++tileCount;
					mTileConnectivity[tile->x()][tile->y()] = currentRegion;

					if(tile->x() > 0 && Broodwar->isWalkable(tile->x()-1, tile->y()) 
						== walkable && mTileConnectivity[tile->x()-1][tile->y()] == 0)
						unvisitedTiles.insert(WalkTile(tile->x()-1, tile->y()));
					if(tile->y() > 0 && Broodwar->isWalkable(tile->x(), tile->y()-1)
						== walkable && mTileConnectivity[tile->x()][tile->y()-1] == 0)
						unvisitedTiles.insert(WalkTile(tile->x(), tile->y()-1));

					if(tile->x() < mMapWidth-1 && Broodwar->isWalkable(
						tile->x()+1, tile->y()) == walkable && 
						mTileConnectivity[tile->x()+1][tile->y()] == 0)
						unvisitedTiles.insert(WalkTile(tile->x()+1, tile->y()));
					if(tile->y() < mMapHeight-1 && Broodwar->isWalkable(
						tile->x(), tile->y()+1) == walkable && 
						mTileConnectivity[tile->x()][tile->y()+1] == 0)
						unvisitedTiles.insert(WalkTile(tile->x(), tile->y()+1));

					unvisitedTiles.erase(tile);
				}

				if(!walkable && tileCount < 200)
					mSmallObstacles.insert(currentRegion);

				++currentRegion;
			}
		}
		// draw image of map connectivity
		if (config_->get<bool>("TerrainAnalyzer.save_bmp_connectivity") )
		{
			CImg<unsigned char> img(mMapWidth, mMapHeight, 1, 3);
			CImg<unsigned char> img_walk(mMapWidth, mMapHeight, 1, 3);
			img.fill(0);
			img_walk.fill(0);
			int landID;
			unsigned char pointColor[3];
			unsigned char black[] = {0,0,0};
			unsigned char white[] = {255,255,255};
			for(int x = 0; x < mMapWidth; ++x)
			{
				for(int y = 0; y < mMapHeight; ++y)
				{
					landID = mTileConnectivity[x][y];
					pointColor[0] = 25 * (landID%10);
					pointColor[2] = 25 * ((landID/10)%10);
					pointColor[1] = 25 * ((landID/100)%10);
					img.draw_point(x,y,pointColor,1);
					if (Broodwar->isWalkable(x,y))
						img_walk.draw_point(x,y,white);
					else img_walk.draw_point(x,y,black);
				}
			}
			string bmp_name = "Connectivity Map - ";
			string walk_name = "Walkability Map - ";
			bmp_name += Broodwar->mapFileName();
			bmp_name += ".bmp";
			walk_name += Broodwar->mapFileName();
			walk_name += ".bmp";
			//img.display("Connectivity Map",0);
			img.save_bmp(bmp_name.c_str());
			if ( config_->get<bool>("TerrainAnalyzer.save_bmp_walkability") )
			{
				img_walk.save_bmp(walk_name.c_str());
			}
		}
	}
	// step 2, fill mTileClearance and mTileClosestObstacle.
	void TerrainAnalyzer::calculateWalkTileClearance()
	{
		mTileClearance.resize(mMapWidth, mMapHeight, -1);
		mTileClosestObstacle.resize(mMapWidth, mMapHeight);

		Heap<WalkTile, int> unvisitedTiles(true);
		for(int x = 0; x < mMapWidth; ++x)
		{
			for(int y = 0; y < mMapHeight; ++y)
			{
				if(!BWAPI::Broodwar->isWalkable(x, y))
				{
					mTileClearance[x][y] = 0;
					mTileClosestObstacle[x][y] = WalkTile(x, y);
					if(mSmallObstacles.count(mTileConnectivity[x][y]) == 0)
						unvisitedTiles.set(WalkTile(x, y), 0);
				}
				else if(x == 0 || y == 0 || x == mMapWidth-1 || y == mMapHeight-1)
				{
					mTileClearance[x][y] = 10;
					mTileClosestObstacle[x][y] = WalkTile((x == 0 ? -1 : (x == mMapWidth-1 ? mMapWidth : x)), (y == 0 ? -1 : (y == mMapHeight-1 ? mMapHeight : y)));
					unvisitedTiles.set(WalkTile(x, y), 10);
				}
			}
		}

		while(!unvisitedTiles.empty())
		{
			WalkTile tile = unvisitedTiles.top().first;
			const int distance = unvisitedTiles.top().second + 10;
			const int diagDistance = distance + 4;
			unvisitedTiles.pop();

			const int west = tile.x() - 1;
			const int north = tile.y() - 1;
			const int east = tile.x() + 1;
			const int south = tile.y() + 1;

			const bool canGoWest = west >= 0;
			const bool canGoNorth = north >= 0;
			const bool canGoEast = east < mMapWidth;
			const bool canGoSouth = south < mMapHeight;

			const WalkTile &currentParent = mTileClosestObstacle[tile.x()][tile.y()];

			if(canGoWest && (mTileClearance[west][tile.y()] == -1 || 
				distance < mTileClearance[west][tile.y()]))
			{
				mTileClearance[west][tile.y()] = distance;
				mTileClosestObstacle[west][tile.y()] = currentParent;
				unvisitedTiles.set(WalkTile(west, tile.y()), distance);
			}

			if(canGoNorth && (mTileClearance[tile.x()][north] == -1 || 
				distance < mTileClearance[tile.x()][north]))
			{
				mTileClearance[tile.x()][north] = distance;
				mTileClosestObstacle[tile.x()][north] = currentParent;
				unvisitedTiles.set(WalkTile(tile.x(), north), distance);
			}

			if(canGoEast && (mTileClearance[east][tile.y()] == -1 || 
				distance < mTileClearance[east][tile.y()]))
			{
				mTileClearance[east][tile.y()] = distance;
				mTileClosestObstacle[east][tile.y()] = currentParent;
				unvisitedTiles.set(WalkTile(east, tile.y()), distance);
			}

			if(canGoSouth && (mTileClearance[tile.x()][south] == -1 || distance < mTileClearance[tile.x()][south]))
			{
				mTileClearance[tile.x()][south] = distance;
				mTileClosestObstacle[tile.x()][south] = currentParent;
				unvisitedTiles.set(WalkTile(tile.x(), south), distance);
			}

			if(canGoWest && canGoNorth && (mTileClearance[west][north] == -1 || diagDistance < mTileClearance[west][north]))
			{
				mTileClearance[west][north] = diagDistance;
				mTileClosestObstacle[west][north] = currentParent;
				unvisitedTiles.set(WalkTile(west, north), diagDistance);
			}

			if(canGoEast && canGoSouth && (mTileClearance[east][south] == -1 || diagDistance < mTileClearance[east][south]))
			{
				mTileClearance[east][south] = diagDistance;
				mTileClosestObstacle[east][south] = currentParent;
				unvisitedTiles.set(WalkTile(east, south), diagDistance);
			}

			if(canGoEast && canGoNorth && (mTileClearance[east][north] == -1 || diagDistance < mTileClearance[east][north]))
			{
				mTileClearance[east][north] = diagDistance;
				mTileClosestObstacle[east][north] = currentParent;
				unvisitedTiles.set(WalkTile(east, north), diagDistance);
			}

			if(canGoWest && canGoSouth && (mTileClearance[west][south] == -1 || diagDistance < mTileClearance[west][south]))
			{
				mTileClearance[west][south] = diagDistance;
				mTileClosestObstacle[west][south] = currentParent;
				unvisitedTiles.set(WalkTile(west, south), diagDistance);
			}
		}
		// display code
		if ( config_->get<bool>("TerrainAnalyzer.save_bmp_clearance") )
		{
			minVal_ = 0;
			maxVal_ = 0;
			for(int x = 0; x < mMapWidth; ++x)
			{
				for(int y = 0; y < mMapHeight; ++y)
				{
					if (mTileClearance[x][y] > maxVal_)
						maxVal_ = mTileClearance[x][y];
					if (mTileClearance[x][y] < minVal_)
						minVal_ = mTileClearance[x][y];
				}
			}
			spread_ = maxVal_ - minVal_;
			CImg<unsigned char> clearanceImg(mMapWidth, mMapHeight, 1, 3);
			clearanceImg.fill(0);
			int sVal = 0;
			unsigned char nColor[3] = {0,0,0};
			for(int x = 0; x < mMapWidth; ++x)
			{
				for(int y = 0; y < mMapHeight; ++y)
				{
					sVal = mTileClearance[x][y];
					getHeatmapColor(sVal, nColor);
					clearanceImg.draw_point(x,y,nColor);
				}
			}
			//clearanceImg.display("Clearance Map",0);
			string imgName = "Clearance Map - ";
			imgName += Broodwar->mapFileName();
			imgName += ".bmp";
			clearanceImg.save_bmp(imgName.c_str());
		}
	}
	/// blue for min to avg, green for avg, red for avg to max
	void TerrainAnalyzer::getHeatmapColor( int sampleValue, unsigned char heatmapcolor[3] )
	{
		double nVal = ((double)sampleValue - minVal_)/(double)spread_;
		assert(nVal <= 1);
		double redComp = 0;
		double greenComp = 0;
		double blueComp = 0;
		if (nVal < 0.5)
		{
			blueComp = 1 - 2 * nVal;
			greenComp = 2 * nVal;
		}
		if (nVal >= 0.5)
		{
			greenComp = 1 - 2 * (nVal - 0.5);
			redComp = 2 * (nVal - 0.5);
		}
		if (redComp > 1) redComp = 1;
		if (blueComp > 1) blueComp = 1;
		if (greenComp > 1) greenComp = 1;
		assert(redComp <= 1);
		assert(blueComp <= 1);
		assert(greenComp <= 1);
		heatmapcolor[0] = (unsigned char)(redComp * 255);
		heatmapcolor[1] = (unsigned char)(greenComp * 255);
		heatmapcolor[2] = (unsigned char)(blueComp * 255);

	}
	// from Skynet
	void TerrainAnalyzer::createRegions()
	{
		mMapWidth = BWAPI::Broodwar->mapWidth() * 4;
		mMapHeight = BWAPI::Broodwar->mapHeight() * 4;

		mWTileToRegion.resize(mMapWidth, mMapHeight);
		std::map<WalkTile, pChoke> chokeTiles;

		for(;;) // look for clearance maxima and create regions
		{
			int currentRegionClearance = 0;
			WalkTile currentRegionTile;
			// find the tile with maximum clearance still unclaimed by region
			// in effect, next largest undeclared region
			for(int x = 0; x < mMapWidth; ++x)
			{
				for(int y = 0; y < mMapHeight; ++y)
				{
					if(mWTileToRegion[x][y]) // initially null
						continue;		// skip this iteration if already assigned

					if(chokeTiles.count(WalkTile(x, y)) != 0)
						continue;

					const int localMaximaValue = mTileClearance[x][y];
					if(localMaximaValue > currentRegionClearance)
					{
						currentRegionClearance = localMaximaValue;
						currentRegionTile.x() = x;
						currentRegionTile.y() = y;
					}
				}
			}

			if(currentRegionClearance == 0) // none left so exit
				break;
			// create new region for maximum found
			pRegion currentRegion(new Region(
				currentRegionTile.getCenterPosition(), currentRegionClearance, 
				getConnectivityID(currentRegionTile) ) );

			mRegions.insert(currentRegion);
			// set up to find minima, chokes
			std::map<WalkTile, WalkTile> tileToLastMinima;
			std::map<WalkTile, std::vector<WalkTile>> tileToChildren;
		// heap probably extracts minimum first
			Heap<WalkTile, int> unvisitedTiles(false);

			unvisitedTiles.set(currentRegionTile, currentRegionClearance);
			tileToLastMinima[currentRegionTile] = currentRegionTile;

			while(!unvisitedTiles.empty())
			{
				WalkTile currentTile = unvisitedTiles.top().first;
				int currentTileClearance = unvisitedTiles.top().second;
				unvisitedTiles.pop();

				if(chokeTiles.count(currentTile) != 0) // current tile is choke
				{
					if(chokeTiles[currentTile]->getRegions().second && 
						chokeTiles[currentTile]->getRegions().second 
						!= currentRegion)
					{
						//DrawBuffer::Instance().drawBufferedBox(BWAPI::CoordinateType::Map, currentTile.x * 8, currentTile.y * 8, currentTile.x * 8 + 8, currentTile.y * 8 + 8, 999999, BWAPI::Colors::Red);
						logfile << "Touched a choke saved to anouther region\n";
					}
					else if(chokeTiles[currentTile]->getRegions().first 
						!= currentRegion)
					{	// touched a choke with first region set, save this
						// region to second slot
						currentRegion->addChokepoint(chokeTiles[currentTile]);
						chokeTiles[currentTile]->setRegion2(currentRegion);
					}

					continue;
				}
	// if region is already set for this tile, meaning no choke between 2 regions
				if(mWTileToRegion[currentTile.x()][currentTile.y()])
				{
					logfile << "2 regions possibly connected without a choke\n";
					continue;
				}
	// this is the map that tracks path back to last minima
				WalkTile lastMinima = tileToLastMinima[currentTile];
				const int chokeSize = mTileClearance[lastMinima.x()][lastMinima.y()];

				bool foundChokepoint = false;
	// test last minima found: 90% of the region max, less than 80% clearance
	// of current tile
				float regionThreshold = 
					config_->get<float>("TerrainAnalyzer.choke.regionThreshold");
				float tileThreshold =
					config_->get<float>("TerrainAnalyzer.choke.tileThreshold");
				if(chokeSize < int(float(currentRegionClearance)*regionThreshold) && 
					chokeSize < int(float(currentTileClearance)*tileThreshold))
					foundChokepoint = true;

				if(foundChokepoint)
				{// must be 32 walktiles away from region center to trigger
					const int minDistance = 32;
					if((abs(currentRegionTile.x() - lastMinima.x()) + 
						abs(currentRegionTile.y() - lastMinima.y())) < minDistance)
						foundChokepoint = false;
					// current tile must be 32 walktiles away from choke
					else if((abs(currentTile.x() - lastMinima.x()) + 
						abs(currentTile.y() - lastMinima.y())) < minDistance)
						foundChokepoint = false;
			// current tile must have more than 10 walk tiles of clearance	
					else if(currentTileClearance < 120)
						foundChokepoint = false;
				}

				if(foundChokepoint)
				{// now to find the sides of the choke point
					const std::pair<WalkTile, WalkTile> &chokeSides = 
						findChokePoint(lastMinima);
	// create new chokepoint
					pChoke currentChokepoint(new Chokepoint(
						chokeSides.first.getCenterPosition(), 
						chokeSides.second.getCenterPosition(), chokeSize));
					mChokepoints.insert(currentChokepoint);
	// walk from one side of choke to the other
					currentChokepoint->setRegion1(currentRegion);
					currentRegion->addChokepoint(currentChokepoint);

					int x0 = chokeSides.second.x();
					int y0 = chokeSides.second.y();

					int x1 = chokeSides.first.x();
					int y1 = chokeSides.first.y();

					int dx = abs(x1 - x0);
					int dy = abs(y1 - y0);

					int sx = x0 < x1 ? 1 : -1;
					int sy = y0 < y1 ? 1 : -1;

					int error = dx - dy;

					std::set<WalkTile> chokeChildren;

					for(;;)
					{// map bounds checking, passable check, not in region
						if(x0 >= 0 && y0 >= 0 && x0 < mMapWidth && 
							y0 < mMapHeight && mTileClearance[x0][y0] != 0 
							&& !mWTileToRegion[x0][y0])
						{
							const WalkTile thisChokeTile(x0, y0);
	// mark current tile part of choke
							tileToChildren[currentRegionTile].push_back(thisChokeTile);
							chokeTiles[thisChokeTile] = currentChokepoint;
							chokeChildren.insert(thisChokeTile);
						}
	// exit loop when other side of choke is reached
						if(x0 == x1 && y0 == y1)
							break;

						int e2 = error*2;
						if(e2 > -dy)
						{
							error -= dy;
							x0 += sx;
						}
						if(e2 < dx)
						{
							error += dx;
							y0 += sy;
						}
					}
	// clean up
					while(!chokeChildren.empty())
					{
						std::set<WalkTile>::iterator currentTile = chokeChildren.begin();

						tileToLastMinima.erase(*currentTile);
						unvisitedTiles.erase(*currentTile);

						for each(WalkTile nextTile in tileToChildren[*currentTile])
						{
							chokeChildren.insert(nextTile);
						}
						tileToChildren.erase(*currentTile);

						chokeChildren.erase(currentTile);
					}
				}
				else// not a chokepoint
				{
					if(mTileClearance[currentTile.x()][currentTile.y()] < chokeSize)
						lastMinima = currentTile;
	// test 4 adjacent tiles
					for(int i = 0; i < 4; ++i)
					{
						int x = (i == 0 ? currentTile.x() - 1 : 
							(i == 1 ? currentTile.x()+1 : currentTile.x()));
						int y = (i == 2 ? currentTile.y() - 1 : 
							(i == 3 ? currentTile.y()+1 : currentTile.y()));

						if(x < 0 || y < 0 || x >= mMapWidth || y >= mMapHeight)
							continue;

						if(mTileClearance[x][y] == 0)
							continue;

						const WalkTile nextTile(x, y);
						if(tileToLastMinima.count(nextTile) == 0)
						{// not part of path, not visited yet?
							tileToLastMinima[nextTile] = lastMinima;
							tileToChildren[currentTile].push_back(nextTile);
	// heap probably keeps us checking along line of greatest clearance
	// to stay away from corners.
							unvisitedTiles.set(nextTile, mTileClearance[x][y]);
						}
					}
				}
			}// while !unusedTileList.empty()

			std::set<WalkTile> tileSteps;
			tileSteps.insert(currentRegionTile);
			int regionSize = 1;
	// count number of tiles in region
			while(!tileSteps.empty())
			{
				std::set<WalkTile>::iterator currentTile = tileSteps.begin();
				++regionSize;

				for each(WalkTile nextTile in tileToChildren[*currentTile])
				{
					tileSteps.insert(nextTile);
				}

				mWTileToRegion[currentTile->x()][currentTile->y()] = currentRegion;

				tileSteps.erase(currentTile);
			}

			currentRegion->size(regionSize);
		}

	// 	for(std::set<Chokepoint>::iterator it = mChokepoints.begin(); it != mChokepoints.end();)
	// 	{
	// 		if(!(*it)->getRegions().first || !(*it)->getRegions().second || (*it)->getRegions().first == (*it)->getRegions().second)
	// 			removeChokepoint(*(it++));
	// 		else
	// 			++it;
	// 	}
		// image drawing
		// draw image of map connectivity
		if ( config_->get<bool>("TerrainAnalyzer.save_bmp_chokes") )
		{
			CImg<unsigned char> img(mMapWidth, mMapHeight, 1, 3);
			img.fill(0);
			unsigned char pointColor[3];
			unsigned char black[] = {0,0,0};
			unsigned char white[] = {255,255,255};
			int regionCount = 1;
			BOOST_FOREACH(pRegion region, mRegions)
			{
				pointColor[0] = 50 * (regionCount%5);
				pointColor[2] = 50 * ((regionCount/5)%5);
				pointColor[1] = 50 * ((regionCount/25)%5);
				for(int x = 0; x < mMapWidth; ++x)
				{
					for(int y = 0; y < mMapHeight; ++y)
					{
						if (region == mWTileToRegion[x][y])
							img.draw_point(x,y,pointColor);
					}
				}
				++regionCount;
			}
			logfile << "mChokepoint has " << mChokepoints.size() << " chokes\n";
			BOOST_FOREACH(pChoke c, mChokepoints)
			{
				img.draw_line(c->sides().second.x()/8, c->sides().second.y()/8,
					c->sides().first.x()/8, c->sides().first.y()/8, white );
				//img.draw_circle(c->mSides.first.x()/8, c->mSides.first.y()/8, 2, white, 1);
				//img.draw_circle(c->mSides.second.x()/8, c->mSides.second.y()/8, 2, white, 1);
				img.draw_circle(c->center().x()/8, c->center().y()/8, 2, white, 1);
			}
			// draw resources]
			unsigned char mineral_blue[3] = {150,200,255};
			BOOST_FOREACH(  BWAPI::Unit* mineral, Broodwar->getStaticMinerals() )
			{
				int top = mineral->getTop()/8;
				int left = mineral->getLeft()/8;
				int bot = mineral->getBottom()/8;
				int right = mineral->getRight()/8;
				img.draw_rectangle(left,top,right,bot,mineral_blue,1);
			}
			unsigned char vespene_green[3] = {50,255,200};
			BOOST_FOREACH(  BWAPI::Unit* vespene, Broodwar->getStaticGeysers() )
			{
				int top = vespene->getTop()/8;
				int left = vespene->getLeft()/8;
				int bot = vespene->getBottom()/8;
				int right = vespene->getRight()/8;
				img.draw_rectangle(left,top,right,bot,vespene_green,1);
			}
			string bmp_name = "Regions and Chokes - ";
			bmp_name += Broodwar->mapFileName();
			bmp_name += ".bmp";
			//img.display("Connectivity Map",0);
			img.save_bmp(bmp_name.c_str());
		}
		if ( config_->get<bool>("TerrainAnalyzer.save_bmp_shock") )
		{
			bool d = config_->get<bool>("TerrainAnalyzer.shock_diagonals");
			saveShockMap(d);
		}
	}

	std::pair<WalkTile, WalkTile> TerrainAnalyzer::findChokePoint( WalkTile center )
	{
		WalkTile side1 = mTileClosestObstacle[center.x()][center.y()];

		if(side1 == center)
			return std::make_pair(side1, side1);

		Vector2D side1Direction( side1.x() - center.x(), side1.y() - center.y() );
		side1Direction.normalize();

		int mapWidth = BWAPI::Broodwar->mapWidth() * 4;
		int mapHeight = BWAPI::Broodwar->mapHeight() * 4;

		int x0 = side1.x();
		int y0 = side1.y();

		int x1 = center.x();
		int y1 = center.y();

		int dx = abs(x1 - x0);
		int dy = abs(y1 - y0);

		int sx = x0 < x1 ? 1 : -1;
		int sy = y0 < y1 ? 1 : -1;

		x0 = x1;
		y0 = y1;

		int error = dx - dy;

		for(;;)
		{
			if( x0 < 0 || y0 < 0 || x0 >= mapWidth || y0 >= mapHeight || 
				!BWAPI::Broodwar->isWalkable(x0, y0) )
				return std::make_pair(side1, WalkTile(x0, y0));

			WalkTile side2 = mTileClosestObstacle[x0][y0];

			Vector2D side2Direction(side2.x() - center.x(), side2.y() - center.y());
			side2Direction.normalize();

			float dot = side2Direction.dot(side1Direction);
			float angle = acos(dot);
			if(angle > 2.0f)
				return std::make_pair(side1, side2);

			int e2 = error*2;
			if(e2 > -dy)
			{
				error -= dy;
				x0 += sx;
			}
			if(e2 < dx)
			{
				error += dx;
				y0 += sy;
			}
		}
	}
	// diagonals: whether to count 8 tiles around or only cardinal 4
	// make certain mMapWidth and mMapHeight are set.
	void TerrainAnalyzer::saveShockMapUB( bool diagonals )
	{
		CImg<unsigned char> img(mMapWidth, mMapHeight, 1, 3);
		img.fill(0);
		std::set<int> obstacleIDs;
		int count = 0;
		 unsigned char blue[3] = {0,0,150}; //blue
		 unsigned char ltBlue[3] = {50,50,255};
		 unsigned char green[3] = {0,150,0};
		 unsigned char ltGreen[3] = {50,255,50};
		 unsigned char red[3] = {150,0,0};
		 unsigned char white[3] = {255,255,255};
		 unsigned char black[3] = {0,0,0};
		string bmp_name = "Shock Map ";
		if ( diagonals ) bmp_name += "Diagonals ";
		for(int x = 0; x < mMapWidth; ++x)
		{
			for(int y = 0; y < mMapHeight; ++y)
			{
				if (Broodwar->isWalkable(x,y))
				{
					obstacleIDs.clear();
					const int west = x - 1;
					const int north = y - 1;
					const int east = x + 1;
					const int south = y + 1;

					const bool canGoWest = west >= 0;
					const bool canGoNorth = north >= 0;
					const bool canGoEast = east < mMapWidth;
					const bool canGoSouth = south < mMapHeight;

					if ( canGoWest ) obstacleIDs.insert( getConnectivityID(
						mTileClosestObstacle[west][y] ));
					if ( canGoNorth ) obstacleIDs.insert( getConnectivityID(
						mTileClosestObstacle[x][north] ));
					if ( canGoEast ) obstacleIDs.insert( getConnectivityID(
						mTileClosestObstacle[east][y] ));
					if ( canGoSouth ) obstacleIDs.insert( getConnectivityID(
						mTileClosestObstacle[x][south] ));

					if (diagonals)
					{
						if (canGoWest&&canGoNorth) 
							obstacleIDs.insert( getConnectivityID(
							mTileClosestObstacle[west][north] ));
						if (canGoWest&&canGoSouth)
							obstacleIDs.insert( getConnectivityID(
							mTileClosestObstacle[west][south] ));
						if (canGoEast&&canGoNorth)
							obstacleIDs.insert( getConnectivityID(
							mTileClosestObstacle[east][north] ));
						if (canGoEast&&canGoSouth)
							obstacleIDs.insert( getConnectivityID(
							mTileClosestObstacle[east][south] ));
					}
					count = obstacleIDs.size();
					if( count == 1 ) img.draw_point(x,y,blue);
					if( count == 2 ) img.draw_point(x,y,ltBlue);
					if( count == 3 ) img.draw_point(x,y,green);
					if( count == 4 ) img.draw_point(x,y,ltGreen);
					if( count == 5 ) img.draw_point(x,y,red);
					if( count >= 6 ) img.draw_point(x,y,white);
					
				}
			}
		}
		bmp_name += "unique bodies- ";
		bmp_name += Broodwar->mapFileName();
		bmp_name += ".bmp";

		img.save_bmp(bmp_name.c_str());
	}

	int TerrainAnalyzer::getConnectivityID( WalkTile sample )
	{
		if ( sample.x() >= 0 && sample.y() >= 0
			&& sample.x() < mMapWidth && sample.y() < mMapHeight )
			return mTileConnectivity[sample.x()][sample.y()];
		else return 0;
	}

	void TerrainAnalyzer::saveShockMap( bool diagonals )
	{
		CImg<unsigned char> img(mMapWidth, mMapHeight, 1, 3);
		img.fill(0);
		std::set<WalkTile> obstacleIDs;
		int count = 0;
		unsigned char blue[3] = {0,0,150}; //blue
		unsigned char ltBlue[3] = {50,50,255};
		unsigned char green[3] = {0,150,0};
		unsigned char ltGreen[3] = {50,255,50};
		unsigned char red[3] = {150,0,0};
		unsigned char white[3] = {255,255,255};
		unsigned char black[3] = {0,0,0};
		string bmp_name = "Shock Map ";
		if ( diagonals ) bmp_name += "Diagonals ";
		for(int x = 0; x < mMapWidth; ++x)
		{
			for(int y = 0; y < mMapHeight; ++y)
			{
				if (Broodwar->isWalkable(x,y))
				{
					obstacleIDs.clear();
					const int west = x - 1;
					const int north = y - 1;
					const int east = x + 1;
					const int south = y + 1;

					const bool canGoWest = west >= 0;
					const bool canGoNorth = north >= 0;
					const bool canGoEast = east < mMapWidth;
					const bool canGoSouth = south < mMapHeight;

					if ( canGoWest ) obstacleIDs.insert( 
						mTileClosestObstacle[west][y] );
					if ( canGoNorth ) obstacleIDs.insert(
						mTileClosestObstacle[x][north] );
					if ( canGoEast ) obstacleIDs.insert( 
						mTileClosestObstacle[east][y] );
					if ( canGoSouth ) obstacleIDs.insert(
						mTileClosestObstacle[x][south] );

					if (diagonals)
					{
						if (canGoWest&&canGoNorth) 
							obstacleIDs.insert( 
							mTileClosestObstacle[west][north] );
						if (canGoWest&&canGoSouth)
							obstacleIDs.insert( 
							mTileClosestObstacle[west][south] );
						if (canGoEast&&canGoNorth)
							obstacleIDs.insert( 
							mTileClosestObstacle[east][north] );
						if (canGoEast&&canGoSouth)
							obstacleIDs.insert( 
							mTileClosestObstacle[east][south] );
					}
					count = obstacleIDs.size();
					if( count == 1 ) img.draw_point(x,y,blue);
					if( count == 2 ) img.draw_point(x,y,ltBlue);
					if( count == 3 ) img.draw_point(x,y,green);
					if( count == 4 ) img.draw_point(x,y,ltGreen);
					if( count == 5 ) img.draw_point(x,y,red);
					if( count >= 6 ) img.draw_point(x,y,white); 

				}
			}
		}
		// add pink circles for region centers
		// NOTE: center apparently not used
		unsigned char pink[3] = {255,200,200};
 		BOOST_FOREACH( pRegion r, mRegions )
 		{
 			if (r->clearance() > 10)
 			{
 				img.draw_circle( r->getCenter().x()/8, r->getCenter().y()/8, 3, pink,1);
 			}
 		}
		bmp_name += "- ";
		bmp_name += Broodwar->mapFileName();
		bmp_name += ".bmp";
		img.save_bmp(bmp_name.c_str());
	}

	void TerrainAnalyzer::createBases()
	{
		// draw null region map
		if ( config_->get<bool>("TerrainAnalyzer.save_nullRegions"))
		{
			string fileName = "Null Regions - ";
			fileName += Broodwar->mapFileName();
			fileName += ".bmp";
			CImg<unsigned char> nullRegions( mMapWidth, mMapHeight, 1, 3);
			nullRegions.fill(0);
			unsigned char white[3] = {255,255,255};
			unsigned char black[3] = {0,0,0};
			for( int x = 0; x < mMapWidth; ++x )
			{
				for( int y = 0; y < mMapHeight; ++y )
				{
					if( !mWTileToRegion[x][y] )
						nullRegions.draw_point( x, y, white );
				}
			}
			nullRegions.save_bmp( fileName.c_str() );
		}
		
		
		logfile << "TerrainAnalyzer creating bases.\n";
		//Create a set of all Geysers and all mineral patches with more than 200 minerals
		set< BWAPI::Unit*> staticMinerals = Broodwar->getStaticMinerals();
		for( set< BWAPI::Unit*>::iterator it = staticMinerals.begin(); 
			it!=staticMinerals.end(); ++it )
		{
			 BWAPI::Unit* mineral = *it;
			if ( mineral->getResources() > 250 )
			{
				UIptr temp( new UnitInfo(mineral) );
				mResources.insert( temp );
			}
		}
		set< BWAPI::Unit*> staticGeysers = Broodwar->getStaticGeysers();
		for( set< BWAPI::Unit*>::iterator it = staticGeysers.begin(); 
			it!=staticGeysers.end(); ++it )
		{
			 BWAPI::Unit* geyser = *it;
			UIptr temp( new UnitInfo(geyser) );
			mResources.insert( temp );
		}



		//Group them into clusters
		int thresh = config_->get<int>("TerrainAnalyzer.resourceClusterThresh");
		int radius = config_->get<int>("TerrainAnalyzer.resourceClusterRadius");
		// make temp clusterman to do cluster analysis
		ClusterManager* clusterer = new ClusterManager(0);
		clusterer->setClusters_DBSCAN(mResources, thresh, radius);
		delete clusterer;
		clusterer = NULL;
		//GameState::getInstance().ClusterMan().setClusters_DBSCAN(mResources, 
		//	thresh, radius);
		// now the cIDs will be set
		// need to separate the clusters into sets.
		map<int,setUI> CIDtoSet;
		mLonelyResources.clear();
		int cid = 0;
		int cidMax = 0;
		setUI::iterator iRes = mResources.begin();
		for( iRes; iRes != mResources.end(); ++iRes )
		{
			UIptr res = *iRes;
			cid = res->clusterID();
			if ( cid == 0 )
			{
				mLonelyResources.insert( res );
			}
			else
			{
				if (cid > cidMax) cidMax = cid;
				CIDtoSet[cid].insert( res );
			}
		}
		// detect and separate bridged clusters
		bool bridgedCluster = false;
		map<pRegion,setUI> regionToSet;
		pRegion cRegion;
		setUI reassignedClusters;
		for ( map<int,setUI>::iterator ci = CIDtoSet.begin(); 
			ci != CIDtoSet.end(); ++ci )
		{// for each cluster set, detect if they are all of same region
			bridgedCluster = false;
			regionToSet.clear();
			setUI::iterator cisecond = ci->second.begin();
			for ( cisecond; cisecond != ci->second.end(); ++ cisecond )
			{
				UIptr res = *cisecond;
				WalkTile pos(res->getPosition());
				cRegion = mWTileToRegion[pos.x()][pos.y()];
				if ( !cRegion ) 
				{
					logfile << "TerrainAnalyzer:NULL region found for tile ["
						<< pos.x()<<","<<pos.y()<<"] \n";
					logWriteBuffer();
				}
				regionToSet[cRegion].insert( res );
			} // now all resources in the cluster divided by region
			if (regionToSet.size() > 1)
			{	// bridged cluster detected!
				// clear original cluster
				ci->second.clear();
				map<pRegion,setUI>::iterator rsi = regionToSet.begin();
				// add cluster in first region back in 
				setUI::iterator iSec = rsi->second.begin();
				for( iSec; iSec != rsi->second.end(); ++iSec )
				{
					UIptr res = *iSec;
					ci->second.insert( res );
				}
				++rsi; // skip past first group
				for ( rsi; rsi != regionToSet.end(); ++rsi) 
				{// reassign 2nd and later groups
					++cidMax; // get new cid for this cluster
					iSec = rsi->second.begin();
					for( iSec; iSec != rsi->second.end(); ++iSec )
					{
						UIptr res = *iSec;
						res->clusterID(cidMax);
						// add to reassigned set, cannot change CIDtoSet now
						reassignedClusters.insert(res);
					}
				}
			}
		}
		// put reassigned clusters back in CIDtoSet
		setUI::iterator rci = reassignedClusters.begin();
		for( rci; rci != reassignedClusters.end(); ++rci )
		{
			UIptr resource = *rci;
			CIDtoSet[resource->clusterID()].insert(resource);
		}
		if ( config_->get<bool>("TerrainAnalyzer.save_resourceCheck"))
		{// draw resource map to find out distortion
			CImg<unsigned char> resourceCheck( mMapWidth, mMapHeight, 1, 3);
			resourceCheck.fill(0);
			unsigned char mineral_blue[3] = {150,200,255};
			unsigned char lonely_yellow[3] = {200,200,20};
			string fileName = "Resource check - ";
			fileName += Broodwar->mapFileName();
			fileName += ".bmp";
			BOOST_FOREACH( UIptr resource, mResources )
			{
				const  BWAPI::Unit* res = resource->getUnit();
				// minerals are 8x4, geysers 16x8
				int top = PtoW(resource->topPx());
				int left = PtoW(resource->leftPx());
				int bot = top + 3;
				if ( resource->getUnit()->getType() == BWAPI::UnitTypes::Resource_Vespene_Geyser )
					bot = top + 7;
				int right = left + 7;
				if ( resource->getUnit()->getType() == BWAPI::UnitTypes::Resource_Vespene_Geyser )
					right = left + 15;
				if ( mLonelyResources.count(resource) )
				{
					resourceCheck.draw_rectangle(left,top,right,bot,lonely_yellow,1);
				}
				else resourceCheck.draw_rectangle(left,top,right,bot,mineral_blue,1);
			}
			resourceCheck.save_bmp( fileName.c_str() );
		}
		
		reassignedClusters.clear();

		// add clusters to regions.
		for ( map<int,setUI>::iterator ci = CIDtoSet.begin(); 
			ci != CIDtoSet.end(); ++ci )
		{
			UIptr res = *ci->second.begin();
			pRegion cReg = getRegion( res->getPosition() );
			cReg->addResourceCluster( 
				pResourceCluster(new ResourceCluster(ci->second, cReg))  );
		}
		// find optimal resource depot construction locations
		mMBTWidth = mMapWidth/4;	// 4x4 walk tile = 1 build tile
		mMBTHeight = mMapHeight/4;
		mDepotScore.resize( mMBTWidth, mMBTHeight, 0 );
		// resource depots are 4x3 build tiles. Traverse only within 7 tiles.
		// visit each resource besides lonely ones, ignore those
		int rect_BT[4] = {0,0,0,0}; // hold resource rect
		int rLeft, rRight, rTop, rBot; // resource box temp
		// for minerals we add 1, but geysers are more valuable
		int gasBias = config_->get<int>("TerrainAnalyzer.gasBias");
		setUI::iterator resIt = mResources.begin();
		for( resIt; resIt != mResources.end(); ++resIt )
		{
			if( mLonelyResources.count(*resIt) > 0 ) continue;
			int val = 1;
			if((*resIt)->getType() == BWAPI::UnitTypes::Resource_Vespene_Geyser ) 
				val = gasBias;
			for( int radius = 7; radius > 2; --radius )
			{
				int radPx = BtoP(radius);
				getBoundingBox( *(*resIt)->getUnit(), rLeft, rTop, rRight, rBot, radPx);
				// convert pixel coords to BuildTile
				getTileBounds( rLeft, rTop, rRight, rBot );
				bool tooClose = radius <= 3;
				addDepotScore( val, rLeft, rTop, rRight, rBot, tooClose );
			}
		}
		// draw image to test, using heatmap
		if ( config_->get<bool>("TerrainAnalyzer.save_depotScore") )
		{
			CImg<unsigned char> depotScore( mMBTWidth, mMBTHeight, 1, 3);
			depotScore.fill(0);
			string filename = "Depot Score - ";
			filename += Broodwar->mapFileName();
			filename += ".bmp";
			minVal_ = 0;
			maxVal_ = 0;
			for ( int x = 0; x < mMBTWidth; ++x )
			{	// get max value
				for ( int y = 0; y < mMBTHeight; ++y )
				{
					if ( mDepotScore[x][y] > maxVal_ )
					{
						maxVal_ = mDepotScore[x][y];
					}
				}
			}
			spread_ = maxVal_ - minVal_;
			unsigned char color[3] = {0,0,0};
			const unsigned char black[3] = {0,0,0};
			for ( int x = 0; x < mMBTWidth; ++x )
			{
				for ( int y = 0; y < mMBTHeight; ++y )
				{
					if ( mDepotScore[x][y] == -1 )
					{
						depotScore.draw_point(x,y,black);
					}
					else
					{
						getHeatmapColor( mDepotScore[x][y], color );
						depotScore.draw_point(x,y,color);
					}
				}
			}
			depotScore.save_bmp( filename.c_str() );
		}
		// get optimal spots for each cluster
		set<pRegion>::iterator iReg = mRegions.begin();
		for ( iReg; iReg != mRegions.end(); ++iReg )
		{
			pRegion region = *iReg;
			set<pResourceCluster>::iterator iRC = region->resourceClusters().begin();
			for ( iRC; iRC != region->resourceClusters().end(); ++iRC )
			{ // iterate over each cluster
				pResourceCluster rCluster = *iRC;
				// l,t,r,b of search area
				int cLeft = -1;
				int cTop = -1;
				int cRight = -1;
				int cBottom = -1;
				setUI::iterator ri = rCluster->resources().begin();
				for ( ri; ri != rCluster->resources().end(); ++ri )
				{	// find bounds of resources cluster area
					UIptr resource = *ri;
					if( cLeft > resource->leftPx() || cLeft == -1 )
						cLeft = resource->leftPx();
					if( cTop > resource->topPx() || cTop == -1 )
						cTop = resource->topPx();
					if( cRight < resource->rightPx() || cRight == -1 )
						cRight = resource->rightPx();
					if( cBottom < resource->bottomPx() || cBottom == -1 )
						cBottom = resource->bottomPx();
				}
				// convert bounds to Build Tile units, extend and crop
				// remember depot is 4x3, and must build 3 tiles away
				cLeft = PtoB(cLeft) - 7;
				cTop = PtoB(cTop) - 6;
				cRight = PtoB(cRight) + 5;
				cBottom = PtoB(cBottom) + 4;
				cropToMap( cLeft, cTop, cRight, cBottom, mMBTWidth, mMBTHeight );
				// scan over area and find maximum score
 				TilePosition best;
				int maxScore = 0;
				int score;
				for ( int x = cLeft; x <= cRight; ++x )
				{
					for ( int y = cTop; y <= cBottom; ++y )
					{
						score = calcDepotScore(x,y,region);
						if( score > maxScore )
						{
							maxScore = score;
							best = TilePosition( x, y );
						}
					}
				}
				// we should have the best location now.
				rCluster->depotPosition( best );
			}
		}
		// draw depot positions for troubleshooting
		if ( config_->get<bool>("TerrainAnalyzer.save_depotPositions") )
		{
			CImg<unsigned char> depotPos( mMBTWidth, mMBTHeight, 1, 3);
			depotPos.fill(0);
			string filename = "Depot Positions - ";
			filename += Broodwar->mapFileName();
			filename += ".bmp";
			unsigned char depotColor[3] = {255,102,0}; // home depot orange
			set<pRegion>::iterator iReg = mRegions.begin();
			for ( iReg; iReg != mRegions.end(); ++iReg )
			{
				pRegion region = *iReg;
				set<pResourceCluster>::iterator iRC = region->resourceClusters().begin();
				for ( iRC; iRC != region->resourceClusters().end(); ++iRC )
				{
					TilePosition depot = (*iRC)->depotPosition();
					depotPos.draw_rectangle(depot.x(), depot.y(), depot.x()+3,
						depot.y()+2, depotColor );
				}
			}
			depotPos.save_bmp( filename.c_str() );
		}
		return;
	}

	void TerrainAnalyzer::drawResourceClusters()
	{
		BOOST_FOREACH( UIptr resource, mResources )
		{
			int x = resource->getUnit()->getPosition().x();
			int y = resource->getUnit()->getPosition().y();
			int cid = resource->clusterID();
			Broodwar->drawTextMap(x,y,"{%d}",cid);
		}
	}

	pRegion TerrainAnalyzer::getRegion( BWAPI::Position pos )
	{
		WalkTile tp(pos);
		return mWTileToRegion[tp.x()][tp.y()];
	}

	TABW::pRegion TerrainAnalyzer::getRegion( WalkTile pos )
	{
		return mWTileToRegion[pos.x()][pos.y()];
	}
	void TerrainAnalyzer::addDepotScore( int val, int left, int top, int right, int bot, bool tooClose )
	{
		for ( int x = left; x < right; ++x )
		{
			for ( int y = top; y < bot; ++y )
			{
				if( mDepotScore[x][y] >= 0 ) mDepotScore[x][y]+=val;
				if(tooClose || !Broodwar->isBuildable(x,y) ) 
					mDepotScore[x][y] = -1;
			}
		}
	}

	int TerrainAnalyzer::calcDepotScore( int leftBT, int topBT, pRegion region )
	{
		int rightBT = leftBT + 4;
		int bottomBT = topBT + 3;
		if( rightBT > mMBTWidth ) return -1;
		if( bottomBT > mMBTHeight ) return -1;
		if( leftBT < 0 ) return -1;
		if( topBT < 0 ) return -1;
		int score = 0;
		for ( int x = leftBT; x < rightBT; ++x )
		{
			for ( int y = topBT; y < bottomBT; ++y )
			{
				if ( mDepotScore[x][y] == -1 || 
					mWTileToRegion[BtoW(x)][BtoW(y)] != region) 
					return -1;
				score += mDepotScore[x][y];
			}
		}
		return score;
	}

	void TerrainAnalyzer::save()
	{
		if ( mArchive == NULL )
		{
			mArchive = new MapArchive();
		}
		mArchive->mapWidthBT = mMBTWidth;
		mArchive->mapHeightBT = mMBTHeight;
		mArchive->wtileConnectivity = mTileConnectivity;
		mArchive->wtileClearance = mTileClearance;
		mArchive->wtileToRegionID.resize(mMapWidth,mMapHeight,0);
		for ( int x = 0; x < mMapWidth; x++ )
		{
			for ( int y = 0; y < mMapHeight; y++)
			{
				pRegion region = mWTileToRegion[x][y];
				mArchive->wtileToRegionID[x][y] = 0;
				if ( region )
					mArchive->wtileToRegionID[x][y] = mWTileToRegion[x][y]->ID();
			}
		}
		BOOST_FOREACH( pRegion region, mRegions )
		{
			RegionArchive ra;
			ra.center.set( region->center() );
			ra.clearance = region->clearance();
			ra.connectivity = region->connectivity();
			ra.id = region->ID();
			ra.size = region->size();
			BOOST_FOREACH( pResourceCluster cluster, region->resourceClusters() )
			{
				ClusterArchive ca;
				ca.id = (*cluster->resources().begin())->clusterID();
				ca.baseLocation.x = cluster->depotPosition().x();
				ca.baseLocation.y = cluster->depotPosition().y();
				BOOST_FOREACH(BWAPI::Position gas, cluster->geyserPositions())
				{
					ca.geysers.push_back( PosA(gas.x(),gas.y()) );
				}
				BOOST_FOREACH(BWAPI::Position min, cluster->mineralPositions())
				{
					ca.minerals.push_back( PosA( min.x(), min.y() ) );
				}
				ra.clusters.push_back(ca);
			}
			mArchive->regions.push_back(ra);
		}
		BOOST_FOREACH( pChoke choke, mChokepoints )
		{
			ChokeArchive cpa;
			cpa.center.set( choke->center() );
			cpa.id = choke->ID();
			cpa.region1 = choke->getRegions().first->ID();
			cpa.region2 = choke->getRegions().second->ID();
			cpa.side1.set(choke->sides().first);
			cpa.side2.set(choke->sides().second);
			cpa.clearance = choke->clearance();
			mArchive->chokepoints.push_back(cpa);
		}
		save_data(mFileName,*mArchive);
		if ( config_->get<bool>("TerrainAnalyzer.save_saveCheck"))
		{
			saveCompositeMap("saveCheck");
			saveFromArchive();
		}
	}

	/// try to load from file, otherwise return false
	bool TerrainAnalyzer::load()
	{
		TimeManager::getInstance().startTimer(TimeManager::TA_load);
		if (!fileExists(mFileName)) return false;
		logfile << "TABW: Map data file exists.\n";
		mArchive = new MapArchive();
		load_data(mFileName, *mArchive);
		if ( mArchive->version != mVersion ) return false;
		logfile << "TABW: file is right version.\n";
		mMBTWidth = mArchive->mapWidthBT;
		mMBTHeight = mArchive->mapHeightBT;
		mMapWidth = mMBTWidth * 4;
		mMapHeight = mMBTHeight * 4;
		mTileConnectivity = mArchive->wtileConnectivity;
		mTileClearance = mArchive->wtileClearance;
		TimeManager::getInstance().stopTimer(TimeManager::TA_load);
		TimeManager::getInstance().startTimer(TimeManager::TA_process);
		// now load regions before we can match ID to pRegion
		map<int,pRegion> idToRegion;
		BOOST_FOREACH(RegionArchive ra, mArchive->regions)
		{
			BWAPI::Position c( ra.center.x, ra.center.y );
			pRegion region( new Region(c, ra.clearance, ra.connectivity) );
			region->ID(ra.id);
			region->size(ra.size);
			BOOST_FOREACH(ClusterArchive ca, ra.clusters)
			{
				// need to find all the resource units from positions.
				setUI resources;
				// pixels from center of unit.
				int dLeft = BWAPI::UnitTypes::Resource_Mineral_Field.dimensionLeft();
				int dUp = BWAPI::UnitTypes::Resource_Mineral_Field.dimensionUp();
				int dRight = BWAPI::UnitTypes::Resource_Mineral_Field.dimensionRight();
				int dDown = BWAPI::UnitTypes::Resource_Mineral_Field.dimensionDown();
				BOOST_FOREACH(PosA pos, ca.minerals)
				{
					Position minPos(pos.x, pos.y);
					set< BWAPI::Unit*> found = Broodwar->getUnitsInRectangle(
						pos.x-dLeft, pos.y-dUp, pos.x+dRight, pos.y+dDown );
					if (found.size() > 1)
						logfile << "Warning: found more than 1 mineral at pos.\n";
					else if (found.empty())
					{
						logfile << "Error: found no mineral at pos.\n";
						return false;
					}
					// get the mineral that matches position recorded
					 BWAPI::Unit* match = NULL;
					set< BWAPI::Unit*>::iterator foundit = found.begin();
					for( foundit; foundit != found.end(); ++foundit)
					{
						if( (*foundit)->getPosition() == minPos ) 
						{
							match=*foundit;
							break;
						}
					}
					if (!match)
					{
						logfile << "Error: found mineral off pos.\n";
						return false;
					}
					UIptr minUI( new UnitInfo(match) );
					minUI->clusterID(ca.id);
					resources.insert(minUI);
					mResources.insert(minUI);
				} // found all the minerals now for gas
				dLeft = BWAPI::UnitTypes::Resource_Vespene_Geyser.dimensionLeft();
				dRight = BWAPI::UnitTypes::Resource_Vespene_Geyser.dimensionRight();
				dUp = BWAPI::UnitTypes::Resource_Vespene_Geyser.dimensionUp();
				dDown = BWAPI::UnitTypes::Resource_Vespene_Geyser.dimensionDown();
				BOOST_FOREACH(PosA pos, ca.geysers)
				{
					Position gasPos(pos.x,pos.y);
					set< BWAPI::Unit*> found = Broodwar->getUnitsInRectangle(
						pos.x-dLeft, pos.y-dUp, pos.x+dRight, pos.y+dDown);
					if (found.size() > 1)
						logfile << "Warning: found more than 1 unit at gas.\n";
					else if (found.empty())
					{
						logfile << "Error: found no gas at pos.\n";
						return false;
					}
					 BWAPI::Unit* match = NULL;
					set< BWAPI::Unit*>::iterator foundit = found.begin();
					for( foundit; foundit != found.end(); ++foundit)
					{
						if( (*foundit)->getPosition() == gasPos ) 
							match=*foundit;
					}
					if (!match)
					{
						logfile << "Error: found mineral off pos.\n";
						return false;
					}
					UIptr gasUI( new UnitInfo(match) );
					gasUI->clusterID(ca.id);
					resources.insert(gasUI);
					mResources.insert(gasUI);
				}	// now found all gas and minerals
				pResourceCluster cluster(new ResourceCluster(resources,region));
				BWAPI::TilePosition depot(ca.baseLocation.x,ca.baseLocation.y);
				cluster->depotPosition(depot);
				region->addResourceCluster(cluster);
			}
			// do chokes later
			idToRegion[region->ID()] = region;
			mRegions.insert(region);
		}
		// now rebuild tiles to region
		mWTileToRegion.resize(mMapWidth,mMapHeight);
		for (int x = 0; x < mMapWidth; ++x)
		{
			for (int y = 0; y < mMapHeight; ++y)
			{
				mWTileToRegion[x][y] = idToRegion[mArchive->wtileToRegionID[x][y]];
			}
		}
		// now to do chokes
		BOOST_FOREACH(ChokeArchive ca, mArchive->chokepoints)
		{
			Position s1(ca.side1.x, ca.side1.y);
			Position s2(ca.side2.x, ca.side2.y);
			pChoke choke( new Chokepoint(s1,s2, ca.clearance) );
			choke->ID(ca.id);
			pRegion r1 = idToRegion[ca.region1];
			pRegion r2 = idToRegion[ca.region2];
			choke->setRegion1(r1);
			choke->setRegion2(r2);
			assert(choke->getRegions().first && choke->getRegions().second);
			if(r1) r1->addChokepoint( choke );
			if(r2) r2->addChokepoint( choke );
			mChokepoints.insert(choke);
		}
		if ( config_->get<bool>("TerrainAnalyzer.save_loadCheck") )
		{
			saveFromArchive();
			saveCompositeMap("loadCheck");
		}
		TimeManager::getInstance().stopTimer(TimeManager::TA_process);
		return true;
	}

	void TerrainAnalyzer::saveCompositeMap( std::string prefix ) const
	{
		logfile << "drawing regions and chokes";
		CImg<unsigned char> img(mMapWidth, mMapHeight, 1, 3);
		img.fill(0);
		unsigned char pointColor[3];
		unsigned char black[] = {0,0,0};
		unsigned char white[] = {255,255,255};
		unsigned char mineral_blue[3] = {150,200,255};
		unsigned char vespene_green[3] = {100,230,200};
		unsigned char home_depot_orange[3] = {255,102,0};
		BOOST_FOREACH(pRegion region, mRegions)
		{
			pointColor[0] = 50 * (region->ID()%5)+50;
			pointColor[2] = 50 * ((region->ID()/5)%5)+50;
			pointColor[1] = 50 * ((region->ID()/25)%5)+50;
			for(int x = 0; x < mMapWidth; ++x)
			{
				for(int y = 0; y < mMapHeight; ++y)
				{
					if (region == mWTileToRegion[x][y])
						img.draw_point(x,y,pointColor);
				}
			}
			// draw resource clusters
			BOOST_FOREACH(pResourceCluster cluster, region->resourceClusters())
			{
				BOOST_FOREACH(Position pos, cluster->mineralPositions())
				{   // minerals are 8x4 walk tiles
					int top = PtoW(pos.y() - 
						BWAPI::UnitTypes::Resource_Mineral_Field.dimensionUp());
					int left = PtoW(pos.x() - 
						BWAPI::UnitTypes::Resource_Mineral_Field.dimensionLeft());
					int bot = top + 3;
					int right = left + 7;
					img.draw_rectangle(left,top,right,bot,mineral_blue,1);
				}
				BOOST_FOREACH(Position pos, cluster->geyserPositions())
				{	// geysers are 16x8
					int top = PtoW(pos.y() -
						BWAPI::UnitTypes::Resource_Vespene_Geyser.dimensionUp());
					int left = PtoW(pos.x() - 
						BWAPI::UnitTypes::Resource_Vespene_Geyser.dimensionLeft());
					int bot = top + 7;
					int right = left + 15;
					img.draw_rectangle(left,top,right,bot,vespene_green,1);
				}
				int left = BtoW(cluster->depotPosition().x());
				int top = BtoW(cluster->depotPosition().y());
				int right = left + 15;//depots 4x3 build tiles, 4 walk = 1 tile
				int bot = top + 11;
				img.draw_rectangle(left,top,right,bot,home_depot_orange,1);
			}
		}
		logfile << "mChokepoint has " << mChokepoints.size() << " chokes\n";
		BOOST_FOREACH(pChoke c, mChokepoints)
		{
			img.draw_line(c->sides().second.x()/8, c->sides().second.y()/8,
				c->sides().first.x()/8, c->sides().first.y()/8, white );
			//img.draw_circle(c->mSides.first.x()/8, c->mSides.first.y()/8, 2, white, 1);
			//img.draw_circle(c->mSides.second.x()/8, c->mSides.second.y()/8, 2, white, 1);
			img.draw_circle(c->center().x()/8, c->center().y()/8, 2, white, 1);
		}
		string bmp_name = prefix;
		bmp_name += " - ";
		bmp_name += Broodwar->mapFileName();
		bmp_name += ".bmp";
		img.save_bmp(bmp_name.c_str());
	}
/// draws contensts of mArchive
	void TerrainAnalyzer::saveFromArchive() const
	{
		if (!mArchive)
		{
			logfile << "No mArchive to draw from!\n";
			return;
		}
		int mapWidth = BtoW(mArchive->mapWidthBT);
		int mapHeight = BtoW(mArchive->mapHeightBT);
		CImg<unsigned char> img(mapWidth, mapHeight, 1, 3);
		img.fill(0);
		unsigned char pointColor[3];
		unsigned char black[] = {0,0,0};
		unsigned char white[] = {255,255,255};
		unsigned char mineral_blue[3] = {150,200,255};
		unsigned char vespene_green[3] = {100,230,200};
		unsigned char home_depot_orange[3] = {255,102,0};

		for ( int x = 0; x < mapWidth; ++x )
		{
			for ( int y = 0; y < mapHeight; ++y )
			{
				int regionID = mArchive->wtileToRegionID[x][y];
				pointColor[0] = 50 * (regionID%5)+50;
				pointColor[2] = 50 * ((regionID/5)%5)+50;
				pointColor[1] = 50 * ((regionID/25)%5)+50;
				if (regionID > 0)
					img.draw_point(x,y,pointColor);
			}
		}
		BOOST_FOREACH(RegionArchive ra, mArchive->regions)
		{
			// draw resource clusters
			BOOST_FOREACH(ClusterArchive ca, ra.clusters)
			{
				BOOST_FOREACH(PosA pos, ca.minerals)
				{   // minerals are 8x4 walk tiles
					int top = PtoW(pos.y - 
						BWAPI::UnitTypes::Resource_Mineral_Field.dimensionUp());
					int left = PtoW(pos.x - 
						BWAPI::UnitTypes::Resource_Mineral_Field.dimensionDown());
					int bot = top + 3;
					int right = left + 7;
					img.draw_rectangle(left,top,right,bot,mineral_blue,1);
				}
				BOOST_FOREACH(PosA pos, ca.geysers)
				{	// geysers are 16x8
					int top = PtoW(pos.y -
						BWAPI::UnitTypes::Resource_Vespene_Geyser.dimensionUp());
					int left = PtoW(pos.x - 
						BWAPI::UnitTypes::Resource_Vespene_Geyser.dimensionLeft());
					int bot = top + 7;
					int right = left + 15;
					img.draw_rectangle(left,top,right,bot,vespene_green,1);
				}
				int left = BtoW(ca.baseLocation.x);
				int top = BtoW(ca.baseLocation.y);
				int right = left + 15;//depots 4x3 build tiles, 4 walk = 1 tile
				int bot = top + 11;
				img.draw_rectangle(left,top,right,bot,home_depot_orange,1);
			}
		}
		BOOST_FOREACH(ChokeArchive ca, mArchive->chokepoints)
		{
			img.draw_line(PtoW(ca.side2.x), PtoW(ca.side2.y),
				PtoW(ca.side1.x), PtoW(ca.side1.y), white );
			img.draw_circle(PtoW(ca.center.x), PtoW(ca.center.y), 2, white, 1);
		}
		string bmp_name = "Archive test";
		bmp_name += " - ";
		bmp_name += Broodwar->mapFileName();
		bmp_name += ".bmp";
		img.save_bmp(bmp_name.c_str());
	}
}