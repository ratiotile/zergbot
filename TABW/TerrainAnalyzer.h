// based off of Skynet code
#pragma once
#ifndef INCLUDED_TERRAINANALYZER
#define INCLUDED_TERRAINANALYZER

#include "RectangleArray.h"
#include <set>
#include "Region.h"
#include "TABW_typedefs.h"
#include "MapArchive.h"

#include "../common/typedefs.h"
#include "../common/BWAPIext.h"

class Config;

namespace TABW
{
class TerrainAnalyzer
{
public:
	static TerrainAnalyzer& getInstance();
	
	void analyzeTerrain();
	bool isMapAnalyzed() { return isMapAnalyzed_; }
	pRegion getRegion( BWAPI::Position pos );
	pRegion getRegion( BWAPIext::WalkTile pos );
	/// return set of ptr to regions
	std::set<pRegion> getRegions() const {return mRegions;}
	/// return set of chokepoints
	std::set<pChoke> getChokepoints() const {return mChokepoints;}
	/// call by DisplayManager
	void drawResourceClusters();
	int mapHeightWT() const {return mMapHeight;}
	int mapWidthWT() const {return mMapWidth;}
	int getConnectivityID(BWAPIext::WalkTile sample);
private:
	TerrainAnalyzer();
	~TerrainAnalyzer();
	static TerrainAnalyzer* pInstance_;
	bool isMapAnalyzed_;
	Config* config_;	// initialize in constructor

	/// map dimensions in WALK TILES
	int mMapWidth;
	int mMapHeight;
	/// map dimensions in BUILD TILES
	int mMBTWidth;
	int mMBTHeight;
	/// version of file format
	const int mVersion;
	/// needed to save analysis to file
	std::string mFileName;
	/// data structure used to save/load from disk
	MapArchive* mArchive;
	/// place data in MapArchive and serialize to disk
	void save();
	/// try to load map from disk, return false if not successful
	bool load();

	/// each walk tile assigned a continent number
	RectangleArray<int> mTileConnectivity;
	/// how large radius around walktile is clear of obstacles?
	/// arbitrary units, 10 per walktile.
	RectangleArray<int> mTileClearance;
	RectangleArray<BWAPIext::WalkTile> mTileClosestObstacle;
	/// region IDs for small zones of unwalkability
	std::set<int> mSmallObstacles;
	/// Skynet style chokes
	std::set<pChoke> mChokepoints;
	/// Skynet style regions
	std::set<pRegion> mRegions;
	/// Tile address pointing to what region it is in
	RectangleArray<pRegion> mWTileToRegion;
	/// hold set of minerals and gas for cluster analysis
	std::set<UIptr> mResources;
	/// set of single resources
	std::set<UIptr> mLonelyResources;
	/// rate the tiles decreasing with distance from resources
	RectangleArray<int> mDepotScore;

	/// assign walk tiles to continents
	void calculateConnectivity();
	void calculateWalkTileClearance();
	void createRegions(); //Skynet version
	std::pair<BWAPIext::WalkTile, BWAPIext::WalkTile> 
		findChokePoint(BWAPIext::WalkTile center);
	void createBases(); // from skynet
	
	//temp image functions
	int maxVal_;
	int minVal_;
	int spread_;
	/// gets heatmap colors based on maxVal and minVal.
	void getHeatmapColor(int sampleValue, unsigned char heatmapcolor[3]);
	/// draws image based on how many different adjacent tiles.
	void saveShockMap( bool diagonals);
	/// special form using unique obstacle bodies
	void saveShockMapUB( bool diagonals);
	/// generate composite image for checking
	void saveCompositeMap( std::string prefix ) const;
	/// generate image from archive file
	void saveFromArchive() const;
	
	

	/// if tooClose, set tiles -1. Else add 1 to mDepotScore tiles in rectangle.
	void addDepotScore( int val, int left, int top, int right, int bot, bool tooClose );
	/// add up score in 4x3 BT depot footprint, return -1 if nonbuildable
	int calcDepotScore( int leftBT, int topBT, pRegion region );
};
}
#endif