#include "TargetGroup.h"
#include "common/cpp_includes.h"
#include "GameState.h"

int TargetGroup::COUNT_ = 1;
constTGptr TargetGroup::Null( new TargetGroup( UnitInfo::Null ) );

TargetGroup::TargetGroup( int clusterID ):
id_(COUNT_),
cID_( clusterID ),
centroid_( BWAPI::Positions::None )
{
	COUNT_++;
}

TargetGroup::TargetGroup( UIptr unitInfo ):
cID_( 0 )
{
	if ( unitInfo == UnitInfo::Null )
	{
		id_ = 0;
		centroid_ = BWAPI::Positions::None;
	}
	else
	{
		id_ = COUNT_;
		centroid_ = unitInfo->getPosition();
		groupMembers_.insert(unitInfo);
	}
}

TargetGroup::~TargetGroup()
{
	groupMembers_.clear();
}

constUIptr TargetGroup::getUI() const
{
	if (groupMembers_.size() == 1)
	{
		return *groupMembers_.begin();
	}
	else return UnitInfo::Null;
}

bool TargetGroup::addUnit( UIptr unit )
{
	return groupMembers_.insert( unit ).second;
}

bool TargetGroup::removeUnit( UIptr unit )
{
	return groupMembers_.erase( unit ) == 1;
}