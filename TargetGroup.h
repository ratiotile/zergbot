#pragma once
#ifndef INCLUDED_TARGETGROUP_H
#define INCLUDED_TARGETGROUP_H

#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#include "common/typedefs.h"

class TargetGroup
{
public:
	static constTGptr Null; //empty group
	/// Group Constructor; increment COUNT
	TargetGroup( int clusterID );
	/// Single Unit Constructor; increment COUNT, set CID=0
	TargetGroup( UIptr unitInfo );
	/// Destructor; nothing to do
	~TargetGroup();
	/// inserts new unit, true if new, false if duplicate
	bool addUnit( UIptr unit );
	/// erases unit, true if success
	bool removeUnit( UIptr unit );

	// accessors
	BWAPI::Position center() const { return centroid_; }
	double id() const { return id_; }
	double cID() const { return cID_; }
	/// return unit if it is single, else NULL UnitInfo.
	constUIptr getUI() const;
private:
	static int COUNT_;
	BWAPI::Position centroid_;
	int id_;
	int cID_;
	SetUI groupMembers_;
};

#endif