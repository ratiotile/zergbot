#include "TargetManager.h"
#include "common/cpp_includes.h"
#include "TargetGroup.h"
#include <limits>
#include "GameState.h"
#include "ProposedTarget.h"
#include <set>

TargetManager::TargetManager():
initialized_(false)
{

}

TargetManager::~TargetManager()
{
	singleTargets_.clear();
	targetGroups_.clear();
	ucc_connection_.disconnect();
}
/**	Add single unit to SingleUnits_.
*/
void TargetManager::addUnit( UIptr unitInfo )
{
	// validate
	assert( unitInfo && unitInfo != UnitInfo::Null );
	if (unitInfo->clusterID() != 0) return; // prevents duplicates
	TGptr tempTG( new TargetGroup(unitInfo) );
	singleTargets_.push_back( tempTG );
	logfile << "adding " << BWAPIext::getUnitNameID(unitInfo->getUnit())
		<< " to SingleTargets. CID:" << unitInfo->clusterID() << endl;
}

void TargetManager::onFrameBegin()
{
	init();
}

void TargetManager::drawSingles()
{
	BOOST_FOREACH( TGptr single, singleTargets_ )
	{
		constUIptr temp = single->getUI();
		if( temp && temp != UnitInfo::Null )
		{
			Broodwar->drawTextMap( temp->getPosition().x()-10, 
				temp->getPosition().y()-10, "s" );
		}
	}
}

constTGptr TargetManager::getPotentialTarget( const BWAPI::Unit* myUnit ) const
{
	if ( singleTargets_.empty() && targetGroups_.empty() )
	{
		return TargetGroup::Null;
	}
	else return getNearestTarget( myUnit->getPosition() );
}

constTGptr TargetManager::getNearestTarget( BWAPI::Position myPos ) const
{
	int nearestDist = numeric_limits<int>::max();
	constTGptr nearestTarget = TargetGroup::Null;
	int dist = numeric_limits<int>::max(); // temp value
	//linear search over SingleUnits first
	BOOST_FOREACH( constTGptr single, singleTargets_ )
	{
		assert( single != TargetGroup::Null );
		dist = single->getUI()->getPosition().getApproxDistance( myPos );
		if ( dist < nearestDist )
		{
			nearestDist = dist;
			nearestTarget = single;
		}
	}
	return nearestTarget;
}

void TargetManager::unitNewCluster( UIptr unit, int newCID, int oldCID )
{
	assert( unit && unit != UnitInfo::Null );
	logfile << "New cluster! Unit: " << BWAPIext::getUnitNameID(unit->getUnit())
		<< " n:" << newCID << " o:" << oldCID << endl;
	// remove unit from old group, or single units list.
	removeFromGroup( unit, oldCID );
	// add unit to new group, or create new group.
	addToGroup( unit, newCID );
}

void TargetManager::init()
{
	if (!initialized_)
	{
		initialized_ = true;
		logfile << "connecting slot to new cluster signal.\n";
		logWriteBuffer();
		// link 'slot' method unitNewCluster to signal, from ANCluster.
		ucc_connection_ = GameState::get().ClusterMan().connectUCCS(
			boost::bind(&TargetManager::unitNewCluster, this, _1, _2, _3));
	}
}

bool TargetManager::removeFromGroup( UIptr unit, int cid )
{
	if ( cid == 0 ) // remove from singleTargets_
	{
		TGlist::iterator sti = singleTargets_.begin();
		while( sti != singleTargets_.end() )
		{
			if ( (*sti)->getUI() == unit )
			{	// found, erase
				singleTargets_.erase(sti);
				sti = singleTargets_.end();
				return true;
			}
			++sti;
		}
		logfile << "Error - remove target: Unit not found in singletargets.\n";
		//Broodwar->printf("Error - remove target: Unit not found in singletargets.");
	}
	else
	{
		TGlist::iterator tgi = targetGroups_.begin();
		while( tgi != targetGroups_.end() )
		{
			if ( (*tgi)->cID() == cid )	// found right group, now erase unit.
			{
				return (*tgi)->removeUnit(unit);
			}
			++tgi;
		}
		logfile << "Error - remove target: Unit not found in targetGroups_.\n";
	}
	return false;
}

void TargetManager::addToGroup( UIptr unit, int cid )
{
	if ( cid == 0 ) // add to singleTargets
	{
		TGptr tempTG( new TargetGroup(unit) );
		singleTargets_.push_back( tempTG );
	}
	else // look for existing TargetGroup; create new
	{
		TGlist::iterator tgi = targetGroups_.begin();
		while( tgi != targetGroups_.end() )
		{
			if ( (*tgi)->cID() == cid )	// found right group, add unit
			{
				bool result = (*tgi)->addUnit( unit );
				if( !result )
				{
					logfile << "TargetManager: cannot add, unit already exists\n";
					//Broodwar->printf("TargetManager: cannot add, unit already exists");
				}
			}
			++tgi;
		}	// did not find right group, create new group
		logfile << "did not find group for unit, creating new one.\n";
		TGptr tempTG( new TargetGroup(cid) );
		tempTG->addUnit(unit);
		targetGroups_.push_back( tempTG );
	}
}

bool TargetManager::proposeTarget( TGptr target, UAptr proposer )
{
	PTptr temp( new ProposedTarget( target, proposer ) );
	proposedTargets_.push_back( temp );
	return true;
}

BWAPI::Unit* TargetManager::getTarget( const BWAPI::Unit* myUnit ) const
{
	return getTarget(myUnit->getPosition());
}

BWAPI::Unit* TargetManager::getTarget( BWAPI::Position position ) const
{
	set< BWAPI::Unit*> firstPriority;
	set< BWAPI::Unit*> lastPriority;
	set< BWAPI::Unit*> units = Broodwar->getAllUnits();
	for(set< BWAPI::Unit*>::iterator iter = units.begin(); iter != units.end(); ++iter)
	{
		BWAPI::Unit* unit = *iter;
		if (unit->getPlayer()->isEnemy(Broodwar->self()))
		{
			if (unit->getType().isWorker() || BWAPIext::isCombatUnit(unit->getType()))
			{
				firstPriority.insert(unit);
			}
			else lastPriority.insert(unit);
		}
	}
	if (!firstPriority.empty())
	{
		return BWAPIext::getClosestUnit(position,firstPriority);
	}
	else if( !lastPriority.empty() )
	{
		return BWAPIext::getClosestUnit(position,lastPriority);
	}
	else
	{
		logfile << "No enemy units left!\n";
		return NULL;
	}
}