#pragma once
#ifndef INCLUDED_TARGETMANAGER_H
#define INCLUDED_TARGETMANAGER_H

#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#include <boost/shared_ptr.hpp>
#include "common/UnitInfo.h"
#include "common/typedefs.h"
#include <list>
#include <boost/signals2.hpp>
#include "typedef_UnitAgent.h"
#include "common/Singleton.h"

class TargetGroup;
class ProposedTarget;

class TargetManager: public Singleton<TargetManager>
{
public:
	/// constructor
	TargetManager();
	/// Destructor; shared_ptr should take care of themselves
	~TargetManager();
	/// call on first frame, since cannot call GameState in constructor
	void init();
	/// update function; call every frame
	void onFrameBegin();
	//accessors
	/// get nearest attackable target
	constTGptr getPotentialTarget( const BWAPI::Unit* myUnit ) const;
	BWAPI::Unit* getTarget( const BWAPI::Unit* myUnit ) const;
	BWAPI::Unit* getTarget( BWAPI::Position position ) const;
	//mutators
	/// call when new unit is discovered, from GameState
	void addUnit( UIptr unitInfo );
	/// call when a unit changes its cluster
	void unitNewCluster( UIptr unit, int newCID, int oldCID );
	/// creates a new proposed target and pledges the unit
	bool proposeTarget( TGptr target, UAptr proposer );
protected:
private:
	
	TGlist targetGroups_;
	TGlist singleTargets_;
	boost::signals2::connection ucc_connection_;
	bool initialized_;
	/// list of Proposed Targets; use push_back to add
	typedef boost::shared_ptr<ProposedTarget> PTptr;
	typedef std::list<PTptr> PTlist;
	PTlist proposedTargets_;

	// helpers
	/// returns the nearest Single or Group target to position given
	constTGptr getNearestTarget( BWAPI::Position myPos ) const;
	// debug
	/// print 's' over all single targets.
	void drawSingles();
	/// remove unit from specified group, return true if success
	bool removeFromGroup( UIptr unit, int cid );
	/// add unit to group, or create new group
	void addToGroup( UIptr unit, int cid );
};

#endif