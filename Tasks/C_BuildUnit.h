/**
To be given to EconManager telling it to create the specified unit.
Also used with Budget to get it to reserve the resources.
*/
#pragma  once
#include "Contract.h"

class C_BuildUnit: public Contract
{
public:
	/// constructor for units
	C_BuildUnit(BWAPI::UnitType build)
		:Contract("BuildUnit"), buildType(build)
		,buildLocation(BWAPI::TilePositions::None), worker(NULL)
	{}
	/// for structures
	C_BuildUnit(BWAPI::UnitType build, BWAPI::TilePosition where)
		:Contract("BuildUnit"), buildType(build), buildLocation(where)
		,worker(NULL)
	{}
	BWAPI::UnitType buildType;
	/// for structures
	BWAPI::TilePosition buildLocation;
	/// only for use by UnitManager
	BWAPI::Unit* worker;
};

typedef boost::shared_ptr<C_BuildUnit> pCBU;