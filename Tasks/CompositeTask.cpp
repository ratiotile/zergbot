#include "CompositeTask.h"
#include "../typedef_UnitAgent.h"
#include "../common/typedefs.h"
#include "../UnitAgent.h"
#include "../UnitManager.h"

CompositeTask::CompositeTask( const std::string& taskName )
:Task(taskName)
,mTaskBlocking(true)
{

}
CompositeTask::~CompositeTask()
{
	removeAllSubTasks();
}
TaskStatus::Enum CompositeTask::update()
{
	return TaskStatus::complete;
}

void CompositeTask::addSubTask( pTask subTask )
{
	mSubTasks.push_back(subTask);
}

void CompositeTask::removeAllSubTasks()
{
	mSubTasks.clear();
}

TaskStatus::Enum CompositeTask::processSubTasks()
{
	if (!mTaskBlocking)
	{
		list<pTask>::iterator it = mSubTasks.begin();
		for (it; it != mSubTasks.end(); )
		{
			pTask task = *it;
			if (task->isComplete() || task->isFailed()  )
			{
				taskCleanup(task);
				task->terminate();
				it = mSubTasks.erase(it);
			}
			else
			{
				task->update();
				++it;
			}
		}
		return mStatus;
	}
	// remove any complete or failed tasks at start
	while (!mSubTasks.empty() && (mSubTasks.front()->isComplete()
		|| mSubTasks.front()->isFailed()) )
	{
		// check to see if it has any resources unspent!
		taskCleanup(mSubTasks.front());
		logfile << "Ending task " << mSubTasks.front()->toString() << endl;
		mSubTasks.front()->terminate();
		mSubTasks.pop_front();
	}
	// run next task
	if (!mSubTasks.empty())
	{
		if (!mResourceBudget.isEmpty())
		{
			allocateResources(); // give resources to subtask
		}
		TaskStatus::Enum subStatus = mSubTasks.front()->update();
		//mResourcesRequired = mSubTasks.front()->resourcesOutstanding();
		// return active if still tasks in list
		if (subStatus == TaskStatus::complete && mSubTasks.size() > 0)
		{
			return TaskStatus::active;
		}
		return subStatus;
	}
	else return TaskStatus::complete;
}

void CompositeTask::taskCleanup( pTask task )
{
	// check to see if it has any resources unspent!
	if (!task->excessUnits.empty())
	{
		mUnits.insert(task->excessUnits.begin()
			, task->excessUnits.end());
		task->excessUnits.clear();
	}
	if (!task->resourcesBudgeted().isEmpty() && task->isFailed())
	{
		logfile << "reclaimed from ended task: " 
			<< task->resourcesBudgeted().toString() <<endl;
		mResourceBudget = mResourceBudget + task->resourcesBudgeted();
		task->clearBudgeted();
	}
}

void CompositeTask::allocateResources()
{
	// assume that front task is active and executing
	ResourceSet toAllocate;
	assert(toAllocate.minerals == 0 && toAllocate.gas == 0 && toAllocate.supply == 0);
	ResourceSet taskRequires = mSubTasks.front()->resourcesOutstanding();
	//logfile << getName() <<  " allocating resources... Task " << mSubTasks.front()->getName() 
	//	<< " requires: " 
	//	<< taskRequires.toString() << endl;
	if (mResourceBudget.minerals > 0)
	{
		if (taskRequires.minerals <= mResourceBudget.minerals)
		{
			mResourceBudget.minerals -= taskRequires.minerals;
			toAllocate.minerals = taskRequires.minerals;
		}
		else // requires more than our budget
		{
			toAllocate.minerals = mResourceBudget.minerals;
			mResourceBudget.minerals = 0;
		}
	}
	if (mResourceBudget.gas > 0)
	{
		if (taskRequires.gas <= mResourceBudget.gas)
		{
			mResourceBudget.gas -= taskRequires.gas;
			toAllocate.gas = taskRequires.gas;
		}
		else // requires more than our budget
		{
			toAllocate.gas = mResourceBudget.gas;
			mResourceBudget.gas = 0;
		}
	}
	if (mResourceBudget.supply > 0)
	{
		if (taskRequires.supply<= mResourceBudget.supply)
		{
			mResourceBudget.supply -= taskRequires.supply;
			toAllocate.supply = taskRequires.supply;
		}
		else // requires more than our budget
		{
			toAllocate.supply = mResourceBudget.supply;
			mResourceBudget.supply = 0;
		}
	}
	// if to be allocated is more than 0, do it.
	if (!toAllocate.isEmpty())
	{
// 		logfile << getName() << " giving task " << mSubTasks.front()->toString() <<
// 			" resources:  " << toAllocate.toString() << " Remaining budget: "
// 			<<mResourceBudget.toString() << endl;
		mSubTasks.front()->ReserveResources(toAllocate);
		mResourcesRequired = mResourcesRequired - toAllocate;
	}
	// now for units
	if (!mSubTasks.front()->resourcesOutstanding().units.empty())
	{
		BOOST_FOREACH( unitCount uc, mSubTasks.front()->resourcesOutstanding().units )
		{
			UAset matchType = UnitManager::get().getSubset(mUnits,uc.first);
			if (!matchType.empty())
			{
				int numToSend = std::min((int)matchType.size(), uc.second );
				UAset::iterator matchesIter = matchType.begin();
				for (int i = 0; i < numToSend; i++)
				{
					UAptr targetUnit = *matchesIter;
					if (removeUnit(targetUnit)) // attmpt to remove unit from our control
					{
						logfile << "successfully sent unit "
							<< targetUnit->getNameID() << " to task "
							<< mSubTasks.front()->getName() << "\n";
						mSubTasks.front()->addUnit(targetUnit);
					}
				}
			}
		}
	}
}



void CompositeTask::updateResources()
{
	mResourcesRequired.clear();
	BOOST_FOREACH(pTask task, mSubTasks)
	{
		if (task->status() == TaskStatus::inactive || task->status() == TaskStatus::active)
		{
			mResourcesRequired = mResourcesRequired + task->resourcesOutstanding();
		}
	}
}

int CompositeTask::displayTaskInfo( int x, int y, int s ) const
{
	Broodwar->drawTextScreen(x,y,"%s",toString().c_str());
	y += s;
	// display sub-tasks
	BOOST_FOREACH(pTask task, mSubTasks)
	{
		y = task->displayTaskInfo(x,y,s);
	}
	//y = displayBuildOrder(x,y,s);
	// display remaining build order
	return y;
}

