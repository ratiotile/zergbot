/**
CompositeTask is a task that is able to hold other tasks as subtasks. This 
way it is able to reuse code and accomplish more complex behavior than
a single task alone
*/
#pragma once
#include "Task.h"
#include "typedef_Task.h"
#include <list>

class CompositeTask: public Task
{
public:
	CompositeTask( const std::string& taskName );
	virtual ~CompositeTask();
	virtual TaskStatus::Enum update();
	virtual void activate() = 0;
	//virtual void terminate() = 0;
	virtual void addSubTask( pTask subTask );
	virtual int displayTaskInfo(int x, int y, int s) const;
protected:
	std::list<pTask> mSubTasks;
	bool mTaskBlocking; //whether to run tasks in order or all at once.

	virtual TaskStatus::Enum processSubTasks();
	// if we have any resources budgeted, give as much as first task needs.
	virtual void allocateResources();
	void removeAllSubTasks();
	virtual void updateResources();
	void taskCleanup(pTask task);
};