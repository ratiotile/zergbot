/**
Contract represents a request of a contractor, which is passed a pointer to
the Contract and must report back on fail or success.

Contractee must call contractor.cancel(contract) to remove once finished.
*/
#pragma once
#include "Task.h"

class Contract
{
public:
	Contract(const std::string& name ): name(name), status(TaskStatus::inactive)
	{}
	std::string name;
	//new

	/// only Contractor should change
	TaskStatus::Enum status;
	bool isComplete() {return status == TaskStatus::complete;}
	bool isFailed() {return status == TaskStatus::failed;}
	bool isInactive() {return status == TaskStatus::inactive;}
	bool isActive() {return status == TaskStatus::active;}
};