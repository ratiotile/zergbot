#include "T_AutoSupply.h"
#include "../common/cpp_includes.h"
#include "../Budget.h"
#include "T_BuildStructure.h"
#include "C_BuildUnit.h"
#include "../Blackboard.h"

using namespace BBS;

const string T_AutoSupply::name = "AutoDepot";

T_AutoSupply::T_AutoSupply()
:CompositeTask(T_AutoSupply::name)
{

}

T_AutoSupply::~T_AutoSupply()
{

}

TaskStatus::Enum T_AutoSupply::update()
{
	activateIfInactive();
	if (Broodwar->self()->supplyTotal() >= 400)
	{// keep active in case we lose supply
		return TaskStatus::active;
	}
	// check supply
	int supplyAvailable = Broodwar->self()->supplyTotal() 
		- Broodwar->self()->supplyUsed();
	int cc = BB.ES.getUnitCount(BWAPI::UnitTypes::Terran_Command_Center);
	int rax = BB.ES.getUnitCount(BWAPI::UnitTypes::Terran_Barracks);
	int fact = BB.ES.getUnitCount(BWAPI::UnitTypes::Terran_Factory);
	int port = BB.ES.getUnitCount(BWAPI::UnitTypes::Terran_Starport);
	int macroRoundSupply = 2*cc + 2*rax + 4*fact + 4*port;
	// also count incomplete depots to avoid spamming!
	int incomplete = BB.ES.getUnderConstruction(BWAPI::UnitTypes::Terran_Supply_Depot);
	supplyAvailable += 16*incomplete;
	if (supplyAvailable <= macroRoundSupply && mSubTasks.empty())
	{
		logfile << Broodwar->getFrameCount() << " Autosupply building depot!\n";
		Broodwar->printf("Supply available:%d Supply macro:%d",supplyAvailable,macroRoundSupply);
		// add build task
		pCBU cbu( new C_BuildUnit(BWAPI::UnitTypes::Terran_Supply_Depot) );
		assert(cbu);
		//pTBS buildSupply( new T_BuildStructure(cbu) );
		//assert(buildSupply);
		//mSubTasks.push_back(buildSupply);
	}
	processSubTasks();
	// never end, unless canceled
	return TaskStatus::active;
}

void T_AutoSupply::activate()
{
	mStatus = TaskStatus::active;
}

void T_AutoSupply::terminate()
{

}

