/** 
Automatically add supply depots to avoid supply block. Activate once build 
order is complete.
*/
#pragma once
#include "CompositeTask.h"
#include <BWAPI.h>
#include "typedef_Task.h"

class T_AutoSupply: public CompositeTask
{
public:
	T_AutoSupply();
	~T_AutoSupply();
	TaskStatus::Enum update();
	void activate();
	void terminate();
	const static std::string name;
private:
};

typedef boost::shared_ptr<T_AutoSupply> pTAS;