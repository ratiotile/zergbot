#include "T_BBS.h"
#include "T_BuildOrder.h"
#include "../BuildPlanner/Openings.h"
#include "../common/cpp_includes.h"

T_BBS::T_BBS()
:CompositeTask("BBS")
{
	mBuildOrder = Openings::get().getBuildOrder("BBS");
}

T_BBS::~T_BBS()
{

}

TaskStatus::Enum T_BBS::update()
{
	activateIfInactive();
	if (isComplete()) return mStatus;
	processSubTasks();
	return mStatus;
}

void T_BBS::activate()
{
	mStatus = TaskStatus::active;
	checkPrereqs(mBuildOrder);
	mSubTasks.push_back( pTask(new T_BuildOrder(mBuildOrder)) );
}

void T_BBS::terminate()
{

}

void T_BBS::checkPrereqs(BuildOrder& bo)
{
	logfile << "Checking completed items in build order\n";
	// get set of units
	set< BWAPI::Unit*> myUnits = Broodwar->self()->getUnits();
	// loop through the build order and pop off units that exist
	while(true)
	{
		UnitType nextBuild = bo.getBuildItem();
		// try to find in unit pool
		set< BWAPI::Unit*>::iterator ut = myUnits.begin();
		bool found = false;
		for (ut; ut != myUnits.end(); )
		{
			if ((*ut)->getType() == nextBuild)
			{
				found = true;
				// remove the unit
				set< BWAPI::Unit*>::iterator toErase = ut;
				++ut;
				myUnits.erase(toErase);
				break;
			}
			else ++ut;
		}
		if (found)
		{
			logfile<< "found and removed existing structure from build Order: "
				<< nextBuild.getName() << endl;
			Broodwar->printf("Found: %s", nextBuild.getName().c_str());
			// pop front of build order list
			bo.advance();
		}
		else
		{
			logfile <<"Item does not exist, next build item: "
				<<nextBuild.getName() << endl;
			Broodwar->printf("Next build item: %s", nextBuild.getName().c_str());
			// not found, break
			break;
		}
	}
}