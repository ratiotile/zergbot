/**
Execute BBS bunker rush, first check prerequisites, build any missing buildings,
pump marines, and attack!
*/
#pragma once
#include "CompositeTask.h"
#include "../BuildPlanner/BuildOrder.h"


class T_BBS: public CompositeTask
{
public:
	T_BBS();
	~T_BBS();
	TaskStatus::Enum update();
	void activate();
	void terminate();
private:
	// pump marines once complete
	bool isBarracksDone;
	BuildOrder mBuildOrder;

	/// look for already completed structures and pop them from build Order
	void checkPrereqs(BuildOrder& bo);
	
};