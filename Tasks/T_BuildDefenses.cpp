#include "T_BuildDefenses.h"
#include "../common/cpp_includes.h"
#include "T_BuildStructure.h"
#include "../HarvestManager.h"
#include <list>
#include "../MapInfo.h"

using namespace BWAPI::UnitTypes;

T_BuildDefenses::T_BuildDefenses()
:CompositeTask("BuildDefenses")
{
	mTaskBlocking = false;
}

TaskStatus::Enum T_BuildDefenses::update()
{
	activateIfInactive();
	processSubTasks();
	if (mSubTasks.empty())
	{
		mStatus = TaskStatus::complete;
	}
	return mStatus;
}

void T_BuildDefenses::activate()
{
	buildSunkens();
	mStatus = TaskStatus::active;
}

void T_BuildDefenses::terminate()
{

}

void T_BuildDefenses::buildSunkens()
{
	list<Position> basePositions = HarvestManager::get().getDepotPositions();
	int maxToBuild = 3;
	int minToBuild = 1;
	int maxCount = -1;
	list<int> sunkCounts;
	BOOST_FOREACH(Position basePosition, basePositions)
	{
		const regionInfo* baseRegion = MapInfo::get().getRegion(basePosition);
		sunkCounts.push_back(MapInfo::get().countUnitsInRegion(Zerg_Sunken_Colony,baseRegion));
		if (sunkCounts.back() > maxCount)
		{
			maxCount = sunkCounts.back();
		}
	}
	list<int>::iterator iCount = sunkCounts.begin();
	BOOST_FOREACH(Position basePosition, basePositions)
	{
		int toBuild = min(maxToBuild, maxCount - *iCount + minToBuild);
		if (maxCount == 0)
		{
			toBuild = maxToBuild;
		}
		for (int i = 0; i < toBuild; ++i)
		{
			addSubTask(pTask(new T_BuildStructure(Zerg_Sunken_Colony,TilePosition(basePosition))));
			BBS::BB.BQ.pushFront(Zerg_Drone);
		}
		iCount++;
	}
}

T_BuildDefenses::~T_BuildDefenses()
{

}