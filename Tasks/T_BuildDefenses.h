/** 
Build enough defense to defend against the enemy's army if he marches out.
*/
#pragma once
#include "CompositeTask.h"
#include <BWAPI.h>
#include "typedef_Task.h"

class T_BuildDefenses: public CompositeTask
{
public:
	T_BuildDefenses();
	~T_BuildDefenses();
	TaskStatus::Enum update();
	void activate();
	void terminate();
private:
	/// produce sunken colonies
	void buildSunkens();
};