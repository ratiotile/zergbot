#include "T_BuildOrder.h"
#include "../common/cpp_includes.h"
#include "C_BuildUnit.h"
#include "../EconManager.h"
#include "T_BuildUnit.h"
#include "T_BuildStructure.h"
#include "../BuildPlanner/earlyBuildItem.h"
#include "../Budget.h"
#include <sstream>
#include "../baseLocInfo.h"
#include "../MapInfo.h"
#include "T_TakeExpo.h"

T_BuildOrder::T_BuildOrder( const BuildOrder& buildOrder )
:CompositeTask("BuildOrder")
,mBuildOrder(buildOrder)
,buildOrderFinished(false)
{

}

T_BuildOrder::~T_BuildOrder()
{

}

TaskStatus::Enum T_BuildOrder::update()
{
	activateIfInactive();
	//updateResources();
	if (isComplete()) return mStatus;
	if(mBuildOrder.getRemaining() == 0)
	{
		buildOrderFinished = true;
	}
	// check to see if condition met for first item
	if (!buildOrderFinished &&
		currentItemConditionsMet())
	{
		mBuildOrder.printCurrentItem();
// 		logfile << Broodwar->getFrameCount() << " Have " << Broodwar->self()->supplyUsed()/2
// 			<< " supply, " << mBuildOrder.getSupply() << " required, starting "
// 			<< "build order item: " << mBuildOrder.getBuildItem().getName() << endl;
		if (mBuildOrder.getCurrentItem().build != BWAPI::UnitTypes::None)
		{
			initiateBuildTask();
		}
		//TODO: deal with upgrades.
		mBuildOrder.advance();
	}
	else if(mBuildOrder.getRemaining() == 0 && mSubTasks.empty())// finished
	{
		mStatus = TaskStatus::complete;
	}
	else // condition not yet met, and not ended, build workers if greedy
	{
		if (canBuildWorker())
		{
			if (mBuildOrder.getWorkerPolicy() == WorkerPolicyType::greedy)
			{
				// build a worker!
				logfile << Broodwar->getFrameCount() << " Can build worker, greedy policy!\n";
				pTBU buildUnit( new T_BuildUnit(Broodwar->self()->getRace().getWorker()) );
				mSubTasks.push_back(buildUnit);
			}
			else if (mBuildOrder.getWorkerPolicy() == WorkerPolicyType::speed)
			{
				if (Budget::get().enoughResourcesFor(Broodwar->self()->getRace().getWorker()))
				{
					pTBU buildUnit( new T_BuildUnit(Broodwar->self()->getRace().getWorker()) );
					mSubTasks.push_back(buildUnit);
				}
			}
			else // don't build workers
			{

			}
		}
	}
	processSubTasks();
	updateResources();
	//displayBuildOrder();
	return mStatus;
}
/// not the strict resource requirements, just the buildOrder conditions!
bool T_BuildOrder::currentItemConditionsMet()
{
	if (mBuildOrder.getSupply() > Broodwar->self()->supplyUsed()/2 ||
		mBuildOrder.getSupplyCap() > Broodwar->self()->supplyTotal()/2 )
	{
		return false;
	}
	// now check extra conditions
	if (mBuildOrder.getCurrentItem().extraCondition == BuildOrderConditionType::buildingProgressHP)
	{
		UnitType targetType = mBuildOrder.getCurrentItem().extraConditionTarget;
		int triggerHP = mBuildOrder.getCurrentItem().extraConditionValue;
		// look in UnitManager or Use Broodwar.getUnits?
		const set< BWAPI::Unit*>& allUnits = Broodwar->self()->getUnits();
		BOOST_FOREACH(  BWAPI::Unit* unit, allUnits )
		{
			if (unit->getType() == targetType && unit->getHitPoints() > triggerHP)
			{
				return true;
			}
		}
		// didn't find it, return false
		return false;
	}
	// no extra conditions
	return true;
}
bool T_BuildOrder::canBuildWorker()
{
	// check to be sure we are able to make
	UnitType wkrType = Broodwar->self()->getRace().getWorker();
	if (!Broodwar->canMake(NULL,wkrType)) return false;
	if (Budget::get().mineralsAvailable() < 50 && Budget::get().supplyAvailable() < 2) return false;
 	// check that there are no other build worker tasks waiting
	BOOST_FOREACH( pTask task, mSubTasks )
	{
		if (task->getName() == "BuildUnit" && task->status() == TaskStatus::inactive)
		{
			pTBU buildTask = boost::static_pointer_cast<T_BuildUnit>(task);
			if (buildTask->getBuildType().isWorker())
			{
				return false; // we still waiting on another buildWorker task to start
			}
		}
	}
	// if cleared all conditions
	return true;
}

void T_BuildOrder::activate()
{
	logfile << "Activating Build Order Task:\n";
	logfile << mBuildOrder.toString();
	mStatus = TaskStatus::active;
}

void T_BuildOrder::terminate()
{
	//logfile << "terminating buildorder, enabling autosupply.\n";
	//EconManager::get().enableAutoSupply();
}

TaskStatus::Enum T_BuildOrder::initiateBuildTask()
{
	// check extra actions
	baseLocInfo* targetExpo = NULL;
	if (mBuildOrder.getCurrentItem().action == BuildOrderActionType::expandGas)
	{
		// find gas expo
		targetExpo = MapInfo::get().getNextExpo(true);
		if (targetExpo)
		{
			pTTE takeExpo( new T_TakeExpo(targetExpo) );
			mSubTasks.push_back(takeExpo);
		}
		else
		{
			logfile << "T_BuildOrder failed, no gas expo location found!\n";
			mStatus = TaskStatus::failed;
		}
	}
	else if (mBuildOrder.getCurrentItem().action == BuildOrderActionType::expand)
	{
		// any expo
		targetExpo = MapInfo::get().getNextExpo();
		if (targetExpo)
		{
			pTTE takeExpo( new T_TakeExpo(targetExpo) );
			mSubTasks.push_back(takeExpo);
		}
		else
		{
			logfile << "T_BuildOrder failed, no expo location found!\n";
			mStatus = TaskStatus::failed;
		}
	}
	else
	{
		logfile << "T_BuildOrder creating new BuildUnit task\n";
		for (int i = 0; i < mBuildOrder.getCurrentItem().number; i++)
		{
			if (mBuildOrder.getBuildItem().isBuilding())
			{
				pTBS buildStruct( new T_BuildStructure(mBuildOrder.getBuildItem()) );
				mSubTasks.push_back(buildStruct);
			}
			else
			{
				pTBU buildUnit( new T_BuildUnit(mBuildOrder.getBuildItem()) );
				mSubTasks.push_back(buildUnit);
			}
		}
	}
	
	return TaskStatus::active;
}

int T_BuildOrder::displayBuildOrder( int x, int y, int s ) const
{
	// save progress to restore at end
	BuildOrder tempBuildOrder = mBuildOrder;
	Broodwar->drawTextScreen(x,y,"Remaining Items:");
// 	y+=s;
// 	ResourceSet ro = resourcesOutstanding();
// 	Broodwar->drawTextScreen(x,y,"m:%d/%d, g%d/%d, U:%s",mResourceBudget.minerals,
// 		mResourcesRequired.minerals, mResourceBudget.gas,
// 		mResourcesRequired.gas,ro.getUnitsStr().c_str());
	y+=s;
	earlyBuildItem current = tempBuildOrder.getCurrentItem();
	if (current.workerPolicy == WorkerPolicyType::greedy)
		Broodwar->drawTextScreen(x,y,"Wkr_Policy: greedy");
	else if (current.workerPolicy == WorkerPolicyType::speed)
		Broodwar->drawTextScreen(x,y,"Wkr_Policy: speed");
	y+=s;
	do
	{
		Broodwar->drawTextScreen(x,y,"%s",tempBuildOrder.getCurrentItem().toString().c_str());
		y+=s;
		tempBuildOrder.advance();
	}while( tempBuildOrder.getRemaining() > 1 );

	return y;
}
/// not used atm, updated in processSubTasks!
void T_BuildOrder::updateResources()
{
	ResourceSet previous = mResourcesRequired;
	// set mResourcesRequired to amount of next item
	CompositeTask::updateResources();
	if (mBuildOrder.getRemaining() > 0)
	{
		mResourcesRequired = mResourcesRequired + mBuildOrder.getCurrentCost();
	}
	
// 	if ( previous != mResourcesRequired )
// 	{
// 		logfile << "BuildOrder difference in resource requirements: from " << previous.toString()
// 			<< " to: " << mResourcesRequired.toString() << endl;
// 	}
}

std::string T_BuildOrder::toString() const
{
	ostringstream oss;
	oss << "T_BuildOrder " << mBuildOrder.name();
	oss << " B: " << mResourceBudget.minerals << "," << mResourceBudget.gas
		<<"," << mResourceBudget.supply<<" R: "<< mResourcesRequired.minerals
		<<","<< mResourcesRequired.gas<<","<<mResourcesRequired.supply;
	return oss.str();
}

int T_BuildOrder::displayTaskInfo( int x, int y, int s ) const
{
	Broodwar->drawTextScreen(x,y,"%s",toString().c_str());
	y += s;
	// display sub-tasks
 	BOOST_FOREACH(pTask task, mSubTasks)
 	{
 		y = task->displayTaskInfo(x,y,s);
 	}
	if (mBuildOrder.getRemaining() > 0)
	{
		y = displayBuildOrder(x,y,s);
	}
	else
	{
		Broodwar->drawTextScreen(x,y,"Build order %s complete.",mBuildOrder.name().c_str());
		y += s;
	}
	// display remaining build order
	return y;
}