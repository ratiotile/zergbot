/**
Task-BuildOrder is produced by Commander and controls the building of units specified 
in a build order. Commander is responsible for checking its requested resources 
and allocating them.
*/
#pragma once
#include "CompositeTask.h"
#include <BWAPI.h>
#include "../BuildPlanner/BuildOrder.h"
#include "../common/ResourceSet.h"
#include "../typedef_UnitAgent.h"
#include <map>

class C_BuildUnit;
typedef boost::shared_ptr<C_BuildUnit> pCBU;
typedef std::set<UAptr> UAset;
typedef std::map<BWAPI::UnitType,int> unitQuantity;

class T_BuildOrder: public CompositeTask
{
public:
	T_BuildOrder(const BuildOrder& buildOrder);
	~T_BuildOrder();
	TaskStatus::Enum update();
	void activate();
	void terminate();

	int displayBuildOrder(int x, int y, int s) const;
	std::string toString() const;
	int displayTaskInfo(int x, int y, int s) const;
private:
	BuildOrder mBuildOrder;
	std::list<pCBU> mBuildContracts;
	bool buildOrderFinished;

	//helpers
	TaskStatus::Enum initiateBuildTask();
	TaskStatus::Enum processContracts();
	void updateResources();
	/// true if conditions are met for front of buildOrder
	bool currentItemConditionsMet();
	/// true if enough free resources to build a worker
	bool canBuildWorker();
};