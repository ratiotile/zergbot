#include "T_BuildStructure.h"
#include "../common/cpp_includes.h"
#include "../MapInfo.h"
#include "../UnitManager.h"
#include "C_BuildUnit.h"
#include "../Budget.h"
#include "../Blackboard.h"
#include "../common/Config.h"
#include "../WorkerAgent.h"
#include "../typedef_UnitAgent.h"
#include "../HarvestManager.h"
#include "../EventManager.h"
#include <boost/bind.hpp>

using namespace BWAPIext;
using namespace BWAPI::UnitTypes;

// still called "BuildUnit", use Type to tell apart
T_BuildStructure::T_BuildStructure( BWAPI::UnitType toBuild, BWAPI::TilePosition buildPos )
:T_BuildUnit(handleTypes(toBuild)) // setting morphType in handleTypes does nothing
,mBuildLocationHint(buildPos)
,mBuildLocation(BWAPI::TilePositions::None)
,mMorphType(BWAPI::UnitTypes::None)
//,mBuildContract(new C_BuildUnit(toBuild,buildPos))
{
	if (toBuild == BWAPI::UnitTypes::Zerg_Sunken_Colony || toBuild == BWAPI::UnitTypes::Zerg_Spore_Colony)
	{
		mMorphType = toBuild;
		// now add resources
		mResourcesRequired.minerals += mMorphType.mineralPrice();
		mResourcesRequired.gas += mMorphType.gasPrice();
	}
	// test if unit is made from worker or is morphed
	UnitType whatmakes = mUnitType.whatBuilds().first;
	if (whatmakes.isWorker())
	{
		// remove worker from required units, will get it directly
		UnitType wkrType = Broodwar->self()->getRace().getWorker();
		mResourcesRequired.units.erase(wkrType);
	}
	// get own required units for now
	mResourcesRequired.units.erase(whatmakes);
	
	logfile << "T_BuildStructure created for " << mUnitType.getName();
	if (mMorphType != BWAPI::UnitTypes::None) logfile << " morph: "<< mMorphType.getName();
	logfile << " connecting slots \n";
	if (Broodwar->self()->getRace() == BWAPI::Races::Zerg)
	{
		mOnUnitMorphConnection = EventManager::get().addSignalOnUnitMorph(boost::bind(
			&T_BuildStructure::onUnitMorph, this, _1));
	}
	else
	{
		mOnUnitCreateConnection = EventManager::get().addSignalOnUnitCreate(boost::bind(
			&T_BuildStructure::onUnitCreate, this, _1));
	}
}

T_BuildStructure::~T_BuildStructure()
{

}

TaskStatus::Enum T_BuildStructure::update()
{
	activateIfInactive(); // will activate only if have needed resources+worker+location
	if (BB.Cfg().get<bool>("Tasks.show_buildingPlacement"))
	{
		draw_buildingPlacement();
	}
	if (isComplete() || isFailed())
	{
		return mStatus;
	}
	if (mState == waitOnResources)
	{
		// if we have all needed resources
		if (resourcesOutstanding().isEmpty())
		{
			// make final check
			if (!Broodwar->canMake(NULL,mUnitType))
			{
				int waitTime = UnitManager::get().unitPrereqsEta(mUnitType);
				if (waitTime>=0)
				{
					logfile << "Waiting on prereqs to finish... " << waitTime << endl;
					mStatus = TaskStatus::inactive;
				}
				else
				{
					logfile << Broodwar->getFrameCount()<<" Error in T_BuildStructure: cannot make " << mUnitType.getName()<< "\n";
					logfile << "Have " << Broodwar->self()->minerals() << " minerals, "
						<< Broodwar->self()->gas() << " gas. Budgeted:[" << mResourceBudget.minerals
						<<","<<mResourceBudget.gas<<"]\n";
					mStatus = TaskStatus::failed;
				}
				return mStatus;
			}
			else mState = orderingBuild;
		}
		else // have resources outstanding
		{
			// try to acquire any needed resources from Budget.
			ResourceSet acquired = Budget::get().reserveResources(resourcesOutstanding());
			if (!acquired.isEmpty())
			{
				ReserveResources(acquired);
			}
		}
	}
	else if (mState == orderingBuild)
	{
		if (mUnitType.whatBuilds().first.isWorker())
		{
			if (!mWorker)
			{
				mState = waitOnResources;
				mStatus = TaskStatus::inactive;
				logfile << "No worker!\n";
			}
			else if (!mWorker->getUnit()->exists())
			{ // worker was killed
				logfile << "BuildStructure, worker was killed!\n";
				mStatus = TaskStatus::failed;
				mWorker.reset();
			}
			else
			{
				// check if structure at location
				if (isUnitInArea(mUnitType,Position(mBuildLocation), 
					Position(BtoP(mBuildLocation.x()+mUnitType.tileWidth()), 
					BtoP(mBuildLocation.y()+mUnitType.tileHeight()))))
				{
					// mark as finished
					logfile << "T_BuildStructure found structure at buildLocation, complete\n";
					if (mMorphType == BWAPI::UnitTypes::None)
					{
						mStatus = TaskStatus::complete;
					}
					else if (isColonyMorph())
					{
						logfile << "Creep colony found at buildLocation, 2nd way, state to orderingMorph\n";
						mStatus = TaskStatus::active;
						mState = orderingMorph;
					}
				}
				else
				{
					// order worker to build if it has not been already
					if (!mWorker->isConstructing())
					{
						logfile << Broodwar->getFrameCount() << 
							" Ordered worker to construct " << mUnitType.getName() << endl;
						mWorker->buildStructure(mUnitType, mBuildLocation);
					}
				}
			}
		}
		else // if not worker-built: mutate building
		{
			logfile << mUnitType.getName() << " is not worker-built, enter state orderingMorph\n";
			mState = orderingMorph;
		}
	}
	else if (mState == orderingMorph)
	{
		// TODO:check requirements
		if (!mNonWorkerParent)
		{
			logfile << "mNonWorkerParent is null, setting inactive\n";
			mStatus = TaskStatus::inactive;
		}
		else if (mNonWorkerParent->getType() == mMorphType)
		{
			logfile << "Morph of " << mNonWorkerParent->getNameID() << " detected.\n";
			//mStatus = TaskStatus::complete;
		}
		else if (mNonWorkerParent->isFinished())
		{
			if (mMorphType != BWAPI::UnitTypes::None) // for 2-stage morphs eg sunken colony
			{
				if (!Broodwar->canMake(NULL,mMorphType))
				{
					return mStatus; // wait until prereqs met
				}
				bool r = mNonWorkerParent->getUnit()->morph(mMorphType);
				if(!r) logfile << "T_BuildStructure sunken morph failed.\n";
			}
			else // ex Lair
			{
				bool r = mNonWorkerParent->getUnit()->morph(mUnitType);
				if(!r) logfile << "T_BuildStructure morph failed.\n";
				else
				{
					mStatus = TaskStatus::complete;
				}
			}
		}
	}

	
	return mStatus;
}

void T_BuildStructure::activate()
{
	T_BuildUnit::activate();
	// should activate on just having resources, since worker is not added.
	// connect slots
	
	if ( isActive() )
	{	
		mState = waitOnResources;
		// first get location then get worker
		if (mUnitType.whatBuilds().first.isWorker())
		{
			if (!mBuildLocation.isValid()) // find new spot
			{
				mBuildLocation = MapInfo::get().getBuildingPlacement(mUnitType, mBuildLocationHint);
				//mBuildContract->buildLocation = mBuildLocation;
				if (mBuildLocation == BWAPI::TilePositions::None)
				{
					logfile << "Error in T_BuildStructure activate: got no build location\n";
					Broodwar->printf("Could not find build location!");
					mStatus = TaskStatus::inactive; // replan!
					return;
				} // else have valid building position, activate
				MapInfo::get().reserveBuildability(mUnitType,mBuildLocation);
			}
			if (!mWorker)
			{
				logfile << "T_BuildStructure needs to get worker\n";
				mWorker = HarvestManager::get().pullNearestWorker(Position(mBuildLocation));
				if (!mWorker)
				{
					logfile << "T_BuildStructure failed to get worker!\n";
					mStatus = TaskStatus::inactive;
					return;
				}
			} 
			mStatus = TaskStatus::active;
			logfile << "T_BuildStructure activated. Get Build location for " 
				<< mUnitType.getName() << endl;
			
		}
		else if( mUnitType.getRace() == BWAPI::Races::Zerg) // morph type
		{
			if (!mNonWorkerParent)
			{
				UnitType parentType = mUnitType.whatBuilds().first;
				assert(parentType != BWAPI::UnitTypes::None);
				mNonWorkerParent = UnitManager::get().getUnitOfType(parentType);
				if (mNonWorkerParent)
				{
					logfile << "Succeeded in getting morph target.\n";
					mStatus = TaskStatus::active;
				}
				else
				{
					logfile << "Failed at getting morph target.\n";
					mStatus = TaskStatus::inactive;
				}
			}
		}
		else
		{
			logfile << "Error! AI T_BuildStructure doesnt know how to handle this type!\n";
		}
	}
}

void T_BuildStructure::terminate()
{
	if (isFailed())
	{
		MapInfo::get().reserveBuildability(mUnitType,mBuildLocation,true);
	}
	logfile << "BuildStructure " << mUnitType.getName() << " "
		<< mBuildLocation.x() << "," << mBuildLocation.y() << " Terminated.\n";
	if (!mResourceBudget.isEmpty())
	{
		Budget::get().clearReserve(mResourceBudget);
		mResourceBudget.clear();
	}
	
	
	T_BuildUnit::terminate();
}

void T_BuildStructure::draw_buildingPlacement() const
{
	if (mBuildLocation.isValid() && mUnitType != BWAPI::UnitTypes::None)
	{
		TilePosition tl = mBuildLocation;
		TilePosition br(tl.x()+mUnitType.tileWidth(),
			tl.y()+mUnitType.tileHeight());
		using namespace BWAPIext;
		Broodwar->drawTextMap(tl.x()+5,tl.y()+10,"%s",mUnitType.getName().c_str());
		Broodwar->drawBoxMap(BtoP(tl.x()), BtoP(tl.y()), BtoP(br.x()), BtoP(br.y())
			,BWAPI::Colors::Green);
	}
}

bool T_BuildStructure::hasUnits( unitQuantity units ) const
{
	// iterate through mUnits, subtracting unit quantity.
	BOOST_FOREACH( UAptr unit, mUnits )
	{
		units[unit->getType()] -= 1;
	}
	// also account for mWorker
	if (mWorker)
	{
		units[mWorker->getType()] -= 1;
	}
	BOOST_FOREACH( unitCount uc, units )
	{
		if( uc.second > 0 ) return false;
	}
	return true;
}

void T_BuildStructure::onUnitCreate( BWAPI::Unit* unit )
{
	// check if the unit is the correct structure and set status complete.
// 	logfile << Broodwar->getFrameCount() << " T_BuildStructure received onUnitCreate signal " << 
// 		BWAPIext::getUnitNameID(unit) << "\n";
	if ( unit->getType() == mUnitType )
	{
		logfile << Broodwar->getFrameCount() << " T_BuildStructure, "<< unit->getType().getName()
			<< " signal detected, check location: " << BWAPIext::getString(unit->getTilePosition())<<
			" vs " << BWAPIext::getString(mBuildLocation) << "\n";
		if ( unit->getTilePosition() == mBuildLocation )
		{
			logfile << Broodwar->getFrameCount() << " T_BuildStructure, "<< unit->getType().getName()<<" signal detected, setting complete\n";
			mStatus = TaskStatus::complete;
		}
	}
}

void T_BuildStructure::onUnitMorph( BWAPI::Unit* unit )
{
	if ( unit->getType() == mUnitType )
	{
		logfile << Broodwar->getFrameCount() << " T_BuildStructure, "<< unit->getType().getName()
			<< " signal detected, check location: " << BWAPIext::getString(unit->getTilePosition())<<
			" vs " << BWAPIext::getString(mBuildLocation) << "\n";
		if ( unit->getTilePosition() == mBuildLocation )
		{
			if (mMorphType == BWAPI::UnitTypes::None)
			{
				logfile << Broodwar->getFrameCount() << " T_BuildStructure, "<< unit->getType().getName()<<" signal detected, setting complete\n";
				mStatus = TaskStatus::complete;
			}
			else // 2-stage mutate
			{
				logfile << "Drone morph detected, waiting on completion to mutate\n";
				if (!mNonWorkerParent)
				{
					mNonWorkerParent = UnitManager::get().getUAptr(unit);
					if (!mNonWorkerParent)
					{
						logfile << "Error, didn't get UAptr from UnitManager!\n";
						mStatus = TaskStatus::failed;
					}
					else
					{
						logfile << "mNonWorkerParent set, state to orderingMorph\n";
						mState = orderingMorph;
					}
				}
				
			}
		}
	}
	else if (mState == orderingMorph && unit->getType() == mMorphType && 
		unit->getTilePosition() == mBuildLocation)
	{
		logfile << "onMorph detected " << BWAPIext::getUnitNameID(unit) << " task complete\n";
		mStatus = TaskStatus::complete;
	}
}

BWAPI::UnitType T_BuildStructure::handleTypes( BWAPI::UnitType toBuild )
{
	if (toBuild == BWAPI::UnitTypes::Zerg_Sunken_Colony || toBuild == BWAPI::UnitTypes::Zerg_Spore_Colony)
	{
		toBuild = BWAPI::UnitTypes::Zerg_Creep_Colony;
		mUnitType = toBuild;
	}
	return toBuild;
}

bool T_BuildStructure::isColonyMorph() const
{
	if (mMorphType == BWAPI::UnitTypes::Zerg_Sunken_Colony || mMorphType == BWAPI::UnitTypes::Zerg_Spore_Colony)
	{
		return true;
	}
	return false;
}