/**
Task-BuildStructure is used to get a structure built, usually from T_BuildOrder
It will directly pull workers from HarvestManager.
*/
#pragma once

#include "T_BuildUnit.h"
#include "../common/typedefs.h"
#include "../typedef_UnitAgent.h"

class T_BuildStructure: public T_BuildUnit
{
public:
	T_BuildStructure( BWAPI::UnitType toBuild, BWAPI::TilePosition buildPos = 
		BWAPI::TilePositions::None );
	~T_BuildStructure();
	TaskStatus::Enum update();
	void activate();
	void terminate();

	bool hasUnits( unitQuantity units ) const;
	// called by EventManager, also registered for morph when race is zerg
	void onUnitCreate( BWAPI::Unit* unit );
	void onUnitMorph( BWAPI::Unit* unit );

	
	// debug
	void draw_buildingPlacement() const;
private:
	WAptr mWorker;
	UAptr mNonWorkerParent;
	BWAPI::TilePosition mBuildLocation;
	BWAPI::TilePosition mBuildLocationHint;
	BWAPI::UnitType mMorphType; // for Sunken/Spore
	
	//pCBU mBuildContract; // for use with worker
	
	BWAPI::UnitType handleTypes( BWAPI::UnitType toBuild );
	bool isColonyMorph() const;
};

typedef boost::shared_ptr<T_BuildStructure> pTBS;