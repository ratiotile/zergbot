#include "T_BuildUnit.h"
#include "../common/cpp_includes.h"
#include "../Budget.h"
#include "../Blackboard.h"
#include "C_BuildUnit.h"
#include "../UnitAgent.h"
#include "../UnitManager.h"

using namespace BBS;

T_BuildUnit::T_BuildUnit( BWAPI::UnitType buildType, 
						 BWAPI::Position buildPos )
:CompositeTask("BuildUnit")
,mUnitType(buildType)
,mState(waitOnResources)
,mTries(0)
{
	mPosition = buildPos;
	mResourcesRequired.gas = buildType.gasPrice();
	mResourcesRequired.minerals = buildType.mineralPrice();
	mResourcesRequired.supply = buildType.supplyRequired();

	if (buildType.isRefinery())
	{
		mResourcesRequired.units[Broodwar->self()->getRace().getWorker()] = 1;
	}
	else
	{
		assert(buildType.whatBuilds().second > 0); // assume unit can be built!
		mResourcesRequired.units[buildType.whatBuilds().first] = buildType.whatBuilds().second;
	}
	BBS::BB.ES.onNewUnitOrder(mUnitType);
	if (mUnitType.isTwoUnitsInOneEgg())
	{
		BBS::BB.ES.onNewUnitOrder(mUnitType);
		logfile << "Task created for double egg unit. " << mUnitType.getName()<<
			"num:" << BBS::BB.ES.getUnitsOrdered(mUnitType)<<"\n";
	}
	logfile << "T_BuildUnit created: [" << buildType.getName()
		<< "] created, need m:" << mResourcesRequired.minerals << " g:"
		<< mResourcesRequired.gas << " s:" << mResourcesRequired.supply <<
		" U:" << mResourcesRequired.getUnitsStr() << endl;
}

T_BuildUnit::~T_BuildUnit()
{

}

TaskStatus::Enum T_BuildUnit::update()
{
	activateIfInactive();

	if (isComplete()) return TaskStatus::complete;
	else if (isFailed() ) return TaskStatus::failed;

	// if active, we have resources
	if (mStatus == TaskStatus::active)
	{
		using namespace BWAPI::Races;
		logfile << "T_BuildUnit has resources, ready to build!\n";
		if (mState == orderingBuild)
		{
		}
		if (mState == orderingMorph)
		{
			mProducer = (*mUnits.begin());
			if (!mProducer->getUnit()->isMorphing())
			{
				bool r = mProducer->getUnit()->morph(mUnitType);
				if (r)
				{
					logfile << Broodwar->getFrameCount() << "Successfully ordered " << mProducer->getNameID() << " to morph into "
						<< mUnitType.getName() << " wait for confirmation\n";
					mState = morphConfirm; 
				}
				else logfile << "could not order unit " << mProducer->getNameID() << " to morph into "
					<< mUnitType.getName() << "\n";
				mTries++;
				if (mTries > 5)
				{
					logfile << "Exceeded 5 tries, failed.\n";
					mStatus = TaskStatus::failed;
				}
			}
		}
		if (mState == morphConfirm)
		{
			logfile << Broodwar->getFrameCount() << " Waiting on morph confirmation: " << mUnitType.getName() 
				<<endl;
		}
	}

	return mStatus;
}

void T_BuildUnit::activate()
{
	// check if resources have been budgeted
	mState = waitOnResources;
	if (haveRequiredResources())
	{
		mStatus = TaskStatus::active;
		mState = orderingBuild;
		if(mUnitType.getRace() == Zerg)
		{
			mState = orderingMorph;
			if (!mOnUnitMorphConnection.connected())
			{
				logfile << Broodwar->getFrameCount() << " Connecting onUnitMorph, for " << mUnitType.getName() << endl;
				mOnUnitMorphConnection = EventManager::get().addSignalOnUnitMorph(
					boost::bind(&T_BuildUnit::onUnitMorph, this, _1));
			}
		}
	}
	else // try to acquire resources
	{
		if (resourcesOutstanding().supply > Budget::get().supplyAvailable() && 
			mUnitType.supplyProvided() <= 0)
		{
			return;
		}
		ResourceSet acquired = Budget::get().reserveResources(resourcesOutstanding());
		if (!acquired.isEmpty())
		{
			ReserveResources(acquired);
		}
		// try to acquire needed units as well.
		unitQuantity unitsNeeded = resourcesOutstanding().units;
		if (!unitsNeeded.empty())
		{
			BOOST_FOREACH( unitCount uq, unitsNeeded )
			{
				if(uq.second < 1) continue;
				UAset matchType;
				BOOST_FOREACH( UAptr unit, UnitManager::get().getFreeUnits())
				{
					if (unit->getType() == uq.first)
					{
						matchType.insert(unit);
					}
				}
				int numToGrab = std::min((int)matchType.size(),uq.second);
				UAset::iterator matchesIter = matchType.begin();
				for ( int i = 0; i < numToGrab; i++)
				{
					UAptr targetUnit = *matchesIter;
					if (UnitManager::get().takeUnit(targetUnit))
					{
						logfile <<Broodwar->getFrameCount()<< " successfully grabbed unit "
							<< targetUnit->getNameID() << " for task "
							<< getName() << endl;
						addUnit(targetUnit);
					}
					else 
					{
						logfile << Broodwar->getFrameCount() << " Warning: could not grab unit "
							<< targetUnit->getNameID() << " for task.\n";
					}
					matchesIter++;
				}
			}
		}
		return;
	}
	UnitType factory = mUnitType.whatBuilds().first;
	if (!Broodwar->canMake(NULL,mUnitType))
	{
		logfile << "Error in BuildUnit: cannot make "<< mUnitType.getName() <<"\n";
		logfile << "Have " << Broodwar->self()->minerals() << " minerals, "
			<< Broodwar->self()->gas() << " gas\n";
		mStatus = TaskStatus::failed;
		if (BBS::BB.ES.getUnitTotal(factory) > 0)
		{
			logfile << "Have got " << factory.getName();
			logfile << " (" << BBS::BB.ES.getUnitsOrdered(factory) << ","
				<< BBS::BB.ES.getUnderConstruction(factory) << ","
				<< BBS::BB.ES.getUnitCount(factory) << ")\n";
			mStatus = TaskStatus::inactive;
		}
		return;
	}

	if ( factory == BWAPI::UnitTypes::None )
	{
		logfile << "Error in BuildUnit: cannot build, no " << factory.getName() <<"!\n";
		mStatus = TaskStatus::failed;
		return;
	}
	logfile << "have resources, Task " <<  getName()
		<< " activated for "<<mUnitType.getName()<<endl;
	mStatus = TaskStatus::active;
}

void T_BuildUnit::terminate()
{
	if (!isComplete())
	{
		logfile << Broodwar->getFrameCount() << " BuildUnit " << mUnitType.getName() 
			<< "terminated, not complete.\n";
		BBS::BB.ES.onUnitOrderCancel(mUnitType);
		if (mUnitType.isTwoUnitsInOneEgg())
		{
			BBS::BB.ES.onUnitOrderCancel(mUnitType);
		}
	}
	if (isFailed())
	{
		logfile << Broodwar->getFrameCount() << " BuildUnit " << mUnitType.getName() 
			<< "failed.\n";
	}
	logfile << Broodwar->getFrameCount() << " BuildUnit " << mUnitType.getName() << " terminate, complete.\n";
	if (mState != unitConfirmed) logfile << "Unit unconfirmed!!\n";
	// reset the Budget if we didn't spend it.
	if (!mResourceBudget.isEmpty())
	{
		Budget::get().clearReserve(mResourceBudget);
		mResourceBudget.clear();
	}
	// if we got units, send back to UnitManager
	for (set<UAptr>::iterator iter = mUnits.begin(); iter != mUnits.end();)
	{
		UnitManager::get().giveUnit(*iter);
		set<UAptr>::iterator toErase = iter;
		++iter;
		mUnits.erase(toErase);
	}
	if (mOnUnitCreateConnection.connected()) mOnUnitCreateConnection.disconnect();
	if (mOnUnitMorphConnection.connected()) mOnUnitMorphConnection.disconnect();
	if (mOnUnitDestroyConnection.connected()) mOnUnitDestroyConnection.disconnect();
	Task::terminate();
}

void T_BuildUnit::updateResources()
{

}

std::string T_BuildUnit::toString() const
{
	ostringstream oss;
	oss << "BuildUnit " << mUnitType.getName();
	oss << " B: " << mResourceBudget.minerals << "," << mResourceBudget.gas
		<<"," << mResourceBudget.supply<<" R: "<< mResourcesRequired.minerals
		<<","<< mResourcesRequired.gas<<","<<mResourcesRequired.supply
		<< " " << getStateName();
	return oss.str();
}

int T_BuildUnit::displayTaskInfo( int x, int y, int s ) const
{
	Broodwar->drawTextScreen(x,y,"%s",toString().c_str());
	y += s;
	return y;
}

void T_BuildUnit::onUnitMorph( BWAPI::Unit* unit )
{
	if (mState == morphConfirm && unit->getType() == Zerg_Egg
		&& unit->getBuildType() == mUnitType)
	{
		// check that larva had mutated, would be mProducer
		if (mProducer->getBuildType() == mUnitType)
		{
			//TODO: time out instead of waiting forever.
			mStatus = TaskStatus::complete;
			Budget::get().clearReserve(mResourceBudget);
			mResourceBudget.clear();
			removeUnit(mProducer);
			logfile << Broodwar->getFrameCount() << "OnUnitMorph confirmed, " 
				<< BWAPIext::getUnitNameID(unit) << endl;
			//logWriteBuffer();
			mState = unitConfirmed;
		}
	}
}

string T_BuildUnit::getStateName() const
{
	switch (mState)
	{
	case waitOnResources:
		return "WaitRes";
	case orderingBuild:
		return "OrderBuild";
	case orderingMorph:
		return "OrderMutate";
	case morphConfirm:
		return "MorphConfirm";
	case unitConfirmed:
		return "UnitConfirmed";
	}
	return "Unknown";
}