/** Task-BuildUnit is designed to be created by EconManager to fulfill a
Contract-BuildUnit.
Pre: check for factory, canBuild(tech etc)
1. get Budget to reserve enough resources, wait
2. Wait until factory is ready
3. Order build, wait until factory is training
4. cancel reserve, done
Build task is inactive while waiting on resources/builder units
*/
#pragma once
#include "CompositeTask.h"
#include <BWAPI.h>

class C_BuildUnit;
typedef boost::shared_ptr<C_BuildUnit> pCBU;

class T_BuildUnit: public CompositeTask
{
public:
	T_BuildUnit(BWAPI::UnitType buildType, 
		BWAPI::Position buildPos = BWAPI::Positions::None);
	~T_BuildUnit();
	virtual TaskStatus::Enum update();
	virtual void activate();
	virtual void terminate();

	BWAPI::UnitType getBuildType() const {return mUnitType;}
	virtual std::string toString() const;
	int displayTaskInfo(int x, int y, int s) const;
	virtual void onUnitMorph( BWAPI::Unit* unit );
	std::string getStateName() const;
	enum states
	{
		waitOnResources, // including units
		orderingBuild, 
		orderingMorph,	// for sunken/spore
		morphConfirm,	// ordered, wait for confirmation before complete
		unitConfirmed
	};
protected:
	int mTries;
	BWAPI::UnitType mUnitType;
	states mState;
	/// The unit that does the building/training/morphing
	UAptr mProducer;
	boost::signals2::connection mOnUnitCreateConnection;
	boost::signals2::connection mOnUnitMorphConnection;
	boost::signals2::connection mOnUnitDestroyConnection;
	/// update resourcesRequired
	void updateResources();
};

typedef boost::shared_ptr<T_BuildUnit> pTBU;