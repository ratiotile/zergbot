// /** Task for UnitManger that receives a location and a buildingType to
// construct. It must oversee a worker to the location and clear out all
// other units, then succeeds when construction begins.
// */
// #pragma once
// #include "CompositeTask.h"
// #include <BWAPI.h>
// 
// class C_BuildUnit;
// typedef boost::shared_ptr<C_BuildUnit> pCBU;
// 
// class T_ConstructWithWorker: public CompositeTask
// {
// public:
// 	T_ConstructWithWorker(pCBU buildContract);
// 	~T_ConstructWithWorker();
// 	 TaskStatus::Enum update();
// 	 void activate();
// 	 void terminate();
// protected:
// 	pCBU mMyContract;
// 	/// sets status, tells Worker to build.
// 	void attemptBuild();
// };
// 
// typedef boost::shared_ptr<T_ConstructWithWorker> pTCWW;