#include "T_OverlordScout.h"
#include "../common/cpp_includes.h"
#include "../MapInfo.h"
#include "../UnitManager.h"

const string T_OverlordScout::name = "OverlordScout";
T_OverlordScout::T_OverlordScout():
CompositeTask(name),
mOverlordTarget(BWAPI::TilePositions::None),
mDroneTarget(BWAPI::TilePositions::None)
{
}

T_OverlordScout::~T_OverlordScout()
{

}

TaskStatus::Enum T_OverlordScout::update()
{
	activateIfInactive();
	if (mStartPositions.size() == 1)
	{
		return on2PlayerMap();
	}
	else if (mStartPositions.size() > 1)
	{
		return onLargeMap();
	}

	logfile << "T_OverlordScout error: number of start locations\n";
	
	mStatus = TaskStatus::failed;
	return mStatus;
}

void T_OverlordScout::activate()
{
	Broodwar->printf("OverlordScout task started");
	logfile << "OverlordScout task started\n";
	mStatus = TaskStatus::active;
	// populate startPositions
	if (MapInfo::get().getEnemyStartLocation().isValid())
	{ // no need to look
		mStartPositions.insert(StartPos(MapInfo::get().getEnemyStartLocation()));
		logfile << "OverlordScout: 2-player map detected\n";
	}
	else // we are on 4-player map, don't know where enemy start is
	{
		BOOST_FOREACH( TilePosition startpos, Broodwar->getStartLocations() )
		{
			if (startpos != Broodwar->self()->getStartLocation())
			{
				StartPos tempPos(startpos);
				tempPos.groundDist = MapInfo::get().getRegionPathDistance(
					BWAPIext::getCenter(Broodwar->self()->getStartLocation()),
					BWAPIext::getCenter(startpos));
				mStartPositions.insert(tempPos);
			}
		}
	}
	
}

void T_OverlordScout::terminate()
{
	if (mOverlord)
	{
		UnitManager::get().giveUnit(mOverlord);
	}
}

TaskStatus::Enum T_OverlordScout::on2PlayerMap()
{
	if( !mStartPositions.begin()->scoutUnit )
	{
		// grab scout overlord
		mOverlord = UnitManager::get().getUnitOfType(BWAPI::UnitTypes::Zerg_Overlord);
		if (!mOverlord)
		{
			logfile << "OverlordScout: no overlord in UnitManager! failing task\n";
			mStatus = TaskStatus::failed;
			return mStatus;
		}
		else
		{
			mStartPositions.begin()->scoutUnit = mOverlord;
			mOverlord->move(BWAPIext::getCenter(mStartPositions.begin()->tile));
			logfile << "T_OverlordScout, sending overlord to enemy start.\n";
			return mStatus;
		}
	}
	else
	{
		if (Broodwar->isExplored(mStartPositions.begin()->tile))
		{
			mOverlord->move(BWAPIext::getCenter(Broodwar->self()->getStartLocation()));
			mStatus = TaskStatus::complete;
		}
		// update unitAgents
		mStartPositions.begin()->scoutUnit->processActions();
		//mStatus = TaskStatus::complete;
		return mStatus;
	}
}

TaskStatus::Enum T_OverlordScout::onLargeMap()
{
	// grab an overlord
	if (!mOverlord)
	{
		mOverlord = UnitManager::get().getUnitOfType(BWAPI::UnitTypes::Zerg_Overlord);
		if (!mOverlord)
		{
			logfile << "OverlordScout: no overlord in UnitManager! failing task\n";
			mStatus = TaskStatus::failed;
			return mStatus;
		}
		else
		{
			// find shortest air-distance unexplored StartPos
			set<StartPos>::iterator iTargetPos = mStartPositions.begin();
			for (set<StartPos>::iterator iter = mStartPositions.begin(); 
				iter != mStartPositions.end(); ++iter)
			{
				if (Broodwar->isExplored(iTargetPos->tile) ||
					(iTargetPos->airDist > iter->airDist &&
					Broodwar->isExplored(iter->tile)) )
				{
					iTargetPos = iter;
				}
			}
			// iTargetPos holds the nearest unexplored tile.
			mOverlordTarget = iTargetPos->tile;
			mOverlord->move(BWAPIext::getCenter(mOverlordTarget));
			return mStatus;
		}
	}
	// wait for proper time to grab a drone.
	// try getting it when Pool finishes

	return mStatus;
}