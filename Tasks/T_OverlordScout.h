/************************************************************************/
/* Controls scouts to find enemy from game start, until his main		
/* discovered. Use starting overlord, and grab a drone if 4player map.
/* Send ovie to nearest base, and drone to next nearest.
/************************************************************************/
#pragma once
#include "CompositeTask.h"
#include <BWAPI.h>
#include "typedef_Task.h"
#include "../common/BWAPIext.h"
#include <set>

struct StartPos
{
	StartPos( BWAPI::TilePosition pos )
		: tile(pos)
		, airDist(BWAPIext::getCenter(pos).getApproxDistance(
		BWAPIext::getCenter(BWAPI::Broodwar->self()->getStartLocation())))
		, groundDist(0) 
		, enemy(false)
	{}
	BWAPI::TilePosition tile; // BWAPI start location
	int groundDist;	// region-path approximation from our start
	int airDist;	// straight-line distance from our start
	UAptr scoutUnit; // set Overlord or Drone to scout
	bool enemy; // is enemy there?
	bool operator <(const StartPos& other) const { return this->tile < other.tile;}
};

class T_OverlordScout: public CompositeTask
{
public:
	T_OverlordScout();
	~T_OverlordScout();
	TaskStatus::Enum update();
	void activate();
	void terminate();
	const static std::string name;
private:
	std::set<StartPos> mStartPositions;
	TaskStatus::Enum on2PlayerMap(); // don't send scouting drone.
	TaskStatus::Enum onLargeMap();
	// these only used for 3+ player maps.
	UAptr mDroneScout;
	UAptr mOverlord;   
	BWAPI::TilePosition mOverlordTarget;
	BWAPI::TilePosition mDroneTarget;
};