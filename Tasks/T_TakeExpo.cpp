#include "T_TakeExpo.h"
#include "../common/cpp_includes.h"
#include "../baseLocInfo.h"
#include "../MapInfo.h"
#include "../RegionInfo.h"
#include "C_BuildUnit.h"
#include "T_BuildStructure.h"

const std::string T_TakeExpo::name = "TakeExpo";

T_TakeExpo::T_TakeExpo()
:CompositeTask(name)
,mExpoLocation(NULL)
{

}

T_TakeExpo::T_TakeExpo( baseLocInfo* base )
:CompositeTask(name)
,mExpoLocation(base)
{
	logfile << "TakeExpo task created for specific baseloc!\n";
}
T_TakeExpo::~T_TakeExpo()
{

}

TaskStatus::Enum T_TakeExpo::update()
{
	activateIfInactive();
	drawExpoSite();
	mStatus = processSubTasks();
	updateResources();
	if(mStatus == TaskStatus::inactive) mStatus = TaskStatus::active;
	if (isFailed())
	{
		// reset possession of baseLoc
		mExpoLocation->controller = NULL;
	}
	if (isComplete())
	{
		logfile << "TakeExpo task complete.\n";
	}
	return mStatus;
}

void T_TakeExpo::activate()
{
	mStatus = TaskStatus::active;
	if (mExpoLocation == NULL)
	{
		// find expo location
		list<baseLocInfo*> potentialExpos( MapInfo::get().baseLocs().begin(),
			MapInfo::get().baseLocs().end() );
		// remove all occupied positions, and unconnected/islands for now
		list<baseLocInfo*>::iterator pei = potentialExpos.begin();
		while (pei != potentialExpos.end())
		{
			if ((*pei)->controller || (*pei)->region->isIsland()
				|| !MapInfo::get().isConnected( WalkTile((*pei)->depotPosition()), 
				WalkTile(Broodwar->self()->getStartLocation()) ) )
			{
				pei = potentialExpos.erase(pei);
			}
			else ++pei;
		}
		// now select the closest one, use startlocation for now
		int closestDist = -1;
		BOOST_FOREACH( baseLocInfo* bloc, potentialExpos )
		{

			int dist = Position(Broodwar->self()->getStartLocation()).getApproxDistance(bloc->center());
			if (dist < closestDist || closestDist == -1)
			{
				closestDist = dist;
				mExpoLocation = bloc;
			}
		}
		if (!mExpoLocation)
		{
			logfile << "T_TakeExpo failed: no more expo locations remain\n";
			mStatus = TaskStatus::failed;
			return;
		}
	}
	
	// mExpoLocation should contain target expo now.
	assert(mExpoLocation);
	UnitType depotType;
	if (Broodwar->self()->getRace() == BWAPI::Races::Terran)
	{
		logfile << "T_TakeExpo creating task for Command Center\n";
		depotType = BWAPI::UnitTypes::Terran_Command_Center;
// 		boost::shared_ptr<C_BuildUnit> pbu( new C_BuildUnit(
// 			BWAPI::UnitTypes::Terran_Command_Center,mExpoLocation->depotPosition()) );
		//pTBS buildStruct( new T_BuildStructure(pbu) );
		//mSubTasks.push_back(buildStruct);
	}
	else if (Broodwar->self()->getRace() == BWAPI::Races::Zerg)
	{
		logfile << "T_Take_Expo creating task for Hatchery.\n";
		depotType = BWAPI::UnitTypes::Zerg_Hatchery;
	}
	else
		depotType = BWAPI::UnitTypes::Protoss_Nexus;
	pTBS buildTask(new T_BuildStructure(depotType, mExpoLocation->depotPosition()));
	mSubTasks.push_back(buildTask);
	// mark baseLoc as taken
	mExpoLocation->controller = Broodwar->self();
}

void T_TakeExpo::terminate()
{
	if (isFailed())
	{
		mExpoLocation->controller = NULL;
	}
}

void T_TakeExpo::drawExpoSite() const
{
	if (mExpoLocation)
	{
		Broodwar->drawTextMap(mExpoLocation->center().x()-15
			,mExpoLocation->center().y()-15,"Expo Site");
	}
}

std::string T_TakeExpo::toString() const
{
	ostringstream oss;
	oss << "T_TakeExpo ";
	oss << " B: " << mResourceBudget.minerals << "," << mResourceBudget.gas
		<<"," << mResourceBudget.supply<<" R: "<< mResourcesRequired.minerals
		<<","<< mResourcesRequired.gas<<","<<mResourcesRequired.supply;
	return oss.str();
}