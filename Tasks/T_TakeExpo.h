/**
Choose location for an expo, build nexus and then transfer workers
*/
#pragma once
#include "CompositeTask.h"
#include "typedef_Task.h"

class baseLocInfo;

class T_TakeExpo: public CompositeTask
{
public:
	T_TakeExpo();
	T_TakeExpo(baseLocInfo* base);
	~T_TakeExpo();
	TaskStatus::Enum update();
	void activate();
	void terminate();
	const static std::string name;
	std::string toString() const;
private:
	baseLocInfo* mExpoLocation;
	void drawExpoSite() const;
};

typedef boost::shared_ptr<T_TakeExpo> pTTE;