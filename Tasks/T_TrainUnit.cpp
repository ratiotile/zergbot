#include "T_TrainUnit.h"
#include "C_BuildUnit.h"
#include "../UnitAgent.h"
#include "../common/cpp_includes.h"
#include "../UnitManager.h"

T_TrainUnit::T_TrainUnit( UnitType trainType, Position trainPos )
:CompositeTask("TrainUnit")
,mTrainType(trainType)
{

}

T_TrainUnit::~T_TrainUnit()
{

}

TaskStatus::Enum T_TrainUnit::update()
{
	activateIfInactive();
	attemptTrain();
	return mStatus;
}

void T_TrainUnit::activate()
{
	//logfile << "Task TrainUnitAtFactory activated\n";
	mStatus = TaskStatus::active;
	// should task try to obtain a producing unit on activate?
	// no, wait for superior to activate.
}

void T_TrainUnit::terminate()
{

}

void T_TrainUnit::attemptTrain()
{
// 	int waitTime = UnitManager::getInstance().trainUnit(mTrainType);
// 	if (waitTime == 0) // success!
// 	{
// 		//logfile << "Task TrainUnitAtFactory training successful... ";
// 		mStatus = TaskStatus::complete;
// 		if (mMyContract)
// 		{
// 			mMyContract->status = TaskStatus::complete;
// 			//logfile << "set contract complete.\n";
// 			return;
// 		}
// 		logfile << "Error in TrainUnitAtFactory: bad contract ptr\n";
// 	}
// 	else if (waitTime < 0) 
// 	{
// 		logfile << "Warning Task TrainUnitAtFactory training failed\n";
// 		mStatus = TaskStatus::failed;
// 		if (mMyContract)
// 		{
// 			mMyContract->status = TaskStatus::failed;
// 		}
// 	}
	// else must wait until ready
}

void T_TrainUnit::updateResources()
{

}