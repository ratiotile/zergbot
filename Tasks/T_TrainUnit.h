/**
TrainUnit is created with a unit type to train and a location hint, where that unit
should be trained. It then requests the resources to train a single unit, and exits
once the unit has finished training. Provide a position to grab the nearest available 
producer.
*/
#pragma once
#include "CompositeTask.h"
#include <BWAPI.h>

class UnitAgent;
typedef boost::shared_ptr<UnitAgent> UAptr;

class C_BuildUnit;
typedef boost::shared_ptr<C_BuildUnit> pCBU;

class T_TrainUnit: public CompositeTask
{
public:
	T_TrainUnit(BWAPI::UnitType trainType, BWAPI::Position trainPos = BWAPI::Positions::None);
	~T_TrainUnit();
	TaskStatus::Enum update();
	void activate();
	void terminate();
private:
	BWAPI::UnitType mTrainType;
	UAptr mProducer;
	/// sets mStatus accordingly
	void attemptTrain();
	void updateResources();
};

typedef boost::shared_ptr<T_TrainUnit> pTTU;