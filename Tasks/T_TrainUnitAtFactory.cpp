#include "T_TrainUnitAtFactory.h"
#include "C_BuildUnit.h"
#include "../UnitAgent.h"
#include "../common/cpp_includes.h"
#include "../UnitManager.h"

T_TrainUnitAtFactory::T_TrainUnitAtFactory( pCBU buildContract )
:CompositeTask("TrainUnitAtFactory")
,mUnitType(buildContract->buildType)
,mMyContract(buildContract)
{

}

T_TrainUnitAtFactory::~T_TrainUnitAtFactory()
{

}

TaskStatus::Enum T_TrainUnitAtFactory::update()
{
	activateIfInactive();
	attemptTrain();
	return mStatus;
}

void T_TrainUnitAtFactory::activate()
{
	//logfile << "Task TrainUnitAtFactory activated\n";
	mStatus = TaskStatus::active;
}

void T_TrainUnitAtFactory::terminate()
{

}

void T_TrainUnitAtFactory::attemptTrain()
{
	int waitTime = UnitManager::get().trainUnit(mUnitType);
	if (waitTime == 0) // success!
	{
		//logfile << "Task TrainUnitAtFactory training successful... ";
		mStatus = TaskStatus::complete;
		if (mMyContract)
		{
			mMyContract->status = TaskStatus::complete;
			//logfile << "set contract complete.\n";
			return;
		}
		logfile << "Error in TrainUnitAtFactory: bad contract ptr\n";
	}
	else if (waitTime < 0) 
	{
		logfile << "Warning Task TrainUnitAtFactory training failed\n";
		mStatus = TaskStatus::failed;
		if (mMyContract)
		{
			mMyContract->status = TaskStatus::failed;
		}
	}
	// else must wait until ready
}