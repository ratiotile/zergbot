/**
Task-TrainUnitAtFactory is created by UnitManager to handle the training 
of the contracted unit at a selected factory.
*/
#pragma once
#include "CompositeTask.h"
#include <BWAPI.h>

class UnitAgent;
typedef boost::shared_ptr<UnitAgent> UAptr;

class C_BuildUnit;
typedef boost::shared_ptr<C_BuildUnit> pCBU;

class T_TrainUnitAtFactory: public CompositeTask
{
public:
	T_TrainUnitAtFactory(pCBU buildContract);
	~T_TrainUnitAtFactory();
	TaskStatus::Enum update();
	void activate();
	void terminate();
private:
	BWAPI::UnitType mUnitType;
	UAptr mFactory;
	pCBU mMyContract;
	/// sets mStatus accordingly
	void attemptTrain();
};

typedef boost::shared_ptr<T_TrainUnitAtFactory> pTTUAF;