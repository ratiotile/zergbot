#include "T_ZerglingRush.h"
#include "../common/cpp_includes.h"
#include "T_BuildOrder.h"
#include "../BuildPlanner/Openings.h"
#include "../Blackboard.h"
#include "../Budget.h"
#include "T_BuildUnit.h"
#include <boost/bind.hpp>
#include "../EventManager.h"
#include "../UnitManager.h"
#include "../MapInfo.h"
#include "../BuildQueue.h"
#include "../HarvestManager.h"

using namespace BWAPI::UnitTypes;
using namespace BBS;

T_ZerglingRush::T_ZerglingRush()
:CompositeTask("ZerglingRush")
,mUnitGrabDelayFrame(0)
,mLingsAvailableInUnitManager(0)
{
	mOnUnitCreateConnection = EventManager::get().addSignalOnUnitCreate(boost::bind(
		&T_ZerglingRush::onUnitCreate, this, _1));
	mOnUnitMorphConnection = EventManager::get().addSignalOnUnitMorph(boost::bind(
		&T_ZerglingRush::onUnitCreate, this, _1));
}

T_ZerglingRush::~T_ZerglingRush()
{

}

TaskStatus::Enum T_ZerglingRush::update()
{
	activateIfInactive();
	grabLings();
	processSubTasks();
	mLings.update();
	zerglingPatrol();
// 	if (mLings.getUnitCount() < 10 ) zerglingPatrol();
// 	else zerglingAttack();
	if (GameState::get().getMyCombatStrength() > .8 * GameState::get().getEnemyCombatStrength() )
	{
		power();
	}
	else zerglingPump();
	if (BB.ES.getUnitTotal(Zerg_Drone) < 15)
	{
		BB.BQ.autoBuildWorkers = true;
	}
	else BB.BQ.autoBuildWorkers = false;
	
	return mStatus;
}

void T_ZerglingRush::activate()
{
	chooseBuildOrder();
	mStatus = TaskStatus::active;
	BB.BQ.autoBuildWorkers = true;
}

void T_ZerglingRush::terminate()
{
	removeAllSubTasks();
	if (mOnUnitCreateConnection.connected()) mOnUnitCreateConnection.disconnect();
	if (mOnUnitMorphConnection.connected()) mOnUnitMorphConnection.disconnect();
}

void T_ZerglingRush::chooseBuildOrder()
{
	//addSubTask(pTask(new T_BuildOrder(Openings::get().getBuildOrder("2HatchMuta"))));
	BBS::BB.BQ.pushBack(Openings::get().getBuildOrder("Overpool"));
}

void T_ZerglingRush::zerglingPump()
{
	static int frameCount = 0;
	const static int interval = 2;
	frameCount++;
	if (frameCount > interval)
	{
		frameCount = 0;
	}
	else
	{
		//return;
	}
	if (this->mSubTasks.size() > 0)
	{
		return;
	}
	if (BB.ES.getUnitCount(Zerg_Larva) > 0)
	{
		if (Budget::get().supplyAvailable() > 0 &&
			Broodwar->canMake(NULL,Zerg_Zergling) &&
			Budget::get().mineralsAvailable() >= 50 &&
			BB.BQ.getNumQueued(Zerg_Zergling) < 2 &&
			BB.BQ.getNumTasked(Zerg_Zergling) < 2 )
		{
			BB.BQ.autoBuildWorkers = false;
			BB.BQ.pushBack(Zerg_Zergling);
		}
// 		int supply = BB.ES.getUnitTotal(Zerg_Overlord)*16+2;
// 		if (supply - Broodwar->self()->supplyUsed() < 1
// 			)
// 		{
// 			Broodwar->printf("%d calculated supply, %d used, %d overlords, %d bw supply"
// 				,supply,Broodwar->self()->supplyUsed(),BB.ES.getUnitTotal(Zerg_Overlord),
// 				Broodwar->self()->supplyTotal());
// 			addSubTask(pTask(new T_BuildUnit(Zerg_Overlord)));
// 		}
	} // macro hatch
	else if (Budget::get().mineralsAvailable() > 300 && BB.ES.getUnitCount(Zerg_Larva) == 0
		&& BB.BQ.getNumQueued(Zerg_Hatchery) == 0 && BB.BQ.getNumTasked(Zerg_Hatchery) == 0
		&& BB.ES.getUnitsOrdered(Zerg_Hatchery) == 0 )
	{
		BB.BQ.pushFront(Zerg_Hatchery);
	}
}

void T_ZerglingRush::onUnitCreate( BWAPI::Unit* unit )
{
	if (unit->getType() != Zerg_Zergling)
	{
		return;
	}
	mLingsAvailableInUnitManager++;
	if (mUnitGrabDelayFrame == 0)
	{
		mUnitGrabDelayFrame++;
	}
}

void T_ZerglingRush::grabLings()
{
	if (mLingsAvailableInUnitManager <= 0) return;
	// wait for lings to be available, and only run when the delay is over.
	if (mUnitGrabDelayFrame > 0)
	{
		mUnitGrabDelayFrame--;
		return;
	}
	for (int i = 0; i < mLingsAvailableInUnitManager; i++)
	{
		UAptr ling = UnitManager::get().getUnitOfType(Zerg_Zergling);
		if (ling)
		{
			// add to internal squad.
			mLings.addUnit(ling);
		}
	}
	mLingsAvailableInUnitManager = 0;
}

void T_ZerglingRush::zerglingPatrol()
{
	mLings.setBehavior(UnitBehaviors::Defend);
	mLings.setTargetPosition(Position(Broodwar->self()->getStartLocation()));
	if (BBS::BB.rallyPoint.isValid())
	{
		mLings.setTargetPosition(BBS::BB.rallyPoint);
	}
}

void T_ZerglingRush::zerglingAttack()
{
	mLings.setBehavior(UnitBehaviors::Rush);
	mLings.setTargetPosition(Position(MapInfo::get().getEnemyStartLocation()));
}

void T_ZerglingRush::power()
{
	int exposRemaining = MapInfo::get().remainingExpoCount();
	bool isSaturated = HarvestManager::get().areAllBasesSaturated();
	if (BBS::BB.BQ.canBuildWorker() && !isSaturated)
	{
		BB.BQ.pushBack(Broodwar->self()->getRace().getWorker());
	}
	else if (Budget::get().mineralsAvailable() > 300
		&& BB.BQ.getNumQueued(Zerg_Hatchery) == 0 && exposRemaining > 0)
	{
		logfile << "expos remaining: " << exposRemaining << endl;
		earlyBuildItem expo;
		expo.build = Zerg_Hatchery;
		expo.action = BuildOrderActionType::expand;
		BB.BQ.pushBack(expo);
		
	}
}