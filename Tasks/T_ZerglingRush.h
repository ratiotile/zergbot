/** 
Opening strategy. Pick a build order (4Pool) and direct Unit commander to attack immediately.
Follow up with more zerglings or transition out. Task detects conditions and exits by itself.
Spawns its own successor strategy, or else Commander can determine successor.
*/
#pragma once
#include "CompositeTask.h"
#include <BWAPI.h>
#include "typedef_Task.h"
#include "../Squad.h"

class T_ZerglingRush: public CompositeTask
{
public:
	T_ZerglingRush();
	~T_ZerglingRush();
	/// takes new units if they are zerglings. has to delay 1 frame for unit to get added to UnitManager.
	void onUnitCreate( BWAPI::Unit* unit );
	TaskStatus::Enum update();
	void activate();
	void terminate();
	const static std::string name;
private:
	/// select BO and start as subtask
	void chooseBuildOrder();
	/// produce zerglings and Overlords
	void zerglingPump();
	/// patrol lings at edge of enemy base
	void zerglingPatrol();
	/// attack
	void zerglingAttack();
	/// ask UnitManager for lings available
	void grabLings();
	/// build drones and hatcheries
	void power();

	/// listen for zergling produced events, wait a frame, then request from UnitManager
	boost::signals2::connection mOnUnitCreateConnection;
	boost::signals2::connection mOnUnitMorphConnection;
	/// holds number of lings available for delayed grab.
	int mLingsAvailableInUnitManager;
	/// if 1, wait till next frame before getting ling from UnitManager.
	int mUnitGrabDelayFrame;
	Squad mLings;
};

typedef boost::shared_ptr<T_ZerglingRush> pZR;