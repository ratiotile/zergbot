#include "Task.h"
#include "../common/cpp_includes.h"
#include "../Budget.h"
#include "../UnitAgent.h"

using BWAPI::Broodwar;

Task::Task( const std::string& taskName, int priority ) :
mTaskName(taskName)
,mFrameCreated( Broodwar->getFrameCount() )
,mStatus(TaskStatus::inactive)
,mResourcesRequired()
,mResourceBudget()
,mPosition(BWAPI::Positions::None)
,mPriority(priority)
{

}

Task::~Task()
{

}

void Task::addSubTask( pTask subTask )
{
	logfile << "Error in Task addSubTask: base Task is not composite!\n";
}

void Task::activateIfInactive()
{
	if ( mStatus == TaskStatus::inactive)
	{
		activate();
	}
}

void Task::reactivateIfFailed()
{
	if ( mStatus == TaskStatus::failed )
	{
		activate();
	}
}
/// used to reserve minerals,gas,supply for task, use Controller addUnit for units.
void Task::ReserveResources( ResourceSet resources )
{
	mResourceBudget = mResourceBudget + resources;
}

ResourceSet Task::resourcesOutstanding() const
{
	return mResourcesRequired - mResourceBudget;
}

bool Task::haveRequiredResources() const
{
	// check outstanding for minerals/gas/supply
	ResourceSet outstanding = resourcesOutstanding();
	if(outstanding.minerals > 0) return false;
	if(outstanding.gas > 0) return false;
	if(outstanding.supply >0 ) return false;
	// need to check units separately
	if (!hasUnits(mResourcesRequired.units)) return false;

	return true;
}

void Task::addUnit( UAptr unit )
{
	mResourceBudget.units[unit->getType()] += 1;
	if(!mUnits.insert(unit).second)
		logfile << "Error adding unit to Task!\n";
}

bool Task::removeUnit( UAptr unit )
{
	if (mUnits.erase(unit) != 1)
	{
		//logfile << "Error removing unit from Controller!\n";
		return false;
	}
	mResourceBudget.units[unit->getType()] -= 1;
	if(mResourceBudget.units[unit->getType()] <= 0)
		mResourceBudget.units.erase(unit->getType());
	return true;
}

void Task::terminate()
{
	// any units, move to excess!
	if (!mUnits.empty())
	{
		excessUnits.insert(mUnits.begin(),mUnits.end());
		mUnits.clear();
	}
	// clear resources required
	mResourcesRequired.clear();
}

int Task::displayTaskInfo( int x, int y, int s ) const
{
	Broodwar->drawTextScreen(x,y,"Task:%s",mTaskName.c_str());
	y += s;
	return y;
}