#pragma once
/**
A task is something that is created by a higher level manager who updates it
each frame, and is able to check status and to cancel the task. Managers own
multiple tasks and updates them all each frame. Managers have knowledge of
their tasks but tasks have no knowledge of the manager.

Task interface exposing resourcesRequired, and priority.

Tasks can spawn sub-tasks that are run internally.
*/
#include "typedef_Task.h"
#include "../Controller.h"
#include "../common/ResourceSet.h"

namespace TaskStatus
{
	enum Enum{
		inactive,	// waiting on prereqs
		active,		// executing
		complete,	// finished successfully
		failed		// something went wrong
	};
}
class Task: public Controller
{
public:
	Task( const std::string& taskName, int priority = 1 );
	virtual ~Task();
	/// call each frame, returns status
	virtual TaskStatus::Enum update() = 0;
	/// plan for task
	virtual void activate() = 0;
	/// cleanup, call when ending task
	virtual void terminate();
	/// for composite tasks
	virtual void addSubTask( pTask subTask );

	const std::string& getName() const { return mTaskName; }
	virtual std::string toString() const { return mTaskName; }
	/// secondary way to poll status, should call update once per frame
	TaskStatus::Enum status() const {return mStatus;}
	bool isInactive() { return mStatus == TaskStatus::inactive;}
	bool isActive() { return mStatus == TaskStatus::active; }
	bool isComplete() { return mStatus == TaskStatus::complete; }
	bool isFailed() { return mStatus == TaskStatus::failed; }
	int priority() const {return mPriority;}

	// resource interface
	const ResourceSet& resourcesRequired() const {return mResourcesRequired;}
	const ResourceSet& resourcesBudgeted() const {return mResourceBudget;}
	void clearBudgeted() {mResourceBudget.clear();}
	ResourceSet resourcesOutstanding() const;
	/// adds resources to budget, commander must set reserve in Budget
	void ReserveResources( ResourceSet resources );
	/// any units no longer needed
	UAset excessUnits;
	BWAPI::Position getPosition() const {return mPosition;}
	// need to override Controller add/rem unit methods to update resourceBudget
	void addUnit(UAptr unit);
	bool removeUnit(UAptr unit);
	virtual int displayTaskInfo(int x, int y, int s) const;
protected:
	TaskStatus::Enum mStatus;
	std::string mTaskName;
	int mFrameCreated;
	/// How many resources we can spend
	ResourceSet mResourceBudget;
	/// How many resources we need
	ResourceSet mResourcesRequired;
	/// important position for this task
	BWAPI::Position mPosition;
	/// higher = greater priority
	int mPriority;

	/// if inactive, call activate
	void activateIfInactive();
	/// if failed, call activate to replan
	void reactivateIfFailed();
	/// update mResourcesRequired and mUnitsRequired based on current item
	virtual void updateResources() {}
	/// true if we have budgeted enough resources and have enough units to start
	bool haveRequiredResources() const;
};

typedef boost::shared_ptr<Task> pTask;