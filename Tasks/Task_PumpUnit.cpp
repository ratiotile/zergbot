#include "Task_PumpUnit.h"
#include "../common/cpp_includes.h"
#include "../Blackboard.h"
#include "C_BuildUnit.h"
#include <list>
#include "../EconManager.h"

using namespace BBS;

Task_PumpUnit::Task_PumpUnit( BWAPI::UnitType toPump )
:CompositeTask("PumpUnit")
,mUnitType(toPump)
{
	//logfile << "Created PumpUnit task for "<< mUnitType.getName() << endl;
}

Task_PumpUnit::~Task_PumpUnit()
{
}

TaskStatus::Enum Task_PumpUnit::update()
{
	activateIfInactive();
	mStatus = processContracts();
	//reactivateIfFailed();
	return mStatus;
}

void Task_PumpUnit::activate()
{
	//logfile << "Task activated. Pumping " << mUnitType.getName() << "s\n";
	UnitType factory = mUnitType.whatBuilds().first;
	if ( !BWAPIext::isFactory(factory) && !BWAPIext::isResourceDepot(factory) )
	{
		logfile << "Error in Task_PumpUnit: requires " << factory.getName() << "?\n";
		mStatus = TaskStatus::failed;
		return;
	}
	// how many of the factory type do we have?
	int n = BB.ES.getUnitCount(factory);
	//logfile << "We have " << n << " " << factory.getName() << endl;
	if (n < 1) // no factories to pump
	{
		logfile << "Error in PumpUnit: no factories to produce unit.\n";
		mStatus = TaskStatus::failed;
		return;
	}
	// add a number of build tasks equal to amount of factories
// 	for (int i = 0; i < n; ++i)
// 	{
// 		initiateBuildContract();
// 	}
	// actually dont need to do that, just keep spamming build.
	mStatus = initiateBuildContract();
}

void Task_PumpUnit::terminate()
{
	mBuildContracts.clear();
	mSubTasks.clear();
}

TaskStatus::Enum Task_PumpUnit::initiateBuildContract()
{
	//logfile << "Task_PumpUnit creating new BuildUnit contract with Econ\n";
	boost::shared_ptr<C_BuildUnit> pbu( new C_BuildUnit(mUnitType) );
	bool r = EconManager::get().addContract(pbu);
	if( !r )
	{	
		logfile << "Error in Task_PumpUnit initiateBuildContract: rejected\n";
		return TaskStatus::failed;
	}
	mBuildContracts.push_back(pbu);
	return TaskStatus::active;
}

TaskStatus::Enum Task_PumpUnit::processContracts()
{
	// look for finished contracts
	if (!mBuildContracts.empty())
	{
		list<pCBU>::iterator citer = mBuildContracts.begin();
		for (citer; citer != mBuildContracts.end(); )
		{
			if ( (*citer)->isComplete() )
			{
				//logfile << Broodwar->getFrameCount() <<" PumpUnit build contract complete.\n";
				citer = mBuildContracts.erase(citer);
			}
			else if( (*citer)->isFailed() )
			{
				logfile << "Warning in Task_PumpUnit: contract failed.";
				citer = mBuildContracts.erase(citer);
				return TaskStatus::failed;
			}
			else ++citer;
		}
	}
	else
	{
		initiateBuildContract();
	}
	return TaskStatus::active;
}