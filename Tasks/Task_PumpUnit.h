/**
Created by Commander, this task orders EconManager to continue building
specified unit from all production facilities.
*/
#include "CompositeTask.h"
#include <bwapi.h>
#include "typedef_Task.h"

class C_BuildUnit;
typedef boost::shared_ptr<C_BuildUnit> pCBU;

class Task_PumpUnit: public CompositeTask
{
public:
	Task_PumpUnit(BWAPI::UnitType toPump);
	~Task_PumpUnit();
	TaskStatus::Enum update();
	void activate();
	void terminate();
private:
	BWAPI::UnitType mUnitType;
	std::list<pCBU> mBuildContracts;
	TaskStatus::Enum initiateBuildContract();
	TaskStatus::Enum processContracts();
};