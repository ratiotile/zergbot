#pragma once

#include <boost/shared_ptr.hpp>

class Task;
typedef boost::shared_ptr<Task> pTask;