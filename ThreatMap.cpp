#include "ThreatMap.h"
#include "common/UnitInfo.h"
#include "common/cpp_includes.h"

ThreatMap::ThreatMap()
{

}
ThreatMap::ThreatMap( size_t mapW, size_t mapH ):
mapH_(mapH), mapW_(mapW)
{
	logfile << "ThreatMap Constructor\n";
	enemyVision_ = tileMatrix(mapW, vector<int>(mapH));
	airThreat_ = tileMatrix(mapW, vector<int>(mapH));
	groundThreat_ = tileMatrix(mapW, vector<int>(mapH));
	for (size_t i = 0; i < mapW; i++)
	{
		for (size_t j = 0; j < mapH; j++)
		{
			enemyVision_[i][j] = 0;
			airThreat_[i][j] = 0;
			groundThreat_[i][j] = 0;
		}
	}
	initRangeMasks();
}
void ThreatMap::initRangeMasks()
{
	logfile << "Initializing Range Masks\n";
	// 3-12, just use boxes for 1-2
	for (int range = 1; range < 14; range++)
	{	// create and zero mask
		int diameter = range*2+1;
		tileMatrix tempMask(diameter,vector<int>(diameter));
		for (int i = 0; i < diameter; i++)
		{
			for (int j = 0; j < diameter; j++)
			{	// test if distance to tile rounds to range, then set mask
				if (getDistance(TilePosition(i,j), TilePosition(range,range)) >= (double)range+0.5)
				{
					tempMask[i][j] = 0;
				} else {
					tempMask[i][j] = 1;
				}
			}
		}
		//printLog2DArray(tempMask, diameter, diameter);
		rangeMasks.insert(pair<int,tileMatrix>(range,tempMask));
	}
}

double ThreatMap::getDistance( const TilePosition& p1, const TilePosition& p2 ) const
{
	return sqrt(pow((double)p2.x() - p1.x(), 2 ) + pow((double)p2.y() - p1.y(),2 ) );
}
int ThreatMap::getThreatValue( const UnitType& rUnitType ) const
{	// just return 1 for vision
	return 1;
}
int ThreatMap::getGroundValue( const BWAPI::Unit& rU ) const
{
	return rU.getType().groundWeapon().damageAmount();
}
int ThreatMap::getGroundValue( const UnitInfo& rUI ) const
{
	return rUI.getType().groundWeapon().damageAmount();
}
int ThreatMap::getAirValue( const BWAPI::Unit& rU ) const
{
	return rU.getType().airWeapon().damageAmount();
}
int ThreatMap::getAirValue( const UnitInfo& rUI ) const
{
	return rUI.getType().airWeapon().damageAmount();
}

int ThreatMap::getThreatRange( const BWAPI::Unit& rUnit ) const
{	// need to convert pixel range to tile
	return rUnit.getPlayer()->sightRange(rUnit.getType())/32;
}
int ThreatMap::getGroundRange( const BWAPI::Unit& rUnit ) const
{
	int range = 1 + rUnit.getPlayer()->groundWeaponMaxRange(rUnit.getType())/32;
	return range;
}
int ThreatMap::getAirRange( const BWAPI::Unit& rUnit ) const
{
	return 1 + rUnit.getPlayer()->airWeaponMaxRange(rUnit.getType())/32;
}

void ThreatMap::applyThreatValue(tileMatrix& mThreat, const TilePosition& pos, const int threatVal, const int threatRange )
{
	if (threatRange > 13 || threatRange < 1)
	{
		logfile << "Error: ThreatRange value not acceptable: " << threatRange << endl;
		return;
	}
	tileMatrix threatMask = rangeMasks[threatRange];
	int top, left, right, bot, oTop, oLeft, oRight, oBot;
	oTop = oLeft = oRight = oBot = 0;
	int diameter = threatMask.size();

	top = pos.y() - threatRange;
	if( top < 0 ) oTop = -top;

	left = pos.x() - threatRange;
	if( left < 0 ) oLeft = -left;

	right = pos.x() + threatRange;
	if( right > mThreat.size() ) oRight = right - mThreat.size();

	bot = pos.y() + threatRange;
	if( bot > mThreat[0].size() ) oBot = bot - mThreat[0].size();

	assert(oTop >= 0);
	assert(oLeft >= 0);
	assert(oRight >= 0);
	assert(oBot >= 0);
	int limitX = diameter - 1 - oRight;
	int limitY = diameter - 1 - oBot;
	assert(left + limitX <= mThreat.size());
	assert(top + limitY <= mThreat[0].size());
	for (int i = oLeft; i < limitX; i++)
	{
		for (int j = oTop; j < limitY; j++)
		{
			mThreat[i+pos.x()-threatRange][j+pos.y()-threatRange] += threatMask[i][j] * threatVal;
			if (mThreat[i+pos.x()-threatRange][j+pos.y()-threatRange] < 0) mThreat[i+pos.x()-threatRange][j+pos.y()-threatRange] = 0;
		}
	}
}
void ThreatMap::drawScreen()
{
	Position screenPos = Position(Broodwar->getScreenPosition());
	if (screenPos == BWAPI::Positions::Unknown || screenPos == BWAPI::Positions::Invalid)
	{
		for (size_t i = 0; i < enemyVision_.size(); i++)
		{
			for (size_t j = 0; j < enemyVision_[i].size(); j++)
			{
				if (enemyVision_[i][j] != 0)
				{
					Broodwar->drawTextMap(i*32,j*32,"%d",enemyVision_[i][j]);
				}
			}
		}
	}
	else
	{
		TilePosition tScreenPos = TilePosition(screenPos);
		unsigned int screenW = 640/32 - 1;
		unsigned int screenH = 384/32 - 1;
		assert(screenW + tScreenPos.x() < enemyVision_.size());
		assert(screenH + tScreenPos.y() < enemyVision_[0].size());
		for (size_t i = tScreenPos.x(); i < screenW + tScreenPos.x(); i++)
		{
			for (size_t j = tScreenPos.y(); j < screenH + tScreenPos.y(); j++)
			{
				if (enemyVision_[i][j] != 0)
				{
					Broodwar->drawBoxMap(i*32,j*32,i*32+33,j*32+33,BWAPI::Colors::Grey);
					Broodwar->drawTextMap(i*32+16,j*32+4,"%d",enemyVision_[i][j]);
					Broodwar->drawTextMap(i*32+16,j*32+12,"%d",airThreat_[i][j]);
					Broodwar->drawTextMap(i*32+16,j*32+20,"%d",groundThreat_[i][j]);
				}
			}
		}
	}
}
void ThreatMap::onEnemyUnitMoved( const TilePosition& from, 
								 const TilePosition& dest, const BWAPI::Unit& rUnit )
{ // assume that the unit actually moved.
	int threatVal = getThreatValue(rUnit.getType());
	int threatRange = getThreatRange(rUnit);
	
	// subtract threatValue from former position
	applyThreatValue(enemyVision_, from, -threatVal, threatRange);
	// add threatValue to new position
	applyThreatValue(enemyVision_, dest, threatVal, threatRange);
	if (rUnit.getType().groundWeapon() != BWAPI::WeaponTypes::None)
	{
		applyThreatValue(groundThreat_, from, -getGroundValue(rUnit), 
			getGroundRange(rUnit));
		applyThreatValue(groundThreat_, dest, getGroundValue(rUnit), 
			getGroundRange(rUnit));
	}
	if (rUnit.getType().airWeapon() != BWAPI::WeaponTypes::None)
	{
		applyThreatValue(airThreat_, from, -getAirValue(rUnit), getAirRange(rUnit));
		applyThreatValue(airThreat_, dest, getAirValue(rUnit), getAirRange(rUnit));
	}
	
	//logfile <<Broodwar->getFrameCount()<< " ThreatMap: enemy moved ["<<from.x()<<","<<from.y()<<"]["<<dest.x()<<","<<dest.y()<<"] " << rUnit.getID()<<endl;
}
void ThreatMap::onEnemyUnitDiscovered( const BWAPI::Unit& rUnit )
{ // assume everything is valid? Do some minimal checking
	TilePosition pos = TilePosition(rUnit.getPosition());
	if (!pos.isValid())
	{
		logfile << "onEnemyUnitDiscovered pos is invalid\n";
		return;
	}
	int threatVal = getThreatValue(rUnit.getType());
	int threatRange = getThreatRange(rUnit);
	applyThreatValue(enemyVision_, pos, threatVal, threatRange);
	applyThreatValue(groundThreat_, pos, getGroundValue(rUnit), getGroundRange(rUnit));
	applyThreatValue(airThreat_, pos, getAirValue(rUnit), getAirRange(rUnit));
	//logfile << "ThreatMap: enemy discovered [" << pos.x()<<","<<pos.y()<<"] "<<rUnit.getID()<<endl;
}

void ThreatMap::onEnemyUnitDestroyed( const BWAPI::Unit& rUnit )
{
	int threatVal = getThreatValue(rUnit.getType());
	int threatRange = getThreatRange(rUnit);
	TilePosition tPos = TilePosition(rUnit.getPosition());

	// subtract threatValue from former position
	applyThreatValue(enemyVision_, tPos, -threatVal, threatRange);

	if (rUnit.getType().groundWeapon() != BWAPI::WeaponTypes::None)
	{
		applyThreatValue(groundThreat_, tPos, -getGroundValue(rUnit), getGroundRange(rUnit));
	}
	if (rUnit.getType().airWeapon() != BWAPI::WeaponTypes::None)
	{
		applyThreatValue(airThreat_, tPos, -getAirValue(rUnit), getAirRange(rUnit));
	}
}
void ThreatMap::update()
{
	drawScreen();
}

void ThreatMap::recalculate( const set<UIptr>& updatePopulation )
{	// first clear all threat value maps
	for ( size_t x = 0; x < mapW_; ++x )
	{
		for ( size_t y = 0; y < mapH_; ++y )
		{
			enemyVision_[x][y] = 0;
			groundThreat_[x][y] = 0;
			airThreat_[x][y] = 0;
		}
	}
	BOOST_FOREACH( UIptr pUI, updatePopulation )
	{
		TilePosition pos = TilePosition( pUI->getPosition() );
		if (!pos.isValid())
		{
			logfile << "onEnemyUnitDiscovered pos is invalid\n";
			return;
		}
		int threatVal = getThreatValue( pUI->getType() );
		int threatRange = getThreatRange( *pUI->getUnit() );
		applyThreatValue(enemyVision_, pos, threatVal, threatRange);
		applyThreatValue(groundThreat_, pos, getGroundValue(*pUI), 
			getGroundRange(*pUI->getUnit() ) );
		applyThreatValue(airThreat_, pos, getAirValue(*pUI), 
			getAirRange(*pUI->getUnit() ) );
	}
}