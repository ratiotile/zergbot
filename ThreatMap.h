#pragma once
#ifndef INCLUDED_THREATMAP_H
#define INCLUDED_THREATMAP_H

#include <vector>
#include <set>
#include <map>
#include <math.h>
#include <cassert>

#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif

#include <boost/thread.hpp>

#include "common/UnitInfo.h"

class ThreatMap
{
public:
	typedef std::vector< std::vector<int> > tileMatrix;
	ThreatMap();
	ThreatMap(size_t mapW, size_t mapH);
	/// update TM values
	void onEnemyUnitMoved(const BWAPI::TilePosition& from, 
		const BWAPI::TilePosition& dest, const BWAPI::Unit& rUnit);
	/// write TM values around unit
	void onEnemyUnitDiscovered(const BWAPI::Unit& rUnit); 
	/// erase TM values around unit
	void onEnemyUnitDestroyed(const BWAPI::Unit& rUnit); 
	/// does nothing right now but draw
	void update();
	const std::map<int, tileMatrix>& getRangeMasks() const {return rangeMasks;}
	int getVision(int x, int y) const {return enemyVision_[x][y];}
	int getGround(int x, int y) const {return groundThreat_[x][y];}
	int getAir(int x, int y) const {return airThreat_[x][y];}
	/// computes ThreatMap from scratch
	void recalculate( const std::set<UIptr>& updatePopulation );
	/// debug, draw values to screen
	void drawScreen();
private:
	size_t mapW_, mapH_;
	/// count number of units that can see tile
	tileMatrix enemyVision_;
	/// count ground damage at each tile
	tileMatrix groundThreat_;
	/// count air damage at each tile
	tileMatrix airThreat_;
	/// pre-calculated circular arrays up to range 13
	std::map<int, tileMatrix> rangeMasks;
	/// synchronize thread writes
	boost::mutex writeMutex_;

	/// call in constructor
	void initRangeMasks();
	/// returns euclidian distance between TilePositions
	double getDistance(const BWAPI::TilePosition& p1, 
		const BWAPI::TilePosition& p2) const;
	/// returns threat level of unit vision, 1 for each unit now.
	int getThreatValue(const BWAPI::UnitType& rUnitType) const; 
	/// ground damage value for unit
	int getGroundValue(const BWAPI::Unit& rU) const;
	int getGroundValue(const UnitInfo& rUI) const;
	/// air damage value for unit
	int getAirValue(const BWAPI::Unit& rU) const;
	int getAirValue(const UnitInfo& rUI) const;

	/// return sight range in tiles
	int getThreatRange(const BWAPI::Unit& rUnit) const; 
	/// returns ground range in tiles
	int getGroundRange(const BWAPI::Unit& rUnit) const;
	/// return air range in tiles
	int getAirRange(const BWAPI::Unit& rUnit) const;

	/// writes values to map.
	void applyThreatValue(tileMatrix& mThreat, 
		const BWAPI::TilePosition& pos, const int threatVal, 
		const int threatRange);
		

};

#endif

