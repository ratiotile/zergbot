#include "TimeDisplay.h"
#include "common/cpp_includes.h"

TimeDisplay::TimeDisplay():
frameTimeExceeded(false),
frameTime30kLeft(false),
frameTime20kLeft(false),
frameTime10kLeft(false),
betweenPeak(0),
peakResetFrames(120),
currentPeak(0),
currentCount(0)
{
	
}
TimeDisplay::~TimeDisplay() {}
void TimeDisplay::onFrameEnd()
{
	if(Broodwar->getFrameCount() < 3) return;
	currentCount++;
	if (currentCount > peakResetFrames)
	{
		currentCount = 0;
		currentPeak = 0;
		betweenPeak = 0;
	}
	double newTime = TimeManager::getInstance().getTime();
	double newBetween = TimeManager::getInstance().getTimeBetweenFrames();
	if (newTime > currentPeak)
	{
		currentPeak = newTime;
		currentCount = 0; // reset count
	}
	if (newBetween > betweenPeak)
	{
		betweenPeak = newBetween;
		currentCount = 0;
	}
}

void TimeDisplay::draw()
{
	int seconds = Broodwar->getFrameCount()/24;
	int minutes = seconds/60;
	seconds = seconds%60;

	BWAPI::Broodwar->drawTextScreen(550,30,"TimeLeft: %5.0f", 42000 - currentPeak);
	BWAPI::Broodwar->drawTextScreen(550,38,"Between: %5.0f", betweenPeak );
	if ( !frameTime30kLeft && currentPeak > 12000 )
	{
		frameTime30kLeft = true;
		logfile << "Frame Time Under 30k remain: time " 
			<< minutes << ":" << seconds << endl;
	}
	else if ( !frameTime20kLeft && currentPeak > 22000 )
	{
		frameTime20kLeft = true;
		logfile << "Frame Time Under 20k remain: time " 
			<< minutes << ":" << seconds << endl;
	}
	else if ( !frameTime10kLeft && currentPeak > 32000 )
	{
		frameTime10kLeft = true;
		logfile << "Frame Time Under 10k remain: time " 
			<< minutes << ":" << seconds << endl;
	}
	else if (currentPeak > 42000)
	{
		frameTimeExceeded = true;
		logfile << "Frame Time Exceeded: time " 
			<< minutes << ":" << seconds << endl;
	}

}