#ifndef INCLUDED_TIMEDISPLAY_H
#define INCLUDED_TIMEDISPLAY_H
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#include "TimeManager.h"

using namespace std;
// show how much time is taken by AI in each frame. Display Peak values only.
class TimeDisplay
{
public:
	TimeDisplay(); // only create 1 instance
	~TimeDisplay();
	void onFrameEnd(); // update value from TimeManager
	void draw(); // display to screen
private:
	unsigned int peakResetFrames;	//5*24 = 120 frames in 5 seconds
	unsigned int currentCount;	// reset on new peak
	double currentPeak;	// time
	double betweenPeak; // peak time between frames
	bool frameTimeExceeded;
	bool frameTime30kLeft;
	bool frameTime20kLeft;
	bool frameTime10kLeft;	// when under 10k time remain in frame
	/// track when the last frame ended, to detect stutter in between frames
};

#endif //INCLUDED_TIMEDISPLAY_H