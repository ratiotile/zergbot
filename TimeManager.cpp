#include "TimeManager.h"
#include "MapInfo.h"
#include "common/cpp_includes.h"
#include "TargetManager.h"

TimeManager* TimeManager::pInstance_ = NULL;

TimeManager::TimeManager() 
: timers_(std::vector<Timer>(numTypes))
, timeCurrentFrameStart(0)
, timeCurrentFrameEnd(0)
, timeLastFrameStart(0)
, timeLastFrameEnd(0)
{
	pTimerFrame_ = new Timer();
	pTimerGame_ = new Timer();
	pTimerGame_->start();
	timerNames_.push_back("StartFrame");
	timerNames_.push_back("SF_Init");
	timerNames_.push_back("SF_Terrain");
	timerNames_.push_back("TA_load");
	timerNames_.push_back("TA_process");
	timerNames_.push_back("MI_convert");
	timerNames_.push_back("MI_regionDistances");
	timerNames_.push_back("MI_BuildLocDistances");
	timerNames_.push_back("SF_MapInfo");
	timerNames_.push_back("SF_Commander");
}
TimeManager::~TimeManager() {
	delete pTimerFrame_;
}
TimeManager& TimeManager::getInstance()
{
	if (!pInstance_)
		pInstance_ = new TimeManager();
	return *pInstance_;
}


void TimeManager::onFrameStart()
{	
	pTimerFrame_->start();

	timeLastFrameStart = timeCurrentFrameStart;
	timeLastFrameEnd = timeCurrentFrameEnd;
	timeCurrentFrameStart = pTimerGame_->getElapsedTimeInMicroSec();
	//logClear();
	//logfile << "begin frame " << Broodwar->getFrameCount() << endl;
	//GameState::getInstance().Regions().update();
	MapInfo::get().update();
	TargetManager::get().onFrameBegin();
	BB.ES.onFrame();
}
void TimeManager::onFrameEnd()
{
	pTimerFrame_->stop();
	timeCurrentFrameEnd = pTimerGame_->getElapsedTimeInMicroSec();
}

double TimeManager::getTime()
{
	return pTimerFrame_->getElapsedTimeInMicroSec();
}

void TimeManager::processActions()
{ 
	UnitManager::get().update();
	//v1: process all units
	//logfile << "Timemanager processing actions\n";
	//logWriteBuffer();
	
	// UnitManager implementation has changed!

// 	list<UnitAgent*>& liUnits = UnitManager::getInstance().getActiveUnitsList();
// 	for (list<UnitAgent*>::iterator i = liUnits.begin();
// 		i != liUnits.end(); i++)
// 	{
// 		(*i)->processActions();
// 	}
}

void TimeManager::logStartFrameInfo()
{
	logfile << "Starting frame timer info:\n";
	for (int i = 0; i < numTypes; i++ )
	{
		logfile << timerNames_[i] << ":\t " << timers_[i].getElapsedTimeInMicroSec() << endl;
	}
}

double TimeManager::getTimeBetweenFrames()
{
	return timeCurrentFrameStart - timeLastFrameEnd;
}