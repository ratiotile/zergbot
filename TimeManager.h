/************************************************************************/
/* TimeManager is responsible for queuing up all processes that need	*/
/* to run. It will place high priority items first and ensure that AI	*/
/* does not go over 42ms/frame											*/
/************************************************************************/
#ifndef INCLUDED_TIMEMANAGER_H
#define INCLUDED_TIMEMANAGER_H
#include <windows.h>
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#include "timer\Timer.h"
#ifndef __UNITAGENT_H__
	#include "UnitAgent.h"
#endif

#include "UnitManager.h"

using namespace std;

// singleton
class TimeManager
{
public:
	static TimeManager& getInstance();
	enum Type{ startFrame, sfInit, sfTA, TA_load, TA_process, MI_convert, MI_RD, MI_BLD,
		sfMapInfo, sfCmdr, numTypes };
	~TimeManager();
	void onFrameStart();	// start timer
	void onFrameEnd();		// end timer
	double getTime();	// get current time on timer
	/// time uS between end of last frame and start of current frame
	double getTimeBetweenFrames();
	void processActions();	// run everything

	void startTimer(const TimeManager::Type t) { timers_[t].start(); }
	void stopTimer(const TimeManager::Type t) { timers_[t].stop(); }
	void logStartFrameInfo();
private:
	TimeManager();
	static TimeManager* pInstance_;
	Timer* pTimerFrame_;	// init in constructor
	Timer* pTimerGame_; // always keep running

	double timeCurrentFrameStart; 
	double timeCurrentFrameEnd;
	double timeLastFrameStart;
	double timeLastFrameEnd;
	
	std::vector<Timer> timers_;
	std::vector<std::string> timerNames_;
};
#endif //INCLUDED_TIMEMANAGER_H