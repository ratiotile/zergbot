#include "UnitAgent.h"
#include "Actions\Action_Think.h"
#include <boost/lexical_cast.hpp>
#include "common/cpp_includes.h"
#include "common/BWAPIext.h"
#include "TargetManager.h"
#include "Unit.h"

UAptr UnitAgent::Null;

UnitAgent::UnitAgent(BWAPI::Unit *mUnit): 
UnitClass(mUnit),
pUnit(mUnit), 
nId(pUnit->getID()),
pSuperior(NULL),
walkRect_(mUnit->getPosition(), mUnit->getType()),
PFdest_(walkRect_), 
targetUnit_(NULL),
posLastTile_(mUnit->getPosition()),
framesInTile_(0),
nameID_(BWAPIext::getUnitNameID(mUnit)),
taskPriority(0)
{
	pMovementBehaviors_ = new MovementBehaviors(this);
	pActions_ = new Action_Think( this );
	int framesRemaining = mUnit->getRemainingBuildTime();
	if (framesRemaining == 0)
	{
		isFinished_ = true;
//  		logfile << "UnitAgent Constructor for "<< pUnit->getType().getName() 
//  			<<", finished\n";

	}else{
		isFinished_ = false;
// 		logfile << "UnitAgent Constructor for "<< pUnit->getType().getName() <<", not finished, calling onNewUnit.\n";
	}
	for (int i = 0; i < 8; i++)
	{// pre-fill moveCandidates_
		moveCandidates_.push_back(walkRect_);
	}
	mOnUnitDestroyConnection = EventManager::get().addSignalOnUnitDestroyed(boost::bind(
		&UnitAgent::onUnitDestroy, this, _1));
}

UnitAgent::~UnitAgent() {
	//logfile << "In unit destructor.\n";
	logWriteBuffer();
	// need to terminate actions
	pActions_->terminate();
	if (mOnUnitDestroyConnection.connected())
	{
		mOnUnitDestroyConnection.disconnect();
	}
}
int UnitAgent::assignUnit(  BWAPI::Unit* mUnit )
{
	if (pUnit == NULL)
	{
		pUnit = mUnit;
		nId = mUnit->getID();	//update ID
		int framesRemaining = mUnit->getRemainingBuildTime();
		if (framesRemaining == 0)
		{
			isFinished_ = true;
			logfile << "UnitAgent AssignUnit, finished, calling onNewUnit.\n";
		}else{
			isFinished_ = false;
			logfile << "UnitAgent AssignUnit, not finished, calling onNewUnit.\n";
		}
		return 0;	//Success
	}
	else{
		return 1;	//fail, already has assigned unit
	}
}
void UnitAgent::orderRightClick(Position posTarget, bool queueAction){
	pUnit->rightClick(posTarget, queueAction);
}
void UnitAgent::orderRightClick( BWAPI::Unit* unitTarget, bool queueAction){
	pUnit->rightClick(unitTarget, queueAction);
}
int UnitAgent::getDistanceTo( BWAPI::Unit* unitTarget) const{
	return pUnit->getDistance(unitTarget);
}
int UnitAgent::getDistanceTo(Position posTarget) const{
	return pUnit->getDistance(posTarget);
}
 BWAPI::Unit* UnitAgent::getUnit() const{
	return pUnit;
}
void UnitAgent::processActions() {
	UnitClass::update();
	// check if morphing, see if it gets removed in time
	if (pUnit->isMorphing())
	{
		//
	}
	if (!isFinished_)
	{	// check if finished
		if (pUnit->isCompleted())
		{
			isFinished_ = true;
			logfile << getNameID() << " has finished morphing...\n";
			BB.ES.onUnitComplete(pUnit->getType());
		}
	}
	else	// finished 
	{	// realign walkRect with actual position
		if (!pUnit->exists())
		{
			logfile << "one of our dead units is trying to act!\n";
			logWriteBuffer();
		}
		if (walkRect_.getPos() != pUnit->getPosition())
		{
			walkRect_.changePosPx(pUnit->getPosition().x(),pUnit->getPosition().y());
		}
		//drawCollisionBox();
		//checkUnderAttack();
		// scanForEnemies if unit has moved a full tile from last position.
		processHasMovedTile();
		//TODO: reintegrate
		pMovementBehaviors_->processActions();
		//pActions_->processActions();

		// was move to dest, but movement behaviors handles that now
	}
}

void UnitAgent::printStatus() const {
	printBasicUnitInfo();
}
void UnitAgent::printBasicUnitInfo() const{
	if(pUnit == NULL){							//check for null pointer
		Broodwar->printf("No unit defined.");
	}
	else {										//looks like there is a unit
		//string mUnitName = pUnit->getType().getName();
		Broodwar->printf("Type: %s, ID: %i",getName().c_str(),nId);
	}
}
int UnitAgent::getID() const {
	return nId;
}
string UnitAgent::getName() const {
	return pUnit->getType().getName();
}
UnitType UnitAgent::getType() const{
	return pUnit->getType();
}
bool UnitAgent::operator==(const UnitAgent &other)const {
	if (nId == other.getID())
	{
		return true;
	} 
	else
	{
		return false;
	}
}
void UnitAgent::checkUnderAttack()
{
	if (pUnit->isUnderAttack())
	{	// draw indicator
		Broodwar->drawCircleMap(pUnit->getPosition().x(),pUnit->getPosition().y(),16,
			BWAPI::Colors::Purple);
	}
	if (!attackers_.empty())
	{
// 		BOOST_FOREACH( const UIptr b, attackers_)
// 		{
// 			if (b != targetUnit_)
// 			{
// 				Broodwar->drawLineMap(pUnit->getPosition().x(), pUnit->getPosition().y(),
// 					b->getPosition().x(), b->getPosition().y(),
// 					BWAPI::Colors::Grey);
// 			}
// 		}
	}
// 	if (state_.front() == sATTACK_TARGET)
// 	{
// 		if (targetUnit_)
// 		{
// 			if (pUnit->isInWeaponRange(targetUnit_))
// 			{
// 				Broodwar->drawLineMap(pUnit->getPosition().x(), pUnit->getPosition().y(),
// 					targetUnit_->getPosition().x(), targetUnit_->getPosition().y(),
// 					BWAPI::Colors::Red);
// 			} else {
// 				if (targetUnit_->isVisible())
// 				{
// 					Broodwar->drawLineMap(pUnit->getPosition().x(), pUnit->getPosition().y(),
// 						targetUnit_->getPosition().x(), targetUnit_->getPosition().y(),
// 						BWAPI::Colors::Orange);
// 				}
// 				else
// 				{
// 					Position targPos = GameState::getInstance().getBanditPosition(targetUnit_);
// 					Broodwar->drawLineMap(pUnit->getPosition().x(), pUnit->getPosition().y(),
// 						targPos.x(), targPos.y(),
// 						BWAPI::Colors::Orange);
// 				}
// 			}
// 			
// 		}
// 		if (pUnit->isAttacking())
// 		{	// draw indicator
// 			Broodwar->drawBoxMap(pUnit->getPosition().x()-16,pUnit->getPosition().y()-16,
// 				pUnit->getPosition().x()+16,pUnit->getPosition().y()+16,BWAPI::BWAPI::Colors::Red);
// 		} else {
// 			Broodwar->drawBoxMap(pUnit->getPosition().x()-16,pUnit->getPosition().y()-16,
// 				pUnit->getPosition().x()+16,pUnit->getPosition().y()+16,BWAPI::BWAPI::Colors::Orange);
// 		}
// 	} 
// 	if (state_.front() == sSCOUT)
// 	{
// 		Broodwar->drawBoxMap(pUnit->getPosition().x()-16,pUnit->getPosition().y()-16,
// 			pUnit->getPosition().x()+16,pUnit->getPosition().y()+16,BWAPI::BWAPI::Colors::Green);
// 	}
//	if (pUnit->isSelected())
//	{	// print orders
// 		Broodwar->drawLineMap(pUnit->getPosition().x(),pUnit->getPosition().y(),
// 			pUnit->getTargetPosition().x(),pUnit->getTargetPosition().y(),BWAPI::BWAPI::Colors::Green);
// 		Broodwar->drawTextMap(pUnit->getPosition().x()-32,pUnit->getPosition().y()-32,"%s",
// 			pUnit->getOrder().getName().c_str());
// 		Broodwar->drawTextMap(pUnit->getPosition().x()-32,pUnit->getPosition().y()-16,"%s",
// 			pUnit->getSecondaryOrder().getName().c_str());
// 		if (!destinations_.empty())
// 		{
// 			if (destinations_.front() != pUnit->getTargetPosition())
// 			{
// 				Broodwar->drawLineMap(pUnit->getPosition().x(),pUnit->getPosition().y(),
// 					pUnit->getTargetPosition().x(),pUnit->getTargetPosition().y(),BWAPI::Colors::White);
// 			} 
// 			Broodwar->drawLineMap(pUnit->getPosition().x(),pUnit->getPosition().y(),
// 				destinations_.front().x(),destinations_.front().y(),BWAPI::Colors::Green);
// 		}
// 		if (!pUnit->isMoving())
// 		{
// 			Broodwar->drawTextMap(pUnit->getPosition().x()-32,pUnit->getPosition().y()-32,
// 				"Not Moving");
// 		}
//	}
}

void UnitAgent::flee()
{ // first check for enemies 
	// if no enemies in range do simple move
}

void UnitAgent::drawAttackerLines() const
{	// draw threats
	set<UIptr>::const_iterator iAtk = threats_.begin();
	for (iAtk; iAtk != threats_.end(); iAtk++)
	{
		if ((*iAtk)->getPosition().isValid())
		{
			Broodwar->drawLineMap((*iAtk)->getPosition().x(),(*iAtk)->getPosition().y()
				,getUnit()->getPosition().x(),getUnit()->getPosition().y()
				,BWAPI::Colors::Grey);
		}
	}
	// draw attackers
	iAtk = attackers_.begin();
	for (iAtk; iAtk != attackers_.end(); iAtk++)
	{
		if ((*iAtk)->getUnit() && (*iAtk)->getUnit()->exists())
		{
			 BWAPI::Unit* target = (*iAtk)->getUnit()->getTarget();
			 BWAPI::Unit* orderTarget = (*iAtk)->getUnit()->getOrderTarget();
			if (orderTarget)
			{
				Broodwar->drawLineMap(orderTarget->getPosition().x(),orderTarget->getPosition().y()
					,(*iAtk)->getUnit()->getPosition().x(),(*iAtk)->getUnit()->getPosition().y()
					,BWAPI::Colors::Blue);
			}
			else if (target)
			{
				Broodwar->drawLineMap(target->getPosition().x(),target->getPosition().y()
					,(*iAtk)->getUnit()->getPosition().x(),(*iAtk)->getUnit()->getPosition().y()
					,BWAPI::Colors::Red);
			}
		}
	}
}

void UnitAgent::onClearAttacker(UIptr attacker)
{
// 		Report* r = new Report();
// 		r->from = Report::fUNITAGENT;
// 		r->topic = Report::tUNIT_CLEAR_ATTACKER;
// 		r->pUnit = this;
// 		r->pFrom = attacker;
// 		pSuperior->report(r);
}

void UnitAgent::processAttackers()
{ // remove is not visible and far away NO LONGER USED
// 	set<UIptr>::const_iterator iAtk = attackers_.begin();
// 	while (iAtk != attackers_.end())
// 	{
// 		int tDist = (int)(*iAtk)->getPosition().getDistance(pUnit->getPosition());
// 		if ( tDist > fleeRange*32 || !(*iAtk)->getUnit()->exists() )
// 		{
// 			onClearAttacker(*iAtk);
// 			iAtk = attackers_.erase(iAtk);
// 		} else {
// 			iAtk++;
// 		}
// 
// 	}
}

BWAPI::Position UnitAgent::getDest() const
{
	return pMovementBehaviors_->getDestination();
}

void UnitAgent::move( Position nDest )
{
	pMovementBehaviors_->move(nDest);
}
void UnitAgent::drawWalkTiles()
{
	WalkTile wtPos(pUnit->getPosition());
	// 4 walktiles across each maptile. 4x4 = 16 walktiles out to search
	// [top,left,bot,right]
	vector<int> bounds;
	int walkMapX = Broodwar->mapWidth() * 4; // convert to walk tiles
	int walkMapY = Broodwar->mapHeight() * 4;
	bounds = BuildHelper::cropToMap(walkMapX, walkMapY, wtPos.x(), wtPos.y(), 16);
	//Broodwar->drawBoxMap(bounds[1]*8, bounds[0]*8, bounds[3]*8, bounds[2]*8, BWAPI::Colors::Yellow );
	// check every tile in bounds, and draw if impassable
	for (int i = bounds[1]; i < bounds[3]; i++)
	{
		for (int j = bounds[0]; j < bounds[2]; j++)
		{
			if (!Broodwar->isWalkable(i,j))
			{
				Broodwar->drawBoxMap(i*8, j*8, (i+1)*8, (j+1)*8, BWAPI::Colors::Red);
			}
		}
	}
	//WalkRect wRect(pUnit->getPosition(), pUnit->getType());
	drawWalkRect(walkRect_, BWAPI::Colors::Teal);
	if (PFdest_.isWalkable()) drawWalkRect(PFdest_, BWAPI::Colors::Green);
	else drawWalkRect(PFdest_, BWAPI::Colors::Red);
	
	//drawCollisionBox();
	// draw surrounding rects
// 	int uHeight = pUnit->getType().dimensionUp() + pUnit->getType().dimensionDown();
// 	int uWidth = pUnit->getType().dimensionLeft() + pUnit->getType().dimensionRight();
 	findMoveLoc( "1", moveCandidates_[0], 32 );
	findMoveLoc( "2", moveCandidates_[1], 32 );
	findMoveLoc( "3", moveCandidates_[2], 32 );
	findMoveLoc( "4", moveCandidates_[3], 32 );
	findMoveLoc( "6", moveCandidates_[4], 32 );
	findMoveLoc( "7", moveCandidates_[5], 32 );
	findMoveLoc( "8", moveCandidates_[6], 32 );
	findMoveLoc( "9", moveCandidates_[7], 32 );
//  if (moveCandidates_[0].isWalkable()) drawWalkRect(moveCandidates_[0], BWAPI::Colors::Green);
//  else drawWalkRect(moveCandidates_[0], BWAPI::Colors::Red);
// 	if (moveCandidates_[1].isWalkable()) drawWalkRect(moveCandidates_[1], BWAPI::Colors::Green);
// 	else drawWalkRect(moveCandidates_[1], BWAPI::Colors::Red);
// 	if (moveCandidates_[2].isWalkable()) drawWalkRect(moveCandidates_[2], BWAPI::Colors::Green);
// 	else drawWalkRect(moveCandidates_[2], BWAPI::Colors::Red);
// 	if (moveCandidates_[3].isWalkable()) drawWalkRect(moveCandidates_[3], BWAPI::Colors::Green);
// 	else drawWalkRect(moveCandidates_[3], BWAPI::Colors::Red);
// 	if (moveCandidates_[4].isWalkable()) drawWalkRect(moveCandidates_[4], BWAPI::Colors::Green);
// 	else drawWalkRect(moveCandidates_[4], BWAPI::Colors::Red);
// 	if (moveCandidates_[5].isWalkable()) drawWalkRect(moveCandidates_[5], BWAPI::Colors::Green);
// 	else drawWalkRect(moveCandidates_[5], BWAPI::Colors::Red);
// 	if (moveCandidates_[6].isWalkable()) drawWalkRect(moveCandidates_[6], BWAPI::Colors::Green);
// 	else drawWalkRect(moveCandidates_[6], BWAPI::Colors::Red);
// 	if (moveCandidates_[7].isWalkable()) drawWalkRect(moveCandidates_[7], BWAPI::Colors::Green);
// 	else drawWalkRect(moveCandidates_[7], BWAPI::Colors::Red);
}
void UnitAgent::drawCollisionBox()
{
	Broodwar->drawBoxMap(pUnit->getPosition().x() - pUnit->getType().dimensionLeft(), 
		pUnit->getPosition().y() - pUnit->getType().dimensionUp(),
		pUnit->getPosition().x() + pUnit->getType().dimensionRight(),
		pUnit->getPosition().y() + pUnit->getType().dimensionDown(), BWAPI::Colors::Cyan);
}

void UnitAgent::PFmove( const string& direction )
{
	Broodwar->printf("PFmove direction: %s", direction.c_str());
	WalkRect moveDest(pUnit->getPosition(), pUnit->getType());
	// dest candidates are already found
	if (direction == "1"){
		PFdest_ = moveCandidates_[0];
	} else if (direction == "2"){
		PFdest_ = moveCandidates_[1];
	} else if (direction == "3"){
		PFdest_ = moveCandidates_[2];
	} else if (direction == "4"){
		PFdest_ = moveCandidates_[3];
	} else if (direction == "6"){
		PFdest_ = moveCandidates_[4];
	} else if (direction == "7"){
		PFdest_ = moveCandidates_[5];
	} else if (direction == "8"){
		PFdest_ = moveCandidates_[6];
	} else if (direction == "9"){
		PFdest_ = moveCandidates_[7];
	}
	if (PFdest_.isWalkable()) pUnit->move(PFdest_.getPos());
}
void UnitAgent::findMoveLoc( const string& direction, WalkRect& rFound, const int distPx )
{
	WalkRect original = rFound;
	if (direction == "1"){
		rFound.changePosPx(walkRect_.xPx() - distPx,	walkRect_.yPx() + distPx);
	}else if (direction == "2"){
		rFound.changePosPx(walkRect_.xPx(),				walkRect_.yPx() + distPx);
	}else if (direction == "3"){
		rFound.changePosPx(walkRect_.xPx() + distPx,	walkRect_.yPx() + distPx);
	}else if (direction == "4"){
		rFound.changePosPx(walkRect_.xPx() - distPx,	walkRect_.yPx());
	}else if (direction == "6"){
		rFound.changePosPx(walkRect_.xPx() + distPx,	walkRect_.yPx());
	}else if (direction == "7"){
		rFound.changePosPx(walkRect_.xPx() - distPx,	walkRect_.yPx() - distPx);
	}else if (direction == "8"){
		rFound.changePosPx(walkRect_.xPx(),				walkRect_.yPx() - distPx);
	}else if (direction == "9"){
		rFound.changePosPx(walkRect_.xPx() + distPx,	walkRect_.yPx() - distPx);
	}else{
		Broodwar->printf("Invalid direction.");
	} // test new position is walkable
	if (rFound.isWalkable())
	{
		return;
	} else
	{
		if (distPx > 1)
		{
			rFound = original;
			findMoveLoc(direction, rFound, distPx/2);
		} else{ // set unwalkable, so it is easier to check later
			rFound.setWalkable(false);
		}
	}
	//PFdest_ = rFound;
}

bool UnitAgent::isWalkable( const WalkRect& walkDest )
{
	if (walkDest.leftPx() < 0 || walkDest.topPx() < 0 || walkDest.rightPx() >= Broodwar->mapWidth()*32 || 
		walkDest.botPx() >= Broodwar->mapHeight()*32) return false;
	for (int i = walkDest.left(); i < walkDest.right(); i++)
	{
		for (int j = walkDest.top(); j < walkDest.bot(); j++)
		{
			if (!Broodwar->isWalkable(i,j))
			{
				return false;
			}
		}
	}
	return true;
}

void UnitAgent::drawWalkRect( const WalkRect& rWR, const BWAPI::Color& rectColor )
{
	Broodwar->drawBoxMap(rWR.leftPx(), rWR.topPx(), rWR.rightPx(), rWR.botPx(), rectColor);
}

void UnitAgent::addNearbyEnemy( const UIptr eUnit )
{
	threats_.insert(eUnit);
	//if (iResult.second)
		//Broodwar->printf("UnitAgent received new bogey, %s", bogey->getType().getName().c_str());
}

bool UnitAgent::attackNearestEnemy()
{	// want to check if they are still visible first
	 BWAPI::Unit* target = TargetManager::get().getTarget(getUnit());
	if (target && target != targetUnit_)
	{
		targetUnit_ = target;
		pUnit->attack(target);
		return true;
	}
	else 
	{
		targetUnit_ = NULL;
		return false;
	}
// 	Unit const * closest = NULL;
// 	int minDist = numeric_limits<int>::max();
// 	BOOST_FOREACH( const  BWAPI::Unit* unit, threats_ )
// 	{
// 		int dist = unit->getPosition().getApproxDistance(pUnit->getPosition());
// 		if (dist < minDist)
// 		{
// 			closest = unit;
// 			minDist = dist;
// 		}
// 	}
// 	if (closest != NULL)
// 	{
// 		targetUnit_ = closest;
// 		//state_.push_front(sATTACK_TARGET);
// 	}
}

void UnitAgent::halt()
{
	pMovementBehaviors_->disable();
	pUnit->stop();
}

/// If unit has moved more than 1 tile, set hasMovedTile_
void UnitAgent::processHasMovedTile()
{	// 32px is a tile
	if (posLastTile_.getApproxDistance(pUnit->getPosition()) > 32)
	{
		framesInTile_ = 0;
		posLastTile_ = pUnit->getPosition();
		scanForEnemies();
	}
	else
	{
		framesInTile_++;
	}
}
/// use BWAPI::getUnitsInRadius to check for nearby enemies.
void UnitAgent::scanForEnemies()
{	// getUnitsInRectangle may be faster
// 	threats_.clear();
// 	set< BWAPI::Unit*> scanResult = Broodwar->getUnitsInRadius(pUnit->getPosition(),12*32);
// 	for (set< BWAPI::Unit*>::const_iterator iUnit = scanResult.begin();
// 		iUnit != scanResult.end(); iUnit++)
// 	{
// 		if ((*iUnit)->getPlayer()->isEnemy(Broodwar->self()))
// 		{
// 			addNearbyEnemy((*iUnit));
// 		}
// 	}
}

void UnitAgent::onBanditDestroyed( const  BWAPI::Unit* bandit )
{
	// obsolete, use onUnitDestroyed callback
// 	set<const  BWAPI::Unit*>::iterator found = threats_.find(bandit);
// 	if (found != threats_.end())
// 	{
// //		logfile << BWAPIext::getUnitNameID(bandit)<<Broodwar->getFrameCount()
// //			<< " found in nearbyBogeys, erasing.\n";
// 		threats_.erase(found);
// 	}
// 	if (targetUnit_ == bandit) targetUnit_ = NULL;
}

std::string UnitAgent::getNameID() const
{
	string id = boost::lexical_cast<string>( getID() );
	string nameID = getName() + "[" + id + "]";
	return nameID;
}

std::string UnitAgent::getActiveActionName() const
{
	return pActions_->getActiveActionName();
}

bool UnitAgent::isIdle() const
{
	return pUnit->isIdle();
}

void UnitAgent::attackMove( BWAPI::Position target )
{
	//move(target);
	pMovementBehaviors_->disable();
	pUnit->attack(target);
}

bool UnitAgent::isMovingTo( Position pos ) const
{
	return pUnit->getTargetPosition().getApproxDistance(pos) < 32;
}

void UnitAgent::trimThreats()
{
	set<UIptr>::iterator iter = attackers_.begin();
	while (iter != attackers_.end())
	{
		int dist = (*iter)->getPosition().getApproxDistance(getPosition());
		int threatRange = BB.groundThreatRange((*iter)->getType());
		// unit not visible and out of threat range
		if (!(*iter)->getUnit()->exists() && dist > threatRange) 
		{
			set<UIptr>::iterator toErase = iter;
			iter++;
			attackers_.erase(toErase);
		}
		else if((*iter)->getUnit()->exists()) // unit exists/ is visible
		{
			bool isTarget = false;
			if ((*iter)->getUnit()->getTarget() == pUnit) isTarget = true;
			else if ((*iter)->getUnit()->getOrderTarget() == pUnit) isTarget = true;
			if (!isTarget && dist > threatRange)
			{
				set<UIptr>::iterator toErase = iter;
				iter++;
				attackers_.erase(toErase);
			}
			else iter++;
		}
		else iter++; // unit not visible but within threat range
	}
	// also eliminate out of range threats
	iter = threats_.begin();
	while (iter != threats_.end())
	{
		int dist = (*iter)->getPosition().getApproxDistance(getPosition());
		if (dist > BB.groundThreatRange((*iter)->getType()) ) 
		{
			set<UIptr>::iterator toErase = iter;
			iter++;
			threats_.erase(toErase);
		}
		else iter++;
	}
}

void UnitAgent::onUnitDestroy( const  BWAPI::Unit* unit )
{
	if (unit->getPlayer() != Broodwar->self())
	{
		UIptr unitInfo = GameState::get().getUnitInfo(unit);
		if (unitInfo)
		{
			threats_.erase(unitInfo);
			attackers_.erase(unitInfo);
		}
	}
}

int UnitAgent::getDistance( UIptr eUnit ) const
{
	if(!eUnit)
		return 0;

	return getDistance(eUnit->getType(), eUnit->getPosition());
}

int UnitAgent::getDistance( BWAPI::UnitType targType, BWAPI::Position position ) const
{
	return BWAPIext::getDistance(getPosition(), getType(), position, targType);
}