#pragma once

#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#ifndef INCLUDED_LOGGING
	#define INCLUDED_LOGGING
	#include <fstream>
#endif
#include <sstream>	// for output
#include <limits>
#include <assert.h>
#include "boost/foreach.hpp"
#include "Agent.h"
#ifndef INCLUDED_BLACKBOARD_H
	#include "Blackboard.h"
#endif
#ifndef INCLUDED_UNITINFO_H
	#include "common/UnitInfo.h"
#endif
#ifndef INCLUDED_WALKTILE_H
	#include "WalkTile.h"
#endif
#ifndef INCLUDED_BUILD_HELPER_H
	#include "BuildHelper.h"
#endif
#ifndef INCLUDED_WALKRECT_H
	#include "WalkRect.h"
#endif
#ifndef INCLUDED_GAMESTATE_H
	#include "GameState.h"
#endif
#ifndef INCLUDED_SCOUTMANAGER_H
	#include "ScoutManager.h"
#endif
#include "MovementBehaviors.h"
#include "Actions\UnitAction.h"
#include "typedef_UnitAgent.h"
#include "Unit.h"

using namespace BBS;

extern ofstream logfile;
class Manager;
/// Implements basic behavior for a single unit. 
/** 
*	Represents a single Unit. Should be inherited by specific UnitNameAgent classes.
*	Contains functions common to all classes representing in-game units or groups
*	of them. Dead Units remain forever within BWAPI, but the UnitAgents of dead
*	Units should be deleted, as they are just taking up space, unless somehow
*	dead Units can be resurrected...
*	We track enemy units that target the UnitAgent, so that it can flee.
*/
class UnitAgent: public UnitClass
{
public:
	static UnitAgent::UAptr Null;
	/// deprecated, soon to be unused! see UnitActions
	enum state_t{
		sDEFAULT,
		sFLEE, 
		sSCOUT,
		sSPIN,		// used for air units to retain max speed
		sMOVE,
		sATTACK_TARGET,
		sLOAD,	// transport states
		sDROP
	};
	/// determines what tasks may take control of this unit.
	int taskPriority;
	// use default parameters: no constructor chaining in C++
//	UnitAgent();
	UnitAgent( BWAPI::Unit* mUnit);
	virtual ~UnitAgent();
	// mutators
	/// attach a Unit to this Agent
	virtual int assignUnit( BWAPI::Unit* mUnit);
	/// right click command as in the GUI game.
	void orderRightClick(Position posTarget, bool queueAction = false);
	void orderRightClick(BWAPI::Unit* unitTarget, bool queueAction = false);
	inline void setSuperior( Manager* pMgr ) {pSuperior = pMgr;}
	inline void setDestinations( list<Position>& nDest ) {
		pMovementBehaviors_->setWaypoints(nDest);
	}
	inline void setDestination(Position nDest) {
		pMovementBehaviors_->setDestination(nDest);
	}
	virtual void move(Position nDest);
	virtual void halt();
	/// 1-9, 1 unit-length in each direction
	virtual void PFmove(const string& direction); 
	/// find a space distPx in the direction that is passable. If none found, return present position
	/// rFound must be the unit's current position
	virtual void findMoveLoc(const string& direction, WalkRect& rFound, const int distPx);
	/// used by Perceptor by UnitManager
	void addNearbyEnemy( const UIptr eUnit ); 
	/// remove the unit from nearbyBogeys
	void onBanditDestroyed( const BWAPI::Unit* bandit );
	/// provide direct access to pMovementBehaviors_
	MovementBehaviors* movementBehavior() {return pMovementBehaviors_;}
	/// set target to closest enemy unit
	bool attackNearestEnemy();
	/// set target to NULL
	void clearTarget() { targetUnit_ = NULL; }
	/// clear set of units attacking this one if they are out of range
	void trimThreats();
	void addAttacker( UIptr attacker ) {attackers_.insert(attacker);}

	// accessors
	/// return edge to edge distance to target in pixels
	int getDistanceTo(BWAPI::Unit* unitTarget) const;
	int getDistanceTo(Position posTarget) const;
	BWAPI::Unit* getUnit() const;
	int getID() const;
	/// returns unit name, Empty string if not assigned
	string getName() const; 
	/// returns string containing unit name and ID
	string getNameID() const;
	UnitType getType() const;
	inline Manager* getSuperior() const { return pSuperior; }
	inline bool isFinished(){ return isFinished_;}
	Position getDest() const;
	/// returns the current position of the Unit.
	BWAPI::Position getPosition() const { return pUnit->getPosition(); }
	/// true if nearbyBandit list is not empty
	bool isEnemyNearby() const { return !threats_.empty(); }
	bool hasTarget() const { return !(targetUnit_ == NULL); }
	BWAPI::Unit const * getTarget() { return targetUnit_; }
	/// true if unit is idle
	bool isIdle() const;
	/// true if unit is moving to within 1 tile of target Position
	bool isMovingTo(Position pos) const;
	bool isUnderAttack() const {return attackers_.size() > 0;}
	bool isThreatened() const { return threats_.size() > 0;}
	int getDistance(UIptr eUnit) const;
	int getDistance(BWAPI::UnitType targType, BWAPI::Position position) const;

	/// order unit to attack move to target position
	void attackMove( BWAPI::Position target );
	/// called by EventManager once a unit dies.
	void onUnitDestroy( const BWAPI::Unit* unit );

	/// unit will act based on percepts and state
	virtual void processActions();

	/// calls printBasicUnitInfo
	virtual void printStatus() const;
	void printBasicUnitInfo() const;
	void drawWalkTiles(); // draw unwalkable walktiles nearby.
	/// draw red lines to any attackers detected
	void drawAttackerLines() const; 

	/// check if ID numbers are equal
	bool operator==(const UnitAgent &other) const;

	string getActiveActionName() const;
protected:
	/// tiles
	static const int fleeRange = 14;
	/// within a single walk tile.
	static const int pxCloseEnoughDist_ = 3; 
	/// for debugging purposes
	std::string nameID_;
	BWAPI::Unit* pUnit;
	/// pointer to a const superior that has control over this unit. Initialize to NULL.
	Manager* pSuperior;
	int nId;
	bool isFinished_;
	/// single Position is destination, multiple are waypoints. Keep last Position.
	//list<BWAPI::Position> destinations_; 
	/// info on enemies that are attacking. OLD
	set<UIptr> attackers_; 
	/// set by Perceptor through UnitManager
	set<UIptr> threats_; 
	BWAPI::Unit const * targetUnit_;
	/// unit's current position, represents collision boundary
	WalkRect walkRect_; 
	/// destination for PF purposes.
	WalkRect PFdest_; 
	/// surrounding 8 positions for PF move, 0-3=1-4, 4-7=6-9
	vector<WalkRect> moveCandidates_; 
	/// used to determine if Unit has moved at least 1 tile
	BWAPI::Position posLastTile_;
	/// count frames unit has stayed in same tile
	unsigned int framesInTile_;
	/// controls movement behavior
	MovementBehaviors* pMovementBehaviors_;
	/// control goal-based behavior
	UnitAction* pActions_;
	boost::signals2::connection mOnUnitDestroyConnection;

	// helper functions
	/// check if unit isUnderAttack, draw indicator
	void checkUnderAttack();	
	/// actions for state sFLEE
	virtual void flee();	
	
	/// draw box around unit's bounds.
	void drawCollisionBox(); 
	/// notify superior
	void onClearAttacker(UIptr attacker); 
	/// clear far away attackers if not visible
	void processAttackers(); 
	/// check position and unit size against walkability
	bool isWalkable(const WalkRect& walkDest); 
	
	
	/// call during ProcessActions()
	void processHasMovedTile();
	/// checks around our unit and add any enemy to nearby list.
	void scanForEnemies();
	
	/// draw walkrect outline in provided color 
	void drawWalkRect(const WalkRect& rWR, const BWAPI::Color& rectColor); 
};

typedef boost::shared_ptr<UnitAgent> UAptr;
typedef boost::weak_ptr<UnitAgent> UAwptr;
typedef std::set<UAptr> UAset;