#pragma once

namespace UnitBehaviors{
	enum Enum{
		None, 
		Rush,
		Observe,	// patrol around and keep enemy in sight, but retreat if attacked.
		Defend
	};
}

#include "MicroAction.h"
#include "SquadGoal.h"
#include "UnitAgent.h"

class UnitGroup;

class UnitBehavior
{
public:
	UnitBehavior(){}
	UnitBehavior(Unit unit);
	UnitBehavior(Unit unit, const std::list<MicroAction> &microActions);

	void addMicroAction(MicroAction action);

	void update(const SquadGoal &squadGoal, const UnitGroup &squadUnits);

	void onDeleted();

	void set(Unit unit);
	void set(Unit unit, const std::list<MicroAction> &microActions);

	void createDefaultActions();

private:
	Unit mUnit;

	std::list<MicroAction> mMicroActions;
};