#include "UnitManager.h"
#include "common/cpp_includes.h"
#include "HarvestManager.h"
#include "UnitAgent.h"
#include "WorkerAgent.h"
#include "Tasks/Contract.h"
#include "Tasks/T_TrainUnitAtFactory.h"
#include "Tasks/C_BuildUnit.h"
#include "Tasks/T_ConstructWithWorker.h"
#include "Budget.h"
#include "MapInfo.h"
#include "Squad.h"

extern ofstream logfile;

using namespace BWAPIext;
using namespace BWAPI::UnitTypes;

UnitManager* UnitManager::pInstance_ = NULL;
UnitManager& UnitManager::get()
{
	if (!pInstance_)
		pInstance_ = new UnitManager();
	return *pInstance_;
}
UnitManager::UnitManager()
:Manager("UnitManager")
,mGlobalBehavior(UnitBehaviors::None)
{
	mArmy = new Squad();
}

UnitManager::~UnitManager()
{
	//delete mArmy;
}

/// call whenever a new unit is created for us.
int UnitManager::addUnitAgent(  BWAPI::Unit* mUnit )
{	
	if (!mUnit)
	{
		logfile << "Error in UnitManager addUnitAgent: bad  BWAPI::Unit* ptr\n";
		return 1;
	}
	if ( !BWAPIext::isControllable(mUnit->getType()) )
	{
		logfile <<"Warning in UnitManager addUnitAgent: tried to add uncontrollable unit\n";
		return 1;
	}
	map<const  BWAPI::Unit*, UAwptr>::iterator found = mUnitToUAwptr.find(mUnit);
	if (found != mUnitToUAwptr.end() )
	{
		logfile << "Warning in UnitManager addUnitAgent: unit already added.\n";
		return 1;
	}
	logfile << "UM: new unit added: " << mUnit->getType().getName() 
		<< "[" << mUnit->getID() <<"]\n";
	UAptr pUA;
	if (mUnit->getType().isWorker())
	{
		//logfile << "UM: new workerAgent added to incompleteUnits\n";
		WAptr pWa( new WorkerAgent(mUnit) );
		pUA = static_cast<UAptr>(pWa);
// 		mAllUnitAgents.insert(pWa);
// 		mUnitToUAwptr[mUnit] = pWa;
	}
	else // generic unit
	{
		pUA = UAptr( new UnitAgent(mUnit) );
		if ( mUnit->getType().isBuilding() )
		{
			MapInfo::get().updateBuildability(mUnit);
		}
	}
// ALL NEW UNITS GO HERE
	mIncompleteUnits.insert(pUA);

	mAllUnitAgents.insert(pUA);
	mUnitToUAwptr[mUnit] = pUA;
	// check for completed buildStructure contracts
	checkCompleteContract(mUnit);
	return 0;
}

void UnitManager::addResourceDepot( UAptr depot )
{
	HarvestManager::get().onNewDepotComplete(depot);
}

void UnitManager::onUnitLost( const  BWAPI::Unit* mUnit)
{
	map< const  BWAPI::Unit*,UAwptr>::const_iterator found;
	found = mUnitToUAwptr.find(mUnit);
	if (found != mUnitToUAwptr.end()) // found it
	{	// check validity
		UAptr pUA( found->second.lock() );
		if ( pUA && mAllUnitAgents.count(pUA) )
		{
			if ( mUnit->getType().isBuilding() )
			{
				MapInfo::get().updateBuildability(mUnit);
			}
			logfile << "our destroyed unit found in mAllUnitAgents, erasing.\n";
			// erase from incomplete and free sets
			eraseUnit(pUA);
// 			mUnits.erase(pUA);
// 			mIncompleteUnits.erase(pUA);
// 			mAllUnitAgents.erase(pUA);
		}
		else // invalid reference
		{
			logfile << "Warning: our destroyed unit not found in mAllUnitAgents\n";
		}
	}

	else
	{
		logfile << "UnitManager::onUnitLost - failed to find unit in mAllUnits_";
	}
	
}

/// erase and re-add the morphed unit. Returns 0 on success, 1 on fail.
int UnitManager::onUnitMorph(  BWAPI::Unit* pUnit )
{
	logfile << "Our unit, " << pUnit->getType().getName() << "[" 
		<< pUnit->getID() << "] has morphed.\n";
	// find corresponding UAptr to the  BWAPI::Unit*
	UAptr pUA = getUAptr(pUnit);
	// find out if unit is egg or not
	if (pUnit->getType() == Zerg_Egg)
	{
		logfile << "Egg morph detected [" << pUnit->getID() << "]\n";
		if (pUnit->getBuildType())
		{
			logfile << "Egg is morphing into " << pUnit->getBuildType().getName() << endl;
		}
	}
	else if (pUnit->getType() == Zerg_Lurker_Egg)
	{
		logfile << "TODO: handle lurker egg morph\n";
	}
	else if (pUnit->getType() == Zerg_Cocoon)
	{
		logfile << "TODO: handle cocoon morph\n";
	}
	else
	{
		// then erase it from all sets
		eraseUnit(pUA);
		// now re-add the unit
		int r = addUnitAgent(pUnit);
		if ( r == 0 )
		{
			logfile << Broodwar->getFrameCount() << " Successful morph erase and re-add :" 
				<< BB.ES.getUnitCountStr(pUnit->getType())
				<< endl;
			return 0;
		}
		else
		{
			logfile << "re-add was not successful.\n";
			return 1;
		}
	}
	return 0;
}

void UnitManager::perceptNearbyEnemy( const BWAPI::Unit* ours, const UIptr enemy )
{
	if (ours->exists())
	{
		map< const  BWAPI::Unit*,UAwptr>::iterator iFound = mUnitToUAwptr.find(ours);
		if (iFound != mUnitToUAwptr.end())
		{
			UAptr pUA( iFound->second.lock() );
			if (pUA) //valid
			{
				pUA->addNearbyEnemy(enemy);
			}
			else // invalid, delete
			{
				mUnitToUAwptr.erase(iFound);
				logfile << "Warning: our unit in mUnitToUAwptr invalid!\n";
			}
		}
	} else {
		Broodwar->printf("PerceptNearbyBogey given our unit that no longer exists!");
		logfile << "PerceptNearbyBogey given our unit that no longer exists!\n";
	}
}

void UnitManager::onEnemyDestroyed( const  BWAPI::Unit* bandit )
{
	BOOST_FOREACH(UAptr uAgent, mAllUnitAgents)
	{
		uAgent->onBanditDestroyed(bandit);
	}
}

void UnitManager::selectedUnitsSetBehavior( string mb, bool b )
{

}

void UnitManager::init()
{// not needed! use onUnitCreated instead!
// 	set< BWAPI::Unit*> allVisibleUnits = Broodwar->getAllUnits();
// 	set< BWAPI::Unit*> ourUnits;
// 	BOOST_FOREACH(  BWAPI::Unit* unit, allVisibleUnits )
// 	{
// 		if ( unit->getPlayer() == Broodwar->self() )
// 		{
// 			ourUnits.insert(unit);
// 		}
// 	}
// 	BOOST_FOREACH(  BWAPI::Unit* unit, ourUnits )
// 	{
// 		addUnitAgent(unit);
// 
// 		if ( isFactory(unit) )
// 		{
// 			UAwptr weak = mUnitToUAwptr[unit]; 
// 			mFactories.push_front(weak);
// 		}
// 	}
}

bool UnitManager::haveDepot()
{
	return BB.ES.getUnitCount(Zerg_Hatchery)
	+ BB.ES.getUnitCount(Protoss_Nexus)
	+ BB.ES.getUnitCount(Terran_Command_Center) != 0;
}

bool UnitManager::haveWorker()
{
	return BB.ES.getUnitCount(BWAPI::UnitTypes::Terran_SCV)
	+ BB.ES.getUnitCount(BWAPI::UnitTypes::Protoss_Probe)
	+ BB.ES.getUnitCount(BWAPI::UnitTypes::Zerg_Drone) != 0;
}

void UnitManager::update()
{
	// if enemy base known, set global behavior to rush.
// 	if (MapInfo::get().getEnemyStartLocation() != BWAPI::TilePositions::Unknown)
// 	{
// 		if (mGlobalBehavior == UnitBehavior::None)
// 		{
// 			Broodwar->printf("setting unit behavior to Rush");
// 			mGlobalBehavior = UnitBehavior::Rush;
// 			mArmy->setBehavior(UnitBehavior::Rush);
// 			mArmy->setTargetPosition(Position(MapInfo::get().getEnemyStartLocation()));
// 		}
// 	}
// 	else // move to center of map
// 	{
// 		mArmy->setTargetPosition(MapInfo::get().mapCenter());
// 	}
	checkAttackers();
	if (mGlobalBehavior == UnitBehaviors::Rush)
	{
		// iterate through all free units and order idle all combat units to attack move 
		// to enemy base
// 		for(UAset::iterator iter = mUnits.begin(); iter != mUnits.end();  )
// 		{
// 			UAptr unit = *iter;
// 			if ( unit->getUnitType() == BWAPI::UnitTypes::Zerg_Zergling && unit->isIdle())
// 			{
// 				mArmy->addUnit(unit);
// 				UAset::iterator toErase = iter;
// 				++iter;
// 				mUnits.erase(toErase);
// 				Broodwar->printf("unit placed in Squad!");
// 			}
// 			else
// 			{
// 				++iter;
// 			}
// 		}
	}
	processFinishedUnits();
	mArmy->update();
	// try to delegate worker to HarvestManager
	// now fulfilled by processFreeWorkers
// 	if (!mWorkers.empty() && HarvestManager::get().nodeCount() > 0)
// 	{
// 		BOOST_FOREACH( WAptr worker, mWorkers)
// 		{
// 			HarvestManager::get().addWorker(worker);
// 		}
// 		mWorkers.clear();
// 	}
	HarvestManager::get().update();
	// update all units here, not in subcontrollers!
	BOOST_FOREACH( UAptr unit, mAllUnitAgents )
	{
		unit->processActions();
	}
	processTasks();
	processContracts();
	processFreeWorkers();
	//mArmy->processActions();
	//setCombatUnits();
}

void UnitManager::processFinishedUnits()
{
	set<UAptr>::iterator inc = mIncompleteUnits.begin();
	for (inc; inc != mIncompleteUnits.end(); )
	{
		UAptr pUA = *inc;
		if (pUA->isFinished())
		{
			UnitType ut = pUA->getType();
			if ( ut.isBuilding() )
			{
				mStructures[ut].push_back(pUA);
				if ( BWAPIext::isResourceDepot(ut))
				{
					addResourceDepot(pUA);
				}
			}
			else if( ut.isWorker() )
			{
				logfile << "UM: finished worker added to mWorkers\n";
				WAptr pWA = boost::shared_polymorphic_downcast<WorkerAgent>(pUA);
				mWorkers.insert(pWA);
			}
// 			else if ( BWAPIext::isCombatUnit(ut) )
// 			{
// 				mArmy->addUnit(pUA);
// 			}
			else
			{
				logfile << "UM: free unit added - " << pUA->getNameID() << endl;
				mUnits.insert(pUA);
			}
			set<UAptr>::iterator toErase = inc;
			inc++;
			mIncompleteUnits.erase(toErase);
		}
		else ++inc;
	}
}

bool UnitManager::addContract( pContract job )
{
	if ( !job )
	{
		logfile << "Error in UnitManager addContract: bad ptr\n";
		return false;
	}
	if ( job->name == "BuildUnit" )
	{
		mContracts.push_back(job);
		pCBU buildContract = boost::static_pointer_cast<C_BuildUnit>(job);
		assert(buildContract);
		if (buildContract->buildType.isBuilding())
		{	// check location
			if (!buildContract->buildLocation.isValid())
			{
				logfile << "Error in UnitManager addContract: bad location\n";
				return false;
			}
			// make task to handle building construction.
			logfile << "UnitManager accepts BuildUnit contract for "
				<<buildContract->buildType.getName() << "\n";
			if (!buildContract->isInactive())
			{
				logfile << "Warning in UnitManager addContract: contract not inactive\n";
			}
			//mTasks.push_back( pTCWW(new T_ConstructWithWorker(buildContract)) );
		}
		else // standard unit train
		{
			// create a task to handle the contract
			mTasks.push_back( pTTUAF(new T_TrainUnitAtFactory(buildContract)) );
			//logfile << "UnitManager accepts BuildUnit contract to train.\n";
		}
		return true;
	}
	logfile << "Warning in UnitManager addContract: unable to handle\n";
	return false;
}

void UnitManager::cancel( pContract job )
{
	//logfile << "UM canceling reserve contract... ";
	list<pContract>::iterator ipc = mContracts.begin();
	for (ipc; ipc != mContracts.end(); )
	{
		if ((*ipc) == job)
		{
			if(job->name == "BuildUnit") cancelBuildUnit(job);
			ipc = mContracts.erase(ipc);
			//logfile << "successful. " << mContracts.size() << " remain\n";
			return;
		}
		else ++ipc;
	}
	logfile << Broodwar->getFrameCount() <<" Warning in UM cancel - not found!\n";
}

int UnitManager::trainUnit( BWAPI::UnitType toTrain )
{
	int framesTillReady = -1;
	UnitType factoryType = toTrain.whatBuilds().first;
	list<UAptr> factories = mStructures[factoryType];
	if (factories.empty()) return framesTillReady;
	// get the shortest wait time.
	BOOST_FOREACH(UAptr factory, factories)
	{
		int waitTime = factory->getUnit()->getRemainingTrainTime();
		if (waitTime == 0)
		{
			if (!Broodwar->canMake(factory->getUnit(),toTrain))
			{
				logfile << "Error in UnitManager trainUnit: cannot make!\n";
				return -1;
			}
			factory->getUnit()->train(toTrain);
// 			logfile << " UnitManager trainUnit: ordered train, frames remaining " 
// 				<< factory->getUnit()->getRemainingTrainTime()<<endl;
			return 0;
		}
		else if (framesTillReady == -1 || waitTime < framesTillReady)
			framesTillReady = waitTime;
	}
	return framesTillReady;
}

void UnitManager::processContracts()
{
	BOOST_FOREACH(pContract job, mContracts)
	{
		if (job->name == "BuildUnit")
		{	// test to see if it involves build structure
			pCBU buildContract = boost::static_pointer_cast<C_BuildUnit>(job);
			assert(buildContract);
			if (buildContract->buildType.isBuilding())
			{
				processBuildStructure(buildContract);
			}
		}
	}
	// remove failed or completed contracts, happens after process so that
	// workers can get returned.
	list<pContract>::iterator citer = mContracts.begin();
	while(citer != mContracts.end())
	{
		if ((*citer)->isFailed() || (*citer)->isComplete())
		{
			citer = mContracts.erase(citer);
		}
		else ++citer;
	}
}

void UnitManager::processBuildStructure(pCBU buildContract)
{
	if (buildContract->isInactive())
	{
		//logfile << "UnitManager activating build contract.\n";
		Position buildPos(getCenter(buildContract->buildLocation));
		// grab worker
		if (!buildContract->worker)
		{
			WAptr worker = pullNearestWorkerFromHarvest(buildPos);
			if (!worker)
			{	// wait for worker.
				//logfile << "Contract Failed - UnitManager buildStructure, no worker\n";
				buildContract->status = TaskStatus::inactive;
			}
			else
			{
				logfile << "UnitManager got worker, send contract\n";
				buildContract->worker = worker->getUnit();
				buildContract->status = TaskStatus::active;
				//worker->addContract(buildContract);
			}
		}
		else // have worker already
		{
			logfile << "Warning in UnitManager processBuildStructure: already got wkr\n";
			buildContract->status = TaskStatus::active;
		}
	}
	if (buildContract->isActive())
	{
		// check on worker
		if (buildContract->worker && buildContract->worker->exists())
		{
			WAptr worker = boost::static_pointer_cast<WorkerAgent>(
				mUnitToUAwptr[buildContract->worker].lock());
			assert(worker);
			// if worker is already building, let it do its stuff
			if (worker->getUnit()->isConstructing())
			{
				// rely on OnUnitCreated trigger to set complete!
				return;
			}
			// check it is moving towards build location
// 			Position buildPos(getCenter(buildContract->buildLocation));
// 			if (worker->getDest() != buildPos)
// 			{
// 				worker->move(buildPos);
// 			}
			// now to check clearance and move units away if necessary
			// also check to see if worker in rectangle
// 			bool workerInPosition = false;
// 			Position TL(buildContract->buildLocation);
// 			Position BR( BtoP(buildContract->buildLocation.x()
// 				+buildContract->buildType.tileWidth())
// 				, BtoP(buildContract->buildLocation.y()
// 				+buildContract->buildType.tileHeight()) );
// 			set< BWAPI::Unit*> units = Broodwar->getUnitsInRectangle(TL,BR);
// 			if (units.count(buildContract->worker) == 1)
// 			{
// 				workerInPosition = true;
// 			}
// 			units.erase(buildContract->worker); // dont count worker

				// final build check
// 			if (Broodwar->canBuildHere(buildContract->worker,
// 				buildContract->buildLocation,false))
// 			{
// 				worker->addContract(buildContract);
// 			}

// 			else
// 			{
// 				// TODO: issue mandate to units to leave area
// 				Broodwar->printf("Something in the way of building placement!");
// 			}
		}
		else	// need to get another worker.
		{
			logfile << "Need to get another worker: ";
			if (!buildContract->worker)
			{
				logfile << "bad worker ptr in buildContract.\n";
			}
			else if( !buildContract->worker->exists() )
			{
				logfile << "worker " << buildContract->worker->getID()
					<<" does not exist.\n";
			}
			buildContract->status = TaskStatus::inactive;
		}
	}
	else if (buildContract->isFailed())
	{
		// return worker
		logfile << "UM buildcontract " << buildContract->buildType.getName()
			<< " failed, ";
		if (buildContract->worker)
		{
			logfile << "returning worker" << buildContract->worker->getID() 
				<<"\n";
			freeWorker(buildContract,true);
		}
		else
		{
			logfile << "no worker to return.\n";
		}
	}
	else if (buildContract->isComplete())
	{
		// return worker
		logfile << "UM buildcontract " << buildContract->buildType.getName()
			<< " complete, ";
		if (buildContract->worker)
		{
			logfile << "returning worker " << buildContract->worker->getID() 
				<<"\n";
			freeWorker(buildContract,false);
		}
		else
		{
			logfile << "no worker to return.\n";
		}
	}
}

WAptr UnitManager::getWorkerAgentPtr(  BWAPI::Unit* worker )
{
	WAptr WAworker;
	if (worker && worker->getType().isWorker())
	{
		WAworker = boost::static_pointer_cast<WorkerAgent>(
			mUnitToUAwptr[worker].lock());
		assert(WAworker);
	}
	return WAworker;
}
void UnitManager::checkCompleteContract( BWAPI::Unit* unitBuilt )
{
	// check structure contracts
	bool isStructure = false;
	UnitType typeBuilt = unitBuilt->getType();
	if (typeBuilt.isBuilding())
	{
		BOOST_FOREACH(pContract job,mContracts)
		{
			if (job->isActive() && job->name == "BuildUnit")
			{
				pCBU buildContract = boost::static_pointer_cast<C_BuildUnit>(job);
				assert(buildContract);
				if (buildContract->buildType.isBuilding())
				{
					isStructure = true;
					if (buildContract->buildType == typeBuilt
						&& buildContract->buildLocation == unitBuilt->getTilePosition())
					{
						buildContract->status = TaskStatus::complete;

						// return worker to free work pool
						logfile << "UnitManager checkCompleteContract: structure "
							<< typeBuilt.getName() << " " 
							<< buildContract->buildLocation.x() << ","
							<< buildContract->buildLocation.y() << 
							" started construction. "
							<< "Worker returned to pool\n";
						freeWorker(buildContract);
						return;
					}
				}
			}
		}
		if(isStructure)
		{
			logfile << "Warning in UnitManager checkCompleteContract: contract for" <<
				unitBuilt->getType().getName() << "not found\n";
		}
	}
	
}

bool UnitManager::returnToHarvest( WAptr worker )
{
	set<WAptr>::iterator found = mWorkers.end();
	set<WAptr>:: iterator it = mWorkers.begin();
	for( it; it != mWorkers.end(); ++ it)
	{
		if ((*it) == worker)
		{
			found = it;
			break;
		}
	}
	if(found == mWorkers.end())
	{
		logfile << "Error in UnitManager returnTOHarvest: worker not found in mWorkers size = "
			<< mWorkers.size() << "\n";
		return false;
	}
	// else we found successful
	mWorkers.erase(found);
	HarvestManager::get().addWorker(worker);
	return true;
}

WAptr UnitManager::pullNearestWorkerFromHarvest( BWAPI::Position pos )
{
	WAptr worker = HarvestManager::get().pullNearestWorker(pos);
	if (!worker)
	{
// 		logfile << "Error in UnitManager pullNearestWorkerFromHarvest, "
// 			<< "no worker gotten from HarvestManager\n";
	}
	else
	{
		worker->clearState();
	}
	return worker;
}

void UnitManager::processFreeWorkers()
{
	if(!HarvestManager::get().haveMiningDepot()) return;
	set<WAptr>::iterator wit = mWorkers.begin();
	for (wit; wit != mWorkers.end(); ++wit)
	{	// if idling, return to Harvest
		if ((*wit)->isIdle())
		{
			if (HarvestManager::get().nodeCount() > 0)
			{
				returnToHarvest(*wit);
				//logfile << "Idle worker sent to harvest\n";
				break;
			}
		}
	}
}

void UnitManager::cancelBuildUnit(pContract job)
{
	// check to see if it is build structure, return the worker!
	if (job->name == "BuildUnit" && job->isFailed())
	{
		pCBU buildContract = boost::static_pointer_cast<C_BuildUnit>(job);
		assert(buildContract);
		if (buildContract->buildType.isBuilding() 
			&& buildContract->worker)
		{
			logfile << "UM cancel BuildStructure and return worker "
				<< buildContract->worker->getID() <<" to wkr pool\n" ;
			freeWorker(buildContract, true);
		}
	}
}

void UnitManager::freeWorker( pCBU buildContract, bool cancel /*= false*/ )
{
	assert(buildContract->worker);
	WAptr returner = getWorkerAgentPtr(buildContract->worker);
	assert(returner);
	logfile << Broodwar->getFrameCount() << " ***returned worker " << returner->getID() << "\n";
	mWorkers.insert(returner);
	//if (cancel) returner->cancel(buildContract);
	buildContract->worker = NULL;
}
/// assume that the donor has already removed control over unit
void UnitManager::giveUnit( UAptr unitAgent )

{
	assert(unitAgent);
	if ( unitAgent->getType().isWorker() )
	{
		// cast into workeragent and put into workers
		WAptr worker(boost::static_pointer_cast<WorkerAgent>(unitAgent));
		assert(worker);
		mWorkers.insert(worker);
	}
	else
	{
		mUnits.insert(unitAgent);
	}
}

bool UnitManager::takeUnit( UAptr unit )
{
	if (unit->getType().isBuilding())
	{
		mStructures[unit->getType()].pop_front();
		return true;
	}
	else
	{// check if unit is in freeUnits
		if (mUnits.find(unit) != mUnits.end())
		{
			mUnits.erase(unit);
			return true;
		}
		logfile << "UnitManager: takeUnit - unit not in mUnits!\n";
		return false;
	}
}

UAset UnitManager::getSubset( const UAset& source, UnitType ut )
{
	UAset subset;
	BOOST_FOREACH( const UAptr unit, source )
	{
		if (unit->getType() == ut)
		{
			subset.insert(unit);
		}
	}
	return subset;
}

bool UnitManager::eraseUnit( UAptr pUA )
{
	int erased = 0;
	if (pUA == UnitAgent::Null)
	{
		logfile << "gave UM a null unit to erase!\n";
		return false;
	}
	if (pUA->getType().isWorker())
	{
		WAptr pWA = boost::static_pointer_cast<WorkerAgent>(pUA); 
		if( mWorkers.erase(pWA) ) erased++;
		else if ( HarvestManager::get().eraseWorker(pWA) )
		{
			erased++;
		}
	}
	if( mIncompleteUnits.erase(pUA) ) erased++;
	if( mUnits.erase(pUA) ) erased++;
	map< const  BWAPI::Unit*,UAwptr>::const_iterator found;
	found = mUnitToUAwptr.find(pUA->getUnit());
	if (found != mUnitToUAwptr.end())
	{
		mUnitToUAwptr.erase(found);
	}
	if( mAllUnitAgents.erase(pUA) ) erased++;
	if (mArmy->removeUnit(pUA) )
	{	erased++ ;
		logfile << "erased unit from army\n";
		logWriteBuffer();
	}
	if (erased > 0)
	{

	}
	else
	{
		logfile << "UnitManager: potential unit not erased on destruction!\n";
	}
	return true;
}
/// returns UnitAgent::Null if cannot find
UAptr UnitManager::getUAptr( BWAPI::Unit* unit )
{
	map< const  BWAPI::Unit*,UAwptr>::const_iterator found;
	found = mUnitToUAwptr.find(unit);
	if (found != mUnitToUAwptr.end()) // found it
	{	// check validity
		UAptr pUA( found->second.lock() );
		if ( pUA ) 
		{
			return pUA;
		}
	}
	logfile << "UAptr either invalid or not found for "<<getUnitNameID(unit) << endl;
	return UnitAgent::Null;
}

UAptr UnitManager::getUnitOfType( BWAPI::UnitType unitType, BWAPI::Position position/*=BWAPI::Positions::None*/ )
{
	UAptr retVal;
	if (unitType.isBuilding())
	{
		// look in structures
		list<UAptr> unitsList = mStructures[unitType];
		if (!unitsList.empty())
		{
			retVal = unitsList.front();
		}
	}
	else
	{// look in units
		BOOST_FOREACH(UAptr uap, mUnits)
		{
			if (uap->getType() == unitType)
			{
				retVal = uap;
				break;
			}
		}
	}
	if (retVal)
	{
		takeUnit(retVal);
	}
	else
	{
		logfile << "UM: getUnitofType, "<< unitType.getName() << " not found!\n";
	}
	return retVal;
}

int UnitManager::unitPrereqsEta( UnitType ut ) const
{
	std::map< UnitType, int > required = ut.requiredUnits();
	int eta = -1;
	//std::map< UnitType, int > timeToComplete;
	// for each unit in required, check to see if there are enough in BB completed.
	typedef pair<UnitType,int> unitNum;
	BOOST_FOREACH( unitNum req, required )
	{
		int x = required[req.second] - BBS::BB.ES.getUnitCount(req.first); 
		if (x <= 0)
		{
			eta = max(eta,0);
			continue;
		}
		int y = x - BBS::BB.ES.getUnderConstruction(req.first);
		if (y <= 0)
		{
			// TODO: find soonest finishing one and set time to complete
			eta = max(eta,100);
			continue;
		}
		int z = y - BBS::BB.ES.getUnitsOrdered(req.first);
		if (z <= 0)
		{
			// best guess based on time to build unit
			eta = max(eta,req.first.buildTime());
			continue;
		}
		logfile << "UM: unit prereqs not found!\n";
		return -1;
	}
	return eta;
}

void UnitManager::setCombatUnits()
{
// 	set< BWAPI::Unit*> combatUnits;
// 	BOOST_FOREACH(UAptr ua, mUnits)
// 	{
// 		combatUnits.insert(ua->getUnit());
// 	}
// 	combatCommander_.update(combatUnits);
}

bool UnitManager::haveFreeUnit( BWAPI::UnitType type, int atLeast /*= 1 */ ) const
{
	int count = 0;
	BOOST_FOREACH( UAptr ua, mUnits )
	{
		if (ua->getType() == type)
		{
			count++;
		}
	}
	if ( count >= atLeast)
	{
		return true;
	}
	return false;
}

void UnitManager::onUnitFinished( BWAPI::Unit* pUnit )
{

}

void UnitManager::checkAttackers()
{
	set< BWAPI::Unit*> gameUnits = Broodwar->getAllUnits();
	for (UAset::iterator iter = mAllUnitAgents.begin(); iter != mAllUnitAgents.end(); ++iter)
	{
		(*iter)->trimThreats();
	}
	BOOST_FOREACH( BWAPI::Unit* unit, gameUnits)
	{
		if (unit->getPlayer()->isEnemy(Broodwar->self()) && unit->getType().canAttack())
		{
			 BWAPI::Unit* target = unit->getTarget();
			 BWAPI::Unit* orderTarget = unit->getOrderTarget();
			if ( target && target->exists() && target->getPlayer() == Broodwar->self() )
			{
				setAttacker(target, unit);
			}
			else if ( orderTarget && orderTarget->exists() && orderTarget->getPlayer() == Broodwar->self())
			{
				setAttacker(target, unit);
			}
		}
	}
}

void UnitManager::setAttacker( BWAPI::Unit* myUnit, BWAPI::Unit* attacker )
{
	if ( !myUnit || !attacker || !myUnit->exists() || !attacker->exists()) return;
	// find myUnit
	UAptr pMyUnit = getUAptr(myUnit);
	// find attacker UnitINfo
	UIptr pAttacker = GameState::get().getUnitInfo(attacker);
	if (!pMyUnit || !pAttacker) return;
	pMyUnit->addAttacker(pAttacker);
}

void UnitManager::drawAttackers() const
{
	for (UAset::const_iterator iter = mAllUnitAgents.begin(); iter != mAllUnitAgents.end(); ++iter)
	{
		(*iter)->drawAttackerLines();
	}
}