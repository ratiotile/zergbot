/************************************************************************/
/* UnitManager controls the access to all UnitAgents. Gets control of	*/
/* units as soon as they are created. Responsible to turn over units
/* from one controller to another.
/************************************************************************/
#pragma once
#ifndef INCLUDED_UNITMANAGER_H
#define INCLUDED_UNITMANAGER_H

#include <fstream>
#include <string>
#include "UnitAgent.h"
#include "typedef_UnitAgent.h"
#include "Contractor.h"
#include "Manager.h"
#include "Tasks/typedef_Task.h"
//#include "Combat/CombatCommander.h"
#include "UnitBehavior.h"


class Squad;

using namespace std;


// singleton
class UnitManager: public Contractor, public Manager
{
public:
	static UnitManager& get();
	~UnitManager();
	/// call each frame after information has been collected and processed
	void update();
	/// add a new unit to manage
	int addUnitAgent( BWAPI::Unit* mUnit);	// return 0 success, 1 fail (duplicate)
	/// add depot to HarvestManager
	void addResourceDepot( UAptr depot );
	void onUnitLost( const BWAPI::Unit* mUnit); // deactivate dead unit
	/// call whenever our unit morphs
	int onUnitMorph( BWAPI::Unit* mUnit );
	/// signal UnitAgents to remove enemy
	void onEnemyDestroyed( const BWAPI::Unit* bandit);

	void perceptNearbyEnemy( const BWAPI::Unit* ours, const UIptr enemy); // add bandit to UnitAgent's list
	/// call at game start to add our units
	void init();
	/// true if we control at least one resource depot
	bool haveDepot();
	
	/// true if we control at least one worker
	bool haveWorker();
	/// turn over control of a unit to UnitManager
	void giveUnit( UAptr unitAgent );
	/// take control of nearest unit of type, given location. return UnitAgent::Null if fail.
	UAptr getUnitOfType( BWAPI::UnitType unitType, 
		BWAPI::Position position=BWAPI::Positions::None);
	/// given a  BWAPI::Unit*, return the corresponding UAptr
	UAptr getUAptr( BWAPI::Unit* unit );
	/// get a reference to set of free units
	const UAset& getFreeUnits() const {return mUnits;}
	/// take control of specified unit
	bool takeUnit( UAptr unit );
	/// erase all references of a unit
	bool eraseUnit( UAptr unit );
	/// -1 if prereqs not found, otherwise time until complete
	int unitPrereqsEta( UnitType ut ) const;
	/// check if we have a free unit of type
	bool haveFreeUnit( BWAPI::UnitType type, int atLeast = 1 ) const;

	// Contractor interface methods
	bool addContract(pContract job);
	void cancel(pContract job);


	///return frames until ready, or 0 if training, -1 if cannot train
	int trainUnit(BWAPI::UnitType toTrain);

	///helper, given set of units, return subset of specified type
	UAset getSubset( const UAset& source, UnitType ut );

	// debug
	void selectedUnitsSetBehavior(string mb, bool b);
	void drawAttackers() const;

protected:
	/// currently used for all our units, only strong references
	set<UAptr> mAllUnitAgents;
	/// fast way to find UAptr from  BWAPI::Unit*
	map<const BWAPI::Unit*, UAwptr> mUnitToUAwptr;
	/// worker units under direct control, the rest delegated
	set<WAptr> mWorkers;
	/// units under direct control
	set<UAptr> mUnits;
	/// incomplete units are in mALlunitAgents and map, but not subcontrollers.
	set<UAptr> mIncompleteUnits;
	/// free units are unbound by any controller, idle
	//set<UAptr> mFreeUnits;
	/// new method of handling buildings, new ones in front of list
	map<BWAPI::UnitType,list<UAptr>> mStructures;
	/// main army group, temporary
	Squad* mArmy;
	/// all units with no behavior set
	UnitBehaviors::Enum mGlobalBehavior;

private:
	UnitManager();
	static UnitManager* pInstance_;

	///put finished units where they belong
	void processFinishedUnits();
	/// call when unit is finished
	void onUnitFinished( BWAPI::Unit* pUnit );
	/// currently act on BuildUnit contracts for structures
	void processContracts();
	void processBuildStructure(pCBU buildContract);
	/// call on new unit, check to see if we can complete contracts
	void checkCompleteContract(BWAPI::Unit* unitBuilt);
	/// null if unset or not worker
	WAptr getWorkerAgentPtr(BWAPI::Unit* worker);
	/// erase worker from mWorkers and return control to HarvestManager
	bool returnToHarvest(WAptr worker);
	/// pull worker from harvest and return pointer
	WAptr pullNearestWorkerFromHarvest(BWAPI::Position pos);
	/// put idle workers back to work
	void processFreeWorkers();
	/// call in processContracts on BuildUnit
	void cancelBuildUnit(pContract job);
	/// return worker to free unit pool
	void freeWorker(pCBU buildContract, bool cancel = false);
	/// find units that are attacking our units.
	void checkAttackers();
	void setAttacker( BWAPI::Unit* myUnit, BWAPI::Unit* attacker );

	// for UAB combat management
	void setCombatUnits();

	// debug draw
	//void label
};

#endif