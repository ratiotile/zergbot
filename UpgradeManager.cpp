#include "UpgradeManager.h"
#include "BuildPlanner/BuildGoalPlanner.h"
#include "BuildPlanner/BuildGoal.h"
#include "common/cpp_includes.h"


UpgradeManager::UpgradeManager()
{
	loadPriorities();
	updateLevels();
}

void UpgradeManager::loadPriorities()
{
	using namespace BWAPI::UpgradeTypes;
	using namespace BWAPI::TechTypes;

	mUpgradeScores[Metabolic_Boost].priority = 100;
	mUpgradeScores[Adrenal_Glands].priority = 200;
	mUpgradeScores[Antennae].priority = 50;
	mUpgradeScores[Pneumatized_Carapace].priority = 150;
	mUpgradeScores[Zerg_Melee_Attacks].priority = 110;
	mUpgradeScores[Zerg_Carapace].priority = 140;
	mTechScores[Burrowing].priority = 10;
}

void UpgradeManager::calculateCosts()
{
	for each( std::pair<BWAPI::UpgradeType,UpgradeItem> upgPair in mUpgradeScores )
	{
		UpgradeItem& upg = upgPair.second;
		int cost = 0;
		if (!Broodwar->canUpgrade(NULL,upgPair.first))
		{
			BuildGoal bg;
			bg.vGoalUpgrades_.push_back(std::make_pair(upgPair.first,upg.level));
			std::list<UnitType> requirements = BuildGoalPlanner::get().generateBuildOrder(bg);
			for each( UnitType ut in requirements )
			{
				cost += ut.mineralPrice() + 2*ut.gasPrice();
			}
		}
		cost += upgPair.first.mineralPrice() + 2*upgPair.first.gasPrice();
	}
}

void UpgradeManager::updateLevels()
{
	for each( std::pair<BWAPI::UpgradeType,UpgradeItem> upgPair in mUpgradeScores )
	{
		UpgradeItem& upg = upgPair.second;
		upg.level = 1;
	}
}

void UpgradeManager::evaluateUpgrades()
{
	// number of units that benefit.
	//std::set<Unit*> myUnits = Broodwar->self()->getUnits();
	for each( std::pair<BWAPI::UpgradeType,UpgradeItem> upgPair in mUpgradeScores )
	{
		int count = 0;
		BWAPI::UpgradeType upg = upgPair.first;
		std::set<UnitType> affectedUnits = upg.whatUses();
		for each (UnitType ut in affectedUnits)
		{	
			count += BB.ES.getUnitTotal(ut);
		}
		// do calculation for score
		UpgradeItem& item = upgPair.second;
		item.utility = count * item.priority/(double)item.cost;
		logfile << upg.getName() << " utility: " << item.utility << endl;
	}
}

void UpgradeManager::update()
{
	calculateCosts();
	evaluateUpgrades();
}