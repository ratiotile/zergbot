#pragma once
#include "Interface.h"
#include "common/Singleton.h"
#include <BWAPI.h>

struct UpgradeItem
{
	int priority;
	int cost; // resources, gas = 2x mineral, add tech reqs.
	double utility;
	int level; // usually 1
};

class UpgradeManager: public Singleton<UpgradeManager>
{
public:
	UpgradeManager();
	void update();
private:
	void loadPriorities();
	void evaluateUpgrades();
	void calculateCosts();
	void updateLevels();
	std::map<BWAPI::UpgradeType, UpgradeItem> mUpgradeScores;
	std::map<BWAPI::TechType, UpgradeItem> mTechScores;
};

