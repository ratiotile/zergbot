#include "WorkerAgent.h"
#include "common/cpp_includes.h"
#include "Tasks/C_BuildUnit.h"

using namespace BWAPIext;

// WorkerAgent::WorkerAgent()
// :UnitAgent(){
// 	sharedConstructor();
// }

WorkerAgent::WorkerAgent( BWAPI::Unit* mUnit):UnitAgent(mUnit)
,mRole(IDLE)
//,mState(NONE)
,mBuildType(BWAPI::UnitTypes::None)
,mBuildTilePos(BWAPI::TilePositions::None)
,gatherTarget(NULL)
{

}
WorkerAgent::~WorkerAgent(){}

bool WorkerAgent::isGatheringMinerals(){
	return pUnit->isGatheringMinerals();
}

void WorkerAgent::processActions()
{
	UnitAgent::processActions();
	if (isConstructing())
	{
		sBuildStructure();
	}
	
	// draw unit ID for debugging
// 	if (pUnit->isSelected())
// 	{
// 		Broodwar->drawTextMap(pUnit->getPosition().x(),pUnit->getPosition().y(),
// 			"{%i}",getID());
// 	}
	//else printWkrState();
}
void WorkerAgent::gather(  BWAPI::Unit* res )
{
	if (!res)
	{
		logfile << "Error in WorkerAgent gather: bad res pointer\n";
		return;
	}
	if (!res->getType().isResourceContainer())
	{
		logfile << "Error in WorkerAgent gather: not a resource\n";
		return;
	}
	if (!res->getPlayer()->isNeutral() && !(res->getPlayer()==Broodwar->self()) )
	{
		logfile << "Error in WorkerAgent gather: someone else owns it/n";
		return;
	}
	bool r = pUnit->gather(res);
	if (!r)
	{
		logfile << "Error: " << getNameID() << " failed to gather " << res->getType().getName() << endl;
	}
	else
	{
		logfile << Broodwar->getFrameCount() << " worker: " << getNameID() << " gather "
			<< res->getType().getName() << endl;
	}
	gatherTarget = res;
// 	setState(WorkerAgent::GATHERING);
 	setRole(WorkerAgent::GATHER_MINERALS);
	if ( BWAPIext::isGasPlant( res->getType() ) )
	{
		setRole(WorkerAgent::GATHER_GAS);
	}
}

void WorkerAgent::clearState()
{
	halt();
	gatherTarget = NULL;
// 	setState(NONE);
	setRole(WorkerAgent::IDLE);
}

void WorkerAgent::printWkrState() const
{
	string statename = "UNKNOWN";
// 	if (wkrState_ == GATHERING)
// 		statename = "Gathering";
// 	else if (wkrState_ == MOVE_TO_BUILD)
// 		statename = "MoveToBuild";
// 	else if (wkrState_ == BUILDING)
// 		statename = "Building";
// 	else if (wkrState_ == DONE_BUILDING)
// 		statename = "DoneBuilding";
// 	else if (wkrState_ == NONE)
// 		statename = "None";
	statename = pUnit->getOrder().getName();
	if (pUnit->isIdle())
	{
		statename += ":Idle";
	}
	Broodwar->drawTextMap(getPosition().x(),getPosition().y(),"%s",
		statename.c_str() );
}
/// ends in role=IDLE if success, role=FAILED if fail. Assumes buildType and pos are set.
void WorkerAgent::sBuildStructure()
{
	assert(isConstructing() && mBuildType != BWAPI::UnitTypes::None && mBuildTilePos.isValid());
	// check to be sure that the entire structure location is visible
	bool isBuildLocVisible = BWAPIext::isVisibleForBuild(
		mBuildTilePos, mBuildType);
	// if building location not visible, make sure we are moving there
	if (!isBuildLocVisible)
	{
		// check if worker destination is center of buildLocation
		Position buildPos(BWAPIext::getCenter(mBuildTilePos));
		if (pUnit->getOrderTargetPosition() != buildPos)
		{
			Broodwar->printf("buildloc not visible, set move");
			pUnit->move(buildPos);
		}
		// check if worker moving
		else if (!pUnit->isMoving())
		{
			pUnit->move(buildPos);
		}
	}
	// if building location visible, is worker building?
	else // entire build location is visible
	{
		if (pUnit->isConstructing())
		{	// worker is building, check if structure is created
			set< BWAPI::Unit*> unitsOnBuildLoc = getUnitsOnTile(mBuildTilePos);
			BOOST_FOREACH(  BWAPI::Unit* unit, unitsOnBuildLoc )
			{
				if (unit->getType() == mBuildType)
				{
					logfile << Broodwar->getFrameCount() << " Worker "
						<< getID() << " finished building " 
						<< mBuildTilePos.x() << ","
						<< mBuildTilePos.y() << ", autocancel\n";
					mRole = IDLE;
					return;
				}
			}
		}
		else
		{	// worker not building, order it to build
			// just order build again
			bool result = pUnit->build(mBuildTilePos,mBuildType);
			if (!result)
			{
				logfile << Broodwar->getFrameCount() << " Worker building failed.\n";
				// check canMake
				bool canMake = Broodwar->canMake(NULL,mBuildType);
				if( canMake )logfile << "can make " << mBuildType.getName() << ". ";
				canMake = Broodwar->canMake(pUnit, mBuildType);
				if (canMake) logfile << "Worker check. ";
				bool canBuildHere = Broodwar->canBuildHere(pUnit,mBuildTilePos,mBuildType,false);
				if (canBuildHere) logfile << "Location check. ";
				canBuildHere = Broodwar->canBuildHere(pUnit,mBuildTilePos,mBuildType,true);
				if ( canBuildHere )
				{
					logfile << "Visibility checked. ";
				}
				logfile << "\n";
				// fail if something is in the way so that we can replan
				mRole = FAILED;
			}
		}
	}
}

void WorkerAgent::buildStructure( UnitType toBuild, TilePosition buildLoc )
{
	mBuildTilePos = buildLoc;
	mBuildType = toBuild;
	mRole = BUILDER;
	// move to loc
	pUnit->move(Position(buildLoc));
}