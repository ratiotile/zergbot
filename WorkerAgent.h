#ifndef __WORKERAGENT_H__
#define __WORKERAGENT_H__

#include <BWAPI.h>
#include "UnitAgent.h"
#include "BuildHelper.h"
#include "Tasks/typedef_Task.h"
#include "Contractor.h"

class C_BuildUnit;
typedef boost::shared_ptr<C_BuildUnit> pCBU;

class WorkerAgent: public UnitAgent
{
public:
	enum wkr_role_t{IDLE,GATHER_MINERALS, GATHER_GAS, MILITIA, BUILDER, FAILED};
	//enum wkr_state{NONE,GATHERING,MOVING,BUILDING,DONE_BUILDING};
	//WorkerAgent();
	WorkerAgent(BWAPI::Unit* mUnit);
	~WorkerAgent();

	
	//inline wkr_state state()const {return wkrState_;} // get current worker state
	inline wkr_role_t role()const {return mRole;}
	//inline void setState(wkr_state newState) {wkrState_ = newState;}
	inline void setRole(wkr_role_t newRole) {mRole = newRole;}
	
	void processActions();
	
	void gather( BWAPI::Unit* res);
	/// reset role and state to default
	void clearState();
	/// true if state NONE or DONE_BUILDING
	inline bool isIdle() const 
		{return pUnit->isIdle();}
	inline bool isHarvesting() const {
		return pUnit->isGatheringGas() || pUnit->isGatheringMinerals();
	}
	// true if worker is moving to construct or is constructing a structure.
	bool isConstructing() const { return mRole == BUILDER; }
	// return true if worker is at work gathering minerals
	bool isGatheringMinerals();
	 BWAPI::Unit* getGatherTarget() {return gatherTarget;}

	//actions
	void buildStructure(UnitType toBuild, TilePosition buildLoc);

	/// debug, print wkr state
	void printWkrState()const;
private:



	wkr_role_t mRole;
	//wkr_state mState;
	// construction
	static const int pxCloseEnoughToBuild = 5*32; // pixels
	BWAPI::UnitType mBuildType;
	BWAPI::TilePosition mBuildTilePos;
	 BWAPI::Unit* gatherTarget;
	void sBuildStructure();
 	
};
#endif
