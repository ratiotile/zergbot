#include "ZergCommander.h"
#include "common/cpp_includes.h"

#include <list>
#include "BuildPlanner/BuildGoalPlanner.h"
#include "BuildPlanner//BuildGoal.h"
#include "Tasks/T_BuildOrder.h"
#include "Tasks/T_OverlordScout.h"
#include "Tasks/T_ZerglingRush.h"
#include "BuildPlanner/Openings.h"
#include "UnitManager.h"
#include "Tasks/T_OverlordScout.h"

ZergCommander::ZergCommander()
:Commander()
{

}

void ZergCommander::onGameStart()
{
	logfile << "ZergCommander onGameStart.\n";
	
	BuildGoal bg;
	bg.clearAll();
	bg.vunGoalUnits_.push_back(make_pair(BWAPI::UnitTypes::Zerg_Mutalisk,8));
	list<UnitType> bo = BuildGoalPlanner::get().generateBuildOrder(bg);

	mTasks.push_back( pTask( new T_ZerglingRush() ));
	
}

void ZergCommander::update()
{
	processTasks();
	displayTaskInfo();
	updateResources();
	// trigger overlord scout task
	static bool hasRunScoutTask = false;
	if ( !hasRunScoutTask
		&& Broodwar->getFrameCount() > 1
		&& Broodwar->self()->getRace() == BWAPI::Races::Zerg
		&& UnitManager::get().haveFreeUnit(BWAPI::UnitTypes::Zerg_Overlord))
	{
		mTasks.push_back( pTask( new T_OverlordScout() ));
		hasRunScoutTask = true;
		// some check to be sure run only once
// 		if (ScoutManager::getInstance().getScoutMax(BWAPI::UnitTypes::Zerg_Overlord) == 0)
// 		{
// 			ScoutManager::getInstance().setScoutMax(BWAPI::UnitTypes::Zerg_Overlord, 1);
// 		}
	}
}

void ZergCommander::onEndZerglingRush()
{
	// look for the task in mTasks
	pTask lingRushTask;
	list<pTask>::iterator it = mTasks.begin();
	for (it; it != mTasks.end(); ++it)
	{
		pTask task = *it;
		if (task->getName() == "Zergling Rush")
		{
			logfile << "Zergling Rush ending\n";
			return;
		}
	}
}