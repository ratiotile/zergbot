#include "Commander.h"

class ZergCommander: public Commander
{
public:
	ZergCommander();
	void onGameStart();
	void update();

	/// set remaining lings to watch enemy base choke for his army leaving
	void onEndZerglingRush();
protected:
private:
};