#include "Action_Attack.h"
#include "../common/cpp_includes.h"
#include "../UnitAgent.h"


Action_Attack::Action_Attack( UnitAgent* pUnitAgent ):
CompositeAction(pUnitAgent, action_attack)
{

}

void Action_Attack::activate()
{	// find nearest enemy unit and set as target
	logfile << BWAPIext::getUnitNameID(pUnitAgent_->getUnit()) 
		<< " Action_Attack activating.\n";
	logWriteBuffer();
	pUnitAgent_->attackNearestEnemy();
	status_ = active;
}

void Action_Attack::terminate()
{
	pUnitAgent_->clearTarget();
}

int Action_Attack::processActions()
{
	activateIfInactive();
	if (!pUnitAgent_->hasTarget())
	{
		status_ = complete;
	} 
	else if (!pUnitAgent_->getTarget()->isVisible())
	{	// look up last position in gameInfo
		if (pUnitAgent_->getUnit()->getOrder() != BWAPI::Orders::Move)
		{
			Position targetPos = 
				GameState::get().getBanditPosition(
					pUnitAgent_->getTarget());
			if (targetPos == BWAPI::Positions::None)
			{	// target dead
				status_ = complete;
			} else {	// move to last position
				pUnitAgent_->movementBehavior()->enable(MovementBehaviors::seek);
				pUnitAgent_->setDestination(targetPos);
			}
		}

	} else{	// attack if visible
		if (pUnitAgent_->getUnit()->getOrder() != BWAPI::Orders::AttackUnit)
		{
			pUnitAgent_->halt(); // clear destinations
			pUnitAgent_->getUnit()->attack(const_cast< BWAPI::Unit*>(pUnitAgent_->getTarget()));
		}
	}
	return status_;
}