#pragma once
#ifndef INCLUDED_ACTION_MOVE_H
#define INCLUDED_ACTION_MOVE_H

#include "CompositeAction.h"

class UnitAgent;

class Action_Attack : public CompositeAction
{
public:
	Action_Attack( UnitAgent* pUnitAgent );

	void activate();
	void terminate();
	int processActions();
	std::string getActionName() const {return "attack";}

};

#endif