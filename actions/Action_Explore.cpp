#include "Action_Explore.h"
#include "Action_Move.h"
#include "../common/cpp_includes.h"
#include "../ScoutManager.h"
#include "../UnitAgent.h"
#include "../Pathfinder.h"

Action_Explore::Action_Explore( UnitAgent* pUnitAgent ):
CompositeAction(pUnitAgent, action_explore),
exploreTarget_(BWAPI::Positions::None)
{

}

void Action_Explore::activate()
{	// ask for new scout mission
	status_ = active;
	logfile << BWAPIext::getUnitNameID(pUnitAgent_->getUnit())
		<< Broodwar->getFrameCount() << " Action_Explore activating\n";
	logWriteBuffer();
	Position nDest = ScoutManager::getInstance().getNextScoutLocation(
		pUnitAgent_->getUnit() );
	if (nDest != BWAPI::Positions::None)
	{
		addComponentAction( new Action_Move( pUnitAgent_, nDest ) );
		return;
	}
	logfile << "Action_Explore activate: did not receive any scout destination!\n";
}

void Action_Explore::terminate()
{	// report back to ScoutManager of mission success/fail
	logfile << BWAPIext::getUnitNameID(pUnitAgent_->getUnit())
		<< " terminating explore.\n";
	removeAllComponentActions();
	if (status_ == complete) // success!
	{
		ScoutManager::getInstance().scoutMissionSuccess(
			pUnitAgent_->getUnit(),
			pUnitAgent_->movementBehavior()->getDestination());
	}
	else
	{
		ScoutManager::getInstance().scoutMissionAbort(pUnitAgent_->getUnit());
	}
}

int Action_Explore::processActions()
{
	// usually we call this, to exec activate on first run
	activateIfInactive();
	// process sub-actions spawned in activate()
	// ignore for now, but will implement a moveTo action
// 	if ( pUnitAgent_->getPosition().getApproxDistance(
// 		pUnitAgent_->movementBehavior()->getDestination()) < 63 )
// 	{
// 		status_ = complete;
// 		// end explore action since enemies are nearby
// 	}
	if ( pUnitAgent_->isEnemyNearby() )
	{	// exit, in danger
		status_ = failed;
		logfile << BWAPIext::getUnitNameID(pUnitAgent_->getUnit())
			<< " Danger detected, cancelling explore, find Flee Path.\n";
		Pathfinder::getInstance().findFleePath( pUnitAgent_->getUnit() );
		// terminate will be called by parent
	}
	else if (!componentActions_.empty())
	{
		status_ = processComponentActions();
		//logfile << "Action_explore done processing sub actions, returning.\n";
		return status_;
	}
	
	return status_;
}