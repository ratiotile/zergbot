#pragma once
#ifndef INCLUDED_ACTION_EXPLORE_H
#define INCLUDED_ACTION_EXPLORE_H

#include "CompositeAction.h"
#include <BWAPI.h>

class UnitAgent;

class Action_Explore : public CompositeAction
{
public:
	Action_Explore( UnitAgent* pUnitAgent );

	void activate();
	void terminate();
	int processActions();
	std::string getActionName() const {return "explore";}
private:
	BWAPI::Position exploreTarget_;
};

#endif