#include "Action_FindTarget.h"
#include "../common/cpp_includes.h"
#include "../UnitAgent.h"
#include "UnitAction.h"

Action_FindTarget::Action_FindTarget( UnitAgent* pUnitAgent )
:UnitAction(pUnitAgent, action_findTarget)
{

}

void Action_FindTarget::activate()
{

}

void Action_FindTarget::terminate()
{

}

int Action_FindTarget::processActions()
{
	return active;
}