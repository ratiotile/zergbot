#pragma once
#ifndef INCLUDED_ACTION_FINDTARGET_H
#define INCLUDED_ACTION_FINDTARGET_H

#include "UnitAction.h"

class UnitAgent;

class Action_FindTarget : public UnitAction
{
public:
	Action_FindTarget( UnitAgent* pUnitAgent );

	void activate();
	void terminate();
	int processActions();
	std::string getActionName() const {return "FindTarget";}
};
#endif