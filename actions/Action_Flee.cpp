#include "Action_Flee.h"
#include "Action_Move.h"
#include "../common/cpp_includes.h"
#include "../UnitAgent.h"
#include "../MapInfo.h"

Action_Flee::Action_Flee( UnitAgent* pUnitAgent ):
 CompositeAction( pUnitAgent, action_flee ),
 fleeTarget_( BWAPI::Positions::None )
{

}

void Action_Flee::activate()
{
	status_ = active;
	logfile << BWAPIext::getUnitNameID(pUnitAgent_->getUnit())
		<< Broodwar->getFrameCount() << " Action_Flee activating\n";
	fleeTarget_ = Position(MapInfo::get().getNearestSafePosition( 
		pUnitAgent_->getPosition() ) );
	if (fleeTarget_ != BWAPI::Positions::None)
	{
		addComponentAction( new Action_Move( pUnitAgent_, fleeTarget_ ) );
		return;
	}
	logfile << "Action_Flee activate: no fleeTarget!\n";
}

void Action_Flee::terminate()
{
	logfile << BWAPIext::getUnitNameID(pUnitAgent_->getUnit())
		<< Broodwar->getFrameCount() << " terminating flee.\n";
	removeAllComponentActions();
}

int Action_Flee::processActions()
{
	activateIfInactive();
	// if enemy close, do something!
	if (!componentActions_.empty())
	{
		status_ = processComponentActions();
	}
	return status_;
}