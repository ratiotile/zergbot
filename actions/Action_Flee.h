#pragma once
#ifndef INCLUDED_ACTION_FLEE_H
#define INCLUDED_ACTION_FLEE_H

#include "CompositeAction.h"
#include <BWAPI.h>

class UnitAgent;

class Action_Flee : public CompositeAction
{
public:
	Action_Flee( UnitAgent* pUnitAgent );

	/// grab fleeTarget_ from RegionManager
	void activate();
	void terminate();
	int processActions();
	std::string getActionName() const { return "flee"; }
private:
	BWAPI::Position fleeTarget_;
};

#endif