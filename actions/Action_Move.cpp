#include "Action_Move.h"
#include "../common/cpp_includes.h"
#include "../UnitAgent.h"

Action_Move::Action_Move( UnitAgent* pUnitAgent, BWAPI::Position target )
:UnitAction( pUnitAgent, action_move ),
targetPos_(target)
{	
}

void Action_Move::activate()
{// simple direct move for now
	status_ = active;
	logfile << BWAPIext::getUnitNameID(pUnitAgent_->getUnit())
		<< " Unit move activated.\n";
	assert(targetPos_.isValid());
	pUnitAgent_->movementBehavior()->enable(MovementBehaviors::seek);
	pUnitAgent_->setDestination(targetPos_);

}

void Action_Move::terminate()
{
	pUnitAgent_->movementBehavior()->disable();
	if (status_ == complete)
	{
		logfile << BWAPIext::getUnitNameID(pUnitAgent_->getUnit())
			<< " Unit move complete.\n";
	}
	else
	{
		logfile << BWAPIext::getUnitNameID(pUnitAgent_->getUnit())
			<< " Unit move terminated, incomplete.\n";
	}
}

int Action_Move::processActions()
{
	activateIfInactive();
	if ( pUnitAgent_->getPosition().getApproxDistance(targetPos_) < 16 )
	{
		status_ = complete;
	}
	return status_;
}