#pragma once
#ifndef INCLUDED_ACTION_MOVE_H
#define INCLUDED_ACTION_MOVE_H

#include "CompositeAction.h"
#include <bwapi.h>

class UnitAgent;
/** Action_Move simply orders the UnitAgent to rightclick move to its
destination, a Position that is held directly by the Action.
For now, a simple move is all. No double-checking unless unit is stopped.
*/
class Action_Move : public UnitAction
{
public:
	Action_Move( UnitAgent* pUnitAgent, BWAPI::Position target );

	void activate();
	void terminate();
	int processActions();
	std::string getActiveActionName() const { return "move"; }

private:
	/// where move command to go
	BWAPI::Position targetPos_;
};

#endif