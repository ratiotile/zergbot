#include "Action_Null.h"
#include "../common/cpp_includes.h"
#include "../UnitAgent.h"

Action_Null::Action_Null( UnitAgent* pUnitAgent ):
UnitAction( pUnitAgent, action_null )
{

}

void Action_Null::activate()
{
	status_ = active;
	string msg = pUnitAgent_->getNameID() + " activating Action_Null.";
	logfile << msg << endl;
	Broodwar->printf( msg.c_str() );
}

void Action_Null::terminate()
{
	string msg = pUnitAgent_->getNameID() + " terminating Action_Null.";
	logfile << msg << endl;
	Broodwar->printf( msg.c_str() );
}

int Action_Null::processActions()
{
	return status_;
}