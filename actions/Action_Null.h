#pragma once
#ifndef INCLUDED_ACTION_NULL_H
#define INCLUDED_ACTION_NULL_H

#include "UnitAction.h"

class UnitAgent;

class Action_Null : public UnitAction
{
public:
	Action_Null( UnitAgent* pUnitAgent );

	void activate();
	void terminate();
	int processActions();
};

#endif