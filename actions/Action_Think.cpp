#include "Action_Think.h"
#include "../common/cpp_includes.h"
#include "../UnitAgent.h"
#include "Action_Attack.h"
#include "Action_Explore.h"
#include "Action_Null.h"
#include "Action_Flee.h"

Action_Think::Action_Think( UnitAgent* pUnitAgent ):
CompositeAction( pUnitAgent, action_think)
{
	pUnitAgent_ = pUnitAgent;
}
/// activates thinking: determine best action to spawn. Will automatically 
/// go inactive to replan when spawned action terminates.
void Action_Think::activate()
{
	status_ = active;
	evaluateActions();
}

void Action_Think::terminate()
{
	// think only terminates on unitAgent death
	removeAllComponentActions();
}

int Action_Think::processActions()
{	// always call on unit update
	// if inactive, adds new subactions
	activateIfInactive();
	// PCA must handle case of empty list!
	int actionStatus = processComponentActions();
	// deactivate if sub-actions completed
	if (actionStatus == complete || actionStatus == failed)
	{
		status_ = inactive;
	}
	return status_;
}

void Action_Think::evaluateActions()
{
// if nearby enemy, attack
// else, explore
	//assert( componentActions_.empty() );
	if ( pUnitAgent_->isEnemyNearby() )
	{	// attack
		initiateAction_Flee();
	}
	else if ( GameState::get().isMapAnalyzed() )
	{	// explore
		initiateAction_Explore();
	}
	else
	{
		//removeAllComponentActions();
		//addComponentAction( new Action_Null(pUnitAgent_) ); 
	}
}

void Action_Think::initiateAction_Explore()
{
	if ( actionNotInitiated(action_explore) )
	{
		logfile << BWAPIext::getUnitNameID(pUnitAgent_->getUnit())
			<< " Action_Think activated, choosing to explore\n";
		logWriteBuffer();
		removeAllComponentActions();
		addComponentAction( new Action_Explore(pUnitAgent_) );
	}
}

void Action_Think::initiateAction_Attack()
{
	if ( actionNotInitiated(action_attack) )
	{
		logfile << BWAPIext::getUnitNameID(pUnitAgent_->getUnit()) 
			<< " Action_Think activated, choosing to attack\n";
		logWriteBuffer();
		removeAllComponentActions();
		addComponentAction( new Action_Attack(pUnitAgent_) );
	}
}

void Action_Think::initiateAction_Flee()
{
	if ( actionNotInitiated(action_flee) )
	{
		logfile << BWAPIext::getUnitNameID(pUnitAgent_->getUnit()) 
			<< " Action_Flee chosen from Think\n";
		logWriteBuffer();
		removeAllComponentActions();
		addComponentAction( new Action_Flee(pUnitAgent_) );
	}
}