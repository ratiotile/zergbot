#pragma once
#ifndef INCLUDED_ACTION_THINK_H
#define INCLUDED_ACTION_THINK_H

#include "CompositeAction.h"

class UnitAgent;

class Action_Think : public CompositeAction
{
public:
	Action_Think( UnitAgent* pUnitAgent );

	void activate();
	void terminate();
	int processActions();

private:
	void evaluateActions();
	// helpers
	/// Add explore action to list, wiping list
	void initiateAction_Explore();
	/// Add attack action to list, wiping list
	void initiateAction_Attack();
	/// Add flee action to list, wiping list
	void initiateAction_Flee();
	std::string getActionName() const {return "think";}
};

#endif