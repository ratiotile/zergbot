#include "CompositeAction.h"
#include "..\common/cpp_includes.h"
CompositeAction::CompositeAction( UnitAgent* pUnitAgent, unitAction_type type )
:UnitAction( pUnitAgent, type )
{
}

CompositeAction::~CompositeAction()
{
}
/// clears componentAction_ list; ensure that each terminate is called.
void CompositeAction::removeAllComponentActions()
{
	for ( ActionList::iterator iter = componentActions_.begin();
		iter != componentActions_.end(); iter++ )
	{
		(*iter)->terminate();
		delete *iter;
	}
	componentActions_.clear();
}

int CompositeAction::processComponentActions()
{
	while ( !componentActions_.empty() && 
			( componentActions_.front()->hasFailed()
			|| componentActions_.front()->isComplete() ) )
	{
		// the front-most action is complete or failed, so remove it
		componentActions_.front()->terminate(); // clean up
		delete componentActions_.front();
		componentActions_.pop_front();
	}
	if ( !componentActions_.empty() )
	{
		int componentStatus = componentActions_.front()->processActions();
		// don't want to return complete if there still are components remaining
		if ( componentStatus == complete && componentActions_.size() > 1 )
		{
			return active;
		}
		return componentStatus;
	}
	else
	{
		return complete;
	}
}

void CompositeAction::addComponentAction( UnitAction* pAction )
{
	componentActions_.push_front(pAction);
}

bool CompositeAction::actionNotInitiated( unitAction_type actionType ) const
{
	if ( !componentActions_.empty() )
	{
		return componentActions_.front()->GetType() != actionType;
	}
	return true;
}

std::string CompositeAction::getActiveActionName() const
{
	if ( !componentActions_.empty() )
	{
		return componentActions_.front()->getActiveActionName();
// 		BOOST_FOREACH( UnitAction* action, componentActions_ )
// 		{
// 			if (action->isActive())
// 			{
// // 				string name =  action->getActiveActionName();
// // 				logfile << getActionName() << 
// // 					": try to get action name for " << name <<endl;
// 				return action->getActiveActionName();
// 			}
// 		}
	}
		
	return getActionName();
}