#pragma once
#ifndef INCLUDED_COMPOSITEACTION_H
#define INCLUDED_COMPOSITEACTION_H

#include <list>
#include "UnitAction.h"

class CompositeAction: public UnitAction
{
public:
	CompositeAction( UnitAgent* pUnitAgent, unitAction_type type );
	virtual ~CompositeAction();
	virtual void activate() = 0;
	virtual void terminate() = 0;
	virtual int processActions() = 0;
	void removeAllComponentActions();
	void addComponentAction( UnitAction* pAction );protected:
	// accessors
	bool actionNotInitiated( unitAction_type actionType ) const;
	std::string getActiveActionName() const;
	virtual std::string getActionName() const = 0;
protected:
	typedef std::list<UnitAction*> ActionList;
	ActionList componentActions_;
	int processComponentActions();
};

#endif

