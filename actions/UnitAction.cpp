#include "UnitAction.h"
#include "..\common/cpp_includes.h"

UnitAction::UnitAction( UnitAgent* pUnitAgent, unitAction_type type ): 
pUnitAgent_(pUnitAgent),
type_(type),
status_(inactive)
{

}

UnitAction::~UnitAction()
{

}
/// if inactive, call activate()
void UnitAction::activateIfInactive()
{
	if (isInactive())
	{
		activate();
	}
}
/// if status is failed, set to inactive
void UnitAction::reactivateIfFailed()
{
	if (hasFailed())
	{
		status_ = inactive;
	}
}