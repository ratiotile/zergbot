#pragma once
#ifndef INCLUDED_UNITACTION_H
#define INCLUDED_UNITACTION_H

#include "UnitActionTypes.h"
#include <stdexcept>

class UnitAgent;

class UnitAction
{
public:
	enum status_t{ active = 0, inactive = 1, complete = 2, failed = 3 };

	/// State to inactive, set pUnitAgent and type using params
	UnitAction( UnitAgent* pUnitAgent, unitAction_type type );
	virtual ~UnitAction();
	/// execute when action is activated
	virtual void activate() = 0;
	/// execute when action is ended
	virtual void terminate() = 0;
	/// call each step to update actions, return status
	virtual int processActions() = 0;
	/// descendants must implement to add composite actions
	virtual void addComponentAction( UnitAction* pAction )
	{throw std::runtime_error("UnitAction is atomic - cannot add component actions!");}

	// accessors
	bool isComplete()const{ return status_ == complete; } 
	bool isActive()const{ return status_ == active; }
	bool isInactive()const{ return status_ == inactive; }
	bool hasFailed()const{ return status_ == failed; }
	unitAction_type GetType()const{ return type_; }
	virtual std::string getActiveActionName() const = 0 ;
protected:
	unitAction_type type_;
	/// conforms to status_t
	int status_;
	/// the owner of this action, the unit that this action is controlling
	UnitAgent* pUnitAgent_;
	/// holds the queue of component actions
	
	/// if inactive, set to active and call activate
	void activateIfInactive();
	/// if failed, set to inactive to be re-planned
	void reactivateIfFailed();
};

#endif