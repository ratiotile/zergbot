#pragma once
#ifndef INCLUDED_UNITACTIONTYPES_H
#define INCLUDED_UNITACTIONTYPES_H

enum unitAction_type
{
	action_think,	// decide on next action
	action_explore,	// explore map
	action_attack,
	action_findTarget,	// look for target to attack
	action_move,	// simply move
	action_null,	// do nothing
	action_flee		// run to safety
};

#endif