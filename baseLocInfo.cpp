#include "baseLocInfo.h"
#include "common/cpp_includes.h"
#include "TABW/ResourceCluster.h"
#include "GameState.h"

baseLocInfo::baseLocInfo( TABW::pResourceCluster cluster ):
mDepotPosition(cluster->depotPosition()),
controller(NULL),
frameLastVisible(0),
priority(0),
region(NULL),
groundDistFromStart(0),
reachableFromStart(false),
isNat(false)
{
	set<UIptr> clusterRes(cluster->resources());
	BOOST_FOREACH(UIptr ui, clusterRes)
	{
		// translate to GameState UnitInfo
		UIptr resInfo = GameState::get().getUnitInfo(ui->getUnit());
		assert(resInfo);
		if (resInfo->getType() == BWAPI::UnitTypes::Resource_Vespene_Geyser)
		{
			geyser = resInfo;
		}
		mResources.insert(resInfo);
	}
//	logfile << "BaseLocInfo, " << mResources.size() << " resources added.\n";
}

BWAPI::Position baseLocInfo::center() const
{
	if (mDepotPosition.isValid())
	{
		int depotWidth = 4*32;
		int depotHeight = 3*32;
		Position topLeft(mDepotPosition);
		return Position(topLeft.x()+depotWidth/2, topLeft.y()+depotHeight/2);
	}
	return BWAPI::Positions::Invalid;
}

bool baseLocInfo::hasGas() const
{
	BOOST_FOREACH(UIptr res, mResources)
	{
		if (res->getType() == BWAPI::UnitTypes::Resource_Vespene_Geyser )
		{
			//logfile << "has gas!\n";
			return true;
		}
	}
	return false;
}