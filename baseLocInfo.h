#pragma once
#include "TABW/TABW.h"
#include <bwapi.h>
#include "common/typedefs.h"

class TABW::ResourceCluster;
class regionInfo;

class baseLocInfo
{
public:
	baseLocInfo(TABW::pResourceCluster cluster);
	/// Top Left tile to build resource depot on.
	BWAPI::TilePosition depotPosition() const {return mDepotPosition;}
	/// returns center of depotPosition
	BWAPI::Position center() const;
	/// if someone has a base here, else NULL
	BWAPI::Player* controller;			
	/// last frame visible
	unsigned int frameLastVisible;	
	int priority;
	/// region this base belongs to
	regionInfo* region;
	SetUI resources() const {return mResources;}
	bool hasGas() const; 
	int groundDistFromStart;	// how far this base is from our start
	bool reachableFromStart;	// can be walked to from starting location
	// tl tile of geyser, none if there is no gas
	UIptr geyser;
	/// is this a natural or not? need calc in MapInfo.
	bool isNat;
private:
	BWAPI::TilePosition mDepotPosition;
	/// from TABW?
	std::set<UIptr> mResources;
};