#include "ANCluster.h"
#include "ScanUnit.h"
#include "../common/cpp_includes.h"

ANCluster::ANCluster( int threshold, double rangeFactor, double speedFactor ):
ClusterMethod(),
threshold_(threshold),
rangeFactor_(rangeFactor),
speedFactor_(speedFactor),
isInit_(false),
maxCoverRange_(12)
{
	assert( threshold_ > 0 && rangeFactor_ >= 0 && speedFactor_ >= 0 );
}

void ANCluster::onUnitMoveTile( UIptr unit, setUI& unitSet )
{
	//updateSingleUnit(unit); // replace with group batch update!
	bool changed = updateLinks( unit, unitSet );
	//call groupUpdate if changed
	if( changed) updateGroup( unit, unitSet, unit->clusterID() );
}
/** Precondition: none 
	Postcondition: all units will have correct Covers and Coveredby associations.
*/
void ANCluster::calculateLinks( setUI& units )
{
	for ( setUI::iterator iter = units.begin(); iter != units.end(); iter++ )
	{
		int coverRange = calculateCoverRange( *iter );
		//getUnitsInRadius( *iter, coverRange );
		getUnitsInRadius( units, *iter, coverRange );
		// add each neighbor to covers set and reciprocal coveredBy
		for ( setUI::iterator niter = neighbors_.begin(); niter != neighbors_.end();
			niter++ )
		{
			(*iter)->addCovers( *niter );
			(*niter)->addCoveredBy( *iter );
		}
	}
	isInit_ = true;
}
void ANCluster::removeLinks( UIptr unit )
{
	for ( setUI::iterator ci = unit->covers().begin();
		ci != unit->covers().end();  )
	{
		(*ci)->removeCoveredBy( unit );
		ci = unit->covers().erase(ci);
	}
	for ( setUI::iterator cb = unit->coveredBy().begin();
		cb != unit->coveredBy().end();  )
	{	// use cover range of unit coveredBY
		(*cb)->removeCovers( unit );
		cb = unit->coveredBy().erase(cb);
	}
}
/**	Removes outdated links from a single unit, then searches for new ones.

*/
bool ANCluster::updateLinks( UIptr unit, setUI& unitSet )
{	// check for any invalid links and delete them
	bool changed = false;
	if (unit->isNotCombatActive()
		|| unit->isNonCombat() )
	{
		removeLinks(unit);
		changed = true;
	}
	else
	{
		int coverRange = calculateCoverRange( unit );
		for ( setUI::iterator ci = unit->covers().begin();
			 ci != unit->covers().end();  )
		{
			if ( unit->getPosition().getApproxDistance(
				(*ci)->getPosition() ) > coverRange ||
				(unit == *ci) || (*ci)->isNonCombat() )
			{	// remove link
				(*ci)->removeCoveredBy( unit );
				ci = unit->covers().erase(ci);
				changed = true;
			} else ci++;
		}
		for ( setUI::iterator cb = unit->coveredBy().begin();
			cb != unit->coveredBy().end();  )
		{	// use cover range of unit coveredBY
			int tCoverRange = calculateCoverRange( *cb );
			if ( unit->getPosition().getApproxDistance(
				(*cb)->getPosition() ) > tCoverRange ||
				(unit == *cb) || (*cb)->isNonCombat() )
			{	// remove link
				(*cb)->removeCovers( unit );
				cb = unit->coveredBy().erase(cb);
				changed = true;
			} else cb++;
		}
		// search for new links
		assert(coverRange <= maxCoverRange_*32);
		neighbors_.clear();
		//getUnitsInRadius( &unit, coverRange );
		getUnitsInRadius( unitSet, unit, coverRange );
		for ( setUI::iterator niter = neighbors_.begin(); niter != neighbors_.end();
			niter++ )
		{	// don't add self, only add same player, no mines, no harvesters
			if ( unit != *niter && 
				unit->getUnit()->getPlayer() == (*niter)->getUnit()->getPlayer()
				&& !(*niter)->isNotCombatActive() 
				&& !(*niter)->isNonCombat() )
			{
				// only attempt adding reciprocal link if it is not already exist
				if ( unit->addCovers( *niter ) )
				{
					(*niter)->addCoveredBy( unit );
				}
				// check if this unit may be covered by the other.
				if ( unit->getDistance(*niter) <= 
					calculateCoverRange( *niter ) )
				{
					if ((*niter)->addCovers(unit))
					{
						unit->addCoveredBy( *niter );
						changed = true;
					}
				}
			}
			else
			{

			}
		}
	}
	return changed;
}

int ANCluster::calculateCoverRange( const UIptr unit )
{	// speed is px/frame, range is in px
	UnitType unitType = unit->getType();
	const BWAPI::Unit* pUnit = unit->getUnit();
	int range = max( unitType.groundWeapon().maxRange(), 
		unitType.airWeapon().maxRange() );
	int coverRange = speedFactor_*pUnit->getPlayer()->topSpeed(unitType) 
		+ rangeFactor_*range;
	if (coverRange > maxCoverRange_*32) coverRange = maxCoverRange_*32;
	return coverRange;
}

void ANCluster::updateClusters( setUI& unitSet )
{
	if (!isInit_) calculateLinks( unitSet );
	// reset
	neighbors_.clear();
	toVisit_.clear();
	usedIDs_.clear();
	UIscan_.clear();
	// fill UIscan_
	for ( setUI::iterator iter = unitSet.begin(); 
		iter != unitSet.end(); iter++ )
	{
		UIscan_.insert( make_pair(*iter, ScanUnit(*iter)) );
	}
	// begin, use iterator through UIscan and check if visited;
	for ( scanMap::iterator iter = UIscan_.begin(); 
		iter != UIscan_.end(); iter++ )
	{
		if( !iter->second.visited )
		{
			currentID_ = 0; // reset
			// if neighbors above thresh, add them toVisit.
			visit( iter->first );

			// propagate cluster by visiting all in toVisit_, adding
			listUI::iterator vi = toVisit_.begin();
			while( vi != toVisit_.end() )
			{
				assert( UIscan_.find(*vi) != UIscan_.end() );
				if ( UIscan_[*vi].visited )	// !!!!!!!!!!!!!!!!!!!!!!!!!!May be better way
				{
					vi++;
				}
				else
				{
					visit( *vi );
					vi++;
				}
			}
		}
	}
	applyCIDs();
	recycleCIDs();
}

void ANCluster::visit( UIptr currUnit )
{	// first check number of connections
	assert( UIscan_.find(currUnit) != UIscan_.end() );
	ScanUnit& currScan = UIscan_[currUnit];
	currScan.visited = true;
	listUI connections;
	for ( setUI::iterator i = currUnit->covers().begin();
		i != currUnit->covers().end(); i++ )
	{
		if ( currUnit->coveredBy(*i) )
		{	// any unit both covered by and covering this unit is connection
			connections.push_back(*i);
		}
	}
	if ( (int)connections.size() >= threshold_ )
	{	
		if (currentID_ == 0) // try to start new group
		{	// get new cID if unit has none or an already used cID
			if ( currUnit->clusterID() == 0 ||
				usedIDs_.count(currUnit->clusterID()) == 1 )
			{
				currentID_ = getNextID();
			} 
			else // use unit's cID if unused (default)
			{
				currentID_ = currUnit->clusterID();
			}
			currScan.newGroup = currentID_;
			usedIDs_.insert(currentID_);
		}
		else // continue currentID group
		{	// assume this unit unvisited
			currScan.newGroup = currentID_;
		}
		// in any case, insert unvisited connections to be visited
		for ( listUI::iterator i = connections.begin();
			i != connections.end(); i++ )
		{
			if( !UIscan_[*i].visited ) 
			{
				toVisit_.push_back(*i);
			}
		}
	}
	else	// isolated; put into group as leaf if expanding or set as noise
	{
		currScan.newGroup = 0;
	}
}

/** Precondition: triggered by onUnitMoveTile, valid groupID to update
		Unit Links have already been updated, since it moved
	Postcondition: group members and neighbors updated with new groupIDs,
		stored in UIscan to be applied later.

	groupSet = all units, units with cID will be placed in GroupMembers
	cID = the ID of the target group to update
*/
void ANCluster::updateGroup( UIptr ui, setUI& groupSet, int cID )
{
	// reset
	neighbors_.clear();
	toVisit_.clear();
	UIscan_.clear();
	// store group members; toVisit used by visit function
	// use toVisit for individual clusters, in case of split group
	// go back at end and give largest cluster original cID
	// only need track largest group and cluster size, compare each to;
	int largestClusterID = 0;
	int largestClusterSize = 0;
	setUI groupMembers;
	if (cID == 0)	// do prelim radius scan to find parent group
	{
// 		for ( setUI::iterator ni = currUnit->coveredBy().begin()
// 			; ni != currUnit->coveredBy().end(); ni++ )
// 		{	// test for 2way link
// 			if ( currUnit->covers(*ni) )
// 		}
		// if new group, keep cID = 0
		groupMembers.insert(ui);
	}
	else
	{
		BOOST_FOREACH( UIptr ui, groupSet )
		{
			UIscan_.insert( make_pair(ui, ScanUnit(ui)) );
			if ( ui->clusterID() == cID )
			{
				groupMembers.insert(ui);
			}// groupMembers will contain all units with matching cID
		}// UIscan includes all units
	}
	// want to visit all group members eventually
	// place one unit into toVisit to start off
	if (groupMembers.empty()) groupMembers.insert(ui);
	assert( !groupMembers.empty() );
	toVisit_.push_back(*groupMembers.begin());
	setUI::iterator gi = groupMembers.begin();
	
	// outer loop finds unconnected clusters
	while( gi != groupMembers.end() )
	{	// only start new cluster if not unit is not visited
		if ( !UIscan_[*gi].visited )
		{
			currentID_ = cID;// ID of this cluster
			if (currentID_ != 0) currentID_ = getNextID();
			toVisit_.clear();	// clear list for accurate count
			visitGroup( *gi, currentID_ );// visit first of cluster to begin
			listUI::iterator vi = toVisit_.begin();
			// rest of neighbors will be placed into toVisit
			while( vi != toVisit_.end() )
			{	// not visited, 
				if ( !UIscan_[*vi].visited )
				{	// visit and place neighbors into toVisit
					visitGroup(*vi, currentID_);
					++vi;
				} else { // erase already visited unit from list for counting
					vi = toVisit_.erase(vi);
				}
			}
			// count and compare size of group
			// need to delete visited members in toVisit to count properly
			if ( toVisit_.size() > largestClusterSize )
			{
				largestClusterSize = toVisit_.size();
				largestClusterID = currentID_;
			}
		}
		++gi; // incrememt visited or not
	}

	// v2 swap: track #units and cID counts for each new group cID, then
	// swap in order of population to highest original cID count.
	
	map<int,SwapGroup> mCID;
	// for scanmap, create a SwapGroup for each new ID; track old ID counts 
	for ( scanMap::iterator smi = UIscan_.begin(); smi != UIscan_.end()
		; ++smi )
	{
		if (smi->second.newGroup != 0 && smi->first->clusterID() != 0)
		{
			mCID[smi->second.newGroup].addCID(smi->first->clusterID());
		}
	}
	// starting with largest group, swap to most common old ID unless
	// exists any members of cID outside scanMap, or ID already taken;
	// keep the new cID in that case 
	list<int> groupsBySize; // sort groups by ID
	while ( groupsBySize.size() != mCID.size() )
	{
		int maxSize = 0;
		int maxID = 0;
		for ( map<int,SwapGroup>::const_iterator citer = mCID.begin(); 
			citer != mCID.end(); ++citer )
		{	// skip ones that are in sorted list
			bool isSorted = false;
			for ( list<int>::const_iterator siter = groupsBySize.begin();
				siter != groupsBySize.end(); ++siter )
			{
				if ( *siter == citer->first )
				{
					isSorted = true;
					break;
				}
			}
			if ( isSorted ) continue;
			else {	// check if current one is largest
				if (citer->second.size_ > maxSize)
				{
					maxSize = citer->second.size_;
					maxID = citer->first;
				}
			}
		}
		// have found the next largest
		assert(maxID != 0);
		groupsBySize.push_back(maxID);
	} // now groupsBySize contains newIDs in descending order of group size
	// iterate through groupsBySize, executing swap if able
	for ( list<int>::const_iterator siter = groupsBySize.begin();
		siter != groupsBySize.end(); ++siter )
	{
		int currentNewID = *siter;
		// retrieve target ID: most common old ID of group
		int targetID = 0;
		int maxCount = 0;
		const map<int,int>& mCounts = mCID[*siter].oldCID_counts;
		for (map<int,int>::const_iterator citer = mCounts.begin(); 
			citer != mCounts.end(); ++citer)
		{
			if ( citer->second > maxCount )
			{
				maxCount = citer->second;	// second is count
				targetID = citer->first;	// first is old ID
			}
		}	// targetID is most common original ID, now check in-use
		assert( targetID != 0 );
		// use the usedIDs_ set to represent those new IDs not applied
		bool isUnusedID = true;
		// cover all IDs set to be switched, within UIscan, excluding current group
		typedef pair<UIptr,ScanUnit> pairScanMap;
		BOOST_FOREACH( pairScanMap pUIscan, UIscan_ )
		{
			if ( pUIscan.second.newGroup != currentNewID)
			{
				if (pUIscan.second.newGroup == targetID)
				{
					isUnusedID = false;
					break;
				}
			}
		}
		// now check all units not included in scanMap, groupSet contains
		BOOST_FOREACH( UIptr pUI, groupSet )
		{	// if has same ID, and is not in UIscan
			if ( pUI->clusterID() == targetID && UIscan_.count(pUI) == 0 )
			{
				isUnusedID = false;
				break;
			}
		}
		if (isUnusedID)
		{	// make the switch from current to targetID
			usedIDs_.insert(targetID);
			usedIDs_.erase(currentNewID);
			for ( scanMap::iterator smi = UIscan_.begin()
				; smi != UIscan_.end(); smi++ )
			{
				if (smi->second.newGroup == currentNewID)
				{
					smi->second.newGroup = targetID;
				}
			}
		}
	}
	applyCIDs();
	recycleCIDs();
}
/**	Precon: cID contains current group ID, is valid non-zero; 
			toVisit contains neighbors of group cID;
			Links have been updated (every time a unit moves)
			UIScan_ includes all units; new units may be grouped
			currUnit = unit to visit
	Postcon: toVisit contains any additional neighbors of currUnit
			UIscan_ is updated with new group for current Unit, and visited
			currUnit unit has lastMoveTile reset to current position
*/
void ANCluster::visitGroup( UIptr currUnit, int cID )
{	// cID is the group that Unit will be assigned to if pass threshold
	assert( UIscan_.find(currUnit) != UIscan_.end() );
	ScanUnit& currScan = UIscan_[currUnit];
	// reset lastMoveTile, to avoid excess triggers
	//currUnit->resetLastMoveTile();
	//updateLinks(*currUnit);
	currScan.visited = true;
	currScan.newGroup = 0;	// in case doesn't pass connection threshold
	// neighbors set used to store 2-way links
	listUI connections;
	setUI::iterator ni = currUnit->coveredBy().begin();
	// find those exceeding threshold # 2way links, add to toVisit
	for ( ni; ni != currUnit->coveredBy().end(); ni++ )
	{	// test for 2way link
		if ( currUnit->covers(*ni) )
		{	// add to neighbors
			connections.push_back( *ni );
// 			if (currentID_ == 0 && (*ni)->clusterID() != 0 )
// 			{
// 				currentID_ = (*ni)->clusterID();
// 			}
		}
	}
	if ( connections.size() >= threshold_ )
	{	// assign to current cID group
		// will current cID ever be 0? Up to updateGroup to feed valid cID
		if (currentID_ == 0)
		{
			currentID_ = getNextID();
		}
		currScan.newGroup = currentID_;
		// add unvisited connections to be visited
		for ( listUI::iterator i = connections.begin();
			i != connections.end(); i++ )
		{
			if( !UIscan_[*i].visited ) 
			{
				toVisit_.push_back(*i);
			}
		}
	}
}

