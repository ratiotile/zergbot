#pragma once
#ifndef INCLUDED_ANCLUSTER_H
#define INCLUDED_ANCLUSTER_H

#include "ClusterMethod.h"
#include <set>
#include <BWAPI.h>
#include "../common/typedefs.h"


class ANCluster: public ClusterMethod
{
	typedef std::set<UIptr> setUI;
public:
	/// threshold=number of neighbors for group, range/speed=multipliers
	ANCluster( int threshold, double rangeFactor, double speedFactor );
	/// run when unit has moved a tile since last run
	void onUnitMoveTile( UIptr unit, setUI& unitSet );
	/// full update: should only run once at startup
	void calculateLinks( setUI& units );
	/// full update of clusters, try not to run
	void updateClusters( setUI& unitSet );
	/// remove links on unit death
	void removeLinks( UIptr unit );

private:
	bool isInit_;
	/// minimum num of neighbors to consider part of cluster 
	int threshold_;
	/// weight on unit range for coverRange
	double rangeFactor_;
	/// weight on unit speed for coverRange
	double speedFactor_;
	int maxCoverRange_;
	

	/// helper; checks CoveredBy sets and remove invalid links
	bool updateLinks( UIptr unit,  setUI& unitSet );
	/// helper; calculates and returns cover range in pixels for a unit type
	int calculateCoverRange( const UIptr unit );
	/// helper; mark visited, set group, add unvisited connections to toVisit_
	void visit( UIptr currUnit );
	/// replacement; updates entire group and neighboring units onUnitMoveTile
	void updateGroup( UIptr ui, setUI& groupSet, int cID );
	/// helper for updateGroup; visit all members and neighbors, reset lastTile
	void visitGroup( UIptr currUnit, int cID );
};

#endif