#include "ClusterManager.h"
#include "../common/cpp_includes.h"
//#include "GameState.h" // unneeded?
#include "DBSCAN.h"
#include "ANCluster.h"

ClusterManager::ClusterManager( int useAlg ):
DBSCAN_(NULL),
pANCluster_(NULL),
useAlg_(useAlg)
{
	DBSCAN_ = new DBSCAN( 3, 6*32 );
	pANCluster_ = new ANCluster( 1, 1, 24 );
	if( useAlg_ < 0 || useAlg_ > 1 )
	{
		throw "ClusterManager invalid algorithm type!";
	}
}

ClusterManager::~ClusterManager()
{
	delete DBSCAN_;
	DBSCAN_ = NULL;
	delete pANCluster_;
	pANCluster_ = NULL;
}

void ClusterManager::atFrameBegin()
{
	updateClusters();
}

void ClusterManager::updateClusters()
{
	if (useAlg_ == 0)
	{
		DBSCAN_->updateClusters( unitSet_ );
	}
	else if (useAlg_ == 1)
	{
		pANCluster_->updateClusters( unitSet_ );
		pANCluster_->scanForUnusedIDs(unitSet_);
	}
}

void ClusterManager::addUnitInfo( UIptr newUI )
{
	assert( newUI != UnitInfo::Null );
	unitSet_.insert(newUI);
	if (pANCluster_)
	{
		pANCluster_->onUnitMoveTile( newUI, unitSet_ );
	}
}

void ClusterManager::removeUnitInfo( UIptr remUI )
{
	if (pANCluster_)	// remove links on death
	{
		pANCluster_->removeLinks( remUI );
	}
	setUI::iterator found = unitSet_.find(remUI);
	if (found != unitSet_.end())
	{
		unitSet_.erase(found);
	}
}

void ClusterManager::onUnitMoveTile( UIptr pUI )
{
	if (pANCluster_)
	{
		pANCluster_->onUnitMoveTile( pUI, unitSet_ );
	}
}

boost::signals2::connection ClusterManager::connectUCCS( 
	const uccsig_t::slot_type &subscriber )
{
	return pANCluster_->connect(subscriber);
}

void ClusterManager::setClusters_DBSCAN( std::set<UIptr>& population, 
										int thresh, int radius )
{
	logfile << "ClusterManager getting DBSCAN clusters.\n";
	logWriteBuffer();
	DBSCAN_->updateClusters( population, thresh, radius );
}