#pragma once
#ifndef INCLUDED_CLUSTERMANAGER_H
#define INCLUDED_CLUSTERMANAGER_H

#include <assert.h>
#include <set>
#include <map>
#include <boost/signals2.hpp>
#include "../common/typedefs.h"
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#include "../common/UnitInfo.h"
class DBSCAN;
class ANCluster;
class ClusterManager
{
public:
	enum cluster_alg{
		alg_DBSCAN = 0, alg_ANC = 1
	};
	ClusterManager( int useAlg );
	~ClusterManager();
	/// call at very start of each frame, to update group associations
	void atFrameBegin();
	/// add to tracking a new unit
	void addUnitInfo( UIptr newUI );
	/// remove a unit from cluster tracking
	void removeUnitInfo( UIptr remUI );
	/// call whenever a unit moves more than a tile
	void onUnitMoveTile( UIptr pUI );
	/// connect a slot to the unit cluster change signal
	boost::signals2::connection connectUCCS( 
		const uccsig_t::slot_type &subscriber );
	/// use DBSCAN to get clusters of resources, cID will be set in UI
	void setClusters_DBSCAN( std::set<UIptr>& population, int thresh, int radius );
private:
	typedef std::set<UIptr> setUI;
	typedef list<UIptr> listUI;
	DBSCAN* DBSCAN_;
	ANCluster* pANCluster_;
	/// holds all the UnitInfos to track
	setUI unitSet_;
	
	/// calculate cluster membership, try to preserve existing clusters
	void updateClusters();
	int useAlg_;	// 0 for dbscan, 1 for ancluster
};

#endif