#include "ClusterMethod.h"
#include "../common/cpp_includes.h"
#include "../GameState.h"
#include "ScanUnit.h"


ClusterMethod::ClusterMethod():
nextClusterID_(1),
currentID_(0)
{
	neighbors_.clear();
	usedIDs_.clear();
	freeIDs_.clear();
}
// void ClusterMethod::getUnitsInRadius( UIptr center, int radiusPx )
// {
// 	GameState::getInstance().getBanditsInRadius( center->getPosition(),
// 		radiusPx, neighbors_ );
// }
void ClusterMethod::getUnitsInRadius( const setUI& unitPopulation, 
									 UIptr center, int radiusPx )
{
	SpatialPositionManager::getInstance().getUnitsInRadius(
		unitPopulation, center->getPosition() ,radiusPx, neighbors_ );
}
int ClusterMethod::getNextID()
{
	int retval = nextClusterID_;
	if ( freeIDs_.empty() )
	{
		nextClusterID_++;
	}
	else
	{
		retval = freeIDs_.front();
		freeIDs_.pop_front();
	}
	usedIDs_.insert(retval);
	return retval;
}
void ClusterMethod::recycleCIDs()
{	// iterate from 1 to nextClusterID_ and check membership in usedIDs_
	freeIDs_.clear();
	for (int i = 1; i < nextClusterID_; i++)
	{
		if ( usedIDs_.count(i) == 0 )
		{
			freeIDs_.push_back(i);
		}
	}
}
void ClusterMethod::applyCIDs()
{
	int oldCID, newCID;
	for ( scanMap::iterator iter = UIscan_.begin();
		iter != UIscan_.end(); iter++ )
	{
		oldCID = iter->first->clusterID();
		newCID = iter->second.newGroup;
		if ( oldCID != newCID )
		{
			// send change of cluster signal, UIptr, newCID, oldCID
			clusterChangeSig_(iter->first, newCID, oldCID);
		}
		iter->first->clusterID( newCID );
	}
}

void ClusterMethod::scanForUnusedIDs( const set<UIptr>& allUnits )
{
	usedIDs_.clear();
	BOOST_FOREACH( UIptr pUI, allUnits )
	{
		if ( pUI->clusterID() != 0)
		{
			usedIDs_.insert( pUI->clusterID() );
		}
	}
	recycleCIDs();
}

boost::signals2::connection ClusterMethod::connect( 
	const uccsig_t::slot_type &subscriber )
{
	return clusterChangeSig_.connect(subscriber);
}