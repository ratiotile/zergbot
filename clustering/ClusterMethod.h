#pragma once
#ifndef INCLUDED_CLUSTERMETHOD_H
#define INCLUDED_CLUSTERMETHOD_H

#include <set>
#include <list>

#include "../common/UnitInfo.h"
#include "../common/typedefs.h"
#include <boost/signals2.hpp>

struct ScanUnit;

struct SwapGroup 
{
	map<int,int> oldCID_counts;
	int size_;
	SwapGroup(): size_(0) {}
	void addCID(int oldCID){ oldCID_counts[oldCID] += 1; size_++; }
};
/// implements shared methods used by cluster algorithms
class ClusterMethod
{
public:
	typedef std::set<UIptr> setUI;
	typedef list<UIptr> listUI;
	typedef map<UIptr,ScanUnit> scanMap;

	ClusterMethod();
	/// run on frame, scans allUnits and deletes unused IDs from usedIDs_
	void scanForUnusedIDs( const setUI& allUnits );
	/// connect a slot to the unit cluster change signal.
	boost::signals2::connection connect(const uccsig_t::slot_type &subscriber);
protected:
	
	/// next ID to be used; never touch, only use getNextID.
	int nextClusterID_;
	/// temporary storage for current ID being assigned
	int currentID_;
	/// set to reuse to store neighbors of current unit
	setUI neighbors_;
	/// hold IDs that are currently assigned groups
	set<int> usedIDs_;
	/// hold IDs that are no longer used and can be recycled
	list<int> freeIDs_;
	/// units that are neighbors, added to visit next
	listUI toVisit_;	
	/// associate ScanUnit to UnitInfo
	scanMap UIscan_;
	uccsig_t clusterChangeSig_;

	/// get set of UnitInfo pointer within radius, store in neighbors_
//	void getUnitsInRadius( UIptr center, int radiusPx );
	/// overload to pass in set of units
	void getUnitsInRadius( const setUI& unitPopulation, 
		UIptr center, int radiusPx );
	/// return nextClusterID and increment.
	int getNextID();
	/// find unused IDs  to store in freeIDs_, call at the end of updateClusters()
	void recycleCIDs();
	/// copies cID values from ScanUnits into UnitInfo
	void applyCIDs();
	
};
#endif