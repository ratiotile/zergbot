#include "DBSCAN.h"
#include "../common/cpp_includes.h"
//#include "../GameState.h"

DBSCAN::DBSCAN( int threshold, int radiusPx ):
ClusterMethod(),
currentPlayer_(NULL)
{
	assert(radiusPx > 0);
	assert(threshold > 0);
	threshold_ = threshold;
	radiusPx_ = radiusPx;
}

void DBSCAN::updateClusters( setUI& unitSet )
{
	// reset
	neighbors_.clear();
	toVisit_.clear();
	usedIDs_.clear();
	UIscan_.clear();
	for ( setUI::iterator iter = unitSet.begin(); 
		iter != unitSet.end(); iter++ )
	{
		UIscan_.insert( make_pair(*iter, ScanUnit(*iter)) );
	}
	// begin, use iterator through UIscan and check if visited;
	for ( scanMap::iterator iter = UIscan_.begin(); 
		iter != UIscan_.end(); iter++ )
	{
		if( !iter->second.visited )
		{
			currentID_ = 0; // reset
			// if neighbors above thresh, add them toVisit.
			visit( unitSet, iter->first );

			// propagate cluster by visiting all in toVisit_, adding
			listUI::iterator vi = toVisit_.begin();
			while( vi != toVisit_.end() )
			{
				assert( UIscan_.find(*vi) != UIscan_.end() );
				if ( UIscan_[*vi].visited )	// !!!!!!!!!!!!!!!!!!!!!!!!!!May be better way
				{
					vi++;
				}
				else
				{
					visit( unitSet, *vi );
					vi++;
				}
			}
		}
	}
	applyCIDs();
	recycleCIDs();
}

void DBSCAN::updateClusters( setUI& units, int threshold, int radius )
{
	assert(radius >= 0);
	assert(threshold >= 0);
	int oldT = threshold_;
	int oldR = radiusPx_;
	threshold_ = threshold;
	radiusPx_ = radius;
	
	updateClusters(units);

	threshold_ = oldT;
	radiusPx_ = oldR;
}
void DBSCAN::visit( setUI& unitSet, UIptr currUnit )
{
	// assume that the unit is in the map
	assert( UIscan_.find(currUnit) != UIscan_.end() );
	ScanUnit& currScan = UIscan_[currUnit];
	currScan.visited = true;
	// check radius for neighboring units, return in neighbors
	neighbors_.clear();
	getUnitsInRadius( unitSet, currUnit, radiusPx_ );
	if ( neighbors_.size() >= threshold_ )
	{	
		if (currentID_ == 0) // try to start new group
		{	// get new cID if unit has none or already used cID
			if ( currUnit->clusterID() == 0 ||
				usedIDs_.count(currUnit->clusterID()) == 1 )
			{
				currentID_ = getNextID();
			} 
			else // use unit's cID if unused (default)
			{
				currentID_ = currUnit->clusterID();
			}
			currScan.newGroup = currentID_;
			usedIDs_.insert(currentID_);
			// insert neighbors into group, check if visited later
		}
		else // continue currentID group
		{	// assume this unit unvisited
			currScan.newGroup = currentID_;
		}
		// in any case, insert neighbors to be visited
		toVisit_.insert( 
			toVisit_.end(), neighbors_.begin(), neighbors_.end() );
	}
	else	// isolated; put into group as leaf if expanding or set as noise
	{
		currScan.newGroup = currentID_;
	}
}