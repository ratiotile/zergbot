#pragma once
#ifndef INCLUDED_DBSCAN_H
#define INCLUDED_DBSCAN_H

#include "ClusterMethod.h"
#include <set>
#include <list>
#include <map>
#include "ScanUnit.h"
#include "BWAPI.h"


class DBSCAN: public ClusterMethod
{
	typedef std::set<UIptr> setUI;
	typedef list<UIptr> listUI;
	typedef map<UIptr,ScanUnit> scanMap;
public:
	/// construct setting threshold and radius
	DBSCAN( int threshold, int radiusPx );
	/// calculate clusters using DBSCAN
	void updateClusters( setUI& units );
	/// update clusters using specifc parameters
	void updateClusters( setUI& units, int threshold, int radius );
private:
	unsigned int threshold_;
	unsigned int radiusPx_;
	/// used to tell groups from different players apart
	BWAPI::Player* currentPlayer_;
	
	/// helper function for DBSCAN, adds neighbors to visit list
	void visit( setUI& unitSet, UIptr currUnit );
};

#endif