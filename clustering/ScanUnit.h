#pragma once
#ifndef INCLUDED_SCANUNIT_H
#define INCLUDED_SCANUNIT_H

#include "../common/UnitInfo.h"

/// temporary data structure used to hold changes before applying
struct ScanUnit
{
	bool visited;
	int newGroup;
	ScanUnit(): visited(false), newGroup(0){}
	/// default keep old cluster ID
	ScanUnit(UIptr baseUI): visited(false), newGroup(baseUI->clusterID()){}
};

#endif