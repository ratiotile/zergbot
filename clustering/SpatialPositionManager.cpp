#include "SpatialPositionManager.h"
#include "../common/cpp_includes.h"
#include "../common/Vector2D.h"

SpatialPositionManager* SpatialPositionManager::pInstance_ = NULL;

SpatialPositionManager::SpatialPositionManager():
getValue_(),
pFinder_(new SpatialVectSearch(0) ) // linear
{
	//xAscending_.clear();
}

SpatialPositionManager::~SpatialPositionManager()
{
	delete pFinder_;
	pFinder_ = NULL;
}

SpatialPositionManager& SpatialPositionManager::getInstance()
{
	if (!pInstance_)
	{
		pInstance_ = new SpatialPositionManager();
	}
	return *pInstance_;
}

void SpatialPositionManager::onFrameBegin()
{
// 	if ( xAscending_.size() > 1 )
// 	{	// no point in sorting smaller
// 		sortX();
// 	}
}
/*
void SpatialPositionManager::getUnitsInRadius( BWAPI::Position pos, 
	int radiusPx, std::set<UIptr> &results )
{
	// assume X Y vectors are sorted ascending
	assert( pos.isValid() );
	assert( radiusPx >= 0 );
	// get all units inside square, by getting all matching X then Y
	vector<UIptr> inRangeX;
	vector<UIptr> inRangeXY;
	getUnitsInRange( xAscending_, pos.x(), radiusPx, false, inRangeX );
	sort( inRangeX.begin(), inRangeX.end(), LessThanY() );
	getUnitsInRange( inRangeX, pos.y(), radiusPx, true, inRangeXY );
	// done, inRangeXY should contain all units in square.
	// check euclidean squared distance
	Vector2D v2dPos(pos);
	results.clear();
	double distSq;
	double radiusSq = radiusPx * radiusPx;
	for ( size_t i = 0; i < inRangeXY.size(); i++ )
	{
		distSq = Vector2D(inRangeXY[i]->getPosition()).distanceSq(v2dPos);
		if (distSq <= radiusSq)
		{
			results.insert( inRangeXY[i] );
		}
	}
}*/
/** Precondition: unitPopulation contains all the units to search through.
	Postcondition: results will be cleared and will hold found units.
*/
void SpatialPositionManager::getUnitsInRadius( 
	const std::set<UIptr> &unitPopulation,
	BWAPI::Position pos, int radiusPx, std::set<UIptr> &results )
{
	// must clone set to an array
	vector<UIptr> temp(unitPopulation.begin(), 
		unitPopulation.end());
	getUnitsInRadius(temp, pos, radiusPx, results);
}

void SpatialPositionManager::getUnitsInRadius( std::vector<UIptr> 
	&unitPopulation, BWAPI::Position pos, int radiusPx, 
	std::set<UIptr> &results )
{// assume results is clear
	if (unitPopulation.empty())
	{
		return;
	}
	assert( !unitPopulation.empty() );
	assert( pos.isValid() );
	assert( radiusPx >= 0 );
	sort( unitPopulation.begin(), unitPopulation.end(), LessThanX() );
	// get all units inside square, by getting all matching X then Y
	vector<UIptr> inRangeX;
	vector<UIptr> inRangeXY;
	getUnitsInRange( unitPopulation, pos.x(), radiusPx, false, inRangeX );
	sort( inRangeX.begin(), inRangeX.end(), LessThanY() );
	getUnitsInRange( inRangeX, pos.y(), radiusPx, true, inRangeXY );
	// done, inRangeXY should contain all units in square.
	// check euclidean squared distance
	Vector2D v2dPos(pos);
	results.clear();
	double distSq;
	double radiusSq = radiusPx * radiusPx;
	for ( size_t i = 0; i < inRangeXY.size(); i++ )
	{
		distSq = Vector2D(inRangeXY[i]->getPosition()).distanceSq(v2dPos);
		if (distSq <= radiusSq)
		{
			results.insert( inRangeXY[i] );
		}
	}
}
/*
void SpatialPositionManager::sortX()
{
	assert( xAscending_.size() > 1 );

	sort( xAscending_.begin(), xAscending_.end(), LessThanX() );

	// check that they are in ascending order ( weak test )
	assert( xAscending_[0]->getPosition().x() <= 
		xAscending_[xAscending_.size()-1]->getPosition().x() );
}*/
/**	Precondition: vAscending must be sorted in ascending order by X or by Y
	if useY=true.
	vResult will be cleared before adding results.
*/
void SpatialPositionManager::getUnitsInRange( const vectorUI &vAscending, 
					int origin, int radius, bool useY, vectorUI &vResult )
{
	assert( radius >= 0 );
	assert( origin > 0 );
	vResult.clear();
	// first get nearest index
	int iCenter = pFinder_->find( vAscending, origin, useY, getValue_ );
	// calculate min and max bounds of coordinate
	int maxR = origin + radius;
	int minR = origin - radius;
	if (minR < 0) minR = 0;
	assert( minR >= 0 );
	// find min and max indices that fall within bounds
	int i = iCenter;
	int iMin = 0;
	int iMax = 0;
	while( (i >= 0) && getValue_( vAscending, i, useY ) >= minR )
	{
		iMin = i;
		i--;
	}
	i = iCenter;
	while( (i < vAscending.size()) && ( getValue_( vAscending, i, useY ) <= maxR ))
	{
		iMax = i;
		i++;
	}
	// now copy range into result vector
	for ( size_t i = iMin; i <= iMax; i++ )
	{
		assert( i < vAscending.size() );
		vResult.push_back( vAscending[i] );
	}
	// done!
}
/*
void SpatialPositionManager::addNewUnit( UIptr newUnit )
{
	assert( newUnit != NULL );
	// find out if unit exists
	bool unitExists = false;
	vectorUI::iterator insertIter = findIterator( newUnit );
	if ( insertIter != xAscending_.end() ) unitExists = true;
	if ( !unitExists )
	{	// find place to insert, std inserts before iterator.
		vectorUI::iterator insertIter = xAscending_.begin();;
		while ( insertIter != xAscending_.end() && 
			(*insertIter)->getPosition() < newUnit->getPosition() )
		{	// stops once insertIter is at next greatest value
			insertIter++;
		}
		xAscending_.insert( insertIter, newUnit );
		//Broodwar->printf("SPM added new unitInfo.");
		return;
	}
	Broodwar->printf("SPM cannot add unit; unit already added.");
}

void SpatialPositionManager::removeUnit( UIptr toRemove )
{
	if ( !toRemove ) return;
	// find out if unit exists
	vectorUI::iterator removeIter = findIterator( toRemove );
	if ( removeIter != xAscending_.end() )
	{
		xAscending_.erase( removeIter );
		//Broodwar->printf("SPM removed unitInfo.");
		return;
	}
	//Broodwar->printf("SPM cannot remove; unit not found.");
	// don't need to sort
}*/
/*
vectorUI::iterator SpatialPositionManager::findIterator( UIptr pUI )
{
	assert( pUI );
	// special case: empty vector
	if(xAscending_.empty()) return xAscending_.end();
	assert( !xAscending_.empty() );
	for ( vectorUI::iterator iter = xAscending_.begin(); 
		iter != xAscending_.end(); iter++ )
	{
		if ( (*iter) == pUI )
		{
			return iter;
		}
	}
	return xAscending_.end();
}*/