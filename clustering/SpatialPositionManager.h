#pragma once
#ifndef INCLUDED_SPATIALPOSITIONMANAGER_H
#define INCLUDED_SPATIALPOSITIONMANAGER_H

#include <assert.h>
#include <vector>
#include <set>
#include <algorithm>	// for std sort
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#include "SpatialVectSearch.h"
#include "../common/UnitInfo.h"
#include "../common/typedefs.h"

/// functor to get value from X or Y vector
struct getValueF 
{
	int operator()( const std::vector<UIptr> &ascendingVect, size_t index, bool getY)
	{
		assert(index < ascendingVect.size());
		if (getY)
		{
			return ascendingVect[index]->getPosition().y();
		}
		else
		{
			return ascendingVect[index]->getPosition().x();
		}
	}
};
/// comparison functor for std::sort, to sort on Y ascending
struct LessThanY : public std::binary_function<UIptr, UIptr, bool>
{
	inline bool operator()(const UIptr lhs, const UIptr rhs)
	{
		return lhs->getPosition().y() < rhs->getPosition().y();
	}
};
/// comparison functor for std::sort to sort on X ascending
struct LessThanX : public std::binary_function<UIptr, UIptr, bool>
{
	inline bool operator()(const UIptr lhs, const UIptr rhs)
	{
		return lhs->getPosition().x() < rhs->getPosition().x();
	}
};

/// Allows searching for enemy units by proximity
/** Holds UnitInfo pointers in X and Y sorted vectors.
	Sorts again at beginning of each frame, so that when new UnitInfo is 
	added, vectors will already be sorted.
*/

class SpatialPositionManager
{
public:
	
	~SpatialPositionManager();
	static SpatialPositionManager& getInstance();
	/// call to sort the internal vectors at beginning of frame.
	void onFrameBegin();
	/// gets all UnitInfo within radius of specified point.
// 	void getUnitsInRadius(BWAPI::Position pos, int radiusPx, 
// 		std::set<UIptr> &results);
	/// Pass in a set of units to search over
	void getUnitsInRadius( const std::set<UIptr> &unitPopulation,
		BWAPI::Position pos, int radiusPx, std::set<UIptr> &results );
	/// Expects vector instead of set, will change order of vector
	void getUnitsInRadius( std::vector<UIptr> &unitPopulation,
		BWAPI::Position pos, int radiusPx, std::set<UIptr> &results );
	/// add new UnitInfo into tracking; check for duplicate
	//void addNewUnit(UIptr newUnit);
	/// remove UnitInfo pointer from tracking; not deleted
	//void removeUnit(UIptr toRemove);
private:
	/// Constructor; does nothing
	SpatialPositionManager();
	static SpatialPositionManager* pInstance_;

	typedef std::vector<UIptr> vectorUI;
	/// functor to get value from X or Y vector
	getValueF getValue_;
	/// pointer to search strategy
	SpatialVectSearch* pFinder_;
	/// vector of pointer to UnitInfo sorted in ascending X coordinate.
//	std::vector<UIptr> xAscending_;
	/// list of search results, reused by clearing on new query

	//helpers
	/// sorts internal X Y vectors
	//void sortX();

	/// populates vector with elements in range, use X value unless useY set.
	void getUnitsInRange( const vectorUI &vAscending, int origin, int radius, 
		bool useY, vectorUI &vResult );
	/// returns iterator to the unit if found, end if not
	//vectorUI::iterator findIterator( UIptr pUI );
};



#endif