#include "SpatialVectSearch.h"
#include "../common/cpp_includes.h"
#include "SpatialPositionManager.h" // for getValue functor

SpatialVectSearch::SpatialVectSearch( int searchType ):
searchStrategy_(searchType)
{
}

int SpatialVectSearch::find( const std::vector<UIptr> &ascendingVect, 
							int target, bool getY, getValueF &valueFunc )
{
	// just do linear for now
	if (searchStrategy_ == LINEAR)
	{
		return linearSearch(ascendingVect, target, getY, valueFunc);
	}
	else
	{
		return linearSearch(ascendingVect, target, getY, valueFunc);
	}
}
/** return the index in the correct place to insert a target value
*/
int SpatialVectSearch::linearSearch( const vectorUI &ascendingVect, 
									int target, bool getY, 
									getValueF &valueFunc )
{
	int index = 0;
	for ( size_t i = 0; i < ascendingVect.size(); i++ )
	{
		int compResult = compare( valueFunc( ascendingVect, i, getY ), target );
		if( compResult == -1 )
		{	// smaller than target, next i
			index = i;
		}
		else if ( compResult == 0 )
		{	// found target, return index
			return i;
		}
		else if ( compResult == 1 )
		{	// too big, return immediately
			return index;
		}
		else
		{
			// error!
		}
	}
	// if not found, return last
	index = ascendingVect.size() - 1;
	if (ascendingVect.size() == 0)
	{
		index = 0;
	}
	return index;
}

int SpatialVectSearch::compare( int candidate, int target )
{
	if ( candidate < target ) return -1;
	else if ( candidate == target ) return 0;
	else return 1;	// candidate > target
}