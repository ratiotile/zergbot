#pragma once
#ifndef INCLUDED_SPATIALVECTSEARCH_H
#define INCLUDED_SPATIALVECTSEARCH_H

#include <vector>
#include "../common/typedefs.h"

struct getValueF;
class UnitInfo;
typedef std::vector<UIptr> vectorUI;

/// Strategy class used by SpatialPositionManager to search vectors
/** Assumes vectors are sorted in ascending order.
*/
class SpatialVectSearch
{
public:
	/// instantiate an instance set to use a type of search
	SpatialVectSearch(int searchType);
	/// enumerate search types
	enum search_t{
		LINEAR = 0, BINARY = 1
	};
	/// call the correct search method that was set in constructor
	int find(const vectorUI &ascendingVect, int target, bool getY,
		getValueF &valueFunc);
private:
	int searchStrategy_;
	/// use linear search to find the index of the nearest value to target
	int linearSearch( const vectorUI &ascendingVect, int target, bool getY,
		getValueF &valueFunc);
	int binarySearch( const vectorUI &ascendingVect, int target, bool getY,
		getValueF &valueFunc);
	/// returns -1 if candidate smaller, 0 if equal, 1 if candidate larger
	int compare( int candidate, int target );
};


#endif

