#include "Config.h"
#include "cpp_includes.h"

using boost::property_tree::ptree;

Config::Config( const std::string &filename )
:filename_(filename)
{
	
	try
	{
		load();
		logfile << "Config file " << filename_ << " successfully read!\n";
	}
	catch (std::exception &e)
	{
		logfile << "Error: " << e.what() << "\n";
		logWriteBuffer();
	}
}

void Config::load()
{
	logfile << "Reading Config File " << filename_ << endl;
	read_info(filename_, pt_);
	// test retrieval
	/*
	try{
	logfile << "Marine Str: " << pt_.get<double>("UnitStrength.Terran.Marine")
		<< endl;
	logfile << "Tank sieged Str: " << pt_.get<double>("UnitStrength.Terran.TankSieged")
		<< endl;
	logfile << "DT Str: " << pt_.get<double>("UnitStrength.Protoss.DarkTemplar")
		<< endl;
	}
	catch(boost::property_tree::ptree_error pe)
	{
		logfile << "Error: " << pe.what() << endl;
	}*/
}