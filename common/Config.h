#pragma once
#ifndef INCLUDED_CONFIG_H
#define INCLUDED_CONFIG_H

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

class Config
{
public:
	/// read in the config file
	Config(const std::string &filename);

	template<typename Type>
	Type get(const std::string &path) const;
private:
	boost::property_tree::ptree pt_;
	std::string filename_;

	void load();
};

template<typename Type>
Type Config::get( const std::string &path ) const
{
	Type retVal;
	try{
		retVal = pt_.get<Type>(path);
	}
	catch(boost::property_tree::ptree_error pe)
	{
		logfile << "Error: " << pe.what() << endl;
	}
	return retVal;
	
}
#endif