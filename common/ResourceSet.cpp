#include "ResourceSet.h"
#include <sstream>
#include "cpp_includes.h"

std::string ResourceSet::getUnitsStr() const
{
	ostringstream oss;
	if (units.empty())
	{
		oss << "none";
	}
	else
	{
		typedef pair<UnitType,int> unitCountPair;
		BOOST_FOREACH(unitCountPair ucp, units)
		{
			UnitType utype = ucp.first;
			oss << ucp.second << "x " << utype.getName() << ". ";
		}
	}
	return oss.str();
}

bool ResourceSet::isEmpty() const
{
	if (minerals <= 0 && gas <= 0 && supply <= 0 && units.empty())
	{
		return true;
	}
	return false;
}

const ResourceSet ResourceSet::operator-( const ResourceSet& other ) const
{
	ResourceSet result;
	result.minerals = minerals - other.minerals;
	if (result.minerals < 0) result.minerals = 0;
	result.gas = gas - other.gas;
	if (result.gas < 0) result.gas = 0;
	result.supply = supply - other.supply;
	if (result.supply < 0) result.supply = 0;
	// can't have negative units, so only take away matching units.
	result.units = units;
	typedef pair<UnitType,int> unitCount;
	BOOST_FOREACH(unitCount uc, other.units)
	{
		unitQuantity::iterator found = result.units.find(uc.first);
		if (found != result.units.end())
		{
			found->second -= uc.second;
			if ( found-> second <= 0 )
			{
				result.units.erase(found);
			}
		}
	}
	return result;
}

const ResourceSet ResourceSet::operator+( const ResourceSet& other ) const
{
	ResourceSet result;
	result.minerals = minerals + other.minerals;
	result.gas = gas + other.gas;
	result.supply = supply + other.supply;
	// simply add the units
	result.units = units;
	typedef pair<UnitType,int> unitCount;
	BOOST_FOREACH(unitCount uc, other.units)
	{
		result.units[uc.first] += uc.second;
	}
	return result;
}

std::string ResourceSet::toString() const
{
	ostringstream oss;
	oss << minerals << "m " << gas << "g " << supply << "s [";
	oss << getUnitsStr() << "]";
	return oss.str();
}

void ResourceSet::clear()
{
	minerals = 0;
	gas = 0;
	units.clear();
	supply = 0;
}

bool ResourceSet::operator==( const ResourceSet& other ) const
{
	if (minerals != other.minerals) return false;
	if (gas != other.gas) return false;
	if (supply != other.supply) return false;
	if (units.size() != other.units.size() 
		|| !std::equal(units.begin(),units.end(),other.units.begin()))
		return false;
	return true;
}

bool ResourceSet::operator!=( const ResourceSet& other ) const
{
	return !(*this == other);
}