/************************************************************************/
/* ResourceSet holds how many minerals/gas/supply a task requires.      */
/* It is up to Commander to allocate these resources among the tasks    */
/************************************************************************/
#pragma once
#include <BWAPI.h>
#include <map>

typedef std::map<BWAPI::UnitType,int> unitQuantity;

class ResourceSet
{
public:
	ResourceSet():
	  minerals(0), gas(0), supply(0)
	  {}
	ResourceSet(int m, int g, int s)
		:minerals(m),gas(g),supply(s)
	{}
	int minerals;
	int gas;
	int supply;
	unitQuantity units;
	std::string getUnitsStr() const;
	bool isEmpty() const;
	void clear();
	const ResourceSet operator-(const ResourceSet& other) const;
	const ResourceSet operator+(const ResourceSet& other) const;
	bool operator== (const ResourceSet& other ) const;
	bool operator!= (const ResourceSet& other ) const;

	std::string toString() const;
};