#include "UnitInfo.h"
#include "../GameState.h"
#include "cpp_includes.h"

UIptr UnitInfo::Null;

UnitInfo::UnitInfo(  BWAPI::Unit* nUnit ):
	UnitClass(nUnit),
	pTarget_(NULL),
	pUnit_(nUnit),
	walkRect_( nUnit->getPosition(), nUnit->getType() ),
	movedThisFrame_( false ),
	clusterID_( 0 ),
	lastMoveTile_( nUnit->getPosition() ),
	lastKnownType_( nUnit->getType() ),
	unitID_(0),
	lastKnownOwner_( nUnit->getPlayer() )
{
}
void UnitInfo::onUnitEvade()
{
	walkRect_.changePosPx( pUnit_->getPosition() );
}

void UnitInfo::onNewTarget(  BWAPI::Unit* nTarget )
{
	pTarget_ = nTarget;
}

void UnitInfo::update()
{ // assume unit is visible
	if (pUnit_->isVisible())
	{
		assert( pUnit_->isVisible() );
		// update type
		lastKnownType_ = pUnit_->getType();
		lastKnownOwner_ = pUnit_->getPlayer();
		unitID_ = pUnit_->getID();
		// update location
		movedThisFrame_ = false;
		if (walkRect_.getPos() != pUnit_->getPosition())
		{
			movedThisFrame_ = true;
			/*
			GameState::getInstance().TM().onEnemyUnitMoved(
				TilePosition(walkRect_.getPos()),
				TilePosition(pUnit_->getPosition()),*pUnit_);
			*/
			walkRect_.changePosPx( pUnit_->getPosition() );
			// check if it has moved more than a tile past lastMoveTile
			if ( walkRect_.getPos().getApproxDistance(lastMoveTile_) > 32)
			{
				lastMoveTile_ = walkRect_.getPos();
				GameState::get().onBanditMoveTile( getThis() );
			}
			//logfile << "UnitInfo Update: set unit lastKnownPos to ["<<TilePosition(lastKnownPos).x()<<","<<TilePosition(lastKnownPos).y()<<"]\n";
		}
		else movedThisFrame_ = false;
	}
}

bool UnitInfo::addCovers( UIptr otherUnit )
{
	pair<setUI::iterator,bool> rp = covers_.insert(otherUnit);
	return rp.second;
}

bool UnitInfo::addCoveredBy( UIptr otherUnit )
{
	pair<setUI::iterator,bool> rp = coveredBy_.insert(otherUnit);
	return rp.second;
}

set<UIptr>::iterator UnitInfo::removeCovers( UIptr otherUnit )
{
	setUI::iterator found = covers_.find(otherUnit);
	if ( found != covers_.end() )
	{
		found = covers_.erase( found );
	}
	return found;
}

set<UIptr>::iterator UnitInfo::removeCoveredBy( UIptr otherUnit )
{
	setUI::iterator found = coveredBy_.find(otherUnit);
	if ( found != coveredBy_.end() )
	{
		found = coveredBy_.erase( found );
	}
	return found;
}

void UnitInfo::resetLastMoveTile()
{
	if ( pUnit_->isVisible() )
	{
		lastMoveTile_ = pUnit_->getPosition();
	}
}

bool UnitInfo::isNotCombatActive() const
{
	return (isHarvesting() || pUnit_->isHallucination() 
		|| pUnit_->isLockedDown() || pUnit_->isMaelstrommed()
		|| pUnit_->isRepairing() || !pUnit_->isCompleted()
		|| pUnit_->isUnpowered() || pUnit_->isConstructing() );
}

bool UnitInfo::isNonCombat() const
{
	return (pUnit_->getType().isNeutral() 
		|| !pUnit_->getType().canAttack() );
}

BWAPI::TilePosition UnitInfo::getTilePosition() const
{
	return TilePosition( BWAPIext::PtoB(leftPx()), BWAPIext::PtoB(topPx()) );
}

BWAPI::Position UnitInfo::getPosition() const
{
	return walkRect_.getPos();
}

bool UnitInfo::isCloaked() const
{
	if(pUnit_->exists())
		return pUnit_->isCloaked();

	return false;
}

bool UnitInfo::isBurrowed() const
{
	if(pUnit_->exists())
		return pUnit_->isBurrowed();

	return false;
}