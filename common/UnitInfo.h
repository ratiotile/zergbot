#pragma once
#ifndef INCLUDED_UNITINFO_H
#define INCLUDED_UNITINFO_H

#include <assert.h>
#include <set>
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#include "WalkRect.h"
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <cassert>
#include "../Unit.h"

using namespace std;

/// contains persistant information about enemy units.
/**	UnitInfo is used to hold pointer to and information about enemy units.
	It will record time last seen, and last known position of the unit, as well
	as last known health/shields, and any other field that is inaccessible
	from BWAPI::Unit when unit is out of sight.
*/
class UnitInfo;
typedef boost::shared_ptr<UnitInfo> UIptr;
typedef boost::shared_ptr<const UnitInfo> constUIptr;
typedef std::set<UIptr> setUI;

class UnitInfo: public UnitClass
{
	boost::shared_ptr<UnitInfo> shared_from_this()
	{
		return boost::static_pointer_cast<UnitInfo>(UnitClass::shared_from_this());
	}
public:
	static UIptr Null; // use for no unit found.
	boost::shared_ptr<UnitInfo> getThis() { return shared_from_this(); }

	UnitInfo(BWAPI::Unit* nUnit);
	// accessors
	inline BWAPI::Unit* getUnit() const { return pUnit_; }
	/// returns the Position where unit was last seen. Unit center
	BWAPI::Position getPosition() const;
	/// return tilePosition of Top Left of unit last position
	BWAPI::TilePosition getTilePosition() const;
	inline bool hasMovedThisFrame() { return movedThisFrame_; }
	inline BWAPI::Unit* getTarget(){return pTarget_;}
	inline int leftPx() const { return walkRect_.leftPx(); }
	inline int topPx() const { return walkRect_.topPx(); }
	inline int rightPx() const { return walkRect_.rightPx(); }
	inline int bottomPx() const { return walkRect_.botPx(); }
	inline UnitType getType() const { return lastKnownType_; }
	inline int clusterID() const { return clusterID_; }
	bool covers( UIptr otherUnit ) const 
		{ return covers_.find(otherUnit) != covers_.end(); }
	bool coveredBy( UIptr otherUnit ) const
		{ return coveredBy_.find(otherUnit) != coveredBy_.end(); }
	int getDistance( UIptr otherUnit ) 
		{return walkRect_.getPos().getApproxDistance(otherUnit->getPosition());}
	bool isHarvesting() const 
	{ return pUnit_->isGatheringGas() || pUnit_->isGatheringMinerals(); }
	bool isNotCombatActive() const;
	bool isNonCombat() const;
	bool isCloaked() const;
	bool isBurrowed() const;

	int getID() const { return unitID_; }
	BWAPI::Player* getOwner() const { return lastKnownOwner_; }

	// mutators
	void update();	// call each frame
	void onUnitEvade(); // update, stop using
	void onNewTarget(BWAPI::Unit* nTarget);
	void clusterID( int newID ) { clusterID_ = newID; }
	bool addCovers( UIptr otherUnit );
	bool addCoveredBy( UIptr otherUnit );
	setUI::iterator removeCovers( UIptr otherUnit );
	setUI::iterator removeCoveredBy( UIptr otherUnit );
	setUI& covers() { return covers_; };
	setUI& coveredBy() { return coveredBy_; }
	void resetLastMoveTile();
private:
	/// true if the unit has moved since last update
	bool movedThisFrame_;
	/// last position triggering onMoveTile; reset by ClusterMethod on visit!
	BWAPI::Position lastMoveTile_;
	/// holds last known position
	WalkRect walkRect_;
	BWAPI::Unit* pUnit_;
	/// last known type
	UnitType lastKnownType_;
	BWAPI::Unit* pTarget_; // unit that is under attack
	/// what cluster this unit is in, 0=none
	int clusterID_;
	/// set of units that this one can cover
	setUI covers_;
	/// set of units that covers this one
	setUI coveredBy_;
	/// store unit ID
	int unitID_;
	/// store Player
	BWAPI::Player* lastKnownOwner_;
};


#endif