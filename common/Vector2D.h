#ifndef INCLUDED_VECTOR2D_H
#define INCLUDED_VECTOR2D_H
#pragma once

#include <math.h>
#include <limits>
#include <iosfwd>
#include <BWAPI.h>

// for when the namespace finally gets cleaned up
//typedef BWAPI::TilePosition TilePosition;
//typedef BWAPI::Position Position;

class Vector2D
{
public:
	double x;
	double y;
	/// default constructor, sets to zero
	Vector2D():
		x(0.0), y(0.0){}
	/// construct given values
	Vector2D(double nx, double ny):
		x(nx), y(ny){}
	/// copy constructor
	Vector2D(const Vector2D &v)
		: x(v.x), y(v.y){}
	/// construct given BWAPI::Position
	Vector2D(const BWAPI::Position &p)
		: x( double(p.x()) ), y( double(p.y()) ){}
	/// construct given BWTA::TilePosition
	Vector2D(const BWAPI::TilePosition &p)
		: x( double(p.x() * 32) ), y( double(p.y() * 32) ){}
	// end constructors
	// operators
	/// typecast to BWAPI::Position
	inline operator BWAPI::Position() { return BWAPI::Position( int(x), int(y) ); }
	/// typecast to BWAPI::TilePosition
	inline operator BWAPI::TilePosition() { return BWAPI::TilePosition( int(x / 32), int(y / 32) ); }

	bool operator==(const Vector2D &v) const;
	bool operator!=(const Vector2D &v) const;
	const Vector2D& operator+=(const Vector2D &rhs);
	const Vector2D& operator-=(const Vector2D &rhs);
	const Vector2D& operator*=(const double& rhs);
	const Vector2D& operator/=(const double& rhs);
	// member functions
	/// sets x and y to zero
	void zero(){x=0.0; y=0.0;}
	/// returns true if both x and y are zero
	bool isZero()const{return (x == 0 && y == 0);}
	/// length of vector
	double length() const;
	/// length of vector squared, without Squareroot operation
	double lengthSq() const;
	/// set length of vector to 1, return original length
	double normalize();
	/// adjust x and y so that length of vector is newLength
	void scale(double newLength);
	/// calculate dot product
	double dot(const Vector2D& v2) const;
	/// calculate distance between vectors
	double distance(const Vector2D &v2) const;
	/// get squared distance
	double distanceSq(const Vector2D &v2) const;
	/// get a vector perpendicular to this one
	Vector2D getPerpendicular() const;
	/// vector becomes its reflection off of norm vector
	void reflect(const Vector2D& norm);
	/// return a copy of this vector with signs flipped
	Vector2D getReverse() const;
};

// overloads which must be defined outside class
inline Vector2D operator*(const Vector2D &lhs, double rhs);
inline Vector2D operator*(double lhs, const Vector2D &rhs);
inline Vector2D operator-(const Vector2D &lhs, const Vector2D &rhs);
inline Vector2D operator+(const Vector2D &lhs, const Vector2D &rhs);
inline Vector2D operator/(const Vector2D &lhs, double val);
std::ostream& operator<<(std::ostream& os, const Vector2D& rhs);
std::ifstream& operator>>(std::ifstream& is, Vector2D& lhs);

// implementations
inline bool Vector2D::operator==(const Vector2D &v) const 
{
	return (x == v.x && y == v.y);
}

inline bool Vector2D::operator!=(const Vector2D &v) const
{
	return !(x == v.x && y == v.y);
}

inline const Vector2D& Vector2D::operator+=(const Vector2D &rhs)
{
	x += rhs.x;
	y += rhs.y;

	return *this;
}

inline const Vector2D& Vector2D::operator-=(const Vector2D &rhs)
{
	x -= rhs.x;
	y -= rhs.y;

	return *this;
}

inline const Vector2D& Vector2D::operator*=(const double& rhs)
{
	x *= rhs;
	y *= rhs;

	return *this;
}

inline const Vector2D& Vector2D::operator/=(const double& rhs)
{
	x /= rhs;
	y /= rhs;

	return *this;
}
/// returns the length of a 2D vector
inline double Vector2D::length() const
{
	return sqrt(x * x + y * y);
}
/// returns the squared length of a 2D vector
inline double Vector2D::lengthSq() const
{
	return (x * x + y * y);
}
/// set length of vector to 1, with some zero checking
inline double Vector2D::normalize()
{ 
	double vector_length = this->length();

	if (vector_length > std::numeric_limits<double>::epsilon())
	{
		this->x /= vector_length;
		this->y /= vector_length;
	}
	return vector_length;
}
/// adjust x and y so that length of vector is newLength
inline void Vector2D::scale(double newLength)
{
	this->normalize();
	*this *= newLength;
}
/// calculate dot product
inline double Vector2D::dot(const Vector2D& v2) const
{
	return x*v2.x + y*v2.y;
}
/// calculate Euclidean distance between two vectors
inline double Vector2D::distance(const Vector2D &v2) const
{
	double ySeparation = v2.y - y;
	double xSeparation = v2.x - x;

	return sqrt(ySeparation*ySeparation + xSeparation*xSeparation);
}
/// calculates the euclidean distance squared between two vectors 
inline double Vector2D::distanceSq(const Vector2D &v2) const
{
	double ySeparation = v2.y - y;
	double xSeparation = v2.x - x;

	return ySeparation*ySeparation + xSeparation*xSeparation;
}
/// Returns a vector getPerpendicular to this vector
inline Vector2D Vector2D::getPerpendicular() const
{
	return Vector2D(-y, x);
}
///  given a normalized vector this method reflects the vector it
///  is operating upon. (like the path of a ball bouncing off a wall)
inline void Vector2D::reflect(const Vector2D& norm)
{
	*this += 2.0 * this->dot(norm) * norm.getReverse();
}
/// return a copy of this vector with signs flipped
inline Vector2D Vector2D::getReverse() const
{
	return Vector2D(-this->x, -this->y);
}

// implementation of operator overloads
inline Vector2D operator*(const Vector2D &lhs, double rhs)
{
	Vector2D result(lhs);
	result *= rhs;
	return result;
}

inline Vector2D operator*(double lhs, const Vector2D &rhs)
{
	Vector2D result(rhs);
	result *= lhs;
	return result;
}

//overload the - operator
inline Vector2D operator-(const Vector2D &lhs, const Vector2D &rhs)
{
	Vector2D result(lhs);
	result.x -= rhs.x;
	result.y -= rhs.y;

	return result;
}

//overload the + operator
inline Vector2D operator+(const Vector2D &lhs, const Vector2D &rhs)
{
	Vector2D result(lhs);
	result.x += rhs.x;
	result.y += rhs.y;

	return result;
}

//overload the / operator
inline Vector2D operator/(const Vector2D &lhs, double val)
{
	Vector2D result(lhs);
	result.x /= val;
	result.y /= val;

	return result;
}

#endif