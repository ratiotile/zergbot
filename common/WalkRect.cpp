#include "WalkRect.h"
#include "cpp_includes.h"

WalkRect::WalkRect( const Position &rPos, const UnitType &rType ):
	uType_(rType), 
		tilePos_(WalkTile(rPos)),
	pxPos_(Position(rPos))
{
	//tilePos_ = new WalkTile(rPos);
	update();
}

WalkRect::~WalkRect()
{
	//delete tilePos_;
}

void WalkRect::changePosPx( int newX, int newY )
{
	pxPos_ = Position(newX, newY);
	update();
}

void WalkRect::changePosPx( const Position newPos )
{
	pxPos_ = newPos;
	update();
}
void WalkRect::update()
{
	pxLeft = pxPos_.x() - uType_.dimensionLeft();
	pxTop =	pxPos_.y() - uType_.dimensionUp();
	pxRight= pxPos_.x() + uType_.dimensionRight();
	pxBot =	pxPos_.y() + uType_.dimensionDown();
	pxHeight = uType_.dimensionUp() + uType_.dimensionDown();
	pxWidth = uType_.dimensionRight() + uType_.dimensionLeft();
	isWalkable_ = checkWalkable();
}

bool WalkRect::checkWalkable()
{
	if (pxLeft < 0 || pxTop < 0 || pxRight >= Broodwar->mapWidth()*32 || 
		pxBot >= Broodwar->mapHeight()*32) 
		return false;
	for (int i = left(); i < right() + 1; i++)
	{
		for (int j = top(); j < bot() + 1; j++)
		{
			if (!Broodwar->isWalkable(i,j))
			{
				return false;
			}
		}
	}
	return true;
}