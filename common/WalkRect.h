#ifndef INCLUDED_WALKRECT_H
#define INCLUDED_WALKRECT_H
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include "BWAPI.h"
#endif
#include "BWAPIext.h"
#include <cmath>
using BWAPI::Position;
class WalkRect
{
public:
	WalkRect(const Position &rPos, const UnitType &rType);
	~WalkRect();

	int x() const {return tilePos_.x();}	// return walktile x
	int y() const {return tilePos_.y();}	// return walktile y
	int xPx() const {return pxPos_.x();}
	int yPx() const {return pxPos_.y();}
	int top() const {return pxTop/8;}
	int left() const {return pxLeft/8;}
	int bot() const	{return pxBot/8;}
	int right() const {return pxRight/8;}
	int topPx() const {return pxTop;}
	int leftPx() const {return pxLeft;}
	int botPx() const	{return pxBot;}
	int rightPx() const {return pxRight;}
	int heightPx() const {return pxHeight;}
	int widthPx() const {return pxWidth;}
	bool isWalkable() const {return isWalkable_;}
	/// center
	const Position& getPos() const {return pxPos_;}

	void changePosPx( int newX, int newY );
	void changePosPx( const Position newPos );
	void setWalkable(bool walkable) {isWalkable_ = walkable;}
	/// updates coordinates of box from position, usually automatically called.
	void update();
protected:
	BWAPIext::WalkTile tilePos_;
	Position pxPos_;
	int pxLeft, pxTop, pxRight, pxBot, pxHeight, pxWidth;
	UnitType uType_;
	bool isWalkable_;
	bool checkWalkable(); // set isWalkable_ based on terrain info
};

#endif