#include "WalkTile.h"

BWAPIext::WalkTile::WalkTile( const BWAPI::Position &rPos )
{
	xWT = rPos.x() / walk_pixels;
	yWT = rPos.y() / walk_pixels;
}
