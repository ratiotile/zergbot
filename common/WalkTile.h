// modified to have same intarface as Skynet's WalkTile
#pragma once
#ifndef INCLUDED_WALKTILE_H
#define INCLUDED_WALKTILE_H
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include "BWAPI.h"
#endif

namespace BWAPIext
{
	class WalkTile
	{
	public:
		WalkTile()
			: xWT(0), yWT(0)
		{}
		/// coordinate constructor, does not error-check
		WalkTile(int x, int y)
			: xWT(x), yWT(y)
		{}
		WalkTile(const BWAPI::Position &rPos);
		WalkTile(const BWAPI::TilePosition &tp)
			: xWT(tp.x() * 4), yWT(tp.y() * 4)
		{ }

		int& x() {return xWT;}
		int& y() {return yWT;}
		int x() const {return xWT;}
		int y() const {return yWT;}

		const static int walk_pixels = 8;

		inline operator BWAPI::Position()
		{
			return BWAPI::Position(xWT * 8, yWT * 8);
		}

		inline operator BWAPI::TilePosition()
		{
			return BWAPI::TilePosition(xWT / 4, yWT / 4);
		}

		inline bool operator==(const WalkTile &v) const
		{
			return (xWT == v.x() && yWT == v.y());
		}

		inline bool operator!=(const WalkTile &v) const
		{
			return !(xWT == v.x() && yWT == v.y());
		}

		inline bool operator<(const WalkTile& v) const
		{
			if(xWT == v.x())
				return yWT < v.y();

			return xWT < v.x();
		}

		inline bool operator>(const WalkTile& v) const
		{
			if(xWT == v.x())
				return yWT > v.y();

			return xWT > v.x();
		}

		inline BWAPI::Position getCenterPosition() const
		{
			return BWAPI::Position(xWT * 8 + 4, yWT * 8 + 4);
		}
	protected:
		int xWT, yWT;
	};

}

#endif