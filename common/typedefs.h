#pragma once

#include <boost/shared_ptr.hpp>
#include <set>
#include <list>
#include <boost/signals2.hpp>
#include <BWAPI.h>

class UnitInfo;
class TargetGroup;

typedef boost::shared_ptr<UnitInfo> UIptr;
typedef boost::shared_ptr<const UnitInfo> constUIptr;
typedef std::set<UIptr> SetUI;
typedef std::set<const UIptr> SetConstUI;

typedef boost::shared_ptr<TargetGroup> TGptr;
typedef boost::shared_ptr<const TargetGroup> constTGptr;
typedef std::list<TGptr> TGlist;

/// signal for unit group changed. The unit, newCID, oldCID.
typedef boost::signals2::signal<void (UIptr, int, int)> uccsig_t;

typedef std::map<BWAPI::UnitType,int> unitQuantity;
typedef std::pair<BWAPI::UnitType,int> unitCount; // for BOOST_FOREACH

