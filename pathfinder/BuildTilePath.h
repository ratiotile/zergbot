#pragma once
#include <BWAPI.h>

class BuildTilePath
{
public:
	BuildTilePath();

	void addNode(BWAPI::TilePosition pos);

	bool isComplete;

	void drawPath();

	std::list<BWAPI::TilePosition> path;

	bool isStillValid();
};