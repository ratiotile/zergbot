#pragma once
#include <list>
class regionInfo;

class RegionPath
{
public:
	RegionPath();
	void addNode(const regionInfo* region) {path.push_front(region);}
	bool isComplete;
	int pathLength;
	std::list<const regionInfo*> path;
};