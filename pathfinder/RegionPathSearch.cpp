#include "RegionPathSearch.h"
#include "../common/cpp_includes.h"
#include "../MapInfo.h"

RegionPathSearch::RegionPathSearch( const std::set<regionInfo*>& regions, 
								   const regionInfo* start, const regionInfo* goal,
								   int maxGValue)
{
	Heap<const regionInfo*, int> open(true);
	std::set<const regionInfo*> closed;
	std::map<cri, cri> parent;
	std::map<cri, int> gmap;

	open.push(make_pair(start, 0));
	gmap[start] = 0;
	parent[start] = start;

	while(!open.empty())
	{
		//examine top of empty set and add more potential nodes
		cri  p = open.top().first;
		int gvalue = gmap[p];

		if ( p == goal || (maxGValue != 0 && gvalue >= maxGValue  ))
		{
			break;
		}

		int fvalue = open.top().second;
		open.pop();
		closed.insert(p);
		// look at neighbors to add to open set
		//set<regionInfo*> potentials = MapInfo::get().getAdjacentRegions(p);
// 		BOOST_FOREACH(cri region, potentials)
// 		{
// 			if (closed.find(region) != closed.end())
// 				continue;
// 		}
	}
}

int RegionPathSearch::g( cri region, cri prev, int gtotal )
{
	return 0;
}