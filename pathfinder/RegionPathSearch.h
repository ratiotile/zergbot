#pragma once
#include "Heap.h"
#include <set>
class regionInfo;

struct RegionNode
{
	const regionInfo* region;
	const regionInfo* previous;
};
class RegionPathSearch
{
public:
	typedef const regionInfo* cri;
	RegionPathSearch(const std::set<regionInfo*>& regions, const regionInfo* start, 
		const regionInfo* goal, int maxGValue = 0);
private:
	
	int f();
	int g(cri region, cri prev, int gtotal);
};