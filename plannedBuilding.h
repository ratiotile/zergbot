#pragma once
#ifndef INCLUDED_PLANNEDBUILDING_H
#define INCLUDED_PLANNEDBUILDING_H

class WorkerAgent;
struct plannedBuilding
{
	enum status_t{
		UNPLANNED = 0, PLANNED = 1, CONSTRUCTION_ORDERED = 2, WORKER_SENT = 3, CONSTRUCTION_FINISHED = 4
	};
	BWAPI::UnitType building;
	status_t status; // 0 for unplanned, 1 planned, 2 construction ordered, 3 under construction, 4 construction finished
	WorkerAgent* worker; // unit responsible for building
	unsigned short planID; // identifier in build map
	unsigned short optimality; // how good the build spot is. cancel plans with lower optimality first.
	BWAPI::TilePosition planPos; // top left tile, for build map
	BWAPI::TilePosition gamePos; // position corresponding to game map
	tileBuildInfo::powered_t powered;	// does build site have power? is power planned?
	string toString()const
	{
		ostringstream oss;
		oss << building.getName() << ": " << (int)status;
		return oss.str();
	}
};

#endif