#pragma once
#ifndef TILEBUILDINFO_H
#define TILEBUILDINFO_H

#include <BWAPI.h>
using BWAPI::UnitType;


struct tileBuildInfo 
{
	

	unsigned short distance; // distance from choke
	unsigned short safety;
	unsigned short planned; // area, road or building id >= 10, planID >=100
	// 0 = no build, 1=unassigned, 2 = road, 3=choke, 4=front, 5=resource, 6=rear
	enum planned_t{ // built structures will have ID here, planned structures have ID as per PlannedBuilding
		NO_BUILD = 0, UNASSIGNED = 1, ROAD = 2, CHOKE = 3, FRONT = 4, RESOURCE = 5, REAR = 6,
		ARBITER_TRIBUNAL=7, ASSIMILATOR=8, CITADEL_OF_ADUN=9, CYBERNETICS_CORE=10, FLEET_BEACON=11,
		FORGE=12, GATEWAY=13, NEXUS=14, OBSERVATORY=15, PHOTON_CANNON=16, PYLON=17, ROBOTICS_FACILITY=18,
		ROBOTICS_SUPPORT_BAY=19, SHIELD_BATTERY=20, STARGATE=21, TEMPLAR_ARCHIVES=22,
		MINERAL_FIELD, VESPENE_GEYSER
	};
	char powered; // is under influence of pylon?
	enum powered_t{
		NO_POWER=0, POWERED=1, POWER_PLANNED=2,
	};
	static planned_t getPlanned_T( UnitType build_t ){
		using namespace BWAPI::UnitTypes;
		if (build_t == Protoss_Arbiter_Tribunal) return ARBITER_TRIBUNAL;			// 7
		if (build_t == Protoss_Assimilator) return ASSIMILATOR;						// 8
		if (build_t == Protoss_Citadel_of_Adun) return CITADEL_OF_ADUN;				// 9
		if (build_t == Protoss_Cybernetics_Core) return CYBERNETICS_CORE;			// 10
		if (build_t == Protoss_Fleet_Beacon) return FLEET_BEACON;					// 11
		if (build_t == Protoss_Forge) return FORGE;									// 12
		if (build_t == Protoss_Gateway) return GATEWAY;								// 13
		if (build_t == Protoss_Nexus) return NEXUS;									// 14
		if (build_t == Protoss_Observatory) return OBSERVATORY;						// 15
		if (build_t == Protoss_Photon_Cannon) return PHOTON_CANNON;					// 16
		if (build_t == Protoss_Pylon) return PYLON;									// 17
		if (build_t == Protoss_Robotics_Facility) return ROBOTICS_FACILITY;			// 18
		if (build_t == Protoss_Robotics_Support_Bay) return ROBOTICS_SUPPORT_BAY;	// 19
		if (build_t == Protoss_Shield_Battery) return SHIELD_BATTERY;				// 20
		if (build_t == Protoss_Stargate) return STARGATE;							// 21
		if (build_t == Protoss_Templar_Archives) return TEMPLAR_ARCHIVES;			// 22
		return NO_BUILD; // if unknown
	}
};

#endif