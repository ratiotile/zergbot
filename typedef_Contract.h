#pragma once
#include <boost/shared_ptr.hpp>

class Contract;
typedef boost::shared_ptr<Contract> pContract;