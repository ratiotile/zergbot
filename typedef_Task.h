#pragma once

#include <boost/shared_ptr.hpp>

class Task;
typedef boost::shared_ptr<Task> pTask;

class Contract;
typedef boost::shared_ptr<Contract> pContract;

class C_BuildUnit;
typedef boost::shared_ptr<C_BuildUnit> pCBU;