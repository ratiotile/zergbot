#pragma once
#ifndef INCLUDED_TYPEDEF_UNITAGENT_H
#define INCLUDED_TYPEDEF_UNITAGENT_H
#include <boost/shared_ptr.hpp>
#include <set>
#include <list>

class UnitAgent;
typedef boost::shared_ptr<UnitAgent> UAptr;
typedef std::set<UAptr> UAset;

class WorkerAgent;
typedef boost::shared_ptr<WorkerAgent> WAptr;
typedef std::set<WAptr> WAset;
typedef std::list<WAptr> WAlist;

#endif